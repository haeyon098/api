# Gradnet 2 API Project

## 주요 개선 사항

- 기존 Gradnet에서 화면 부분을 분리시키고 API 서버 로직만 포함
- 멀티 모듈 프로젝트로 구성
  - 사용자 API와 어드민 API를 별도의 서브 모듈로 구성하여 분리 빌드/배포 가능
  - entity를 별도의 모듈로 구성하고 사용자 API와 어드민 API에서 함께 사용 가능
- Java 13, Spring Boot 2.2, Gradle 6 기반
- entity에 QueryDSL 적용
  - admin-api 에서는 QueryDSL 사용 예정

## 기타 가이드라인

- 도메인 계층의 엔티티 객체가 그대로 클라이언트에 반환되지 않도록 서비스 계층에서 DTO로 변환하여 반환
- 도메인 계층에서 Jackson 라이브러리 사용 제거
- `@JsonIgnore`, `@JsonFormat` 등은 DTO에서만 사용하고 도메인 계층에서는 사용하지 않음
- 리포지터리에서 일부 컬럼을 추출해서 반환할 때는 projection 객체 사용
  - gradnet2-entity/src/main/java/.../adms/projection 폴더 참고
- 엔티티 객체, 밸류 객체, projection 객체는 엔티티 모듈에 위치
- DTO는 DTO가 사용되는 API 서버 모듈에 위치
- 패키지 이름에 대문자 사용 지양
  - adms.domain.Id -> adms.domain.id 로 변경 완료

## 주요 작업 내용

### Gradle 버전 설정

- 로컬 전역 Gradle이 아니라 프로젝트에 포함된 Gradle Wrapper 사용
  - gradnet2 에서 사용하는 Gradle Wrapper 버전은 6.2 
- HOWTO: IntelliJ 버전에 따라 아래 중 택일
  - ![Imgur](https://i.imgur.com/NyOCB6R.png)
  - ![Imgur](https://i.imgur.com/IpVTmj0.png)
  
#### 4.10.3 으로 다운그레이드

- IntelliJ 버전 2017.2 에서는 Gradle 5+ 를 지원하지 않으므로 Gradle 4.10.3 으로 다운그레이드
  - HOWTO: http://apexsoft.iptime.org:8090/hanmomhanda/gradnet2-api/commit/02ec028063fa8cdffa1e253a7692a15d6bcaf7aa 참고
  
  
### Java 버전

- Linux나 Mac이면 [jenv](https://www.jenv.be/)를 이용해서 프로젝트별 서로 다른 JDK 쉽게 사용 가능
  - https://blog.benelog.net/installing-jdk.html 참고
- Windows에서는 IntelliJ가 특정 프로젝트 별로 사용할 JDK를 IntelliJ에서 설정해주면 됨
- HOWTO:
  1. ![Imgur](https://i.imgur.com/aAYqFzA.png)
  1. ![Imgur](https://i.imgur.com/y2XBMW7.png)
  1. ![Imgur](https://i.imgur.com/6obyr75.png)
  
#### 11 로 설정

- Gradle 4 가 ~~Java 11 까지만 지원하므로 11로 설정~~ **-> Java 8까지만 지원하므로 아래 내용에서 11을 모두 8 로 설정**
- HOWTO:
  1. Java 11 설치
  1. ![Imgur](https://i.imgur.com/UrTAYeu.png)
  1. ![Imgur](https://i.imgur.com/XCWz6OH.png)
  1. ![Imgur](https://i.imgur.com/zASfOCf.png)
  1. ![Imgur](https://i.imgur.com/mYTMJj7.png)
    
### 멀티 모듈 프로젝트 구성

- 루트 프로젝트를 두고 하위에 entity, user-api, admin-api 세 개의 서브 모듈을 포함하는 멀티 모듈 프로젝트 구성
- HOWTO: https://gwonsungjun.github.io/articles/2019-04/gradle_multi_module 참고

### 엔티티 모듈 추출 및 공유

- user-api 모듈에 있던 domain 패키지, repository 패키지를 entity 서브 모듈로 이동
- admin-api -> entity, user-api -> entity 컴파일타임/런타임 의존관계 설정
- HOWTO: [IntelliJ-Gradle-Entity-모듈-분리](https://github.com/HomoEfficio/dev-tips/blob/master/IntelliJ-Gradle-Entity-모듈-분리.md) 참고
  
### 기타 코드 변경

- 도메인 계층에서 Jackson 제거
- 클라이언트에 엔티티 객체를 반환하는 API를 DTO를 반환하도록 수정
- 리포지터리에서 일부 컬럼 추출에 사용되는 클래스는 projection 폴더로 이동
