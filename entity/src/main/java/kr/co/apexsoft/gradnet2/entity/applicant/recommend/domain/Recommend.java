package kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-04
 */
@Entity
@Table(name="APPL_REC")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class Recommend extends AbstractBaseEntity {

    @Id
    @Column(name = "APPL_REC_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "PROF_NAME")
    private String name;


    @Column(name = "PROF_EMAIL")
    private String email;


    @Column(name = "PROF_INST")
    private String institute;


    @Column(name = "PROF_POSITION")
    private String position;


    @Column(name = "PROF_PHONE_NUM")
    private String phoneNum;


    @Column(name = "REC_TITLE")
    private String title;

    @Column(name = "REC_KEY")
    private String key;

    @Column(name = "REC_STS_CODE")
    private String stsCode;

    public void initStsCode(String stsCode) {
        this.stsCode = stsCode;
    }

    public void initAppl(Appl appl) {
        this.appl = appl;
    }

    public void initKey(String key) {
        this.key = key;
    }

    public void initTitle(String title) {
        this.title = title;
    }

    public enum status{
        SENT("SENT","00010") ,
        OPENED("OPENED","00020") ,
        COMPLETED("COMPLETED","00040") ,
        CREATING("CREATING","00030");

        private String name;
        private String code;

        status(String name, String code) {
            this.name = name;
            this.code = code;
        }

        public String getCode() {
            return code;
        }

        public String getName() {
            return name;
        }
    }


}
