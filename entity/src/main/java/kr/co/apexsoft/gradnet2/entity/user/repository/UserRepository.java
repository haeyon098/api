package kr.co.apexsoft.gradnet2.entity.user.repository;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * created by hanmomhanda@gmail.com
 */
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUserId(String userId);

    Optional<User> findByEmail(String email);

    Optional<User> findByEmailAndIdNot(String email, Long id);

    Page<User> findAllByUserIdContaining(String email, Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "update User u set u.failCnt = u.failCnt + 1 where u.userId =:userId")
    int updateFailCnt(@Param("userId") String userId);

    @Transactional
    @Modifying
    @Query(value = "update User u set u.locked = true " +
            "where u.userId =:userId and u.failCnt > :failMaxCnt")
    int updateLock(@Param("userId") String userId, @Param("failMaxCnt") Integer failMaxCnt);

    Optional<User> findByUserIdAndEmail(String userId, String email);
}
