package kr.co.apexsoft.gradnet2.entity.applicant.doc.projection;

/**
 *
 */
public interface DocumentProjection {

    Long getId();

    Long getApplNo();

    String getDocGrpCode();

    String getFilePath();

    String getPageCnt();

    String getDocItemCode();

    String getDocItemKorName();

    String getDocItemEngName();

    String getDocSeq();





}
