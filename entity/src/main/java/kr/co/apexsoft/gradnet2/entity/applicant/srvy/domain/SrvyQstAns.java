package kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 *
 *
 * @author aran
 * @since 2020-02-26
 */
@Entity
@Table(name="SRVY_QST_ANS")
@IdClass(SrvyQstAnsId.class)
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SrvyQstAns extends AbstractBaseEntity {

    @Id
    private Long srvyQstNo;

    @Id
    private String recruitSchoolCode;

    @Id
    private Long srvyNo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "MULTI_YN")
    private Boolean isMultiAnswer;

    @Column(name = "ANSWER")
    private String answer;
}
