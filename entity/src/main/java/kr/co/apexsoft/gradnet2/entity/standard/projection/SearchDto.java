package kr.co.apexsoft.gradnet2.entity.standard.projection;

import lombok.Getter;
import lombok.Setter;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-19
 */
@Getter
@Setter
public class SearchDto {

    private String id;

    private String name;

    public SearchDto(String code, String kor, String eng) {
        this.id = code;
        this.name = kor + " (" + eng+ ")";
    }

}
