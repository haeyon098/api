package kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.SrvyQstAns;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.projection.SrvyQstAnsProjection;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
*
*
* @author aran
* @since 2020-02-27
*/
public interface SrvyQstAnsRepository extends JpaRepository<SrvyQstAns, Long> {
    Optional<SrvyQstAns> findByAppl_IdAndSrvyQstNo(Long applNo, Long srvyQstNo);

    @Query(value = "SELECT SRVY_QST_NO, ANSWER "+
            "FROM SRVY_QST_ANS "+
            "WHERE appl_no=:applNo "+
            "AND SRVY_QST_NO IN ('900', '1000', '1100', '1400') "+
            "ORDER BY SRVY_QST_NO", nativeQuery = true)
    List<SrvyQstAnsProjection> findSrvyQstAnsForReport(@Param("applNo") Long applNo);
}
