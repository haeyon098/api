package kr.co.apexsoft.gradnet2.entity.applicant.doc.domain;

/**
 * 모든학교 공통으로 사용하는 DOC_ITEM 코드
 *
 * @since 2020-02-26
 */
public enum DocItem {
    PHOTO("00001"),     // 사진
    ESSAY("00002"),     // 자기소개서
    EMAIL_REC("00013"), // 이메일 추천서
    ETC("00099"), // 기타파일(사용자 직접입력 업로드)
    APPL_FORM("00100"), // 지원서
    SLIP("00200"), // 수험표
    ZIP("00300") // zip파일
    ;

    private String value;

    private DocItem(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static String[] NotUploadedDirectlyItems(){
        return new String[]{DocItem.EMAIL_REC.getValue(), DocItem.APPL_FORM.getValue(), DocItem.SLIP.getValue(),DocItem.ZIP.getValue()};
    }
}

