package kr.co.apexsoft.gradnet2.entity.recpart.repository;

import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartId;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.entity.recpart.projection.RecruitPartGuideResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface RecruitPartRepository extends JpaRepository<RecruitPart, RecruitPartId>, RecruitPartRepositoryCustom {
    List<RecruitPart> findByRecruitSchoolCodeAndStartDateBeforeAndEndDateAfter(@Param("schoolCode") String schoolCode,
                                                                               @Param("startDate") LocalDateTime startDate,
                                                                               @Param("endDate") LocalDateTime endDate);

    List<RecruitPart> findByStartDateBeforeAndEndDateAfter(@Param("startDate") LocalDateTime startDate,
                                                           @Param("endDate") LocalDateTime endDate);

    Optional<RecruitPart> findByRecruitSchoolCodeAndEnterYearAndSeq(String schoolCode,
                                                                    String enterYear,
                                                                    int seq);

    Optional<RecruitPart> findByRecruitSchoolCodeAndEnterYearAndSeqAndTime_TypeAndTime_StartDateBeforeAndTime_EndDateAfter(@Param("schoolCode") String schoolCode,
                                                                                                                           @Param("enterYear") String enterYear,
                                                                                                                           @Param("seq") Integer seq,
                                                                                                                           @Param("type") RecruitPartTime.TimeType type,
                                                                                                                           @Param("startDate") LocalDateTime startDate,
                                                                                                                           @Param("endDate") LocalDateTime endDate);

    List<RecruitPart> findByRecruitSchoolCodeInOrderByRecruitSchoolCodeAscEnterYearDescSeqDesc(@Param("SchoolCode") String[] schoolCode);

    @Query(value = "SELECT A.RECR_SCHL_CODE as recruitSchoolCode, A.ENTR_YEAR as enterYear, A.RECR_PART_SEQ as recruitPartSeq, A.ORDER_SEQ as orderSeq, A.SITE_URL as siteUrl, A.SITE_TITLE_KOR as siteTitleKor, A.SITE_TITLE_ENG as siteTitleEng " +
            "FROM RECR_PART_GUIDE A " +
            "WHERE A.RECR_SCHL_CODE = :schoolCode AND A.ENTR_YEAR = :enterYear AND A.RECR_PART_SEQ = :recruitPartSeq " +
            "AND   A.USE_YN='Y' " +
            "ORDER BY A.ORDER_SEQ" +
            "", nativeQuery = true)
    List<RecruitPartGuideResponse> findGuideByRecruitSchool(@Param("schoolCode") String schoolCode,
                                                    @Param("enterYear") String enterYear,
                                                    @Param("recruitPartSeq") Integer recruitPartSeq );

    Page<RecruitPart> findByRecruitSchoolCodeContaining(String schoolCode, Pageable pageable);
}
