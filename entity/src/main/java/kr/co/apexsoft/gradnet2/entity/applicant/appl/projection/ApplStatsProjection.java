package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-22
 */
@Getter
@AllArgsConstructor
public class ApplStatsProjection {

    private String recruitPartName;
    private String admissionName;
    private String courseName;
    private String majorName;
    private String categoryName;
//    private Long masterCount;
//    private Long doctorCount;
    private Long totalCount;
}
