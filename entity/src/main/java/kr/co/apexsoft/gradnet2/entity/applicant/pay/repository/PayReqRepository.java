package kr.co.apexsoft.gradnet2.entity.applicant.pay.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayReq;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-09-23
 */
public interface PayReqRepository extends JpaRepository<PayReq, String > {

    Optional<PayReq> findByMerchantUid(String merchantUid);
}
