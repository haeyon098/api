package kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-18
 */
@Getter
@AllArgsConstructor
public class RecommendSimpleForListProjection {

    private String applicantId;
    private String applId;
    private Integer recruitPartSeq;
    private String recruitPartName;
    private String recruitPartEngName;
    private String admissionCode;
    private String admissionName;
    private String admissionEngName;
    private String courseCode;
    private String courseName;
    private String courseEngName;
    private String categoryCode;
    private String categoryName;
    private String categoryEngName;
    private String majorCode;
    private String majorName;
    private String majorEngName;
    private String applicantKorName;
    private String applicantEngName;
    private String email;
    private String recStsCode;
    private String profName;
    private String profEmail;
    private String profPhoneNumber;
    private Appl.Status status;
}
