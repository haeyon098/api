package kr.co.apexsoft.gradnet2.entity.applicant.acad.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-03
 */
@Embeddable
@Getter
public class Grade {
    @Column(name = "GRAD_FORM_CODE", nullable = false)
    private String formCode;

    @Column(name = "GRAD_AVR", nullable = false)
    private String average;

    @Column(name = "GARD_FULL", nullable = false)
    private String full;
}
