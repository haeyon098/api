package kr.co.apexsoft.gradnet2.entity.applicant.doc.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.CompleteDocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.DocumentEtcNameProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.DocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.ZipInDocumentsProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-06
 */
public interface DocumentRepository extends JpaRepository<Document, Long> {
    Optional<Document> findByItemCodeAndAppl_Id(String itemCode, Long id);

    Optional<List<Document>> findByAppl_IdAndIsPdfTrueAndItemCodeNotInOrderByFilePath(Long id, String[] itemCode);

    void deleteByAppl_IdAndGroupCodeAndItemCode(Long id, String groupCode, String itemCode);

    @Query(value = "SELECT A.DOC_ITEM_CODE AS itemCode, A.ORG_FILE_NAME AS originFileName, A.FILE_PATH AS filePath, " +
            "SCHL_CODEVAL('KR', B.RECR_SCHL_CODE, B.ENTR_YEAR, B.RECR_PART_SEQ, 'DOC_ITEM', A.DOC_ITEM_CODE) AS korItemName, " +
            "SCHL_CODEVAL('EN', B.RECR_SCHL_CODE, B.ENTR_YEAR, B.RECR_PART_SEQ, 'DOC_ITEM', A.DOC_ITEM_CODE) AS engItemName " +
            "FROM APPL_DOC A, APPL B " +
            "WHERE A.APPL_NO = :id " +
            "AND A.APPL_NO = B.APPL_NO " +
            "AND A.DOC_ITEM_CODE IN (:itemCode) ORDER BY A.DOC_ITEM_CODE", nativeQuery = true)
    List<CompleteDocumentProjection> findCompleteDocuments(@Param("id")Long id, @Param("itemCode")String... itemCode);

    @Query(value = "SELECT A.DOC_ITEM_CODE AS itemCode, A.ORG_FILE_NAME AS originFileName, A.FILE_PATH AS filePath, " +
            "IF(A.DOC_ITEM_CODE=:etc, CONCAT(SCHL_CODEVAL('KR', B.RECR_SCHL_CODE, B.ENTR_YEAR, B.RECR_PART_SEQ, 'DOC_ITEM', A.DOC_ITEM_CODE), '(', A.DOC_ITEM_NAME ,')')," +
            "        SCHL_CODEVAL('KR', B.RECR_SCHL_CODE, B.ENTR_YEAR, B.RECR_PART_SEQ, 'DOC_ITEM', A.DOC_ITEM_CODE) )AS korItemName, " +
            "IF(A.DOC_ITEM_CODE=:etc, CONCAT(SCHL_CODEVAL('EN', B.RECR_SCHL_CODE, B.ENTR_YEAR, B.RECR_PART_SEQ, 'DOC_ITEM', A.DOC_ITEM_CODE), '(', A.DOC_ITEM_NAME ,')')," +
            "        SCHL_CODEVAL('EN', B.RECR_SCHL_CODE, B.ENTR_YEAR, B.RECR_PART_SEQ, 'DOC_ITEM', A.DOC_ITEM_CODE) ) AS engItemName " +
            "FROM APPL_DOC A, APPL B " +
            "WHERE A.APPL_NO = :id " +
            "AND A.APPL_NO = B.APPL_NO " +
            "AND A.DOC_ITEM_CODE NOT IN (:itemCode) ORDER BY A.DOC_ITEM_CODE", nativeQuery = true)
    List<DocumentEtcNameProjection> findUploadDocEtcName(@Param("id")Long id, @Param("etc") String etc, @Param("itemCode")String... itemCode);

    @Query(value = "SELECT (SELECT ORDER_SEQ FROM RECR_SCHL_COMM_CODE " +
            "WHERE RECR_SCHL_CODE=:schoolCode AND ENTR_YEAR=:enterYear AND RECR_PART_SEQ=:recruitPartSeq AND CODE_GRP='DOC_ITEM' AND CODE=D.DOC_ITEM_CODE) AS docSeq, " +
            "D.APPL_DOC_NO AS id,D.APPL_NO AS applNo, D.DOC_GRP_CODE AS docGrpCode, D.FILE_PATH AS filePath,D.PDF_PAGE_CNT AS pageCnt, " +
            "D.DOC_ITEM_CODE AS docItemCode, " +
            "CONCAT_WS(' '," +
            "CASE " +
            "WHEN D.DOC_GRP_CODE=:lang " +
            "THEN (SELECT SCHL_CODEVAL('KR',:schoolCode, :enterYear, :recruitPartSeq,'LANG_INFO',LANG_INFO_TYPE_CODE) FROM APPL_LANG WHERE APPL_LANG_NO=D.DOC_GRP_NO ) " +
            "WHEN D.DOC_GRP_CODE=:career " +
            "THEN (SELECT COMPANY_NAME FROM APPL_CAREER WHERE APPL_CAREER_NO=D.DOC_GRP_NO ) " +
            "WHEN D.DOC_GRP_CODE IN :academy  " +
            "THEN (SELECT IF(A.SCHL_CODE = '999', A.SCHL_NAME, (SELECT SCHL_KOR_NAME FROM SCHL WHERE SCHL_CODE = A.SCHL_CODE) ) FROM APPL_ACAD A WHERE A.APPL_ACAD_NO=D.DOC_GRP_NO) " +
            "ELSE null END, IF(D.DOC_ITEM_CODE=:etc,D.DOC_ITEM_NAME,SCHL_CODEVAL('KR',:schoolCode, :enterYear, :recruitPartSeq,'DOC_ITEM',D.DOC_ITEM_CODE)) " +
            ")AS docItemKorName, " +
            "CONCAT_WS('_'," +
            "CASE " +
            "WHEN D.DOC_GRP_CODE=:lang " +
            "THEN (SELECT SCHL_CODEVAL('EN',:schoolCode, :enterYear, :recruitPartSeq,'LANG_INFO',LANG_INFO_TYPE_CODE) FROM APPL_LANG WHERE APPL_LANG_NO=D.DOC_GRP_NO ) " +
            "WHEN D.DOC_GRP_CODE=:career " +
            "THEN (SELECT COMPANY_NAME FROM APPL_CAREER WHERE APPL_CAREER_NO=D.DOC_GRP_NO )" +
            "WHEN D.DOC_GRP_CODE IN :academy " +
            "THEN (SELECT IF(A.SCHL_CODE = '999', A.SCHL_NAME, (SELECT SCHL_ENG_NAME FROM SCHL WHERE SCHL_CODE = A.SCHL_CODE) ) FROM APPL_ACAD A WHERE A.APPL_ACAD_NO=D.DOC_GRP_NO) " +
            "ELSE null END, IF(D.DOC_ITEM_CODE=:etc,D.DOC_ITEM_NAME,SCHL_CODEVAL('EN',:schoolCode, :enterYear, :recruitPartSeq,'DOC_ITEM',D.DOC_ITEM_CODE))" +
            ")AS docItemEngName " +
            "FROM APPL_DOC D WHERE D.APPL_NO=:applNo AND D.DOC_ITEM_CODE NOT IN (:itemCode) " +
            "ORDER BY docSeq+0,docSeq,filePath", nativeQuery = true)
    List<DocumentProjection> findUploadDoc(@Param("applNo") Long applNo,
                                           @Param("etc") String etc,
                                           @Param("schoolCode") String schoolCode,
                                           @Param("enterYear") String enterYear,
                                           @Param("recruitPartSeq") Integer recruitPartSeq,
                                           @Param("lang") String lang,
                                           @Param("career") String career,
                                           @Param("academy") String[] academy,
                                           @Param("itemCode") String[] itemCode


    );

    void deleteByAppl_IdAndGroupNoAndGroupCodeAndItemCodeAndSeq(Long id, Long groupNo, String groupCode, String itemCode, Integer seq);

    void deleteByAppl_IdAndGroupNoAndGroupCodeAndItemCode(Long id, Long groupNo, String groupCode, String itemCode);

    List<Document> findByAppl_IdAndGroupCode(Long id, String groupCode);

//    Optional<List<Document>> findByAppl_IdAndItemCode(Long id, String itemCode);

    List<Document> findByAppl_IdAndItemCode(Long id, String itemCode);

    Optional<List<Document>> findByAppl_IdAndGroupCodeIn(Long id, String[] groupCode);

    Optional<List<Document>> findByAppl_IdAndGroupCodeInAndGroupNoIn(Long id, String[] groupCode, Long[] groupNo);

    int deleteByAppl_IdAndGroupCodeIn(Long id, String[] groupCode);

    int deleteByAppl_IdAndGroupCodeInAndGroupNoIn(Long id, String[] groupCode, Long[] groupNo);

    List<Document> findByAppl_IdAndItemCodeIn(Long id, String... itemCode);

    @Query(value = "SELECT A.APPL_NO AS applNo, A.APPL_DOC_NO AS applDocNo, A.DOC_GRP_CODE AS groupCode, " +
            "A.DOC_ITEM_CODE AS itemCode, A.SEQ AS seq, A.FILE_PATH AS filePath, " +
            "CONCAT(SUBSTRING_INDEX(A.FILE_PATH, '/', 4), '/', A.SEQ, '_', SUBSTRING_INDEX(A.FILE_PATH, '/', -1)) AS newFilePath " +
            "FROM (SELECT A.APPL_NO, A.APPL_DOC_NO, A.DOC_GRP_CODE, A.DOC_ITEM_CODE, A.FILE_PATH, " +
            "(SELECT ORDER_SEQ FROM RECR_SCHL_COMM_CODE " +
            "WHERE RECR_SCHL_CODE = :schoolCode AND ENTR_YEAR = :enterYear AND RECR_PART_SEQ = :recruitPartSeq " +
            "AND USE_YN = 'Y' AND CODE_GRP = 'DOC_ITEM' AND CODE = A.DOC_ITEM_CODE) SEQ " +
            "FROM APPL_DOC A) A " +
            "WHERE A.APPL_NO = :applNo AND A.DOC_ITEM_CODE NOT IN (:itemCode) " +
            "ORDER BY LENGTH(A.SEQ), A.SEQ", nativeQuery = true)
    List<ZipInDocumentsProjection> findZipInDocuments(@Param("applNo") Long applNo,
                                                      @Param("schoolCode") String schoolCode,
                                                      @Param("enterYear") String enterYear,
                                                      @Param("recruitPartSeq") Integer recruitPartSeq,
                                                      @Param("itemCode")String... itemCode);


    /*
     * 시스템관리자 - KDIS 지원완료후 전달(제출파일) for Excel
     * 제외파일 : 기타파일, 이메일추천서, 지원서, 수험표, zip 파일 제외
     * 2020-12-17 최은주
     */
    @Query(value =
            "SELECT appl.APPL_ID as APPLICANT_NO, " +
            "       (CASE doc.DOC_ITEM_CODE " +
            "        WHEN '00001' THEN '108' " +
            "        WHEN '00002' THEN '102' " +
            "        WHEN '00005' THEN '305' " +
            "        WHEN '00006' THEN '307' " +
            "        WHEN '00008' THEN '306' " +
            "        WHEN '00009' THEN '308' " +
            "        WHEN '00015' THEN '107' " +
            "        WHEN '00017' THEN '203' " +
            "        WHEN '00019' THEN '325' " +
            "        WHEN '00025' THEN '111' " +
            "        WHEN '00027' THEN '319' " +
            "        WHEN '00021' THEN '204' " +
            "        WHEN '00023' THEN '310' " +
            "        WHEN '00024' THEN '311' " +
            "        ELSE 'ERROR_코드확인필요' " +
            "        END ) as SUBMIT_DOC, " +
            "       SCHL_CODEVAL('KR', :schoolCode, :enterYear, :recruitPartSeq, 'DOC_ITEM', doc.DOC_ITEM_CODE)  as 'DOC_TYPE_NAME', " +
            "       DATE_FORMAT(doc.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as SUBMIT_DATE, " +
            "       doc.CREATED_BY as INPUT_ID, DATE_FORMAT(doc.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as INPUT_DTIME,  " +
            "       '' as INPUT_IP, " +
            "       doc.LAST_MODIFIED_BY as UPDATE_ID, DATE_FORMAT(doc.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s') as UPDATE_DTIME, " +
            "       '' as UPDATE_IP, " +
            "       (SELECT C.ORDER_SEQ FROM RECR_SCHL_COMM_CODE as C WHERE C.RECR_SCHL_CODE = :schoolCode and C.ENTR_YEAR = :enterYear and C.RECR_PART_SEQ = :recruitPartSeq and C.CODE_GRP= 'DOC_ITEM' and  C.CODE = doc.DOC_ITEM_CODE ) as SORT " +
            "FROM   APPL appl, APPL_DOC doc, APPL_PART_DOC partDoc " +
            "WHERE  appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
            "AND    appl.APPL_NO =  doc.APPL_NO " +
            "AND    appl.RECR_SCHL_CODE = partDoc.RECR_SCHL_CODE " +
            "AND    appl.ENTR_YEAR = partDoc.ENTR_YEAR " +
            "AND    appl.RECR_PART_SEQ = partDoc.RECR_PART_SEQ " +
            "AND    appl.APPL_PART_NO = partDoc.APPL_PART_NO " +
            "AND    doc.DOC_GRP_CODE = partDoc.DOC_GRP_CODE  " +
            "AND    doc.DOC_ITEM_CODE = partDoc.DOC_ITEM_CODE " +
            "AND    doc.DOC_ITEM_CODE not in ('00099', '00013', '00100', '00200', '00300')" +
            "ORDER BY appl.APPL_ID, SORT+0", nativeQuery = true)
    List<Object[]> findKidsSubmitFileList(@Param("schoolCode") String schoolCode,
                                      @Param("enterYear") String enterYear,
                                      @Param("recruitPartSeq") Integer recruitPartSeq);
}