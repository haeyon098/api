package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Address;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
@Getter
@AllArgsConstructor
public class ApplDetailProjection {

    private Long applNo;
    private String applicantId;
    private Appl.Status status;

    private String applId;
    private LocalDateTime applDate;
    private Integer recruitPartSeq;
    private String recruitPartName;
    private String recruitPartEngName;
    private String admissionCode;
    private String admissionName;
    private String admissionEngName;
    private String courseCode;
    private String courseName;
    private String courseEngName;
    private String categoryCode;
    private String categoryName;
    private String categoryEngName;
    private String majorCode;
    private String majorName;
    private String majorEngName;

    private String applicantName;
    private String applicantEngName;
    private LocalDate dateOfBirth;
    private String gender;
    private String telNumber;
    private String phoneNumber;
    private Address address;
    private String email;
    private String emergencyName;
    private String emergencyTel;

}
