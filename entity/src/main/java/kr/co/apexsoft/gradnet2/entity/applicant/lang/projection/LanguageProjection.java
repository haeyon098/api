package kr.co.apexsoft.gradnet2.entity.applicant.lang.projection;


public interface LanguageProjection {

     Long getId();


    String getLangTypeCode();

    String getLangInfoTypeCode();

    String getExamName();

    String getExamKorName();

    String getSubName();

    String getExamDay();

    String getExpirationDay();

    String getScore();

    String getUploadYn();

    String getMdtYn();



}
