package kr.co.apexsoft.gradnet2.entity.applpart.repository;

import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartLanguage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ApplPartLanguageRepository extends JpaRepository<ApplPartLanguage, Long> {


    List<ApplPartLanguage> findByPartAndParentNode_IdIsNull(@Param("part") ApplPart part);

}