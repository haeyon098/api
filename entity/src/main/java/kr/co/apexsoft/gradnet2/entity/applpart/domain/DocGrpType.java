package kr.co.apexsoft.gradnet2.entity.applpart.domain;


/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-21
 */
public enum DocGrpType {
    BASIC("00010"),
    CAREER("00020"),
    ADMISSION("00030"),
    UNDERGRADUATE("00040"),
    GRADUATE("00050"),
    LANG("00060"),
    ETC("00099");

    private String value;

    private DocGrpType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static String[] Academy(){
        return new String[]{DocGrpType.UNDERGRADUATE.getValue(), DocGrpType.GRADUATE.getValue()};
    }

}
