package kr.co.apexsoft.gradnet2.entity.applicant.pay.domain;

import kr.co.apexsoft.gradnet2.entity._common.EnumModel;
import lombok.Getter;

import java.util.Arrays;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-11
 */
public class PayEnums {
    // 결제 종류
    @Getter
    public enum PayType implements EnumModel {
        CARD("신용카드", "Credit Card", "card"),
        VBANK("가상계좌", "Virtual Bank", "vbank"),
        REALTIME_TRANSFER("실시간계좌이체", "Real-time Transfer", "trans"),
        WIRE_TRANSFER("무통장입금", "Wire Transfer", "wire"),
        PAYPAL("페이팔", "Paypal", "paypal")
        ;

        String korName;
        String engName;
        String input;

        PayType(String korName, String engName, String input) {
            this.korName = korName;
            this.engName = engName;
            this.input = input;
        }

        @Override
        public String getValue() {
            return name();
        }

        public static PayType setValue(String pgInput, String input) {
            if(pgInput.equals(PG.PAYPAL.input)) return PayType.PAYPAL;
            return Arrays.stream(PayType.values())
                    .filter(payType -> payType.hasPayTypeCode(input))
                    .findAny()
                    .orElse(null);
        }

        public boolean hasPayTypeCode(String input) {
            return this.input.equals(input);
        }
    }

    // PG 종류 - PG사별 결제 종류
    @Getter
    public enum PG implements EnumModel {
        // 아임포트에 넣는 파라미터명 변경되면 input 부분 수정
        TOSS_PAYMENTS("토스페이먼츠", "Toss Payments", new PayType[] {
                PayType.CARD, PayType.VBANK, PayType.REALTIME_TRANSFER
        }, "uplus"),
        PAYPAL("페이팔", "Paypal", new PayType[] { PayType.PAYPAL }, "paypal"),
        NONE("없음", "None", new PayType[] { PayType.WIRE_TRANSFER }, "none")
        ;

        String korName;
        String engName;
        PayType[] payTypes;
        String input;

        PG(String korName, String engName, PayType[] payTypes, String input) {
            this.korName = korName;
            this.engName = engName;
            this.payTypes = payTypes;
            this.input = input;
        }

        PG(String korName, String engName, PayType[] payTypes) {
            this.korName = korName;
            this.engName = engName;
            this.payTypes = payTypes;
        }

        @Override
        public String getValue() {
            return name();
        }

        public static PG setValue(String input) {
            return Arrays.stream(PG.values())
                    .filter(pg -> pg.hasPGCode(input))
                    .findAny()
                    .orElse(NONE);
        }

        public boolean hasPGCode(String input) {
            return this.input.equals(input);
        }
    }

    // 화폐단위
    public enum Currency implements EnumModel {
        KRW, USD;

        @Override
        public String getValue() {
            return name();
        }

        @Override
        public String getKorName() {
            return "";
        }

        @Override
        public String getEngName() {
            return "";
        }
    }
}
