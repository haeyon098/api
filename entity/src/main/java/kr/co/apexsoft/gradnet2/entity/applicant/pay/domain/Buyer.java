package kr.co.apexsoft.gradnet2.entity.applicant.pay.domain;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-10
 */
@Embeddable
public class Buyer {
    @Column(name = "BUYER_NAME")
    private String name;

    @Column(name = "BUYER_EMAIL")
    private String email;

    @Column(name = "BUYER_TEL")
    private String telNum;
}
