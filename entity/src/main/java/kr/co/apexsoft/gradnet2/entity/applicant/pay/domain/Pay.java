package kr.co.apexsoft.gradnet2.entity.applicant.pay.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity._common.EnumModel;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Class Description
 * 결제정보
 *
 * @author 김혜연
 * @since 2020-09-10
 */
@Entity
@Table(name="PAY_INFO")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Pay extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PAY_INFO_NO")
    private Long id;

    @Getter
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "RECR_SCHL_CODE")
    private String schoolCode;

    @Column(name = "ENTR_YEAR")
    private String enterYear;

    @Column(name = "RECR_PART_SEQ")
    private Integer partSeq;

    @Getter
    @Column(name = "IMP_UID")
    private String impUid;

    @Getter
    /* 가맹점고유번호 */
    @Column(name = "MERCHANT_UID")
    private String merchantUid;

    /* 수동처리 여부 */
    @Getter
    @ColumnDefault(value = "false")
    @Column(name = "MANUAL_YN")
    private Boolean isManual;
    
    /* 수동처리 처리내용 */
    @Column(name = "MANUAL_DESC")
    private String manualDescription;

    @Column(name = "PG_TYPE_CODE")
    @Enumerated(EnumType.STRING)
    private PayEnums.PG pgType;

    /* 결제종류 */
    @Column(name = "PAY_TYPE_CODE")
    @Enumerated(EnumType.STRING)
    private PayEnums.PayType payType;

    /* 화폐단위 */
    @Column(name = "CURRENCY_UNIT")
    @Enumerated(EnumType.STRING)
    private PayEnums.Currency currency;

    @Getter
    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "PAY_STS_CODE")
    @Enumerated(EnumType.STRING)
    private Status status;

    /* 카드승인번호 */
    @Column(name = "CARD_APPLY_NUM")
    private String cardApplyNum;

    @Getter
    @Column(name = "PAY_DATE")
    private LocalDateTime payDate;

    @Column(name = "REMK")
    private String remk;

    @Embedded
    private Buyer buyer;

    @Embedded
    private VBank vBank;

    @Getter
    public enum Status implements EnumModel {
        COMPLETE("완료", "Complete"),
        VBANK_ISSUE("가상계좌 발급", "Issue"),
        CANCEL("취소", "Cancel")
        ;

        String korName;
        String engName;

        Status(String korName, String engName) {
            this.korName = korName;
            this.engName = engName;
        }

        @Override
        public String getValue() {
            return name();
        }
    }

    public void initPGAndPayTypeValue(String pgInput, String payTypeInput) {
        pgType = PayEnums.PG.setValue(pgInput);
        payType = PayEnums.PayType.setValue(pgInput, payTypeInput);
    }

    public void changeStatusIssue() { this.status = Status.VBANK_ISSUE; }

    public void changeStatusComplete() {
        this.status = Status.COMPLETE;
    }

    public boolean isIssueVBank() {
        return this.status.equals(Status.VBANK_ISSUE);
    }

    public boolean isComplete() { return this.status.equals(Status.COMPLETE); }

    public void initSchoolInfo(String schoolCode, String enterYear, Integer partSeq, String remk) {
        this.schoolCode = schoolCode;
        this.enterYear = enterYear;
        this.partSeq = partSeq;
        this.remk = remk;
    }

    public void initDate(Long payDate, String vbankDueDate) {
        if(payDate != null) this.payDate = new Timestamp(payDate*1000L).toLocalDateTime();
        if(vbankDueDate != null) this.vBank.initDueDate(vbankDueDate);
    }

    public void initManual(String remk) {
        this.isManual = true;
        this.remk = remk;
    }

    public void cancelPay() {
        this.status = Status.CANCEL;
    }

}
