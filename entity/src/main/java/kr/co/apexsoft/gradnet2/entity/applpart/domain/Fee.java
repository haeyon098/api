package kr.co.apexsoft.gradnet2.entity.applpart.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-15
 */
@Getter
@Embeddable
public class Fee {
    @Column(name = "ADMS_FEE_KRW")
    private Integer krw;

    @Column(name = "ADMS_FEE_USD")
    private Double usd;
}
