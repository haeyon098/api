package kr.co.apexsoft.gradnet2.entity.applicant.essay.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-04
 */
@Entity
@Table(name="APPL_ESSAY")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Essay extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPL_ESSAY_NO")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "ESSAY_SEQ")
    private Integer seq;

    @Column(name = "DATA")
    private String data;
}
