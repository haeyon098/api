package kr.co.apexsoft.gradnet2.entity.recpart.repository;

import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.recpart.domain.QRecruitPart.recruitPart;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-19
 */
public class RecruitPartRepositoryImpl extends QuerydslRepositorySupport implements RecruitPartRepositoryCustom {

    public RecruitPartRepositoryImpl() {
        super(RecruitPart.class);
    }

    @Override
    public List<RecruitPart> findAllBySchoolCodeAndEnterYear(String schoolCode, String enterYear) {
        return from(recruitPart)
                .where(
                        recruitPart.recruitSchoolCode.eq(schoolCode),
                        recruitPart.enterYear.eq(enterYear)
                )
                .fetch();
    }
}
