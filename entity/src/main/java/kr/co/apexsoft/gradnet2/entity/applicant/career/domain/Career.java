package kr.co.apexsoft.gradnet2.entity.applicant.career.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="APPL_CAREER")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Career extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPL_CAREER_NO")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "CAREER_SEQ")
    private Integer seq;

    @Column(name = "JOIN_DAY")
    private LocalDate joinDay;

    @Column(name = "RETR_DAY")
    private LocalDate retireDay;

    @Column(name = "CURR_YN")
    @ColumnDefault(value = "true")
    private Boolean currently; //현재 재직여부

    @Column(name = "COMPANY_NAME")
    private String name;

    @Column(name = "COMPANY_TYPE_CODE")
    private String compTypeCode;

    @Column(name = "COMPANY_ADDRESS")
    private String address;

    @Column(name = "COMPANY_DEPARTMENT")
    private String department;

    @Column(name = "COMPANY_POSITION")
    private String position;

    @Column(name = "CAREER_DESC")
    private String description;

    public void initAppl(Appl appl) {
        this.appl = appl;
    }

    public boolean notChangeLangInfoType(Long id, String name) {
        return this.id.equals(id) && this.name.equals(name);
    }
}
