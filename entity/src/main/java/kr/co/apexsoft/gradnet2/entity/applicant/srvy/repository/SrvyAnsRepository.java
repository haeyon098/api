package kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.SrvyAns;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
*
*
* @author aran
* @since 2020-02-26
*/
public interface SrvyAnsRepository extends JpaRepository<SrvyAns, Long> {
    Optional<SrvyAns> findByAppl_Id(Long applNo);


    /*
     * 시스템관리자 - KDIS 지원완료후 전달(설문) for Excel
     *
     * 2020-12-17 최은주
     */
    @Query(value =
            "SELECT subA.APPl_ID as APPLICANT_NO, SUBSTRING_INDEX(subA.ANSWER_1,') ',1 ) as 'ANS_1_CD', SUBSTRING_INDEX(subA.ANSWER_1,') ',-1 ) as 'ANS_1_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_1_1,') ',1 ) as 'ANS_1_1_CD', SUBSTRING_INDEX(subA.ANSWER_1_1,') ',-1 ) as 'ANS_1_1_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_2,') ',1 ) as 'ANS_2_CD', SUBSTRING_INDEX(subA.ANSWER_2,') ',-1 ) as 'ANS_2_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_2_1,') ',1 ) as 'ANS_2_1_CD', SUBSTRING_INDEX(subA.ANSWER_2_1,') ',-1 ) as 'ANS_2_1_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_3,') ',1 ) as 'ANS_3_CD', SUBSTRING_INDEX(subA.ANSWER_3,') ',-1 ) as 'ANS_3_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_3_1,') ',1 ) as 'ANS_3_1_CD', SUBSTRING_INDEX(subA.ANSWER_3_1,') ',-1 ) as 'ANS_3_1_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_3,') ',1 ) as 'ANS_4_CD', SUBSTRING_INDEX(subA.ANSWER_3,') ',-1 ) as 'ANS_4_VAL', " +
            "       subA.ANSWER_5 as 'ANS_5_CD', (IF (subA.ANSWER_5='N', 'No', 'Yes') ) as 'ANS_5_VAL', " +
            "       subA.ANSWER_6 as 'ANS_6_VAL',  " +
            "       subA.ANSWER_7 as 'ANS_7_CD', (IF (subA.ANSWER_7='N', 'No', 'Yes') ) as 'ANS_7_VAL', " +
            "       subA.ANSWER_8 as 'ANS_8_CD', (IF (subA.ANSWER_8='N', 'No', 'Yes') ) as 'ANS_8_VAL', " +
            "       SUBSTRING_INDEX(subA.ANS_Interested, ')%', 1 ) as 'ANS_Interested_CD', SUBSTRING_INDEX(subA.ANS_Interested, ')%',-1 ) as 'ANS_Interested_VAL', " +
            "       subA.ANS_TopicStudy,  " +
            "       subA.ANS_EmpPubSec as ANS_EmpPubSec_CD, (CASE subA.ANS_EmpPubSec WHEN 'N' THEN 'No' WHEN 'Y' THEN 'YES' ELSE subA.ANS_EmpPubSec END) as 'ANS_EmpPubSec_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_9,') ',1 ) as 'ANS_9_CD', SUBSTRING_INDEX(subA.ANSWER_9,') ',-1 ) as 'ANS_9_VAL', " +
            "       SUBSTRING_INDEX(subA.ANSWER_10,') ',1 ) as 'ANS_10_CD', SUBSTRING_INDEX(subA.ANSWER_10,') ',-1 ) as 'ANS_10_VAL', " +
            "       subA.ANS_Supervisor, " +
            "       subA.ANSWER_11_Visted, " +
            "       subA.ANSWER_12_Subscribed, " +
            "       (SELECT srvyAns.CREATED_BY FROM SRVY_ANS srvyAns WHERE srvyAns.SRVY_NO = subA.SRVY_NO ) as INPUT_ID,  " +
            "       (SELECT srvyAns.LAST_MODIFIED_DATE FROM SRVY_ANS srvyAns WHERE srvyAns.SRVY_NO = subA.SRVY_NO ) as INPUT_DTIME " +
            "FROM ( " +
            "  SELECT appl.APPL_ID, srvy.SRVY_NO, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='100' THEN srvy.ANSWER END) AS ANSWER_1, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='101' THEN srvy.ANSWER END) AS ANSWER_1_1, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='200' THEN srvy.ANSWER END) AS ANSWER_2, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='201' THEN srvy.ANSWER END) AS ANSWER_2_1, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='300' THEN srvy.ANSWER END) AS ANSWER_3, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='301' THEN srvy.ANSWER END) AS ANSWER_3_1, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='400' THEN srvy.ANSWER END) AS ANSWER_4, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='500' THEN srvy.ANSWER END) AS ANSWER_5, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='600' THEN srvy.ANSWER END) AS ANSWER_6, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='700' THEN srvy.ANSWER END) AS ANSWER_7, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='800' THEN srvy.ANSWER END) AS ANSWER_8, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='900' THEN srvy.ANSWER END) AS 'ANS_Interested', " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1000' THEN srvy.ANSWER END) AS 'ANS_TopicStudy', " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1100' THEN srvy.ANSWER END) AS 'ANS_EmpPubSec', " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1200' THEN srvy.ANSWER END) AS ANSWER_9, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1300' THEN srvy.ANSWER END) AS ANSWER_10, " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1400' THEN srvy.ANSWER END) AS 'ANS_Supervisor', " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1500' THEN srvy.ANSWER END) AS 'ANSWER_11_Visted', " +
            "       MAX(CASE WHEN srvy.SRVY_QST_NO='1600' THEN srvy.ANSWER END) AS 'ANSWER_12_Subscribed' " +
            "  FROM  APPL appl " +
            "        INNER JOIN SRVY_QST_ANS srvy ON ( appl.APPL_NO = srvy.APPL_NO ) " +
            "  WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
            "  GROUP BY appl.APPL_ID " +
            ") subA " +
            "ORDER BY subA.APPl_ID", nativeQuery = true)
    List<Object[]> findKidsSurveyList(@Param("schoolCode") String schoolCode,
                                      @Param("enterYear") String enterYear,
                                      @Param("recruitPartSeq") Integer recruitPartSeq);
}
