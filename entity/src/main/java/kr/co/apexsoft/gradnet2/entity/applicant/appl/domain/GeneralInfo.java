package kr.co.apexsoft.gradnet2.entity.applicant.appl.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-02
 */
@Getter
@Embeddable
public class GeneralInfo {
    @Column(table = "APPL_GEN", name = "EMGR_NAME")
    private String emergencyName;

    @Column(table= "APPL_GEN", name = "ENG_FIRST_NAME")
    private String engFirstName;

    @Column(table = "APPL_GEN", name = "ENG_LAST_NAME")
    private String engLastName;

    @Column(table = "APPL_GEN", name = "EMGR_RELA")
    private String emergencyRelation;

    @Column(table = "APPL_GEN", name = "EMRG_TEL")
    private String emergencyTelNum;

    @Column(table = "APPL_GEN", name = "HNDC_GRAD")
    private String handicapGrade;

    @Column(table = "APPL_GEN", name = "HNDC_TYPE")
    private String handicapType;

    @Column(table = "APPL_GEN", name = "GMP_YN")
    private Boolean isGmp;
}
