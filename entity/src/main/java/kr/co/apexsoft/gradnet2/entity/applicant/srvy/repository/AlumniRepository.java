package kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.Alumni;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
*
*
* @author 박지환
* @since 2020-03-06
*/
public interface AlumniRepository extends JpaRepository<Alumni, Long> {

    List<Alumni> findByAppl_id(Long applNo);


    /*
     * 시스템관리자 - KDIS 지원완료후 전달(동문정보) for Excel
     * 2020-12-17 최은주
     */
    @Query(value =
            "SELECT appl.APPL_ID as APPLICANT_NO, " +
            "       alumni.ALUMNI_NAME as NAME, " +
            "       DATE_FORMAT(alumni.ALUMNI_ADM_YEAR, '%Y') as ADM_YEAR, " +
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'ALUMNI_MAJOR_TYPE', alumni.ALUMNI_MAJOR_TYPE_CODE)  as PROGRAM, " +
            "       alumni.ALUMNI_REMAKR as REMARK, " +
            "       srvyAns.CREATED_BY as INPUT_ID, " +
            "       DATE_FORMAT(srvyAns.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as INPUT_DTIME, " +
            "       '' as 'INPUT_IP', " +
            "       '' as 'UPDATE_ID', " +
            "       '' as 'UPDATE_DTIME', " +
            "       '' as 'UPDATE_IP', " +
            "       alumni.ALUMNI_TYPE  as RECOMMENDER " +
            "FROM APPL_ALUMNI alumni " +
            "     INNER JOIN APPL appl ON ( alumni.APPL_NO = appl.APPL_NO) " +
            "     INNER JOIN SRVY_ANS srvyAns ON srvyAns.APPL_NO = alumni.APPL_NO " +
            "     WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
            "     ORDER BY appl.APPL_ID, alumni.ALUMNI_NO", nativeQuery = true)
    List<Object[]> findKidsAlumniList(@Param("schoolCode") String schoolCode,
                                        @Param("enterYear") String enterYear,
                                        @Param("recruitPartSeq") Integer recruitPartSeq);


}
