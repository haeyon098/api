package kr.co.apexsoft.gradnet2.entity.applicant.career.projection;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-03-11
 */
@AllArgsConstructor
@Getter
@Setter
public class CareerProjection {

    private String compTypeName;

    private LocalDate joinDay;

    private LocalDate retireDay;

    private String name;


    private String address;

    private String department;

    private String position;

    private String description;

    private Boolean currently; //현재 재직여부


}
