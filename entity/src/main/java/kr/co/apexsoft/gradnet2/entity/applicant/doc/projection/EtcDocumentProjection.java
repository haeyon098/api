package kr.co.apexsoft.gradnet2.entity.applicant.doc.projection;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-02
 */
@Getter
@NoArgsConstructor
public class EtcDocumentProjection {
    private Long id;

    private String itemName;

    private String fileName;

    private String filePath;

    private Integer seq;
}
