package kr.co.apexsoft.gradnet2.entity._common;

import lombok.*;

import javax.persistence.*;

/**
 * Class Description
 * 밸류타입 PK
 *
 * @author 김혜연
 * @since 2019-07-10
 */
@MappedSuperclass
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public abstract class IdentifiedValueObject extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter(AccessLevel.PACKAGE)
    @Setter(AccessLevel.PACKAGE)
    @Column(name = "ID")
    private Long id;
}
