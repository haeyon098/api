package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface RecruitSchoolRepository extends JpaRepository<RecruitSchool, String> {
    List<RecruitSchool> findByUsed(Boolean used);
    Optional<RecruitSchool> findById(String id);

    List<RecruitSchool> findByIdIn(String... id);


    List<RecruitSchool> findAllBy();
}
