package kr.co.apexsoft.gradnet2.entity.adms.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-10
 */
public interface AdmsResponse {
    String getCode();

    String getCodeKrValue();

    String getCodeEnValue();
}
