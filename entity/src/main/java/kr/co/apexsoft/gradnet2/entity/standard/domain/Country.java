package kr.co.apexsoft.gradnet2.entity.standard.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 국가 기준정보
 *
 * @author 김효숙
 * @since 2020-01-02
 */
@Entity
@Table(name = "CNTR")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class Country extends AbstractBaseEntity {
    @Id
    @Column(name = "CNTR_CODE")
    private String id;

    @Column(name = "CNTR_KOR_NAME")
    @NonNull
    private String korName;

    @Column(name = "CNTR_ENG_NAME")
    @NonNull
    private String engName;

    @Column(name = "CNTR_GRP_CD")
    private String grpCode;

    public String getCntrName() {
        return this.korName + "(" + this.engName + ")";
    }
}

