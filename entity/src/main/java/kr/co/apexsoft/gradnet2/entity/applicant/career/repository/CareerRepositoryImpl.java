package kr.co.apexsoft.gradnet2.entity.applicant.career.repository;

import com.querydsl.core.types.Projections;
import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.entity.applicant.career.projection.CareerProjection;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.applicant.career.domain.QCareer.career;
import static kr.co.apexsoft.gradnet2.entity.comcode.domain.QSchoolCommCode.schoolCommCode;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-03-11
 */
public class CareerRepositoryImpl extends QuerydslRepositorySupport implements CareerRepositoryCustom {

    public CareerRepositoryImpl() {
        super(Career.class);
    }

    @Override
    public List<CareerProjection> findCareerInfo(long applNo){
        return
                from(career)
                        .innerJoin(schoolCommCode)
                        .on(schoolCommCode.enterYear.eq(career.appl.part.enterYear)
                                .and(schoolCommCode.recruitPartSeq.eq(career.appl.part.recruitPartSeq)
                                        .and(schoolCommCode.schoolCode.eq(career.appl.part.schoolCode))
                                        .and(schoolCommCode.codeGrp.eq("COMP_TYPE")).and(schoolCommCode.code.eq(career.compTypeCode))))
                        .where(career.appl.id.eq(applNo)).orderBy(career.retireDay.asc())
                        .select(Projections.constructor(CareerProjection.class, schoolCommCode.codeEnValue,
                                career.joinDay, career.retireDay,career.name,career.address,career.department, career.position, career.description,career.currently))
                        .fetch();


    }

}
