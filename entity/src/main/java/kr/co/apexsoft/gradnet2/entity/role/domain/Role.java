package kr.co.apexsoft.gradnet2.entity.role.domain;

import lombok.Getter;

import javax.persistence.*;

@Entity
@Table(name = "ROLE")
@Getter
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE_TYPE", length = 60)
    private RoleType type;

    protected Role() {
    }

    public Role(RoleType type) {
        this.type = type;
    }

    public Role(Long id, RoleType type) {
        this.id = id;
        this.type = type;
    }
}
