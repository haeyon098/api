package kr.co.apexsoft.gradnet2.entity.adms.repository;

import com.querydsl.core.types.Projections;
import kr.co.apexsoft.gradnet2.entity.adms.domain.Category;
import kr.co.apexsoft.gradnet2.entity.adms.domain.QCategory;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.adms.domain.QCategory.category;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QMajor.major;
import static kr.co.apexsoft.gradnet2.entity.applpart.domain.QApplPart.applPart;


/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
public class CategoryRepositoryImpl extends QuerydslRepositorySupport implements CategoryRepositoryCustom {

    public CategoryRepositoryImpl() {
        super(Category.class);
    }

    @Override
    public List<AdmsProjection> findBy(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode, String courseCode) {
        return from(category)
                .innerJoin(applPart)
                .on(
                        category.recruitSchoolCode.eq(applPart.schoolCode),
                        category.id.eq(applPart.categoryCode)
                )
                .where(
                        applPart.schoolCode.eq(schoolCode),
                        applPart.enterYear.eq(enterYear),
                        applPart.recruitPartSeq.eq(recruitPartSeq),
                        applPart.admissionCode.eq(admissionCode),
                        applPart.courseCode.eq(courseCode)
                )
                .distinct()
                .select(Projections.constructor(AdmsProjection.class,
                        category.id, category.korName, category.engName))
                .fetch();
    }
}
