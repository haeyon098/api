package kr.co.apexsoft.gradnet2.entity.comcode.repository;

import kr.co.apexsoft.gradnet2.entity.comcode.domain.SchoolCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SchoolCommCodeIds;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface SchoolCommCodeRepository extends JpaRepository<SchoolCommCode, SchoolCommCodeIds> {
  List<SchoolCommCode> findAllByCodeGrpAndSchoolCodeAndEnterYearAndRecruitPartSeqAndUsedTrueOrderByCodeAsc(String codeGrp, String schoolCode, String enterYear, int recruitPartSeq);

  SchoolCommCode findBySchoolCodeAndEnterYearAndRecruitPartSeqAndCodeGrpAndCode(String schoolCode, String enterYear,
                                                                                Integer recruitPartSeq,
                                                                                String codeGrp,
                                                                                String code);

  List<SchoolCommCode> findAllByCodeGrp(String codeGrp);
}
