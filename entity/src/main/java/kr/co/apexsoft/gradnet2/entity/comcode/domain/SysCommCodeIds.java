package kr.co.apexsoft.gradnet2.entity.comcode.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-01-03
 */
public class SysCommCodeIds implements Serializable {
    /* 공통코드 그룹 */
    @Id
    @Column(name = "CODE_GRP")
    private String codeGrp;

    @Id
    @Column(name = "CODE")
    private String code;
}
