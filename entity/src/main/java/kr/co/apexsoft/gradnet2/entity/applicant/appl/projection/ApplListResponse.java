package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-07
 */
public interface ApplListResponse {
   Long getId();

   String getStatus();

   String getSchoolCode();

   String getEnterYear();

   String getRecruitPartSeq();

   String getSemesterTypeCode();

   String getSemesterTypeKorName();

   String getSemesterTypeEngName();

   String getAdmissionCode();

   String getAdmissionKorName();

   String getAdmissionEngName();

   String getCourseCode();

   String getCourseKorName();

   String getCourseEngName();

   String getCategoryCode();

   String getCategoryKorName();

   String getCategoryEngName();

   String getMajorCode();

   String getMajorKorName();

   String getMajorEngName();

   String getRecrPartEndDate();

   String getRecrPartKorName();

   String getRecrPartEngName();
}
