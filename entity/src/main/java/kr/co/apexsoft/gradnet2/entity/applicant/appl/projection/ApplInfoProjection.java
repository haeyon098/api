package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;


public interface ApplInfoProjection {

    String getApplId();

    Long getApplNo();

    String getKorName();

    String getEmailAddr();

    String getUserId();

    String getEngName();


    String getEnterYear();

    String getAdmissionCode();

    String getAdmsKorName();

    String getAdmsEngName();

    String getCourseCode();

    String getSchoolCode();

    String getSchoolEngName();

    String getSchoolKorName();

    Integer getRecruitPartSeq();

    String getCorsKorName();

    String getCorsEngName();

    String getCategoryCodeCode();

    String getCategoryKorName();

    String getCategoryEngName();

    String getMajorCode();

    String getMajorKorName();

    String getMajorEngName();
    String getRecrPartKorName();

    String getRecrPartEngName();

}
