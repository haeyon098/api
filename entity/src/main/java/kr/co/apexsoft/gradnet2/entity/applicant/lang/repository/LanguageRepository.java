package kr.co.apexsoft.gradnet2.entity.applicant.lang.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.projection.LanguageProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-04
 */
public interface LanguageRepository extends JpaRepository<Language, Long> {
    List<Language> findByAppl_Id(@Param("id") Long id);

    @Transactional
    @Query(value = "SELECT  L.APPL_LANG_NO AS id ,L.LANG_TYPE_CODE AS langTypeCode , L.LANG_INFO_TYPE_CODE AS langInfoTypeCode , " +
            "SCHL_CODEVAL('EN', A.RECR_SCHL_CODE, A.ENTR_YEAR, A.RECR_PART_SEQ, 'LANG_INFO', L.LANG_INFO_TYPE_CODE) AS examName , " +
            "SCHL_CODEVAL('KR', A.RECR_SCHL_CODE, A.ENTR_YEAR, A.RECR_PART_SEQ, 'LANG_INFO', L.LANG_INFO_TYPE_CODE) AS examKorName , " +
            " SYS_CODEVAL('',L.DETL_GRP,L.DETL_CODE) AS subName,L.SCORE  AS score,  " +
            "DATE_FORMAT(L.EXAM_DAY, '%Y-%m-%d' )AS examDay ,DATE_FORMAT(L.EXPR_DAY, '%Y-%m-%d' )AS expirationDay, " +
            "L.UPLOAD_YN AS uploadYn,L.MDT_YN AS mdtYn  " +
            "FROM  APPL A JOIN APPL_LANG L ON A.APPL_NO=L.APPL_NO " +
            "WHERE A.APPL_NO=:applNo",nativeQuery = true)
    List<LanguageProjection> findLangInfoByApplNo(@Param("applNo") Long applNo);

    void deleteByAppl_IdAndIdNotIn(Long applNo, Long[] ids);

    void deleteByAppl_Id(Long applNo);

    /* 시스템관리자 - KDIS 지원완료후 전달( 어학)  for Excel
     * 2020-12-17 최은주
     */
    @Query(value =
            "SELECT appl.APPL_ID as APPLICANT_NO, " +
            "       ( CASE lang.LANG_INFO_TYPE_CODE " +
            "         WHEN '00001' THEN SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'LANG_INFO', lang.LANG_INFO_TYPE_CODE) " +
            "         WHEN '00002' THEN 'TOEIC' " +
            "         WHEN '00003' THEN 'TEPS' " +
            "         WHEN '00004' THEN 'NEW-TEPS' " +
            "         WHEN '00005' THEN 'IELTS' " +
            "         WHEN '00006' THEN 'TOES' " +
            "         WHEN '00007' THEN 'OPIC' " +
            "         WHEN '00020' THEN '면제' " +
            "         WHEN '00099' THEN 'AMOI' " +
            "         ELSE 'ERROR_어학종류확인필요' " +
            "         END " +
            "       ) as LANG_CODE, " +
            "       (IF (lang.LANG_INFO_TYPE_CODE = '00007', '', lang.SCORE) )as OBTAIN_SCORE, " +
            "       date_format(lang.EXAM_DAY, '%Y%m%d') as OBTAIN_DATE, " +
            "       '' as noTitle1, " +
            "       lang.CREATED_BY as INPUT_ID, DATE_FORMAT(lang.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as INPUT_DTIME,  " +
            "       '' as INPUT_IP, " +
            "       lang.LAST_MODIFIED_BY as UPDATE_ID, DATE_FORMAT(lang.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s') as UPDATE_DTIME, " +
            "       '' as UPDATE_IP, " +
            "       (IF (lang.LANG_INFO_TYPE_CODE = '00007', lang.SCORE, '' ) )as OPIC_SCORE " +
            " " +
            "FROM APPL_LANG lang " +
            "     INNER JOIN APPL appl ON ( lang.APPL_NO = appl.APPL_NO) " +
            "WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
            "ORDER BY appl.APPL_ID", nativeQuery = true)
    List<Object[]> findKidsLanguageList(@Param("schoolCode") String schoolCode,
                                      @Param("enterYear") String enterYear,
                                      @Param("recruitPartSeq") Integer recruitPartSeq);

}