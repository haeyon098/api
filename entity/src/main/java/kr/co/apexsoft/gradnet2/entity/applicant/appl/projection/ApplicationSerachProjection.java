package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.Getter;

/**
 * 시스템 관리자
 * 전체 지원자 목록
 */

@Getter
public class ApplicationSerachProjection {

    private Long applNo;
    private String schoolName;
    private String partName;

    private String courseName;
    private String majorName;

    private String korName;
    private String engName;
    private String userId;
    private String phoneNumber;
    private String email;
    private String applId;
    private String status;

    public ApplicationSerachProjection(Long applNo,String schoolName, String partName, String courseName, String majorName,
                                       String korName, String engName, String userId, String phoneNumber,
                                       String email, Appl.Status status, String applId) {
        this.applNo = applNo;
        this.schoolName= schoolName;
        this.partName = partName;
        this.courseName = courseName;
        this.majorName = majorName;
        this.korName = korName;
        this.engName = engName;
        this.userId = userId;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.status = status.getKorName();
        this.applId = applId;
    }
}
