package kr.co.apexsoft.gradnet2.entity.applicant.lang.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="APPL_LANG")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Language extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPL_LANG_NO")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;


    @Enumerated(EnumType.STRING)
    @Column(name = "LANG_TYPE_CODE")
    private LangType type;

    @Column(name = "LANG_EXAM_YN")
    private Boolean isExam;

    @Column(name = "LANG_INFO_TYPE_CODE")
    private String infoTypeCode;


    @Column(name = "DETL_GRP")
    private String detailGrp;


    @Column(name = "DETL_CODE")
    private String detailCode;


    @Column(name = "EXAM_DAY")
    private LocalDate examDay;

    @Column(name = "EXPR_DAY")
    private LocalDate expirationDay;

    @Column(name = "SCORE")
    private String score;

    @Column(name = "MDT_YN")
    private Boolean mandatory;

    @Column(name = "UPLOAD_YN")
    private Boolean uploaded;

    @Getter
    public enum LangType {
        KOREAN("한국어","KOREAN"),   // 한국어
        ENGLISH("영어","ENGLISH");   // 영어
        private String korName;
        private String engName;

        LangType(String korName, String engName) {
            this.korName = korName;
            this.engName = engName;
        }
    }

    public void initAppl(Appl appl) {
        this.appl = appl;
    }

    public boolean notChangeLangInfoType(Long id, String infoTypeCode) {
        return this.id.equals(id) && this.infoTypeCode.equals(infoTypeCode);
    }
}
