package kr.co.apexsoft.gradnet2.entity.applicant.acad.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.projection.AcademyProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.List;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-12
 */
public interface AcademyRepository extends JpaRepository<Academy, Long> {

   @Transactional
   @Query(value = "select new kr.co.apexsoft.gradnet2.entity.applicant.acad.projection.AcademyProjection (" +
           "c.appl.id,c.id,c.type,c.seq,c.lasted,c.statusCode,c.enterDay ,c.graduateDay ," +
           "c.grade,c.school.collegeName ,c.school.majorName,c.school.degreeNo,c.school.countryCode,c.school.code ," +
           "CASE WHEN c.school.code =:etcCode THEN c.school.name  ELSE (select concat(shl.korName,'(',shl.engName,')') from School shl where shl.id = c.school.code) END, " +
           "CASE WHEN c.school.countryCode=:etcCode THEN c.school.etcCountryName  ELSE  (select concat(cntr.korName,'(',cntr.engName,')') " +
           "from Country cntr where cntr.id = c.school.countryCode) END)" +
           "from Academy c  where c.appl.id =:applNo and c.type in(:type) " +
           " order by c.seq")
   List<AcademyProjection> findAcademyByApplNo(@Param("applNo") Long applNo, @Param("etcCode") String etcCode, @Param("type") Collection<Academy.AcademyType> type);

   List<Academy> findByAppl_IdAndTypeOrderBySeq(Long id, Academy.AcademyType type);

   List<Academy> findByAppl_Id(Long id);

   void deleteByAppl_Id(Long applNo);

   void deleteByAppl_IdAndIdNotIn(Long applNo, Long[] ids);


   /* 시스템관리자 - KDIS 지원완료후 전달(학력) for Excel
    * TO-DO 1. 지원자 학교의 소속국가 코드 정리 : DB 의 999 인것에 대하여 ETC_SCHL_CNTR_CODE_FIX 컬럼에 수정해야함 ( 국가코드의 경우 기타가 없어지면 변경필요 )
    * 2020-12-08 최은주
    */
   @Query(value = " SELECT appl.APPL_ID as APPLICANT_NO, "+
           "        (IF (acad.SCHL_CODE = '999', acad.SCHL_NAME, schl.SCHL_KOR_NAME  )) as SCHOOL_NAME, "+
           "        (IF (acad.SCHL_CODE = '999', acad.SCHL_NAME, schl.SCHL_ENG_NAME  )) as SCHOOL_ENAM, "+
           "        acad.MAJ_NAME as MAJOR_NAME, "+
           "        acad.MAJ_NAME as MAJOR_ENAME, "+
           "        (IF (acad.ACAD_TYPE_CODE = 'UNIV', '08', '06' )) as DEGREE, "+
           "        DATE_FORMAT(acad.ENTR_DAY, '%Y%m') as FROM_DATE, " +
           "        DATE_FORMAT(acad.GRDA_DAY, '%Y%m') as TO_DATE, "+
           "        ( CASE acad.ACAD_STS_CODE "+
           "          WHEN '00001' THEN '01' "+
           "          WHEN '00002' THEN '02' "+
           "          WHEN '00003' THEN '03' "+
           "          WHEN '00004' THEN '05' "+
           "          WHEN '00005' THEN '06' "+
           "          ELSE 'ERROR_코드확인필요' "+
           "          END ) as ACAD_TYPE, "+
           "        '' REMARK, "+
           "        acad.CREATED_BY as INPUT_ID, "+
           "        DATE_FORMAT(acad.CREATED_DATE, '%Y-%m-%d %H:%i:%s')as INPUT_DTIME, "+
           "        DATE_FORMAT(acad.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s')as UPDATE_DTIME, "+
           "        (IF ( ( acad.SCHL_CNTR_CODE='410' or (acad.SCHL_CNTR_CODE='999' and acad.ETC_SCHL_CNTR_CODE_FIX='410' ) ), 'K', 'N' )) as LOCATION, "+
           "        (IF ( acad.SCHL_CNTR_CODE='999' , acad.ETC_SCHL_CNTR_CODE_FIX, acad.SCHL_CNTR_CODE) ) as SCHOOL_NATION, "+
           "        SYS_CODEVAL('EN', 'GRAD_FORM', acad.GRAD_FORM_CODE) as GRAD_TYPE, "+
           "        acad.GRAD_AVR, "+
           "        acad.GARD_FULL "+
           " FROM APPL_ACAD acad "+
           "      INNER JOIN APPL appl ON ( acad.APPL_NO = appl.APPL_NO) " +
           "      LEFT JOIN CNTR cntr ON ( acad.SCHL_CNTR_CODE = cntr.CNTR_CODE ) "+
           "      LEFT JOIN SCHL schl ON ( acad.SCHL_CODE = schl.SCHL_CODE ) "+
           " WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq "+
           " ORDER BY appl.APPl_ID, acad.ACAD_TYPE_CODE desc , acad.ACAD_SEQ  ", nativeQuery = true)
   List<Object[]> findKidsAcademyList(@Param("schoolCode") String schoolCode,
                                      @Param("enterYear") String enterYear,
                                      @Param("recruitPartSeq") Integer recruitPartSeq);

}
