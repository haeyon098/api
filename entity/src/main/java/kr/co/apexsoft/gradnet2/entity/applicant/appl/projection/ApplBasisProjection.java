package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * 지원서 생성시 필요한 데이터
 */

public interface ApplBasisProjection {

    String getApplId();

    Long getApplNo();

    String getKorName();

    String getUserId();

    String getEngName();

    String getApplDate();

    String getCntrEngName();

    String getCntrKorName();

    String getEnterYear();

    String getAdmissionCode();

    String getAdmsKorName();

    String getAdmsEngName();

    String getCourseCode();

    String getSchoolCode();

    Integer getRecruitPartSeq();

    String getCorsKorName();

    String getCorsEngName();

    String getCategoryCodeCode();

    String getCategoryKorName();

    String getCategoryEngName();

    String getMajorCode();

    String getMajorKorName();

    String getMajorEngName();

    String getGend();

    String getGendKorName();

    String getDateOfBirth();

    String getRgstBronDate();

    String getRegistrationEncr();

    String getMailAddr();

    String getTelNum();

    String getPhoneNum();

    String getPostNo();

    String getAddr();

    String getDetailAddr();

    String getFornRgstEncr();

    String getPassportEncr();

    String getVisaEncr();

    String getVisaExprDay();

    String getForTypeKorName();

    String getSkypeId();

    String getHomeAddress();

    String getVideoEssay();

    String getResidencyType();

    String getOverseasKorean();

    String getEmpCat();

    String getGmpYn();

    String getFirstName();

    String getMiddleName();

    String getLastName();

    String getContactName();


}
