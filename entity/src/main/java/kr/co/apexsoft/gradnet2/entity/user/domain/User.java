package kr.co.apexsoft.gradnet2.entity.user.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USER")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
public class User extends AbstractBaseEntity {
    @Id
    @Column(name = "USER_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "USER_ID")
    private String userId;

    @NonNull
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "CONFIRM_YN")
    private Boolean confirmed;  // 가입인증여부

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private Status status = Status.ACTIVE;  // 사용자 상태 코드

    @Column(name = "FAIL_CNT")
    private int failCnt = 0;    // 로그인 실패횟수

    @Column(name = "LOCK_YN")
    private Boolean locked = false; // 계정 잠김

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "USER_ROLES",
                joinColumns = @JoinColumn(name = "USER_NO"),
                inverseJoinColumns = @JoinColumn(name = "ROLE_ID"))
    private Set<Role> roles = new HashSet<>();

    @Column(name = "SCHL_CODE") //학교 관리자용 학교코드
    private String schoolCode;

    public enum Status {
        ACTIVE, INACTIVE, WITHDRAW
    }

    public User(String userId, String password, Set<Role> roles) {
        this.userId = userId;
        this.password = password;
        this.roles = roles;
    }

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public void changePassword(@NonNull String password) {
        this.password = password;
    }

    public void changeEmail(@NonNull String email) {
        this.email = email;
    }

    public void changeStatus(@NonNull Status status) {
        this.status = status;
    }

    public void unlock() {
        this.failCnt = 0;
        this.locked = false;
    }

    public void initUser() {
        this.locked = false;
        this.status = Status.ACTIVE;
    }

    public void withdrawUser() {
        this.userId = "";
        this.email = "";
        this.password = "";
        this.status = Status.WITHDRAW;
    }

    public boolean checkActiveUser() {
        return this.getStatus().equals(Status.ACTIVE) ? true : false;
    }

    public boolean checkWithdrawUser() {
        return this.getStatus().equals(Status.WITHDRAW) ? true : false;
    }

    public boolean isSchoolAdmin() {
        for(Role role : getRoles()) {
            if(role.getType().equals(RoleType.ROLE_SCHL_ADMIN)) return true;
        }
        return false;
    }

    public boolean isSysAdmin(){
       return  getRoles().stream().anyMatch(role -> (role.getType().equals(RoleType.ROLE_SYS_ADMIN)));
    }
}
