package kr.co.apexsoft.gradnet2.entity.applicant.doc.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-08
 */
public interface CompleteDocumentProjection {
    String getKorItemName();

    String getEngItemName();

    String getItemCode();

    String getFilePath();

    String getOriginFileName();
}
