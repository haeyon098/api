package kr.co.apexsoft.gradnet2.entity.applicant.appl.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-04
 */
public interface ApplRepositoryCustom {

    long countBySchoolEnterYearRecruitPartSeqAndStatusIn(String schoolCode, String enterYear, Integer recruitPartSeq, Collection<Appl.Status> statuses);

    Map<LocalDate, Long> countByStatusAndDate(String schoolCode, String enterYear, Integer recruitPartSeq, Appl.Status status);

    Page<ApplicationSimpleForListProjection> findAllBySearchCondition(
            String schoolCode,
            String enterYear,
            Integer recruitPartSeq,
            String admissionCode,
            String courseCode,
            String categoryCode,
            String majorCode,
            String applId, Long applNo,
            String applicantName,
            String email,
            String phoneNumber,
            String applicantId,
            Appl.Status applStatus, Pageable pageable);

    Optional<Appl> findByApplNo(Long id);

    List<CorsCount> findCompletedApplCountsByCors(String schoolCode, String enterYear, Integer recruitPartSeq);

    Page<ApplicationSerachProjection> findApplAllListBySearchProjection(Long applNo,
                                                                        String schoolCode,
                                                                        String enterYear,
                                                                        Integer reqPartSeq,
                                                                        String email,
                                                                        String phoneNumber,
                                                                        String userId,
                                                                        String korName, String engName,
                                                                        Appl.Status status, Pageable pageable);

    List<ApplStatsProjection> findApplStatsByAdmission(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode);

    Optional<ApplDetailProjection> findApplDetailById(Long applNo);

    List<ApplDocProjection> findApplDocsById(Long applNo);

    Optional<ApplNoZipPrintedProjection> findApplNoZipPrintedById(Long applNo);
    void updateApplNoZipPrintedById(Long applNo, Boolean zipPrinted);



    List<Appl> findApplicantsDetail(  String schoolCode,
                                                   String enterYear,
                                                   Integer recruitPartSeq,
                                                   String admissionCode,
                                                   String courseCode,
                                                   String categoryCode,
                                                   String majorCode,
                                                   String applId);
}
