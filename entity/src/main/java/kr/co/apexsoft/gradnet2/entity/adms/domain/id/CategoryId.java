package kr.co.apexsoft.gradnet2.entity.adms.domain.id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Getter
public class CategoryId implements Serializable {
    @Id
    @Column(name = "RECR_SCHL_CODE")
    private String recruitSchoolCode;

    @Id
    @Column(name = "CATEGORY_CODE")
    private String id;
}
