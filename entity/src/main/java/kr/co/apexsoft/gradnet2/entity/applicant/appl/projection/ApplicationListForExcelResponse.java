package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * Class Description
 *
 * @author 최은주
 * @since 2020-06-18

 * 시스템 관리자
 * 전체 지원자 목록
 */
public interface ApplicationListForExcelResponse {

   String getUserId();

   Long getApplNo();

   String getApplId();

   String getSeqForYear();

   String getSmstTypeKorName();

   String getAdmsKorName();


   String getCorsKorName();

   String getCategoryEngName();

   String getMajKorName();

   String getKorName();

   String getEngName();

   String getApplStatusName();

   String getPhoneNumber();

   String getEmail();

   String getRecRequestCnt();
   String getRecSubmitCnt();

   String getCountry();
   String getCountryGroupCode();
   String getCountryGroupName();

   String getEmpCat();
   String getRgstBornDate();
   String getGend();
   String getVideoEssay();
}
