package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.domain.id.SemesterTypeId;
import kr.co.apexsoft.gradnet2.entity.adms.domain.SemesterType;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface SemesterTypeRepository extends JpaRepository<SemesterType, SemesterTypeId> {

    @Query(value = "SELECT A.SMST_TYPE_CODE AS code, A.SMST_TYPE_KOR_NAME AS codeKrValue, " +
            "A.SMST_TYPE_ENG_NAME AS codeEnValue " +
            "FROM SMST_TYPE A, " +
            "(SELECT DISTINCT SMST_TYPE_CODE " +
            "FROM APPL_PART " +
            "WHERE RECR_SCHL_CODE = :schoolCode AND ENTR_YEAR = :enterYear " +
            "AND RECR_PART_SEQ = :recruitPartSeq AND APPL_PART_YN = 'Y') B " +
            "WHERE A.RECR_SCHL_CODE = :schoolCode " +
            "AND A.SMST_TYPE_CODE = B.SMST_TYPE_CODE " +
            "ORDER BY A.SMST_TYPE_CODE", nativeQuery = true)
    List<AdmsResponse> findSemesterType(@Param("schoolCode") String schoolCode,
                                        @Param("enterYear") String enterYear,
                                        @Param("recruitPartSeq") Integer recruitPartSeq);

    List<SemesterType> findAllByRecruitSchoolCode(String recruitSchoolCode);
}
