package kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendInfoResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;

import java.util.List;
import java.util.Optional;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
public interface RecommedRepository extends JpaRepository<Recommend, Long>, RecommedRepositoryCustum {


    @Query(value = "SELECT A.APPL_NO AS id,A.ENG_NAME AS engName,A.USER_NO AS userId,  " +
            "CASE WHEN A.CITZ_CNTR_CODE='999' THEN A.ETC_CITZ_CNTR_NAME  ELSE  (SELECT CNTR_ENG_NAME FROM  CNTR where CNTR_CODE= A.CITZ_CNTR_CODE) END AS engCntr, " +
            "A.ENTR_YEAR AS enterYear, A.RECR_PART_SEQ AS recruitPartSeq ,S.RECR_SCHL_CODE AS recrShclCode, S.RECR_SCHL_ENG_NAME AS recrShclEngName, " +
            "E.CORS_ENG_NAME AS courseEngName,G.MAJ_ENG_NAME AS majorEngName ,G.MAJ_CODE AS majorCode, E.CORS_KOR_NAME AS courseKorName, " +
            "RE.REC_STS_CODE AS stauts,RE.PROF_NAME AS profName, A.RECR_SCHL_CODE AS schoolCode, G.MAJ_KOR_NAME AS majorKorName, " +
            "P.RECR_PART_ENG_NAME AS recrPartEngName ,T.END_DATE AS recommendEndTime ,T.END_DATE AS recrPartEndDate  " +
            "FROM APPL_REC RE JOIN APPL A  ON RE.APPL_NO = A.APPL_NO " +
            "JOIN  APPL_PART B ON A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND A.ENTR_YEAR = B.ENTR_YEAR  AND A.RECR_PART_SEQ = B.RECR_PART_SEQ  AND A.APPL_PART_NO = B.APPL_PART_NO  " +
            "JOIN  CORS E ON E.RECR_SCHL_CODE =B.RECR_SCHL_CODE   AND E.CORS_CODE=B.CORS_CODE " +
            "JOIN MAJ G ON G.RECR_SCHL_CODE=B.RECR_SCHL_CODE AND G.ENTR_YEAR=B.ENTR_YEAR AND G.RECR_PART_SEQ =B.RECR_PART_SEQ AND G.MAJ_CODE=B.MAJ_CODE " +
            "JOIN RECR_SCHL S ON S.RECR_SCHL_CODE =B.RECR_SCHL_CODE " +
            "JOIN RECR_PART P ON P.RECR_SCHL_CODE=B.RECR_SCHL_CODE AND P.ENTR_YEAR=B.ENTR_YEAR AND P.RECR_PART_SEQ =B.RECR_PART_SEQ " +
            "JOIN RECR_PART_TIME T ON T.RECR_SCHL_CODE=B.RECR_SCHL_CODE AND T.ENTR_YEAR = B.ENTR_YEAR AND T.RECR_PART_SEQ =B.RECR_PART_SEQ AND " +
            "T.TIME_TYPE_CODE ='RECOMMEND' " +
            "WHERE RE.APPL_REC_NO = :recNo", nativeQuery = true)
    RecommendInfoResponse findRecInfo(@Param("recNo") Long recNo);

    Optional<Recommend> findByKey(String key);

    List<Recommend> findByAppl_Id(Long applNo);

    Long countByAppl(Appl appl);


    /*
     * 시스템관리자 - KDIS 지원완료후 전달(제출완료한 추천서정보) for Excel
     * 2020-12-17 최은주
     */
    @Query(value =
            "SELECT appl.APPL_ID as APPLICANT_NO, " +
            "       recProf.PROF_NAME as NAME, recProf.PROF_INST as ORGANIZATION, " +
            "       recProf.PROF_POSITION as POSITION , recProf.PROF_EMAIL as EMAIL, " +
            "       recProf.CREATED_BY as INPUT_ID, DATE_FORMAT(recProf.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as INPUT_DTIME, '' as INPUT_IP," +
            "       recProf.LAST_MODIFIED_BY as UPDATE_ID,  DATE_FORMAT(recProf.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s') as UPDATE_DTIME, '' as UPDATE_IP, " +
            "       recProf.PROF_PHONE_NUM as PHONE_NO, " +
            "       recProf.PROF_ADDR as ADDRESS, " +
            "       recProf.PROF_SIGN as SIGNATURE " +
            "FROM  APPL_REC rec " +
            "      INNER JOIN APPL appl on (rec.appl_no = appl.appl_no) " +
            "      LEFT JOIN APPL_REC_PROF_INFO recProf ON ( rec.APPL_REC_NO = recProf.APPL_REC_NO ) " +
            "WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
            "AND rec.REC_STS_CODE = '00040' " +
            "ORDER BY appl.APPL_ID, rec.APPL_REC_NO", nativeQuery = true)
    List<Object[]> findKidsRecommenderList(@Param("schoolCode") String schoolCode,
                                      @Param("enterYear") String enterYear,
                                      @Param("recruitPartSeq") Integer recruitPartSeq);

    /*
     * 시스템관리자 - KDIS 지원완료후 전달(제출완료한 추천서답변) for Excel
     *
     * 2020-12-17 최은주
     */
    @Query(value =
            "SELECT subA.APPL_ID as APPLICANT_NO, "+
            "       subA.CREATE_DTIME, subA.UPDATE_DTIME, "+
            "       subA.ANSWER_01, subA.ANSWER_02, subA.ANSWER_03, subA.ANSWER_04, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_05) as ANSWER_05, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_06) as ANSWER_06, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_07) as ANSWER_07, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_08) as ANSWER_08, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_09) as ANSWER_09, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_10) as ANSWER_10, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_11) as ANSWER_11, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_12) as ANSWER_12, "+
            "       SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'REC_EVAL', subA.ANSWER_13) as ANSWER_13, "+
            "       subA.ANSWER_14, subA.ANSWER_15, "+
            "       '' as ANSWER_16, "+
            "       '' as ANSWER_17, "+
            "       '' as ANSWER_18, "+
            "       '' as ANSWER_19, "+
            "       '' as ANSWER_20 "+
            "FROM ( SELECT appl.APPL_ID, "+
            "              DATE_FORMAT(recProfAns.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as CREATE_DTIME, " +
            "              DATE_FORMAT(recProfAns.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s') as  UPDATE_DTIME, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 1 THEN recProfAns.ANSWER END) AS ANSWER_01, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 2 THEN recProfAns.ANSWER END) AS ANSWER_02, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 3 THEN recProfAns.ANSWER END) AS ANSWER_03, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 4 THEN recProfAns.ANSWER END) AS ANSWER_04, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 5 THEN recProfAns.ANSWER END) AS ANSWER_05, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 6 THEN recProfAns.ANSWER END) AS ANSWER_06, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 7 THEN recProfAns.ANSWER END) AS ANSWER_07, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 8 THEN recProfAns.ANSWER END) AS ANSWER_08, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 9 THEN recProfAns.ANSWER END) AS ANSWER_09, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 10 THEN recProfAns.ANSWER END) AS ANSWER_10, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 11 THEN recProfAns.ANSWER END) AS ANSWER_11, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 12 THEN recProfAns.ANSWER END) AS ANSWER_12, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 13 THEN recProfAns.ANSWER END) AS ANSWER_13, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 14 THEN recProfAns.ANSWER END) AS ANSWER_14, "+
            "              MAX(CASE WHEN recProfAns.QST_NO = 15 THEN recProfAns.ANSWER END) AS ANSWER_15 "+
            "       FROM  APPL_REC rec "+
            "             INNER JOIN APPL appl ON (rec.appl_no = appl.appl_no) "+
            "             LEFT JOIN APPL_REC_PROF_ANS recProfAns ON ( rec.APPL_REC_NO = recProfAns.APPL_REC_NO ) "+
            "       WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ = :recruitPartSeq "+
            "       AND   rec.REC_STS_CODE = '00040' "+
            "       GROUP BY appl.APPL_NO, rec.APPL_REC_NO "+
            "       ORDER BY appl.APPL_ID, rec.APPL_REC_NO )subA ", nativeQuery = true)
    List<Object[]> findKidsRecommenderAnswerList(@Param("schoolCode") String schoolCode,
                                                 @Param("enterYear") String enterYear,
                                                 @Param("recruitPartSeq") Integer recruitPartSeq);
}