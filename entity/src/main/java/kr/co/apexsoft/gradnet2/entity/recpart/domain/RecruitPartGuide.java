package kr.co.apexsoft.gradnet2.entity.recpart.domain;

import kr.co.apexsoft.gradnet2.entity._common.IdentifiedValueObject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Class Description
 *
 * 모집회차별 가이드 링크
 *
 * @author 최은주
 * @since 2020-06-19
 */
@Entity
@Table(name = "RECR_PART_GUIDE")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RecruitPartGuide extends IdentifiedValueObject {
    @Column(name = "RECR_SCHL_CODE")
    private String recruitSchoolCode;

    @Column(name = "ENTR_YEAR")
    private String enterYear;

    @Column(name = "RECR_PART_SEQ")
    private Integer seq;

    @Column(name = "ORDER_SEQ")
    private Integer orderSeq;

    @Column(name = "SITE_URL")
    private String siteUrl;

    @Column(name = "SITE_TITLE_KOR")
    private String siteTitleKor;

    @Column(name = "SITE_TITLE_ENG")
    private String siteTitleEng;
}
