package kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
*
*
* @author aran
* @since 2020-02-27
*/
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Getter
public class SrvyQstAnsId implements Serializable {
    @Id
    @Column(name = "SRVY_QST_NO")
    private Long srvyQstNo;

    @Id
    @Column(name = "RECR_SCHL_CODE")
    private String recruitSchoolCode;

    @Id
    @Column(name = "SRVY_NO")
    private Long srvyNo;
}
