package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.domain.Cors;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.CorsId;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface CorsRepository extends JpaRepository<Cors, CorsId>, CorsRepositoryCustom {
    @Query(value = "SELECT A.CORS_CODE AS code, A.CORS_KOR_NAME AS codeKrValue, A.CORS_ENG_NAME AS codeEnValue " +
            "FROM CORS A, " +
            "(SELECT DISTINCT CORS_CODE " +
            "FROM APPL_PART " +
            "WHERE RECR_SCHL_CODE = :schoolCode AND ENTR_YEAR = :enterYear AND RECR_PART_SEQ = :recruitPartSeq " +
            "AND SMST_TYPE_CODE = :semesterTypeCode AND ADMS_CODE = :admissionCode AND APPL_PART_YN = 'Y') B " +
            "WHERE A.CORS_CODE = B.CORS_CODE " +
            "AND A.RECR_SCHL_CODE = :schoolCode " +
            "ORDER BY A.CORS_CODE", nativeQuery = true)
    List<AdmsResponse> findCors(@Param("schoolCode") String schoolCode,
                                @Param("enterYear") String enterYear,
                                @Param("recruitPartSeq") Integer recruitPartSeq,
                                @Param("semesterTypeCode") String semesterTypeCode,
                                @Param("admissionCode") String admissionCode);
}
