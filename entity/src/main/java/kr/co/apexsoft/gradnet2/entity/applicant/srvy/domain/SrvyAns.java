package kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
*
*
* @author aran
* @since 2020-02-26
*/
@Entity
@Table(name="SRVY_ANS")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class SrvyAns extends AbstractBaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SRVY_NO")
    private Long id;

    @Column(name = "RECR_SCHL_CODE")
    private String recruitSchoolCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "SUBMIT_YN")
    private Boolean answered;

    @Column(name = "SUBMIT_DATE")
    private LocalDateTime submitDate;

    public void srvyAnsClose() {
        this.answered = true;
        this.submitDate = LocalDateTime.now();
    }
}
