package kr.co.apexsoft.gradnet2.entity.recpart.projection;

/**
 * Class Description
 *
 * @author 최은주
 * @since 2020-06-22
 */
public interface RecruitPartGuideResponse {
    String getRecruitSchoolCode();

    String getEnterYear();

    String getRecruitPartSeq();

    String getOrderSeq();

    String getSiteUrl();

    String getSiteTitleKor();

    String getSiteTitleEng();
}
