package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-05
 */
@Getter
@AllArgsConstructor
public class DateCountProjection {

    private LocalDateTime date;
    private Long count;

    public DateCountProjection(String date, Long count) {
        this.date = LocalDate.parse(date).atStartOfDay();
        this.count = count;
    }
}
