package kr.co.apexsoft.gradnet2.entity.recpart.repository;

import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface RecruitPartTimeRepository extends JpaRepository<RecruitPartTime, Long> {
}
