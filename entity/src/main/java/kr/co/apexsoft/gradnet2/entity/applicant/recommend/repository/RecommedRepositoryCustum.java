package kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendSimpleForListProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
public interface RecommedRepositoryCustum {

    Page<RecommendSimpleForListProjection> findAllBySearchCondition(
        String schoolCode,
        String enterYear,
        Integer recruitPartSeq,
        String admissionCode,
        String courseCode,
        String categoryCode,
        String majorCode,
        String applId,
        String applicantName,
        String email,
        String applicantId,
        String profName,
        String profEmail,
        String profPhoneNumber,
        String recStatus, Pageable pageable);


}
