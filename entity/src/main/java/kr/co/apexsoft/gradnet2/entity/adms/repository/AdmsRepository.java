package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.domain.Adms;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.AdmsId;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface AdmsRepository extends JpaRepository<Adms, AdmsId>, AdmsRepositoryCustom {

    @Query(value = "SELECT A.ADMS_CODE AS code, A.ADMS_KOR_NAME AS codeKrValue, A.ADMS_ENG_NAME AS codeEnValue " +
            "FROM ADMS A, " +
            "(SELECT DISTINCT ADMS_CODE " +
            "FROM APPL_PART " +
            "WHERE RECR_SCHL_CODE = :schoolCode " +
            "AND ENTR_YEAR = :enterYear " +
            "AND RECR_PART_SEQ = :recruitPartSeq " +
            "AND SMST_TYPE_CODE = :semesterTypeCode " +
            "AND APPL_PART_YN = 'Y') B " +
            "WHERE A.RECR_SCHL_CODE = :schoolCode " +
            "AND A.ADMS_CODE = B.ADMS_CODE ORDER BY A.ADMS_CODE", nativeQuery = true)
    List<AdmsResponse> findAdms(@Param("schoolCode") String schoolCode,
                                @Param("enterYear") String enterYear,
                                @Param("recruitPartSeq") Integer recruitPartSeq,
                                @Param("semesterTypeCode") String semesterTypeCode);
}
