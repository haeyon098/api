package kr.co.apexsoft.gradnet2.entity.applicant.career.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-04
 */
public interface CareerRepository extends JpaRepository<Career, Long> , CareerRepositoryCustom {
    List<Career> findByAppl_IdOrderBySeq(@Param("id") Long id);

    void deleteByAppl_IdAndIdNotIn(Long applNo, Long[] ids);

    void deleteByAppl_Id(Long applNo);

    /* 시스템관리자 - KDIS 지원완료후 전달(경력)  for Excel
     * 2020-12-17 최은주
     */
    @Query(value = "SELECT appl.APPL_ID as APPLICANT_NO, " +
                   "       career.COMPANY_NAME as COMPANY_NAME, career.COMPANY_NAME as COMPANY_ENAME, " +
                   "       DATE_FORMAT(career.JOIN_DAY, '%Y%m') as FROM_DATE, date_format(career.RETR_DAY, '%Y%m') as TO_DATE, " +
                   "       career.CURR_YN as CURRENT_YN, " +
                   "       career.COMPANY_DEPARTMENT as DEPT_KNAME, " +
                   "       career.COMPANY_DEPARTMENT as DEPT_ENAME, " +
                   "       career.COMPANY_POSITION as POSITION_NAME, " +
                   "       career.COMPANY_POSITION as POSITION_ENAME, " +
                   "       '' as REMARK, " +
                   "       career.CREATED_BY as INPUT_ID, " +
                   "       DATE_FORMAT(career.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as INPUT_DTIME, " +
                   "       '' as INPUT_IP, " +
                   "       career.LAST_MODIFIED_BY as UPDATE_ID, " +
                   "       DATE_FORMAT(career.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s') as UPDATE_DTIME, " +
                   "       '' as UPDATE_IP, " +
                   "       (CASE career.COMPANY_TYPE_CODE WHEN '01' then '02' WHEN '02' THEN '01' WHEN '03' THEN '03' ELSE 'ERROR_확인필요' END )as EMP_CAT " +
                   "FROM APPL_CAREER career " +
                   "     INNER JOIN APPL appl ON ( career.APPL_NO = appl.APPL_NO) " +
                   "WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
                   "ORDER BY appl.APPL_ID, career.APPL_CAREER_NO", nativeQuery = true)
    List<Object[]> findKidsCareerList(@Param("schoolCode") String schoolCode,
                                       @Param("enterYear") String enterYear,
                                       @Param("recruitPartSeq") Integer recruitPartSeq);
}
