package kr.co.apexsoft.gradnet2.entity.applpart.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class Description
 * 회차별 서술입력항목
 *
 * @author 김혜연
 * @since 2020-08-04
 */
@Entity
@Table(name = "APPL_SEQ_ESSAY")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ApplSeqEssay extends AbstractBaseEntity {
    @Id
    @Column(name = "APPL_SEQ_ESSAY_NO")
    private Long id;

    @Column(name = "RECR_SCHL_CODE")
    private String schoolCode;

    @Column(name = "ENTR_YEAR")
    private String enterYear;

    @Column(name = "RECR_PART_SEQ")
    private Integer recruitPartSeq;

    @Column(name = "ESSAY_SEQ")
    private Integer seq;

    @Column(name = "KOR_TITLE")
    private String korTitle;

    @Column(name = "ENG_TITLE")
    private String engTitle;

    @Column(name = "KOR_DESC")
    private String korDescription;

    @Column(name = "ENG_DESC")
    private String engDescription;

    @Column(name = "USE_YN")
    private Boolean used;
}
