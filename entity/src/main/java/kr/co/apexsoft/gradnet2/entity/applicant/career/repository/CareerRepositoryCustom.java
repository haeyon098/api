package kr.co.apexsoft.gradnet2.entity.applicant.career.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.career.projection.CareerProjection;

import java.util.List;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-03-11
 */
public interface CareerRepositoryCustom {

    List<CareerProjection> findCareerInfo(long applNo);
}
