package kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-25
 */
public interface RecommendInfoResponse {

   //ApplNo
   Long getId();

   String getCourseKorName();

   String getRecrShclCode();

   String getEnterYear();

   Integer getRecruitPartSeq();

   String getCourseEngName();

   String getMajorKorName();

   String getMajorEngName();

   String getMajorCode();

   String getRecrPartEndDate();

   String getRecrPartKorName();

   String getRecrPartEngName();

   String getRecommendEndTime();

   String getEngName();

   String getkorName();

   String getEngCntr();

   String getkorCntr();

   String getStauts();

   String getProfName();

   String getUserId();

   String getSchoolCode();

   String getRecrShclEngName();
}
