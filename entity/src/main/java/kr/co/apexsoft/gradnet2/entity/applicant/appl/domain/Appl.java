package kr.co.apexsoft.gradnet2.entity.applicant.appl.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity._common.EnumModel;
import kr.co.apexsoft.gradnet2.entity.adms.domain.Adms;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-30
 */
@Entity
@Table(name = "APPL")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@SecondaryTables({
        @SecondaryTable(
                name = "APPL_GEN",
                pkJoinColumns = {@PrimaryKeyJoinColumn(name = "APPL_NO")}
        ),
        @SecondaryTable(
                name = "APPL_FORN",
                pkJoinColumns = {@PrimaryKeyJoinColumn(name = "APPL_NO")}
        ),
})
public class Appl extends AbstractBaseEntity {
    @Id
    @Column(name = "APPL_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumns({
            @JoinColumn(name = "RECR_SCHL_CODE", referencedColumnName = "RECR_SCHL_CODE"),
            @JoinColumn(name = "ENTR_YEAR", referencedColumnName = "ENTR_YEAR"),
            @JoinColumn(name = "RECR_PART_SEQ", referencedColumnName = "RECR_PART_SEQ"),
            @JoinColumn(name = "APPL_PART_NO", referencedColumnName = "APPL_PART_NO")
    })
    private ApplPart part;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "USER_NO")
//    @JsonIgnore
// @JsonIgnore 사용이유 : APPL_DOC, APPL_CAREER 등에서 APPL_NO로 쿼리메소드 실행했을때, applicant Initaialize 관련 에러가 발생
//    현재 EAGER로 수정
    private User applicant;

    @Column(name = "APPL_ID")
    private String applId;

    @Column(name = "APPL_DATE")
    private LocalDateTime applDate;

    @Embedded
    private ApplicantInfo applicantInfo;

    @Column(name = "APPL_STS_CODE")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "PRIV_INFO_YN")
    private Boolean privateInfoAgree;

    @Column(name = "DOC_CHK_YN")
    private Boolean documentChecked;

    @Setter
    @Column(name = "ZIP_PRINT_YN")
    private Boolean zipPrinted;

    @Embedded
    private GeneralInfo generalInfo;

    @Embedded
    private ForeignerInfo foreignerInfo;

    @Getter
    public enum Status implements EnumModel {
        APPL_PART_SAVE("지원사항 저장", "Application Details Saved"), // 지원사항 저장
        BASIS_SAVE("기본정보 저장", "Basic Info Saved"),              // 기본정보 저장
        ACAD_SAVE("학력 저장", "Educational Background Saved"),      // 학력정보 저장
        LANG_SAVE("어학 및 경력 저장", "Language&Career Info Saved"), // 어학정보 저장
        ESSAY_SAVE("자기소개서 저장", "Essay Saved"),                 // 서술입력
        FILE_SAVE("첨부파일 저장", "File Saved"),                    // 첨부파일 저장
        SUBMIT("작성완료", "Form Completed"),                       // 원서작성완료
        VBANK_ISSUE("가상계좌발급", "Virtual Account Issue"),        // 가상계좌발급
        COMPLETE("지원완료", "Completed"),                          // 지원완료
        CANCEL("지원취소", "Cancel")                                // 지원취소
        ;

        private String korName;
        private String engName;

        Status(String korName, String engName) {
            this.korName = korName;
            this.engName = engName;
        }

        @Override
        public String getValue() {
            return name();
        }
    }

    public void changeApplId(String applId) {
        this.applId = applId;
    }

    public void changeApplicant(User user) {
        this.applicant = user;
    }

    public void changeStatus(Status status) {
        this.status = status;
    }

    public void changeComplete() {
        this.status = Status.COMPLETE;
        this.applDate = LocalDateTime.now();
    }

    public void initBasisInfo(Appl appl) {
        this.generalInfo = appl.getGeneralInfo();
        this.foreignerInfo = appl.getForeignerInfo();
        this.applicantInfo = appl.getApplicantInfo();
    }

    public void encryptRegistrationEncr(String registrationEncr) {
        this.getApplicantInfo().encryptRegistrationEncr(registrationEncr);
    }

    public void encryptForeignerInfo(String passportNo, String visaNo, String foreignerRegistrationNo) {
        this.getForeignerInfo().encryptForeignerInfo(passportNo, visaNo, foreignerRegistrationNo);
    }

    public boolean isCompleteStatus() {
        return this.getStatus() == Status.COMPLETE;
    }

    public boolean isRecRequestNotAvailable() {
        return this.getStatus() == Status.APPL_PART_SAVE || this.getStatus() == Status.CANCEL;
    }

    public boolean isCancelStatus() {
        return this.getStatus() == Status.CANCEL;
    }

    public boolean isSubmitStatus() { return this.getStatus() == Status.SUBMIT; }

    public void documentCheckedTrue() {
        this.documentChecked = true;
    }

    public void initContactCode(String contactCode) {
        this.applicantInfo.initContactCode(contactCode);
    }

    public boolean isGeneral() {
        return Adms.AmdsCodeType.KR.getValue().equals(this.getPart().getAdmissionCode());
    }

    public void checkAndRollbackOneStep() {
        Appl.Status[] statusArr = RecruitSchool.SchoolCodeType.getSteps(this.part.getSchoolCode());
        int index = Arrays.asList(statusArr).indexOf(this.status);

        if(this.status.equals(Appl.Status.FILE_SAVE)) this.status = statusArr[index-1];
    }

}
