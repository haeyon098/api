package kr.co.apexsoft.gradnet2.entity.recpart.domain;

import kr.co.apexsoft.gradnet2.entity._common.IdentifiedValueObject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Class Description
 *
 * 모집단위별시간
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Getter
@Table(name = "RECR_PART_TIME")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RecruitPartTime extends IdentifiedValueObject {

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "RECR_SCHL_CODE"),
            @JoinColumn(name = "ENTR_YEAR"),
            @JoinColumn(name = "RECR_PART_SEQ")
    })
    private RecruitPart recruitPart;

    @Column(name = "TIME_TYPE_CODE")
    @Enumerated(EnumType.STRING)
    private TimeType type;

    @Column(name = "START_DATE")
    private LocalDateTime startDate;

    @Column(name = "END_DATE")
    private LocalDateTime endDate;

    @Column(name = "TIME_DESC")
    private String description;

    public enum TimeType {
        SUBMIT, // 원서 작성
        COMPLETE,
        RECOMMEND,          // 추천서작성
        RECOMMEND_REQUEST   // 추천서요청
    }
}
