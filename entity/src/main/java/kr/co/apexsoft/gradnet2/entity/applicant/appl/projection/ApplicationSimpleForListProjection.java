package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-18
 */
@Getter
@AllArgsConstructor
public class ApplicationSimpleForListProjection {

    private String applicantId;
    private Long applNo;
    private String applId;
    private Integer recruitPartSeq;
    private String recruitPartName;
    private String recruitPartEngName;
    private String admissionCode;
    private String admissionName;
    private String admissionEngName;
    private String courseCode;
    private String courseName;
    private String courseEngName;
    private String categoryCode;
    private String categoryName;
    private String categoryEngName;
    private String majorCode;
    private String majorName;
    private String majorEngName;
    private String applicantKorName;
    private String applicantEngName;
    private String phoneNumber;
    private String email;
    private LocalDate dateOfBirth;
    private Boolean zipPrinted;
    private Appl.Status status;
    private Long recLetterCount;
    private Long recLetterReqCount;
}
