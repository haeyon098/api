package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
public interface CorsRepositoryCustom {

    List<AdmsProjection> findBySchoolAndEnterYearAndRecruitPartSeqAndAdmission(
            @Param("schoolCode") String schoolCode,
            @Param("enterYear") String enterYear,
            @Param("recruitPartSeq") Integer recruitPartSeq,
            @Param("admissionCode") String admissionCode);
}
