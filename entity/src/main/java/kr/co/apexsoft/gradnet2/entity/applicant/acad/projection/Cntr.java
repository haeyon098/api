package kr.co.apexsoft.gradnet2.entity.applicant.acad.projection;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
@AllArgsConstructor
public class Cntr {
    private String id;
    private String name;
}
