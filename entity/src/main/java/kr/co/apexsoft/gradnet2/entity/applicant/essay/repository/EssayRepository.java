package kr.co.apexsoft.gradnet2.entity.applicant.essay.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.essay.domain.Essay;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-04
 */
public interface EssayRepository extends JpaRepository<Essay, Long> {
    List<Essay> findByAppl_Id(Long id);
}
