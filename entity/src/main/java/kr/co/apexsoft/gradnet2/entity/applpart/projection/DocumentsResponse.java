package kr.co.apexsoft.gradnet2.entity.applpart.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-20
 */
public interface DocumentsResponse {
    String getGroupCode();

    String getCode();

    String getKorName();

    String getEngName();

    String getApplDocNo();

    String getFileName();

    String getDocGrpNo();

    String getFilePath();

    String getRequired();

    String getMsgNo();

    String getKorMsg();

    String getEngMsg();
}
