package kr.co.apexsoft.gradnet2.entity.applicant.acad.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-03
 */
@Entity
@Table(name="APPL_ACAD")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Academy extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPL_ACAD_NO")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Enumerated(EnumType.STRING)
    @Column(name = "ACAD_TYPE_CODE")
    private AcademyType type;

    @Column(name = "ACAD_SEQ")
    private Integer seq;

    @Column(name = "LAST_SCHL_YN")
    private Boolean lasted;

    @Embedded
    private School school;

    @Column(name = "ENTR_DAY")
    private LocalDate enterDay;

    @Column(name = "GRDA_DAY")
    private LocalDate graduateDay;

    @Embedded
    private Grade grade;

    @Column(name = "ACAD_STS_CODE")
    private String statusCode;

    public enum AcademyType {
        UNIV,   // 대학교
        GRAD    // 대학원
    }

    public boolean notChangeSchoolName(Long id, String schoolCode, String schoolName) {
        return id.equals(this.id) && school.getCode().equals(schoolCode) && school.getName().equals(schoolName);
    }
}
