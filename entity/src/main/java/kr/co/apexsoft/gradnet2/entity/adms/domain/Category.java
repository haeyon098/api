package kr.co.apexsoft.gradnet2.entity.adms.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.CategoryId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * 지원구분
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name = "CATEGORY")
@Getter
@IdClass(CategoryId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Category extends AbstractBaseEntity {
    @Id
    private String recruitSchoolCode;

    @Id
    private String id;

    @Column(name = "CATEGORY_KOR_NAME")
    private String korName;

    @Column(name = "CATEGORY_ENG_NAME")
    private String engName;
}
