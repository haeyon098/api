package kr.co.apexsoft.gradnet2.entity.adms.repository;

import com.querydsl.core.types.Projections;
import kr.co.apexsoft.gradnet2.entity.adms.domain.Major;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.adms.domain.QMajor.major;
import static kr.co.apexsoft.gradnet2.entity.applpart.domain.QApplPart.applPart;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
public class MajorRepositoryImpl extends QuerydslRepositorySupport implements MajorRepositoryCustom {

    public MajorRepositoryImpl() {
        super(Major.class);
    }

    @Override
    public List<AdmsProjection> findBy(String schoolCode,
                                       String enterYear,
                                       Integer recruitPartSeq,
                                       String admissionCode,
                                       String courseCode,
                                       String categoryCode) {
        return from(major)
                .innerJoin(applPart)
                .on(
                        major.recruitSchoolCode.eq(applPart.schoolCode),
                        major.enterYear.eq(applPart.enterYear),
                        major.recruitPartSeq.eq(applPart.recruitPartSeq),
                        major.id.eq(applPart.majorCode)
                )
                .where(
                        applPart.schoolCode.eq(schoolCode),
                        applPart.enterYear.eq(enterYear),
                        applPart.recruitPartSeq.eq(recruitPartSeq),
                        applPart.admissionCode.eq(admissionCode),
                        applPart.courseCode.eq(courseCode),
                        applPart.categoryCode.eq(categoryCode)
                )
                .distinct()
                .select(Projections.constructor(AdmsProjection.class,
                        major.id, major.korName, major.engName))
                .fetch();
    }
}
