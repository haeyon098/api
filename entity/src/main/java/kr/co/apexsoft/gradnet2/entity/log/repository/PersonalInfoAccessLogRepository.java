package kr.co.apexsoft.gradnet2.entity.log.repository;

import kr.co.apexsoft.gradnet2.entity.log.domain.PersonalInfoAccessLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-11-17
 */
public interface PersonalInfoAccessLogRepository extends JpaRepository<PersonalInfoAccessLog, String> {
}
