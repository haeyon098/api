package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.domain.Major;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.MajorId;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface MajorRepository extends JpaRepository<Major, MajorId>, MajorRepositoryCustom {

    @Query(value = "SELECT B.APPL_PART_NO AS code, A.MAJ_KOR_NAME AS codeKrValue, A.MAJ_ENG_NAME AS codeEnValue " +
            "FROM MAJ A, " +
            "(SELECT DISTINCT MAJ_CODE, APPL_PART_NO " +
            "FROM APPL_PART " +
            "WHERE RECR_SCHL_CODE = :schoolCode AND ENTR_YEAR = :enterYear AND RECR_PART_SEQ = :recruitPartSeq " +
            "AND SMST_TYPE_CODE = :semesterTypeCode AND ADMS_CODE = :admissionCode AND CORS_CODE = :courseCode " +
            "AND CATEGORY_CODE = :categoryCode AND APPL_PART_YN = 'Y') B " +
            "WHERE A.MAJ_CODE = B.MAJ_CODE " +
            "AND A.RECR_SCHL_CODE = :schoolCode AND A.ENTR_YEAR =:enterYear AND A.RECR_PART_SEQ = :recruitPartSeq " +
            "ORDER BY A.MAJ_CODE", nativeQuery = true)
    List<AdmsResponse> findMajor(@Param("schoolCode") String schoolCode,
                                 @Param("enterYear") String enterYear,
                                 @Param("recruitPartSeq") Integer recruitPartSeq,
                                 @Param("semesterTypeCode") String semesterTypeCode,
                                 @Param("admissionCode") String admissionCode,
                                 @Param("courseCode") String courseCode,
                                 @Param("categoryCode") String categoryCode);
}
