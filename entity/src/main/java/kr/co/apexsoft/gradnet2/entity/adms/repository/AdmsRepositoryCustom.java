package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-21
 */
public interface AdmsRepositoryCustom {

    List<AdmsProjection> findBySchoolAndEnterYearAndRecruitPartSeq(
            @Param("schoolCode") String schoolCode,
            @Param("enterYear") String enterYear,
            @Param("recruitPartSeq") Integer recruitPartSeq);
}
