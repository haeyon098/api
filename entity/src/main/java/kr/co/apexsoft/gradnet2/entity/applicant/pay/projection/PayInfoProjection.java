package kr.co.apexsoft.gradnet2.entity.applicant.pay.projection;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-09-15
 */
public interface PayInfoProjection {
    Long getId();

    String getStatus();

    String getEmail();

    String getSchoolCode();

    String getSchoolEngName();

    String getSchoolKorName();

    String getEnterYear();


    String getSemesterTypeCode();

    String getSemesterTypeKorName();

    String getSemesterTypeEngName();

    String getAdmissionCode();

    String getAdmissionKorName();

    String getAdmissionEngName();

    String getCourseCode();

    String getCourseKorName();

    String getCourseEngName();

    String getCategoryCode();

    String getCategoryKorName();

    String getCategoryEngName();

    String getMajorCode();

    String getMajorKorName();

    String getMajorEngName();
    String getEngName();
    String getKorName();

    String getFeeKRW();
    String getFeeUSD();

    String getPayDueDay();

    String getTelNum();

    Integer getRecruitPartSeq();
}
