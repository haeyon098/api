package kr.co.apexsoft.gradnet2.entity.adms.repository;

import kr.co.apexsoft.gradnet2.entity.adms.domain.Category;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.CategoryId;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface CategoryRepository extends JpaRepository<Category, CategoryId>, CategoryRepositoryCustom {

    @Query(value = "SELECT A.CATEGORY_CODE AS code, A.CATEGORY_KOR_NAME AS codeKrValue, A.CATEGORY_ENG_NAME AS codeEnValue " +
            "FROM CATEGORY A, " +
            "(SELECT DISTINCT CATEGORY_CODE " +
            "FROM APPL_PART " +
            "WHERE RECR_SCHL_CODE = :schoolCode AND ENTR_YEAR = :enterYear AND RECR_PART_SEQ = :recruitPartSeq " +
            "AND SMST_TYPE_CODE = :semesterTypeCode AND ADMS_CODE = :admissionCode " +
            "AND CORS_CODE = :courseCode AND APPL_PART_YN = 'Y') B " +
            "WHERE A.CATEGORY_CODE = B.CATEGORY_CODE " +
            "AND A.RECR_SCHL_CODE = :schoolCode " +
            "ORDER BY A.CATEGORY_CODE", nativeQuery = true)
    List<AdmsResponse> findCategory(@Param("schoolCode") String schoolCode,
                                    @Param("enterYear") String enterYear,
                                    @Param("recruitPartSeq") Integer recruitPartSeq,
                                    @Param("semesterTypeCode") String semesterTypeCode,
                                    @Param("admissionCode") String admissionCode,
                                    @Param("courseCode") String courseCode);
}
