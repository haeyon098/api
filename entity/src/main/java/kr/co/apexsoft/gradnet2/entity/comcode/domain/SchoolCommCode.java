package kr.co.apexsoft.gradnet2.entity.comcode.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-01-03
 */
@Entity
@Table(name = "RECR_SCHL_COMM_CODE")
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@IdClass(SchoolCommCodeIds.class)
public class SchoolCommCode {

    /*모집학교코드*/
    @Id
    @Column(name = "RECR_SCHL_CODE",length =50 )
    private String schoolCode;

    /*입학연도*/
    @Id
    @Column(name = "ENTR_YEAR",length =50 )
    private String enterYear;

    /*모집회차*/
    @Id
    @Column(name = "RECR_PART_SEQ",length =50 )
    private Integer recruitPartSeq;



    /* 공통코드 그룹 */
    @Id
    @Column(name = "CODE_GRP",length =50 )
    private String codeGrp;

    @Id
    @Column(name = "CODE",length =50 )
    private String code;


    /*코드 한글명*/
    @Column(name = "CODE_KOR_VAL")
    private String codeKrValue;

    /*코드 영문명*/
    @Column(name = "CODE_ENG_VAL")
    private String codeEnValue;

    /* 사용여부 */
    @Column(name = "USE_YN")
    @ColumnDefault(value = "true")
    private Boolean used;

}
