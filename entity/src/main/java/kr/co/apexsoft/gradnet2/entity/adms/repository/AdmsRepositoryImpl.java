package kr.co.apexsoft.gradnet2.entity.adms.repository;

import com.querydsl.core.types.Projections;
import kr.co.apexsoft.gradnet2.entity.adms.domain.Adms;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.adms.domain.QAdms.adms;
import static kr.co.apexsoft.gradnet2.entity.applpart.domain.QApplPart.applPart;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-21
 */
public class AdmsRepositoryImpl extends QuerydslRepositorySupport implements AdmsRepositoryCustom {

    public AdmsRepositoryImpl() {
        super(Adms.class);
    }

    @Override
    public List<AdmsProjection> findBySchoolAndEnterYearAndRecruitPartSeq(String schoolCode, String enterYear, Integer recruitPartSeq) {
        return from(adms)
                    .innerJoin(applPart)
                        .on(
                                adms.recruitSchoolCode.eq(applPart.schoolCode),
                                adms.id.eq(applPart.admissionCode)
                        )
                    .where(
                            applPart.schoolCode.eq(schoolCode),
                            applPart.enterYear.eq(enterYear),
                            applPart.recruitPartSeq.eq(recruitPartSeq)
                    )
                    .distinct()
                    .select(Projections.constructor(AdmsProjection.class, adms.id, adms.korName, adms.engName))
                    .fetch();
    }
}
