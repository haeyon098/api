package kr.co.apexsoft.gradnet2.entity.adms.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.MajorId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * 전공
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name = "MAJ")
@Getter
@IdClass(MajorId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Major extends AbstractBaseEntity {
    @Id
    private String recruitSchoolCode;

    @Id
    private String enterYear;

    @Id
    private Integer recruitPartSeq;

    @Id
    private String id;

    @Column(name = "MAJ_KOR_NAME")
    private String korName;

    @Column(name = "MAJ_ENG_NAME")
    private String engName;
}
