package kr.co.apexsoft.gradnet2.entity.log.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Class Description
 * 개인정보 접근기록
 *
 * @author 김혜연
 * @since 2020-11-17
 */
@Entity
@Table(name = "PERSONAL_INFO_ACCESS_LOG")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PersonalInfoAccessLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USER_ID")
    private String userId;

    @Column(name = "IP")
    private String ip;

    @Column(name = "CRE_DATE")
    private LocalDateTime creDate;

    @Column(name = "ACTION")
    private String action;

    @Column(name = "URI")
    private String uri;

    @Column(name = "REQ_PARAMS")
    private String requestParams;

    public PersonalInfoAccessLog(String userId, String ip, String action, String uri, String requestParams) {
        this.userId = userId;
        this.ip = ip;
        this.action = action;
        this.uri = uri;
        this.requestParams = requestParams;
        this.creDate = LocalDateTime.now();
    }
}
