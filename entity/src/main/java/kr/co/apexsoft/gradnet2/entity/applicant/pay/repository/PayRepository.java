package kr.co.apexsoft.gradnet2.entity.applicant.pay.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.Pay;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.projection.PayInfoProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-10
 */
public interface PayRepository extends JpaRepository<Pay, Long> {

    @Query(value = "SELECT DISTINCT A.APPL_NO AS id, A.APPL_STS_CODE AS STATUS, A.KOR_NAME AS korName, A.ENG_NAME AS engName,  A.EMAIL AS email, " +
            "            A.RECR_SCHL_CODE AS schoolCode,S.RECR_SCHL_KOR_NAME AS schoolKorName,S.RECR_SCHL_ENG_NAME AS schoolEngName, " +
            "            A.ENTR_YEAR AS enterYear, A.RECR_PART_SEQ AS recruitPartSeq, " +
            "            C.SMST_TYPE_CODE AS semesterTypeCode, C.SMST_TYPE_KOR_NAME AS semesterTypeKorName, C.SMST_TYPE_ENG_NAME AS semesterTypeEngName, " +
            "            D.ADMS_CODE AS admissionCode, D.ADMS_KOR_NAME AS admissionKorName, D.ADMS_ENG_NAME AS admissionEngName, " +
            "            E.CORS_CODE AS courseCode, E.CORS_KOR_NAME AS courseKorName, E.CORS_ENG_NAME AS courseEngName, " +
            "            F.CATEGORY_CODE AS categoryCode, F.CATEGORY_KOR_NAME AS categoryKorName, F.CATEGORY_ENG_NAME AS categoryEngName, " +
            "            G.MAJ_CODE AS majorCode, G.MAJ_KOR_NAME AS majorKorName, G.MAJ_ENG_NAME AS majorEngName, " +
            "            B.ADMS_FEE_KRW AS feeKRW, B.ADMS_FEE_USD AS feeUSD, date_format(T.END_DATE,'%Y-%m-%d %H:%i:%S') AS payDueDay, " +
            "            A.TEL_NUM AS telNum " +
            "            FROM APPL A, USER U, APPL_PART B, SMST_TYPE C, ADMS D, CORS E, CATEGORY F, MAJ G, RECR_SCHL S ,RECR_PART_TIME T" +
            "            WHERE 1 = 1 " +
            "            AND A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "            AND A.ENTR_YEAR = B.ENTR_YEAR " +
            "            AND A.RECR_PART_SEQ = B.RECR_PART_SEQ " +
            "            AND A.APPL_PART_NO = B.APPL_PART_NO " +
            "            AND C.RECR_SCHL_CODE = A.RECR_SCHL_CODE  " +
            "            AND C.SMST_TYPE_CODE = B.SMST_TYPE_CODE " +
            "            AND D.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "            AND D.ADMS_CODE = B.ADMS_CODE " +
            "            AND E.RECR_SCHL_CODE = A.RECR_SCHL_CODE  " +
            "            AND E.CORS_CODE = B.CORS_CODE " +
            "            AND F.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "            AND F.CATEGORY_CODE = B.CATEGORY_CODE " +
            "            AND G.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "            AND G.ENTR_YEAR = A.ENTR_YEAR  " +
            "            AND G.RECR_PART_SEQ = A.RECR_PART_SEQ " +
            "            AND G.MAJ_CODE = B.MAJ_CODE " +
            "            AND S.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "            AND S.RECR_SCHL_CODE =B.RECR_SCHL_CODE " +
            "            AND S.RECR_SCHL_CODE =C.RECR_SCHL_CODE " +
            "            AND S.RECR_SCHL_CODE =D.RECR_SCHL_CODE " +
            "            AND S.RECR_SCHL_CODE =E.RECR_SCHL_CODE " +
            "            AND S.RECR_SCHL_CODE =F.RECR_SCHL_CODE " +
            "            AND S.RECR_SCHL_CODE =G.RECR_SCHL_CODE " +
            "            AND T.RECR_SCHL_CODE = A.RECR_SCHL_CODE" +
            "            AND T.ENTR_YEAR = A.ENTR_YEAR" +
            "            AND T.RECR_PART_SEQ = A.RECR_PART_SEQ" +
            "            AND T.TIME_TYPE_CODE = 'COMPLETE'" +
            "            AND A.APPL_NO = :applNo", nativeQuery = true)
    PayInfoProjection findPayInfo(@Param("applNo") Long applNo);

    Optional<Pay> findByMerchantUid(String merchantUid);

    Optional<Pay> findByAppl_IdAndStatusNot(Long applNo, Pay.Status status);


    Optional<Pay> findByAppl_IdAndApplStatusAndStatus(Long applNo, Appl.Status status, Pay.Status payStatus);

    /**
     * 아임포트 결제완료 기준으로, 지원완료 미처리 체크
     * 아임포트 결제시간과 PAY_INFO 테이블 삽입 시간 차이 1분씩 고려
     * FIXME: PAY_STS_CODE 변경시 수정필요
     * @param from
     * @param to
     * @return
     */
    @Transactional
    @Query(value = "select IMP_UID from PAY_INFO  where " +
            "LAST_MODIFIED_DATE between  DATE_SUB(:from , INTERVAL 1 MINUTE) and DATE_ADD(:to, INTERVAL 1 MINUTE)  " +
            "and PAY_STS_CODE='COMPLETE' ", nativeQuery = true)
    List<String> findByCompleteImpuid(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to);


}
