package kr.co.apexsoft.gradnet2.entity.applicant.appl.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-02
 */
@Getter
@Embeddable
public class ForeignerInfo {
    @Column(table = "APPL_FORN", name = "FORN_TYPE_CODE")
    private String foreignerTypeCode;

    @Column(table = "APPL_FORN", name = "FORN_RGST_NO")
    private String foreignerRegistrationNo;

    @Column(table = "APPL_FORN", name = "PASSPORT_NO")
    private String passportNo;

    @Column(table = "APPL_FORN", name = "VISA_NO")
    private String visaNo;

    @Column(table = "APPL_FORN", name = "VISA_TYPE_CODE")
    private String visaTypeCode;

    @Column(table = "APPL_FORN", name = "VISA_TYPE_ETC")
    private String visaTypeEtc;

    @Column(table = "APPL_FORN", name = "VISA_EXPR_DAY")
    private LocalDate visaExprDay;

    @Column(table = "APPL_FORN", name = "HOME_ADDR")
    private String homeAddress;

    @Column(table = "APPL_FORN", name = "HOME_TEL_NUM")
    private String homeTelNum;

    @Column(table = "APPL_FORN", name = "KOR_EMRG_NAME")
    private String korEmergencyName;

    @Column(table = "APPL_FORN", name = "KOR_EMRG_RELA")
    private String korEmergencyRelation;

    @Column(table = "APPL_FORN", name = "KOR_EMRG_TEL")
    private String korEmergencyTel;

    @Column(table = "APPL_FORN", name = "HOME_EMRG_NAME")
    private String homeEmergencyName;

    @Column(table = "APPL_FORN", name = "HOME_EMRG_TEL")
    private String homeEmergencyTel;

    @Column(table = "APPL_FORN", name = "HOME_EMRG_RELA")
    private String homeEmergencyRelation;

    @Column(table = "APPL_FORN", name = "OVERSEAS_KOREAN")
    private Boolean overseasKorean;

    @Column(table = "APPL_FORN", name = "SKYPE_ID")
    private String skype;

    @Column(table = "APPL_FORN", name = "RESIDENCY_TYPE")
    private String residencyType;

    @Column(table = "APPL_FORN", name = "VIDEO_ESSAY")
    private String videoEssay;

    @Column(table = "APPL_FORN", name = "FIRST_NAME")
    private String firstName;

    @Column(table = "APPL_FORN", name = "MIDDLE_NAME")
    private String middleName;

    @Column(table = "APPL_FORN", name = "LAST_NAME")
    private String lastName;

    public void encryptForeignerInfo(String passportNo, String visaNo, String foreignerRegistrationNo) {
        this.passportNo = passportNo;
        this.visaNo = visaNo;
        this.foreignerRegistrationNo = foreignerRegistrationNo;
    }
}
