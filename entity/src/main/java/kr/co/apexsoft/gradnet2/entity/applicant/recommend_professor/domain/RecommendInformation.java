package kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
@Entity
@Table(name="APPL_REC_PROF_INFO")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class RecommendInformation extends AbstractBaseEntity {
    @Id
    private Long id;

    @MapsId
    @OneToOne(fetch = FetchType.LAZY , optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name="APPL_REC_NO" , nullable = false, unique = true)
    private Recommend recommend;

    @Column(name = "PROF_NAME")
    private String name;


    @Column(name = "PROF_EMAIL")
    private String email;


    @Column(name = "PROF_INST")
    private String institute;

    @Column(name = "PROF_ADDR")
    private String address;


    @Column(name = "PROF_POSITION")
    private String position;


    @Column(name = "PROF_PHONE_NUM")
    private String phoneNum;

    @Column(name = "PROF_SIGN")
    private String sign;


    @Column(name = "REC_Day")
    private LocalDate recDay;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "APPL_REC_NO")
    private List<RecommendAnswer> answer;

    public void initId(Long id) {
        this.id = id;
    }

    public void initRecommend(Recommend recommend) {
        this.recommend = recommend;
    }
}
