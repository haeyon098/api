package kr.co.apexsoft.gradnet2.entity.comcode.domain;

import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-01-03
 */
@NoArgsConstructor
public class SchoolCommCodeIds implements Serializable {

    /*모집학교코드*/
    @Column(name = "RECR_SCHL_CODE",length =50 )
    private String schoolCode;

    /*입학연도*/
    @Column(name = "ENTR_YEAR",length =50 )
    private String enterYear;

    /*회자*/
    @Column(name = "RECR_PART_SEQ",length =50 )
    private Integer recruitPartSeq;

    /* 공통코드 그룹 */
    @Column(name = "CODE_GRP",length =50 )
    private String codeGrp;

    @Column(name = "CODE",length =50 )
    private String code;
}
