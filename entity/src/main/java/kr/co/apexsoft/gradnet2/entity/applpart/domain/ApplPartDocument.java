package kr.co.apexsoft.gradnet2.entity.applpart.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-20
 */
@Entity
@Table(name = "APPL_PART_DOC")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ApplPartDocument {
    @Id
    @Column(name = "APPL_PART_DOC_NO")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "RECR_SCHL_CODE", referencedColumnName = "RECR_SCHL_CODE"),
            @JoinColumn(name = "ENTR_YEAR", referencedColumnName = "ENTR_YEAR"),
            @JoinColumn(name = "RECR_PART_SEQ", referencedColumnName = "RECR_PART_SEQ"),
            @JoinColumn(name = "APPL_PART_NO", referencedColumnName = "APPL_PART_NO")
    })
    private ApplPart part;

    @Column(name = "DOC_GRP_CODE")
    private String grpCode;

    @Column(name = "DOC_ITEM_CODE")
    private String itemCode;

    @Formula("SCHL_CODEVAL('KR', RECR_SCHL_CODE,ENTR_YEAR,RECR_PART_SEQ,'DOC_GRP',  DOC_GRP_CODE)")
    private String grpKorName;

    @Formula("SCHL_CODEVAL('EN', RECR_SCHL_CODE,ENTR_YEAR,RECR_PART_SEQ,'DOC_GRP',  DOC_GRP_CODE)")
    private String grpEngName;

    public Boolean isCareerGroup() {
        return grpCode.equals(DocGrpType.CAREER.getValue());
    }

    public Boolean isUnivGroup() {
        return grpCode.equals(DocGrpType.UNDERGRADUATE.getValue());
    }

    public Boolean isGradGroup() {
        return grpCode.equals(DocGrpType.GRADUATE.getValue());
    }

    public Boolean isLangGroup() {
        return grpCode.equals(DocGrpType.LANG.getValue());
    }
}
