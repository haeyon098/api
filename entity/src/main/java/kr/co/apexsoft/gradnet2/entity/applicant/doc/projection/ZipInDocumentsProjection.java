package kr.co.apexsoft.gradnet2.entity.applicant.doc.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-04-23
 */
public interface ZipInDocumentsProjection {
    Long getApplNo();

    Long getApplDocNo();

    String getGroupCode();

    String getItemCode();

    String seq();

    String getFilePath();

    String getNewFilePath();
}
