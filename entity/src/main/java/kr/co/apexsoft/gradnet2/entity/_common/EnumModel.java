package kr.co.apexsoft.gradnet2.entity._common;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-11
 */
public interface EnumModel {
    String getValue();
    String getKorName();
    String getEngName();
}
