package kr.co.apexsoft.gradnet2.entity.applicant.appl.domain;

import lombok.Getter;
import org.hibernate.annotations.Formula;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-12
 */
@Getter
@Embeddable
public class Country {
    @Column(name = "CITZ_CNTR_CODE")
    private String id;

    @Column(name = "ETC_CITZ_CNTR_NAME")
    private String name;




}
