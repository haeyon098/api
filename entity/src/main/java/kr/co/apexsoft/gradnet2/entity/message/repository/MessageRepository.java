package kr.co.apexsoft.gradnet2.entity.message.repository;

import kr.co.apexsoft.gradnet2.entity.message.domain.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-05-04
 */
public interface MessageRepository extends JpaRepository<Message, String> {
}
