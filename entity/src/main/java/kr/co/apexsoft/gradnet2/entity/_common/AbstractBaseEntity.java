package kr.co.apexsoft.gradnet2.entity._common;

import lombok.Getter;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
@Access(AccessType.FIELD)
@Getter
public abstract class AbstractBaseEntity implements Serializable {

    @CreatedDate
    @Column(name = "CREATED_DATE", updatable = false)
    protected LocalDateTime createdDateTime;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE", updatable = true)
    protected LocalDateTime lastModifiedDateTime;

    @Column(name = "CREATED_BY", updatable = false)
    @CreatedBy
    protected String createdBy;

    @Column(name = "LAST_MODIFIED_BY", updatable = true)
    @LastModifiedBy
    protected String lastModifiedBy;
}
