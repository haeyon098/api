package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-12
 */
public interface ApplIdSeqProjection {
    String getApplId();

    Integer getNewSeq();

    String getRuleNo();
}
