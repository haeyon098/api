package kr.co.apexsoft.gradnet2.entity.adms.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.CorsId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * 모집과정
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name = "CORS")
@Getter
@IdClass(CorsId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Cors extends AbstractBaseEntity {
    @Id
    private String recruitSchoolCode;

    @Id
    private String id;

    @Column(name = "CORS_KOR_NAME")
    private String korName;

    @Column(name = "CORS_ENG_NAME")
    private String engName;
}
