package kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

/**
 *
 *
 * @author 박지환
 * @since 2020-03-06
 */
@Entity
@Table(name="APPL_ALUMNI")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class Alumni {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ALUMNI_NO")
    private Long id;

    @Column(name = "ALUMNI_TYPE")
    private String type;

    @Column(name = "ALUMNI_NAME")
    private String name;

    @Column(name = "ALUMNI_ADM_YEAR")
    private LocalDate admYear;

    @Column(name = "ALUMNI_MAJOR_TYPE_CODE")
    private String majorTypeCode;

    @Column(name = "ALUMNI_REMAKR")
    private String remark;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    public void initAppl(Appl appl) {
        this.appl = appl;
    }
}
