package kr.co.apexsoft.gradnet2.entity.message.domain;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-05-04
 */
@Entity
@Table(name = "MSG")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Message {
    @Id
    @Column(name = "MSG_NO")
    private String id;

    @Column(name = "MSG_KOR_VAL")
    private String korValue;

    @Column(name = "MSG_ENG_VAL")
    private String engValue;
}
