package kr.co.apexsoft.gradnet2.entity.applicant.doc.projection;

/**
 * Class Description
 *
 * @author 최은주
 * @since 2020-03-08
 */
public interface DocumentEtcNameProjection {
    String getKorItemName();

    String getEngItemName();

    String getItemCode();

    String getFilePath();

    String getOriginFileName();
}
