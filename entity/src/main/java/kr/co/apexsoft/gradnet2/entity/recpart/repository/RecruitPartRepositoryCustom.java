package kr.co.apexsoft.gradnet2.entity.recpart.repository;

import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;

import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-19
 */
public interface RecruitPartRepositoryCustom {

    List<RecruitPart> findAllBySchoolCodeAndEnterYear(String schoolCode, String enterYear);
}
