package kr.co.apexsoft.gradnet2.entity.adms.repository;

import com.querydsl.core.types.Projections;
import kr.co.apexsoft.gradnet2.entity.adms.domain.Cors;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.adms.domain.QCors.cors;
import static kr.co.apexsoft.gradnet2.entity.applpart.domain.QApplPart.applPart;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
public class CorsRepositoryImpl extends QuerydslRepositorySupport implements CorsRepositoryCustom {

    public CorsRepositoryImpl() {
        super(Cors.class);
    }

    @Override
    public List<AdmsProjection> findBySchoolAndEnterYearAndRecruitPartSeqAndAdmission(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode) {
        return from(cors)
                .innerJoin(applPart)
                .on(
                        cors.recruitSchoolCode.eq(applPart.schoolCode),
                        cors.id.eq(applPart.courseCode)
                )
                .where(
                        applPart.schoolCode.eq(schoolCode),
                        applPart.enterYear.eq(enterYear),
                        applPart.recruitPartSeq.eq(recruitPartSeq),
                        applPart.admissionCode.eq(admissionCode)
                )
                .distinct()
                .select(Projections.constructor(AdmsProjection.class, cors.id, cors.korName, cors.engName))
                .fetch();
    }
}
