package kr.co.apexsoft.gradnet2.entity.applicant.srvy.projection;

/*
 * 지원서 생성시 필요한 데이터
 */
public interface SrvyQstAnsProjection {
    Long getSrvyQstNo();

    String getAnswer();
}
