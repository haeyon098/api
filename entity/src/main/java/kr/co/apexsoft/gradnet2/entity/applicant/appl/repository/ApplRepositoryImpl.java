package kr.co.apexsoft.gradnet2.entity.applicant.appl.repository;

import com.querydsl.core.QueryResults;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateTemplate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.JPQLQuery;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.*;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QAdms.adms;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QCategory.category;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QCors.cors;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QMajor.major;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QRecruitSchool.recruitSchool;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QSemesterType.semesterType;
import static kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.QAppl.appl;
import static kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.QDocument.document;
import static kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.QRecommend.recommend;
import static kr.co.apexsoft.gradnet2.entity.applpart.domain.QApplPart.applPart;
import static kr.co.apexsoft.gradnet2.entity.recpart.domain.QRecruitPart.recruitPart;
import static kr.co.apexsoft.gradnet2.entity.standard.domain.QSchool.school;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-04
 */
public class ApplRepositoryImpl extends QuerydslRepositorySupport implements ApplRepositoryCustom {

    public ApplRepositoryImpl() {
        super(Appl.class);
    }

    @Override
    public long countBySchoolEnterYearRecruitPartSeqAndStatusIn(String schoolCode, String enterYear, Integer recruitPartSeq, Collection<Appl.Status> statuses) {

        return from(appl)
                .where(
                        appl.part.schoolCode.eq(schoolCode),
                        appl.part.enterYear.eq(enterYear),
                        appl.part.recruitPartSeq.eq(recruitPartSeq),
                        appl.status.in(statuses)
                )
                .fetchCount();
    }

    @Override
    public Map<LocalDate, Long> countByStatusAndDate(String schoolCode, String enterYear, Integer recruitPartSeq, Appl.Status status) {
        DateTemplate<LocalDateTime> formattedDate =
                Expressions.dateTemplate(LocalDateTime.class,
                        "DATE_FORMAT({0}, {1})", appl.applDate,
                        "%Y-%m-%d");

        return from(appl)
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .innerJoin(recruitPart)
                .on(
                        appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                        appl.part.enterYear.eq(recruitPart.enterYear),
                        appl.part.recruitPartSeq.eq(recruitPart.seq)
                )
                .where(
                        appl.part.schoolCode.eq(schoolCode)
                                .and(appl.part.enterYear.eq(enterYear))
                                .and(appl.part.recruitPartSeq.eq(recruitPartSeq))
                                .and(appl.status.eq(status)
                                        .and(appl.applDate.isNotNull()))
                                .and(appl.applDate.between(recruitPart.startDate, recruitPart.endDate))
                )
                .groupBy(formattedDate)
                .select(formattedDate, appl.count())
                .orderBy(formattedDate.asc())
                .fetch().stream()
                .map(t -> new DateCountProjection(t.get(0, String.class), t.get(1, Long.class)))
                .collect(groupingBy(dc -> dc.getDate().toLocalDate(), TreeMap::new, summingLong(DateCountProjection::getCount)));
    }

    @Override
    public Page<ApplicationSimpleForListProjection> findAllBySearchCondition(
            String schoolCode,
            String enterYear,
            Integer recruitPartSeq,
            String admissionCode,
            String courseCode,
            String categoryCode,
            String majorCode,
            String applId,
            Long applNo,
            String applicantName,
            String email,
            String phoneNumber,
            String applicantId,
            Appl.Status applStatus,
            Pageable pageable) {
        List<ApplicationSimpleForListProjection> contents = from(appl)
                .leftJoin(recommend)
                .on(
                        appl.id.eq(recommend.appl.id),
                        recommend.stsCode.eq(Recommend.status.COMPLETED.getCode())
                )
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .innerJoin(recruitPart)
                .on(
                        appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                        appl.part.enterYear.eq(recruitPart.enterYear),
                        appl.part.recruitPartSeq.eq(recruitPart.seq)
                )
                .innerJoin(semesterType)
                .on(
                        appl.part.semesterTypeCode.eq(semesterType.id),
                        appl.part.schoolCode.eq(semesterType.recruitSchoolCode)
                )
                .innerJoin(adms)
                .on(
                        appl.part.admissionCode.eq(adms.id),
                        appl.part.schoolCode.eq(adms.recruitSchoolCode)
                )
                .innerJoin(cors)
                .on(
                        appl.part.courseCode.eq(cors.id),
                        appl.part.schoolCode.eq(cors.recruitSchoolCode)
                )
                .innerJoin(category)
                .on(

                        appl.part.categoryCode.eq(category.id),
                        appl.part.schoolCode.eq(category.recruitSchoolCode)
                )
                .innerJoin(major)
                .on(
                        appl.part.schoolCode.eq(major.recruitSchoolCode),
                        appl.part.enterYear.eq(major.enterYear),
                        appl.part.recruitPartSeq.eq(major.recruitPartSeq),
                        appl.part.majorCode.eq(major.id)
                )
                .where(
                        eqSchoolCode(schoolCode),
                        eqEnterYear(enterYear),
                        eqRecruitPartSeq(recruitPartSeq),
                        eqAdmissionCode(admissionCode),
                        eqCourseCode(courseCode),
                        eqCategoryCode(categoryCode),
                        eqMajorCode(majorCode),
                        containsApplId(applId),
                        containsApplNo(applNo),
                        containsApplicantName(applicantName),
                        containsEmail(email),
                        containsPhoneNumber(phoneNumber),
                        containsApplicantId(applicantId),
                        eqApplStatus(applStatus)
                )
                .groupBy(recommend.appl, appl.id)
                .orderBy(appl.applId.asc())
                .orderBy(appl.id.asc())
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .select(Projections.constructor(ApplicationSimpleForListProjection.class,
                        appl.applicant.userId,
                        appl.id,
                        appl.applId,
                        recruitPart.seq, recruitPart.korName, recruitPart.engName,
                        adms.id, adms.korName, adms.engName,
                        cors.id, cors.korName, cors.engName,
                        category.id, category.korName, category.engName,
                        major.id, major.korName, major.engName,
                        appl.applicantInfo.korName, appl.applicantInfo.engName, appl.applicantInfo.phoneNum, appl.applicantInfo.email, appl.applicantInfo.registrationBornDate,
                        appl.zipPrinted,
                        appl.status,
                        recommend.id.count(),
                        ExpressionUtils.as(
                                JPAExpressions.select(recommend.id.count())
                                        .from(recommend)
                                        .where(recommend.appl.id.eq(appl.id)), "recReqCount")

                        )
                )
                .fetch();

        JPQLQuery<Appl> countQuery = from(appl)
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .where(
                        eqSchoolCode(schoolCode),
                        eqEnterYear(enterYear),
                        eqRecruitPartSeq(recruitPartSeq),
                        eqAdmissionCode(admissionCode),
                        eqCourseCode(courseCode),
                        eqCategoryCode(categoryCode),
                        eqMajorCode(majorCode),
                        containsApplId(applId),
                        containsApplNo(applNo),
                        containsApplicantName(applicantName),
                        containsEmail(email),
                        containsPhoneNumber(phoneNumber),
                        containsApplicantId(applicantId),
                        eqApplStatus(applStatus)
                )
                .select(appl);
        return PageableExecutionUtils.getPage(contents, pageable, countQuery::fetchCount);
    }

    @Override
    public Optional<Appl> findByApplNo(Long id) {
        return Optional.ofNullable(
                from(appl)
                        .innerJoin(appl.part)
                        .innerJoin(recruitPart)
                        .on(
                                appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                                appl.part.enterYear.eq(recruitPart.enterYear),
                                appl.part.recruitPartSeq.eq(recruitPart.seq)
                        )
                        .where(
                                appl.part.schoolCode.eq("SCL00001")
                                        .and(appl.part.enterYear.eq("2020"))
                                        .and(appl.part.recruitPartSeq.eq(1))
                                        .and(appl.id.eq(id))
                        )
                        .fetchOne()
        );
    }

    @Override
    public List<CorsCount> findCompletedApplCountsByCors(String schoolCode, String enterYear, Integer recruitPartSeq) {
        return from(appl)
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .innerJoin(cors)
                .on(
                        appl.part.schoolCode.eq(cors.recruitSchoolCode)
                )
                .where(
                        appl.part.schoolCode.eq(schoolCode),
                        appl.part.enterYear.eq(enterYear),
                        appl.part.recruitPartSeq.eq(recruitPartSeq),
                        appl.status.eq(Appl.Status.COMPLETE),
                        appl.part.courseCode.eq(cors.id)
                )
                .groupBy(cors.korName)
                .select(Projections.constructor(CorsCount.class, cors.korName, appl.id.count()))
                .fetch();
    }

    @Override
    public Page<ApplicationSerachProjection> findApplAllListBySearchProjection(
            Long applNo,
            String schoolCode,
            String enterYear,
            Integer reqPartSeq,
            String email,
            String phoneNumber,
            String userId,
            String korName, String engName,
            Appl.Status status, Pageable pageable) {
        QueryResults<ApplicationSerachProjection> result =
                from(appl)
                        .innerJoin(applPart)
                        .on(
                                appl.part.schoolCode.eq(applPart.schoolCode),
                                appl.part.enterYear.eq(applPart.enterYear),
                                appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                                appl.part.applPartNo.eq(applPart.applPartNo)
                        )
                        .innerJoin(recruitPart)
                        .on(
                                appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                                appl.part.enterYear.eq(recruitPart.enterYear),
                                appl.part.recruitPartSeq.eq(recruitPart.seq)
                        )
                        .innerJoin(recruitSchool)
                        .on(appl.part.schoolCode.eq(recruitSchool.id))
                        .innerJoin(cors)
                        .on(
                                appl.part.courseCode.eq(cors.id),
                                appl.part.schoolCode.eq(cors.recruitSchoolCode)
                        )
                        .leftJoin(major)
                        .on(

                                appl.part.schoolCode.eq(major.recruitSchoolCode),
                                appl.part.enterYear.eq(major.enterYear),
                                appl.part.recruitPartSeq.eq(major.recruitPartSeq),
                                appl.part.majorCode.eq(major.id)
                        )
                        .where(
                                eqApplNo(applNo),
                                eqSchoolCode(schoolCode),
                                eqEnterYear(enterYear),
                                eqRecruitPartSeq(reqPartSeq),
                                containsEmail(email),
                                containsPhoneNumber(phoneNumber),
                                eqApplicantId(userId),
                                containsApplicantName(korName),
                                containsApplicantEngName(engName),
                                eqApplStatus(status)
                        )
                        .limit(pageable.getPageSize()).offset(pageable.getOffset())
                        .select(Projections.constructor(ApplicationSerachProjection.class,
                                appl.id,
                                recruitSchool.korName,
                                recruitPart.korName,
                                cors.korName,
                                major.korName,
                                appl.applicantInfo.korName,
                                appl.applicantInfo.engName,
                                appl.applicant.userId,
                                appl.applicantInfo.phoneNum,
                                appl.applicantInfo.email,
                                appl.status, appl.applId))
                        .fetchResults();
        return new PageImpl<>(result.getResults(), pageable, result.getTotal());

    }

    @Override
    public List<ApplStatsProjection> findApplStatsByAdmission(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode) {
        return from(appl)
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .innerJoin(recruitPart)
                .on(
                        appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                        appl.part.enterYear.eq(recruitPart.enterYear),
                        appl.part.recruitPartSeq.eq(recruitPart.seq)
                )
                .innerJoin(adms)
                .on(
                        appl.part.admissionCode.eq(adms.id),
                        appl.part.schoolCode.eq(adms.recruitSchoolCode)
                )
                .innerJoin(cors)
                .on(
                        appl.part.courseCode.eq(cors.id),
                        appl.part.schoolCode.eq(cors.recruitSchoolCode)
                )
                .innerJoin(major)
                .on(
                        appl.part.majorCode.eq(major.id),
                        appl.part.schoolCode.eq(major.recruitSchoolCode),
                        appl.part.enterYear.eq(major.enterYear),
                        appl.part.recruitPartSeq.eq(major.recruitPartSeq)
                )
                .innerJoin(category)
                .on(
                        appl.part.categoryCode.eq(category.id),
                        appl.part.schoolCode.eq(category.recruitSchoolCode)
                )
                .where(
                        eqSchoolCode(schoolCode),
                        eqEnterYear(enterYear),
                        eqRecruitPartSeq(recruitPartSeq),
                        eqAdmissionCode(admissionCode),
                        eqApplStatus(Appl.Status.COMPLETE)
                )
                .groupBy(recruitPart.korName, adms.korName, cors.korName, major.korName, category.korName)
                .select(Projections.constructor(ApplStatsProjection.class,
                        recruitPart.korName, adms.korName, cors.korName, major.korName, category.korName,
                        category.korName.count()
                ))
                .fetch();
    }

    @Override
    public Optional<ApplDetailProjection> findApplDetailById(Long applNo) {
        return Optional.ofNullable(
                from(appl)
                        .innerJoin(applPart)
                        .on(
                                appl.part.schoolCode.eq(applPart.schoolCode),
                                appl.part.enterYear.eq(applPart.enterYear),
                                appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                                appl.part.applPartNo.eq(applPart.applPartNo)
                        )
                        .innerJoin(recruitPart)
                        .on(
                                appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                                appl.part.enterYear.eq(recruitPart.enterYear),
                                appl.part.recruitPartSeq.eq(recruitPart.seq)
                        )
                        .innerJoin(semesterType)
                        .on(
                                appl.part.semesterTypeCode.eq(semesterType.id),
                                appl.part.schoolCode.eq(semesterType.recruitSchoolCode)
                        )
                        .innerJoin(adms)
                        .on(
                                appl.part.admissionCode.eq(adms.id),
                                appl.part.schoolCode.eq(adms.recruitSchoolCode)
                        )
                        .innerJoin(cors)
                        .on(
                                appl.part.courseCode.eq(cors.id),
                                appl.part.schoolCode.eq(cors.recruitSchoolCode)
                        )
                        .innerJoin(category)
                        .on(

                                appl.part.categoryCode.eq(category.id),
                                appl.part.schoolCode.eq(category.recruitSchoolCode)
                        )
                        .innerJoin(major)
                        .on(
                                appl.part.schoolCode.eq(major.recruitSchoolCode),
                                appl.part.enterYear.eq(major.enterYear),
                                appl.part.recruitPartSeq.eq(major.recruitPartSeq),
                                appl.part.majorCode.eq(major.id)
                        )
                        .where(
                                appl.id.eq(applNo)
                        )
                        .select(Projections.constructor(ApplDetailProjection.class,
                                appl.id, appl.applicant.userId, appl.status,

                                appl.applId, appl.applDate,
                                recruitPart.seq, recruitPart.korName, recruitPart.engName,
                                adms.id, adms.korName, adms.engName,
                                cors.id, cors.korName, cors.engName,
                                category.id, category.korName, category.engName,
                                major.id, major.korName, major.engName,

                                appl.applicantInfo.korName, appl.applicantInfo.engName,
                                appl.applicantInfo.registrationBornDate, appl.applicantInfo.gend,
                                appl.applicantInfo.telNum, appl.applicantInfo.phoneNum,
                                appl.applicantInfo.address, appl.applicantInfo.email,
                                appl.generalInfo.emergencyName, appl.generalInfo.emergencyTelNum
                        ))
                        .fetchOne()
        );
    }

    @Override
    public List<ApplDocProjection> findApplDocsById(Long applNo) {
        return from(appl)
                .innerJoin(document)
                .on(
                        appl.id.eq(document.appl.id)
                )
                .where(
                        appl.id.eq(applNo)
                )
                .select(Projections.constructor(ApplDocProjection.class,
                        document.itemName, document.originFileName, document.filePath
                ))
                .fetch();
    }

    @Override
    public Optional<ApplNoZipPrintedProjection> findApplNoZipPrintedById(Long applNo) {
        return Optional.ofNullable(
                from(appl)
                        .where(
                                appl.id.eq(applNo)
                        )
                        .select(Projections.constructor(ApplNoZipPrintedProjection.class,
                                appl.id, appl.zipPrinted
                        ))
                        .fetchOne()
        );
    }

    @Override
    public void updateApplNoZipPrintedById(Long applNo, Boolean zipPrinted) {
        update(appl)
                .where(
                        appl.id.eq(applNo)
                )
                .set(
                        appl.zipPrinted, zipPrinted
                )
                .execute();
    }


    @Override
    public List<Appl> findApplicantsDetail(String schoolCode,
                                           String enterYear,
                                           Integer recruitPartSeq,
                                           String admissionCode,
                                           String courseCode,
                                           String categoryCode,
                                           String majorCode,
                                           String applId) {
        return
                from(appl)
                        .where(
                                eqSchoolCode(schoolCode),
                                eqEnterYear(enterYear),
                                eqRecruitPartSeq(recruitPartSeq),
                                eqAdmissionCode(admissionCode),
                                eqCourseCode(courseCode),
                                eqCategoryCode(categoryCode),
                                eqMajorCode(majorCode),
                                eqApplId(applId),
                                eqApplStatus(Appl.Status.COMPLETE)
                        ).orderBy(appl.applId.asc())
                        .fetch();


    }

    // 아래는 동적 쿼리용 helper 메서드
    private BooleanExpression eqSchoolCode(String schoolCode) {
        return StringUtils.isEmpty(schoolCode) ? null : appl.part.schoolCode.eq(schoolCode);
    }

    private BooleanExpression eqEnterYear(String enterYear) {
        return StringUtils.isEmpty(enterYear) ? null : appl.part.enterYear.eq(enterYear);
    }

    private BooleanExpression eqRecruitPartSeq(Integer recruitPartSeq) {
        return recruitPartSeq == null ? null : appl.part.recruitPartSeq.eq(recruitPartSeq);
    }

    private BooleanExpression eqSemesterTypeCode(String semesterTypeCode) {
        return StringUtils.isEmpty(semesterTypeCode) ? null : appl.part.semesterTypeCode.eq(semesterTypeCode);
    }

    private BooleanExpression eqAdmissionCode(String admissionCode) {
        return StringUtils.isEmpty(admissionCode) ? null : appl.part.admissionCode.eq(admissionCode);
    }

    private BooleanExpression eqCourseCode(String courseCode) {
        return StringUtils.isEmpty(courseCode) ? null : appl.part.courseCode.eq(courseCode);
    }

    private BooleanExpression eqCategoryCode(String categoryCode) {
        return StringUtils.isEmpty(categoryCode) ? null : appl.part.categoryCode.eq(categoryCode);
    }

    private BooleanExpression eqMajorCode(String majorCode) {
        return StringUtils.isEmpty(majorCode) ? null : appl.part.majorCode.eq(majorCode);
    }

    private BooleanExpression eqApplId(String applId) {
        return applId == null ? null : appl.applId.eq(applId);
    }

    private BooleanExpression containsApplId(String applId) {
        return applId == null ? null : appl.applId.contains(applId);
    }

    private BooleanExpression eqApplNo(Long applNo) {
        return applNo == null ? null : appl.id.eq(applNo);
    }

    private BooleanExpression containsApplNo(Long applNo) {
        return applNo == null ? null : appl.id.like("%" + applNo + "%");
    }

    private BooleanExpression eqApplicantName(String applicantName) {
        return StringUtils.isEmpty(applicantName) ? null : appl.applicantInfo.korName.eq(applicantName);
    }

    private BooleanExpression containsApplicantName(String applicantName) {
        return StringUtils.isEmpty(applicantName) ? null :
                (appl.applicantInfo.korName.contains(applicantName).or(appl.applicantInfo.engName.containsIgnoreCase(applicantName)));
    }

    private BooleanExpression eqApplicantEngName(String applicantName) {
        return StringUtils.isEmpty(applicantName) ? null : appl.applicantInfo.engName.eq(applicantName);
    }

    private BooleanExpression containsApplicantEngName(String applicantName) {
        return StringUtils.isEmpty(applicantName) ? null : appl.applicantInfo.engName.containsIgnoreCase(applicantName);
    }

    private BooleanExpression eqEmail(String email) {
        return StringUtils.isEmpty(email) ? null : appl.applicantInfo.email.eq(email);
    }

    private BooleanExpression containsEmail(String email) {
        return StringUtils.isEmpty(email) ? null : appl.applicantInfo.email.containsIgnoreCase(email);
    }

    private BooleanExpression eqPhoneNumber(String phoneNumber) {
        return StringUtils.isEmpty(phoneNumber) ? null : appl.applicantInfo.phoneNum.eq(phoneNumber);
    }

    private BooleanExpression containsPhoneNumber(String phoneNumber) {
        return StringUtils.isEmpty(phoneNumber) ? null : appl.applicantInfo.phoneNum.contains(phoneNumber);
    }

    private BooleanExpression eqApplicantId(String applicantId) {
        return StringUtils.isEmpty(applicantId) ? null : appl.applicant.userId.eq(applicantId);
    }

    private BooleanExpression containsApplicantId(String applicantId) {
        return StringUtils.isEmpty(applicantId) ? null : appl.applicant.userId.contains(applicantId);
    }

    private BooleanExpression eqApplStatus(Appl.Status applStatus) {
        return applStatus == null ? null : appl.status.eq(applStatus);
    }
}
