package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-06
 */
public interface MyApplResponse {
    Long getId();

    String getStatus();

    String getSchoolCode();

    String getSchoolKorName();

    String getSchoolEngName();

    String getEnterYear();

    Integer getRecruitPartSeq();

    String getStatusKorName();

    String getStatusEngName();

    String getMajorKorName();

    String getMajorEngName();

    String getAdmissionKorName();

    String getAdmissionEngName();

    String getCourseKorName();

    String getCourseEngName();
}
