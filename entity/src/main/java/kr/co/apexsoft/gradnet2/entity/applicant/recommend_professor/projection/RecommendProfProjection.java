package kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.projection;


/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-25
 */
public interface RecommendProfProjection {

    Long getApplNo();

    String getEngName();

    String getCourseEngName();

    String getMajorEngName();

    String getProfName();

    String getProfInst();

    String getProfInstLoca();

    String getProfPosition();

    String getProfMail();

    String getProfPhone();

    String getProfCreDate();

    String getProfSign();
}
