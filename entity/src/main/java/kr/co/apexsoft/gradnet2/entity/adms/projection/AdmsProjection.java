package kr.co.apexsoft.gradnet2.entity.adms.projection;

import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-21
 */
@Getter
public class AdmsProjection {

    private String code;
    private String korName;
    private String engName;

    public AdmsProjection(String code, String korName, String engName) {
        this.code = code;
        this.korName = korName;
        this.engName = engName;
    }
}
