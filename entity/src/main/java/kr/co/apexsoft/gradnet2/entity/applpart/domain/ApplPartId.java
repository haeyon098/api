package kr.co.apexsoft.gradnet2.entity.applpart.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-30
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Getter
public class ApplPartId implements Serializable {
    @Id
    @Column(name = "RECR_SCHL_CODE")
    private String schoolCode;

    @Id
    @Column(name = "ENTR_YEAR")
    private String enterYear;

    @Id
    @Column(name = "RECR_PART_SEQ")
    private Integer recruitPartSeq;

    @Id
    @Column(name = "APPL_PART_NO")
    private Long applPartNo;
}
