package kr.co.apexsoft.gradnet2.entity.standard.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import lombok.*;

import javax.persistence.*;

/**
 * 학교 기준정보
 *
 * @author 김효숙
 * @since 2020-01-02
 */
@Entity
@Table(name = "SCHL")

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
public class School extends AbstractBaseEntity {
    @Id
    @Column(name = "SCHL_CODE")
    private String id;

    @Column(name = "CNTR_CODE")
    @NonNull
    private String countryCode;

    @Column(name = "SCHL_KOR_NAME")
    @NonNull
    private String korName;

    @Column(name = "SCHL_ENG_NAME")
    @NonNull
    private String engName;



    @Enumerated(EnumType.STRING)
    @Column(name = "SCHL_TYPE")
    private Type type = Type.U;

    public enum Type {
       U
    }
    public boolean isChineseSchool() {
        return "141".equals(getCountryCode());
    }


}

