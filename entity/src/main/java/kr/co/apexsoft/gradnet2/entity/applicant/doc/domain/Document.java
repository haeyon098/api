package kr.co.apexsoft.gradnet2.entity.applicant.doc.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;

@Entity
@Table(name="APPL_DOC")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Document extends AbstractBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "APPL_DOC_NO")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "APPL_NO")
    private Appl appl;

    @Column(name = "DOC_GRP_CODE")
    private String groupCode;

    @Column(name = "DOC_GRP_NO")
    private Long groupNo;

    @Column(name = "DOC_ITEM_CODE")
    private String itemCode;

    @Column(name = "DOC_ITEM_NAME")
    private String itemName;

    @Column(name = "PDF_PAGE_CNT")
    private Integer pdfPageCount;

    @Column(name = "ORG_FILE_NAME")
    private String originFileName;

    @Column(name = "FILE_PATH")
    private String filePath;

    @Column(name = "IMG_YN")
    private Boolean isImage;

    @Column(name = "FILE_EXT")
    private String extension;

    @Column(name = "PDF_YN")
    private Boolean isPdf;

    @Column(name = "ETC_SEQ")
    private Integer seq;

    public Document(Appl appl, String groupCode, Long groupNo, String itemCode, Integer pdfPageCount,
                    String filePath, String originFileName, Boolean isPdf) {
        this.appl = appl;
        this.groupCode = groupCode;
        this.groupNo = groupNo;
        this.itemCode = itemCode;
        this.pdfPageCount = pdfPageCount;
        this.filePath = filePath;
        this.originFileName = originFileName;
        this.isPdf = isPdf;
    }

    public void changeFileName(String fileName) {
        this.originFileName = fileName;
    }

    public void changeDocumentInfo(Appl appl, String filePath, Integer pdfPageCount, Boolean isPdf) {
        this.appl = appl;
        this.filePath = filePath;
        this.pdfPageCount = pdfPageCount;
        this.isPdf = isPdf;

    }
}
