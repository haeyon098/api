package kr.co.apexsoft.gradnet2.entity.standard.repository;

import kr.co.apexsoft.gradnet2.entity.standard.domain.School;
import kr.co.apexsoft.gradnet2.entity.standard.projection.SearchDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface SchoolRepository extends JpaRepository<School, String> {

    @Transactional
    @Query(value = "select new kr.co.apexsoft.gradnet2.entity.standard.projection.SearchDto(s.id,s.korName,s.engName) from School s order by s.id")
    List<SearchDto> findAllCustomList();
}
