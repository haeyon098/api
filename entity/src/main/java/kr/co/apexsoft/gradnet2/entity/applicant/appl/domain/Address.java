package kr.co.apexsoft.gradnet2.entity.applicant.appl.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-02
 */
@Getter
@Embeddable
public class Address {
    @Column(name = "POST_NO")
    private String postNo;

    @Column(name = "ADDR")
    private String address;

    @Column(name = "DETL_ADDR")
    private String detailAddress;

    public String getFullAddress() {
        return postNo + " " + address + " " + detailAddress;
    }
}
