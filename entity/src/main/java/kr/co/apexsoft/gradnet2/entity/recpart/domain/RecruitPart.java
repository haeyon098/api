package kr.co.apexsoft.gradnet2.entity.recpart.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name="RECR_PART")
@IdClass(RecruitPartId.class)
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RecruitPart extends AbstractBaseEntity {
    @Id
    private String recruitSchoolCode;

    @Id
    private String enterYear;

    @Id
    private Integer seq;

    @OneToMany(mappedBy = "recruitPart", orphanRemoval= true, fetch = FetchType.EAGER)
    private Set<RecruitPartTime> time;

    @Column(name = "RECR_PART_KOR_NAME")
    private String korName;

    @Column(name = "RECR_PART_ENG_NAME")
    private String engName;

    @Column(name = "SITE_URL")
    private String siteUrl;

    @Column(name = "SITE_URL_EN")
    private String siteUrlEn;

    @Column(name = "START_DATE")
    private LocalDateTime startDate;

    @Column(name = "END_DATE")
    private LocalDateTime endDate;

    public RecruitPartTime getSubmit() {
        return time.stream().filter(i -> i.getType().equals(RecruitPartTime.TimeType.SUBMIT)).findFirst().get();
    }

    public RecruitPartTime getCmplete() {
        return  time.stream().filter(i -> i.getType().equals(RecruitPartTime.TimeType.COMPLETE)).findFirst().get();
    }
}
