package kr.co.apexsoft.gradnet2.entity.comcode.domain;

import lombok.*;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-01-03
 */
@Entity
@Table(name = "SYS_COMM_CODE")
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@IdClass(SysCommCodeIds.class)
public class SysCommCode {

    /* 공통코드 그룹 */
    @Id
    @Column(name = "CODE_GRP")
    private String codeGrp;

    /* 코드값 */
    @Id
    @Column(name = "CODE")
    private String code;


    /*코드 한글명*/
    @Column(name = "CODE_KOR_VAL")
    private String codeKrValue;

    /*코드 영문명*/
    @Column(name = "CODE_ENG_VAL")
    @NonNull
    private String codeEnValue;

    /* 사용여부 */
    @Column(name = "USE_YN")
    @ColumnDefault(value = "true")
    private Boolean used;

}
