package kr.co.apexsoft.gradnet2.entity.applpart.repository;

import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartDocument;
import kr.co.apexsoft.gradnet2.entity.applpart.projection.DocumentsResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-20
 */
public interface ApplPartDocumentRepository extends JpaRepository<ApplPartDocument, Long> {
    @Query(value = "select a from ApplPartDocument a " +
            "where a.part =:part group by a.grpCode order by a.grpCode")
    List<ApplPartDocument> findDcoumentGrpGroupBy(@Param("part") ApplPart part);

    @Query(value = "SELECT A.DOC_GRP_CODE AS groupCode, " +
            "SCHL_CODEVAL('KR', :schoolCode, :enterYear, :recruitPartSeq, 'DOC_ITEM', A.DOC_ITEM_CODE) AS korName, " +
            "SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'DOC_ITEM', A.DOC_ITEM_CODE) AS engName, " +
            "A.DOC_ITEM_CODE AS code, A.MDT_YN AS required, A.MSG_NO AS msgNo, C.MSG_KOR_VAL AS korMsg, C.MSG_ENG_VAL AS engMsg, " +
            "B.APPL_DOC_NO AS applDocNo, B.DOC_GRP_NO AS docGrpNo, " +
            "B.FILE_PATH AS filePath, B.ORG_FILE_NAME AS fileName " +
            "FROM APPL_PART_DOC A LEFT JOIN APPL_DOC B " +
            "ON A.DOC_GRP_CODE = B.DOC_GRP_CODE AND A.DOC_ITEM_CODE = B.DOC_ITEM_CODE AND B.APPL_NO =:applNo " +
            "AND B.DOC_GRP_NO = :docGrpNo " +
            "LEFT JOIN MSG C ON A.MSG_NO = C.MSG_NO " +
            "WHERE A.RECR_SCHL_CODE = :schoolCode AND A.ENTR_YEAR = :enterYear AND A.RECR_PART_SEQ = :recruitPartSeq " +
            "AND A.APPL_PART_NO =:partNo " +
            "AND A.DOC_GRP_CODE =:docGrp AND A.UPLOAD_YN = 'Y' " +
            "ORDER BY A.DOC_GRP_CODE, A.DOC_ITEM_CODE" +
            "", nativeQuery = true)
    List<DocumentsResponse> findDocumentList(@Param("schoolCode") String schoolCode,
                                             @Param("enterYear") String enterYear,
                                             @Param("recruitPartSeq") Integer recruitPartSeq,
                                             @Param("partNo") Long partNo,
                                             @Param("docGrp") String docGrp,
                                             @Param("applNo") Long applNo,
                                             @Param("docGrpNo") Long docGrpNo
    );

    @Query(value = "SELECT A.DOC_GRP_CODE AS groupCode, " +
            "SCHL_CODEVAL('KR', :schoolCode, :enterYear, :recruitPartSeq, 'DOC_ITEM', A.DOC_ITEM_CODE) AS korName, " +
            "SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'DOC_ITEM', A.DOC_ITEM_CODE) AS engName, " +
            "A.DOC_ITEM_CODE AS code, A.MDT_YN AS required, A.MSG_NO AS msgNo, C.MSG_KOR_VAL AS korMsg, C.MSG_ENG_VAL AS engMsg, " +
            "B.APPL_DOC_NO AS applDocNo, B.DOC_GRP_NO AS docGrpNo, B.ORG_FILE_NAME AS fileName, B.FILE_PATH AS filePath " +
            "FROM APPL_PART_DOC A LEFT JOIN APPL_DOC B " +
            "ON A.DOC_GRP_CODE = B.DOC_GRP_CODE AND A.DOC_ITEM_CODE = B.DOC_ITEM_CODE AND B.APPL_NO =:applNo " +
            "LEFT JOIN MSG C ON A.MSG_NO = C.MSG_NO " +
            "WHERE A.RECR_SCHL_CODE = :schoolCode AND A.ENTR_YEAR = :enterYear AND A.RECR_PART_SEQ = :recruitPartSeq " +
            "AND A.APPL_PART_NO =:partNo " +
            "AND A.DOC_GRP_CODE =:docGrp AND A.UPLOAD_YN = 'Y' " +
            "ORDER BY A.DOC_GRP_CODE, A.DOC_ITEM_CODE" +
            "", nativeQuery = true)
    List<DocumentsResponse> findDocumentListAsNotGrp(@Param("schoolCode") String schoolCode,
                                             @Param("enterYear") String enterYear,
                                             @Param("recruitPartSeq") Integer recruitPartSeq,
                                             @Param("partNo") Long partNo,
                                             @Param("docGrp") String docGrp,
                                             @Param("applNo") Long applNo
    );
}
