package kr.co.apexsoft.gradnet2.entity.adms.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.SemesterTypeId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * 학기구분
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name = "SMST_TYPE")
@Getter
@IdClass(SemesterTypeId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SemesterType extends AbstractBaseEntity {
    @Id
    private String recruitSchoolCode;

    @Id
    private String id;

    @Column(name = "SMST_TYPE_KOR_NAME")
    private String korName;

    @Column(name = "SMST_TYPE_ENG_NAME")
    private String engName;
}
