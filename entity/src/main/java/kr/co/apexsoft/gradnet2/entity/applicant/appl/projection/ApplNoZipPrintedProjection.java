package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-25
 */
@Getter
@AllArgsConstructor
public class ApplNoZipPrintedProjection {

    private Long applNo;
    private Boolean zipPrinted;
}
