package kr.co.apexsoft.gradnet2.entity._common;

import lombok.Getter;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-11
 */
@Getter
public class EnumValue {
    private String value;
    private String korName;
    private String engName;

    public EnumValue(EnumModel enumModel) {
        this.value = enumModel.getValue();
        this.korName = enumModel.getKorName();
        this.engName = enumModel.getEngName();
    }
}
