package kr.co.apexsoft.gradnet2.entity.adms.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Arrays;

/**
 * Class Description
 * <p>
 * 모집학교
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name = "RECR_SCHL")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class RecruitSchool extends AbstractBaseEntity {
    @Id
    @Column(name = "RECR_SCHL_CODE")
    private String id;

    @Column(name = "RECR_SCHL_KOR_NAME")
    private String korName;

    @Column(name = "RECR_SCHL_ENG_NAME")
    private String engName;

    @Column(name = "NOTICE")
    private String notice;

    @Column(name = "USE_YN")
    private Boolean used;

    @Getter
    public enum SchoolCodeType {
        KDI("SCL00001", new Appl.Status[] {
                Appl.Status.APPL_PART_SAVE, Appl.Status.BASIS_SAVE, Appl.Status.ACAD_SAVE, Appl.Status.LANG_SAVE,
                Appl.Status.ESSAY_SAVE, Appl.Status.FILE_SAVE, Appl.Status.SUBMIT, Appl.Status.COMPLETE, Appl.Status.CANCEL
        }),
        YONSEI("YS001", new Appl.Status[] {Appl.Status.APPL_PART_SAVE, Appl.Status.BASIS_SAVE}),
        SKKGSB("SKK_GSB", new Appl.Status[] {
                Appl.Status.APPL_PART_SAVE, Appl.Status.BASIS_SAVE, Appl.Status.ACAD_SAVE, Appl.Status.LANG_SAVE,
                Appl.Status.FILE_SAVE, Appl.Status.SUBMIT, Appl.Status.VBANK_ISSUE, Appl.Status.COMPLETE, Appl.Status.CANCEL
        });

        private String value;
        private Appl.Status[] steps;

        SchoolCodeType(String value, Appl.Status[] steps) {
            this.value = value;
            this.steps = steps;
        }

        public String getValue() {
            return this.value;
        }
        public static String getLowerCaseName(String code) {
            for (RecruitSchool.SchoolCodeType e : RecruitSchool.SchoolCodeType.values()) {
                if (e.getValue().equals(code)) {
                    return e.name().toLowerCase();
                }
            }
            return "";
        }

        public static String getUpperCaseName(String code) {
            for (RecruitSchool.SchoolCodeType e : RecruitSchool.SchoolCodeType.values()) {
                if (e.getValue().equals(code)) {
                    return e.name().toUpperCase();
                }
            }
            return "";
        }

        public static Appl.Status[] getSteps(String schoolCode) {
            return Arrays.stream(SchoolCodeType.values())
                    .filter(code -> code.hasCode(schoolCode))
                    .findFirst().get().getSteps();
        }

        public boolean hasCode(String schoolCode) {
            return this.value.equals(schoolCode);
        }

    }

}
