package kr.co.apexsoft.gradnet2.entity.applpart.repository;

import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartId;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface ApplPartRepository extends JpaRepository<ApplPart, ApplPartId> {
    ApplPart findBySchoolCodeAndEnterYearAndSemesterTypeCodeAndApplPartNo(String schoolCode, String enterYear, String semesterTypeCode, Long applPartNo);
}
