package kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.repository;


import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain.RecommendInformation;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.projection.RecommendProfProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
public interface RecommendProfessorRepository extends JpaRepository<RecommendInformation, Long> {

    @Query(value =
        "SELECT A.APPL_NO AS applNo, A.ENG_NAME AS engName, G.MAJ_ENG_NAME AS majorEngName , E.CORS_ENG_NAME AS courseEngName, " +
        "RI.PROF_NAME AS profName, RI.PROF_INST AS profInst, RI.PROF_ADDR AS profInstLoca, " +
        "RI.PROF_POSITION AS profPosition, RI.PROF_EMAIL AS profMail, RI.PROF_PHONE_NUM AS profPhone, DATE_FORMAT(RI.REC_Day,'%Y-%m-%d') AS profCreDate, RI.PROF_SIGN AS profSign " +
        "FROM APPL A, APPL_PART B , CORS E, MAJ G, APPL_REC RE, APPL_REC_PROF_INFO RI " +
        "WHERE 1 = 1 " +
        "AND RE.APPL_REC_NO = RI.APPL_REC_NO " +
        "AND A.RECR_PART_SEQ = B.RECR_PART_SEQ " +
        "AND A.APPL_PART_NO = B.APPL_PART_NO " +
        "AND A.ENTR_YEAR = B.ENTR_YEAR " +
        "AND A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
        "AND G.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
        "AND G.ENTR_YEAR = A.ENTR_YEAR " +
        "AND G.RECR_PART_SEQ = A.RECR_PART_SEQ " +
        "AND G.MAJ_CODE = B.MAJ_CODE " +
        "AND E.CORS_CODE = B.CORS_CODE " +
        "AND B.RECR_SCHL_CODE = E.RECR_SCHL_CODE " +
        "AND A.APPL_NO = RE.APPL_NO " +
        "AND RI.APPL_REC_NO = :recNo", nativeQuery = true)
    RecommendProfProjection findRecProfInfo(@Param("recNo") Long recNo);

}
