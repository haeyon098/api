package kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain;

import kr.co.apexsoft.gradnet2.entity._common.IdentifiedValueObject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
@Entity
@Table(name="APPL_REC_PROF_ANS")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class RecommendAnswer extends IdentifiedValueObject {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "APPL_REC_NO", insertable = false, updatable = false)
    private RecommendInformation information;

    @Column(name = "QST_NO")
    private String questionNo;

    @Column(name = "ANSWER")
    private String answer;
}
