package kr.co.apexsoft.gradnet2.entity.adms.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import kr.co.apexsoft.gradnet2.entity.adms.domain.id.AdmsId;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Class Description
 *
 * 모집전형
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@Entity
@Table(name = "ADMS")
@Getter
@IdClass(AdmsId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Adms extends AbstractBaseEntity {
    @Id
    private String recruitSchoolCode;

    @Id
    private String id;

    @Column(name = "ADMS_KOR_NAME")
    private String korName;

    @Column(name = "ADMS_ENG_NAME")
    private String engName;

    public enum AmdsCodeType {
        KR("ADM00001"),
        EN("ADM00002");
        private String value;

        private AmdsCodeType(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }
}
