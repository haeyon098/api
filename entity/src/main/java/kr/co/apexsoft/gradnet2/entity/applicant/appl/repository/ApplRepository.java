package kr.co.apexsoft.gradnet2.entity.applicant.appl.repository;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.*;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-02-20
 */
public interface ApplRepository extends JpaRepository<Appl, Long>, ApplRepositoryCustom {


    @Query(value = "SELECT a.applicantInfo.registrationEncr " +
            "FROM  Appl  a " +
            "WHERE a.part.recruitPartSeq =:recruitPartSeq and a.part.schoolCode =:schoolCode and a.part.enterYear =:enterYear " +
            "AND a.part.admissionCode=:admissionCode  AND a.applicantInfo.registrationBornDate=:registrationBornDate " +
            "AND a.status=:status")
    List<String> findRegistration(@Param("schoolCode") String schoolCode,
                                  @Param("enterYear") String enterYear,
                                  @Param("recruitPartSeq") Integer recruitPartSeq,
                                  @Param("admissionCode") String admissionCode,
                                  @Param("registrationBornDate") LocalDate registrationBornDate,
                                  @Param("status") Appl.Status status
    );


    Optional<Appl> findById(Long id);

    @Query(value = "SELECT APPL_STS_CODE FROM APPL  WHERE APPL_NO=:applNo", nativeQuery = true)
    String findApplStatus(@Param("applNo") Long applNo);

    @Query(value = "SELECT USER_NO FROM APPL  WHERE APPL_NO=:applNo ", nativeQuery = true)
    Long findUserNoByApplNo(@Param("applNo") Long applNo);

    @Query(value = "SELECT A.APPL_NO AS id, A.APPL_STS_CODE AS status, U.USER_ID AS userId, A.KOR_NAME AS korName, A.ENG_NAME AS engName,  " +
            "A.RECR_SCHL_CODE AS schoolCode,S.RECR_SCHL_KOR_NAME AS schoolKorName,S.RECR_SCHL_ENG_NAME AS schoolEngName,  " +
            "A.ENTR_YEAR AS enterYear, A.RECR_PART_SEQ AS recruitPartSeq, " +
            "C.SMST_TYPE_CODE AS semesterTypeCode, C.SMST_TYPE_KOR_NAME AS semesterTypeKorName, C.SMST_TYPE_ENG_NAME AS semesterTypeEngName, " +
            "D.ADMS_CODE AS admissionCode, D.ADMS_KOR_NAME AS admissionKorName, D.ADMS_ENG_NAME AS admissionEngName, " +
            "E.CORS_CODE AS courseCode, E.CORS_KOR_NAME AS courseKorName, E.CORS_ENG_NAME AS courseEngName, " +
            "F.CATEGORY_CODE AS categoryCode, F.CATEGORY_KOR_NAME AS categoryKorName, F.CATEGORY_ENG_NAME AS categoryEngName, " +
            "G.MAJ_CODE AS majorCode, G.MAJ_KOR_NAME AS majorKorName, G.MAJ_ENG_NAME AS majorEngName " +
            "FROM APPL A, USER U, APPL_PART B, SMST_TYPE C, ADMS D, CORS E, CATEGORY F, MAJ G, RECR_SCHL S " +
            "WHERE 1 = 1 " +
            "AND U.USER_NO = A.USER_NO " +
            "AND A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND A.ENTR_YEAR = B.ENTR_YEAR " +
            "AND A.RECR_PART_SEQ = B.RECR_PART_SEQ " +
            "AND A.APPL_PART_NO = B.APPL_PART_NO " +
            "AND C.RECR_SCHL_CODE = A.RECR_SCHL_CODE  " +
            "AND C.SMST_TYPE_CODE = B.SMST_TYPE_CODE " +
            "AND D.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND D.ADMS_CODE = B.ADMS_CODE " +
            "AND E.RECR_SCHL_CODE = A.RECR_SCHL_CODE  " +
            "AND E.CORS_CODE = B.CORS_CODE " +
            "AND F.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND F.CATEGORY_CODE = B.CATEGORY_CODE " +
            "AND G.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND G.ENTR_YEAR = A.ENTR_YEAR  " +
            "AND G.RECR_PART_SEQ = A.RECR_PART_SEQ " +
            "AND G.MAJ_CODE = B.MAJ_CODE " +
            "AND S.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =B.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =C.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =D.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =E.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =F.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =G.RECR_SCHL_CODE " +
            "AND A.APPL_NO = :applNo", nativeQuery = true)
    ApplPartResponse findMyApplPart(@Param("applNo") Long applNo);


    @Query(value = "SELECT A.APPL_NO AS id, A.APPL_STS_CODE AS status, " +
            "A.RECR_SCHL_CODE AS schoolCode, F.RECR_SCHL_KOR_NAME AS schoolKorName, F.RECR_SCHL_ENG_NAME AS schoolEngName, " +
            "A.ENTR_YEAR AS enterYear, A.RECR_PART_SEQ AS recruitPartSeq, " +
            "SYS_CODEVAL('KR','APPL_STS', A.APPL_STS_CODE) AS statusKorName, " +
            "SYS_CODEVAL('EN','APPL_STS', A.APPL_STS_CODE) AS statusEngName, " +
            "C.MAJ_KOR_NAME AS majorKorName, C.MAJ_ENG_NAME AS majorEngName, " +
            "D.ADMS_KOR_NAME AS admissionKorName, D.ADMS_ENG_NAME AS admissionEngName, " +
            "E.CORS_KOR_NAME AS courseKorName, E.CORS_ENG_NAME AS courseEngName " +
            "FROM APPL A, APPL_PART B, MAJ C, ADMS D, CORS E, RECR_SCHL F " +
            "WHERE 1=1 " +
            "AND A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND A.ENTR_YEAR = B.ENTR_YEAR " +
            "AND A.RECR_PART_SEQ = B.RECR_PART_SEQ " +
            "AND A.APPL_PART_NO = B.APPL_PART_NO " +
            "AND B.RECR_SCHL_CODE = C.RECR_SCHL_CODE " +
            "AND B.ENTR_YEAR = C.ENTR_YEAR " +
            "AND B.RECR_PART_SEQ = C.RECR_PART_SEQ " +
            "AND B.MAJ_CODE = C.MAJ_CODE " +
            "AND D.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND D.ADMS_CODE = B.ADMS_CODE " +
            "AND E.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND E.CORS_CODE = B.CORS_CODE " +
            "AND F.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND A.USER_NO = :userNo " +
            "ORDER BY A.CREATED_DATE"
            , nativeQuery = true)
    List<MyApplResponse> fintApplList(@Param("userNo") Long userNo);

    @Query(value = "SELECT new kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.TimeDto(t.startDate ,t.endDate) " +
            "FROM RecruitPart p join p.time as t , Appl  a " +
            "WHERE a.part.recruitPartSeq = p.seq and a.part.schoolCode =p.recruitSchoolCode and a.part.enterYear =p.enterYear " +
            "AND a.id =:useNo  AND t.type=:type ")
    TimeDto findRecruitPartPeriod(@Param("useNo") Long useNo, @Param("type") RecruitPartTime.TimeType type);

    List<Appl> findAllByApplicantInfo_EmailAndStatus(String email, Appl.Status status);

    @Query(value = "SELECT  A.KOR_NAME AS korName, A.ENG_NAME AS engName, U.USER_ID AS userId, A.APPL_NO AS applNo, " +
            "A.RECR_SCHL_CODE AS schoolCode, A.ENTR_YEAR AS enterYear, A.RECR_PART_SEQ AS recruitPartSeq, " +
            "SYS_CODEVAL('EN', 'GEND_TYPE', A.GEND) AS gend, SYS_CODEVAL('KR', 'GEND_TYPE', A.GEND) AS gendKorName, " +
            "DATE_FORMAT(A.RGST_BORN_DATE, '%Y%m%d' ) AS rgstBronDate ," +
            "DATE_FORMAT(A.RGST_BORN_DATE, '%Y-%m-%d' ) AS dateOfBirth ," +
            "DATE_FORMAT(A.APPL_DATE, '%Y-%m-%d' ) AS applDate ," +
            "D.ADMS_CODE AS admissionCode, D.ADMS_KOR_NAME AS admsKorName, D.ADMS_ENG_NAME AS admsEngName, " +
            "E.CORS_CODE AS courseCode, E.CORS_KOR_NAME AS corsKorName, E.CORS_ENG_NAME AS corsEngName, " +
            "F.CATEGORY_CODE AS categoryCode, F.CATEGORY_KOR_NAME AS categoryKorName, F.CATEGORY_ENG_NAME AS categoryEngName, " +
            "G.MAJ_CODE AS majorCode, G.MAJ_KOR_NAME AS majorKorName, G.MAJ_ENG_NAME AS majorEngName ," +
            "A.APPL_ID as applId," +
            "A.POST_NO AS postNo, A.ADDR AS addr , A.DETL_ADDR AS detailAddr , A.RGST_BORN_DATE AS registrationBornDate , A.RGST_ENCR AS registrationEncr, " +
            "A.EMAIL AS mailAddr ,A.TEL_NUM AS telNum ,A.PHONE_NUM AS phoneNum, " +
            "CASE WHEN A.CITZ_CNTR_CODE='999' THEN ETC_CITZ_CNTR_NAME  ELSE  (SELECT CNTR_KOR_NAME FROM  CNTR where CNTR_CODE= A.CITZ_CNTR_CODE) END AS cntrKorName, " +
            "CASE WHEN A.CITZ_CNTR_CODE='999' THEN ETC_CITZ_CNTR_NAME  ELSE  (SELECT CNTR_ENG_NAME FROM  CNTR where CNTR_CODE= A.CITZ_CNTR_CODE) END AS cntrEngName, " +
            "SCHL_CODEVAL('KR',  A.RECR_SCHL_CODE, A.ENTR_YEAR, A.RECR_PART_SEQ, 'FORN_TYPE', R.FORN_TYPE_CODE) AS forTypeKorName, " +
            "SCHL_CODEVAL('EN', A.RECR_SCHL_CODE, A.ENTR_YEAR, A.RECR_PART_SEQ, 'FORN_TYPE',  R.FORN_TYPE_CODE) AS forTypeEngName, " +
            "R.FORN_RGST_NO AS fornRgstEncr,R.PASSPORT_NO AS passportEncr,R.VISA_NO AS visaEncr,DATE_FORMAT(R.VISA_EXPR_DAY, '%Y-%m-%d' ) AS visaExprDay, " +
            "R.FIRST_NAME AS firstName,R.MIDDLE_NAME AS middleName,R.LAST_NAME lastName, " +
            "R.SKYPE_ID AS skypeId, " +
            "R.HOME_ADDR AS homeAddress, " +
            "R.HOME_TEL_NUM AS homeTelNum," +
            "R.VIDEO_ESSAY AS videoEssay ," +
            "SCHL_CODEVAL('EN',A.RECR_SCHL_CODE,A.ENTR_YEAR,A.RECR_PART_SEQ,'RESIDENCY_TYPE',R.RESIDENCY_TYPE) AS residencyType, " +
            "R.OVERSEAS_KOREAN AS overseasKorean," +
            "SCHL_CODEVAL('EN', A.RECR_SCHL_CODE, A.ENTR_YEAR, A.RECR_PART_SEQ, 'EMP_GRP',  A.EMP_CAT) AS empCat, " +
            "N.GMP_YN AS gmpYn, " +
            "SCHL_CODEVAL('EN', A.RECR_SCHL_CODE, A.ENTR_YEAR, A.RECR_PART_SEQ, 'CONTACT_TYPE',  A.CONTACT_CODE) AS contactName " +
            "FROM " +
            "APPL A JOIN USER U ON U.USER_NO = A.USER_NO " +
            "JOIN APPL_PART B ON  A.RECR_SCHL_CODE = B.RECR_SCHL_CODE  AND A.ENTR_YEAR = B.ENTR_YEAR   AND A.RECR_PART_SEQ = B.RECR_PART_SEQ AND A.APPL_PART_NO = B.APPL_PART_NO " +
            "JOIN  ADMS D  ON D.RECR_SCHL_CODE = A.RECR_SCHL_CODE   AND D.ADMS_CODE = B.ADMS_CODE " +
            "JOIN CORS E  ON   E.RECR_SCHL_CODE = A.RECR_SCHL_CODE   AND E.CORS_CODE = B.CORS_CODE  " +
            "JOIN  CATEGORY F ON F.RECR_SCHL_CODE = A.RECR_SCHL_CODE   AND F.CATEGORY_CODE = B.CATEGORY_CODE " +
            "JOIN  MAJ G ON  G.RECR_SCHL_CODE = A.RECR_SCHL_CODE AND G.RECR_PART_SEQ = A.RECR_PART_SEQ  AND G.ENTR_YEAR = A.ENTR_YEAR  AND G.MAJ_CODE = B.MAJ_CODE " +
            "LEFT JOIN APPL_FORN R ON A.APPL_NO=R.APPL_NO " +
            "LEFT JOIN APPL_GEN N ON A.APPL_NO=N.APPL_NO " +
            "WHERE A.APPL_NO = :applNo", nativeQuery = true)
    ApplBasisProjection findMyApplBasis(@Param("applNo") Long applNo);


    @Query(value = "SELECT A.APPL_ID AS applId, A.APPL_NO AS applNo ,A.ENTR_YEAR AS enterYear ,A.RECR_PART_SEQ AS recruitPartSeq, " +
            "A.ENG_NAME AS engName ,A.KOR_NAME AS korName, U.USER_ID AS userId,  A.RECR_SCHL_CODE AS schoolCode, " +
            "D.ADMS_CODE AS admissionCode, D.ADMS_KOR_NAME AS admsKorName, D.ADMS_ENG_NAME AS admsEngName, " +
            "G.MAJ_CODE AS majorCode , G.MAJ_KOR_NAME AS majorKorName ,G.MAJ_ENG_NAME AS majorEngName, " +
            "E.CORS_KOR_NAME AS corsKorName, E.CORS_ENG_NAME AS corsEngName, " +
            "F.CATEGORY_KOR_NAME AS categoryKorName ,F.CATEGORY_ENG_NAME AS categoryEngName, " +
            "DOC.FILE_PATH AS photoFilePath " +
            "FROM APPL A, USER U, APPL_PART B, ADMS D, CORS E, CATEGORY F, MAJ G ,APPL_DOC DOC " +
            "WHERE 1 = 1 " +
            "AND U.USER_NO = A.USER_NO " +
            "AND A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND A.ENTR_YEAR = B.ENTR_YEAR " +
            "AND A.RECR_PART_SEQ = B.RECR_PART_SEQ " +
            "AND A.APPL_PART_NO = B.APPL_PART_NO " +
            "AND D.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND D.ADMS_CODE = B.ADMS_CODE " +
            "AND E.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND E.CORS_CODE = B.CORS_CODE " +
            "AND F.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND F.CATEGORY_CODE = B.CATEGORY_CODE " +
            "AND G.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND G.ENTR_YEAR = A.ENTR_YEAR " +
            "AND G.RECR_PART_SEQ = A.RECR_PART_SEQ " +
            "AND G.MAJ_CODE = B.MAJ_CODE " +
            "AND A.APPL_NO =DOC.APPL_NO " +
            "AND A.APPL_NO = :applNo AND DOC.DOC_ITEM_CODE=:itemCode", nativeQuery = true)
    ApplSlipProjection findMyApplSlip(@Param("applNo") Long applNo, @Param("itemCode") String itemCode);


    @Query(value = "SELECT A.APPL_ID AS applId, A.APPL_NO AS applNo ,A.ENTR_YEAR AS enterYear ,A.RECR_PART_SEQ AS recruitPartSeq, " +
            "A.ENG_NAME AS engName ,A.KOR_NAME AS korName,A.EMAIL AS emailAddr, U.USER_ID AS userId,  A.RECR_SCHL_CODE AS schoolCode, " +
            "D.ADMS_CODE AS admissionCode, D.ADMS_KOR_NAME AS admsKorName, D.ADMS_ENG_NAME AS admsEngName, " +
            "G.MAJ_CODE AS majorCode , G.MAJ_KOR_NAME AS majorKorName ,G.MAJ_ENG_NAME AS majorEngName, " +
            "E.CORS_KOR_NAME AS corsKorName, E.CORS_ENG_NAME AS corsEngName, " +
            "F.CATEGORY_KOR_NAME AS categoryKorName ,F.CATEGORY_ENG_NAME AS categoryEngName, " +
            " S.RECR_SCHL_KOR_NAME AS schoolKorName,S.RECR_SCHL_ENG_NAME AS schoolEngName ," +
            "P.RECR_PART_KOR_NAME AS recrPartKorName ,P.RECR_PART_ENG_NAME AS recrPartEngName " +
            "FROM APPL A, USER U, APPL_PART B, ADMS D, CORS E, CATEGORY F, MAJ G ,RECR_SCHL S ,RECR_PART P  " +
            "WHERE 1 = 1 " +
            "AND U.USER_NO = A.USER_NO " +
            "AND A.RECR_SCHL_CODE = B.RECR_SCHL_CODE " +
            "AND A.ENTR_YEAR = B.ENTR_YEAR " +
            "AND A.RECR_PART_SEQ = B.RECR_PART_SEQ " +
            "AND A.APPL_PART_NO = B.APPL_PART_NO " +
            "AND E.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND E.CORS_CODE = B.CORS_CODE " +
            "AND F.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND F.CATEGORY_CODE = B.CATEGORY_CODE " +
            "AND G.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND G.ENTR_YEAR = A.ENTR_YEAR " +
            "AND G.RECR_PART_SEQ = A.RECR_PART_SEQ " +
            "AND G.MAJ_CODE = B.MAJ_CODE " +
            "AND D.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND D.ADMS_CODE = B.ADMS_CODE " +
            "AND S.RECR_SCHL_CODE = A.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =B.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =D.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =E.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =F.RECR_SCHL_CODE " +
            "AND S.RECR_SCHL_CODE =G.RECR_SCHL_CODE " +
            "AND P.RECR_PART_SEQ = A.RECR_PART_SEQ " +
            "AND P.RECR_SCHL_CODE =B.RECR_SCHL_CODE " +
            "AND P.ENTR_YEAR=B.ENTR_YEAR " +
            "AND A.APPL_NO = :applNo ", nativeQuery = true)
    ApplInfoProjection findMyApplInfo(@Param("applNo") Long applNo);


    @Query(value = "SELECT CONCAT(RIGHT( A.ENTR_YEAR, 2), :ruleNo,LPAD( MAX(A.APPL_ID_SEQ)+1, '4', '0')) AS applId, " +
            "MAX(A.APPL_ID_SEQ)+1 AS newSeq, A.RULE_NO AS ruleNo " +
            "FROM APPL_ID_SEQ A " +
            "WHERE A.RECR_SCHL_CODE = :schoolCode " +
            "AND A.ENTR_YEAR = :enterYear " +
            "AND A.RECR_PART_SEQ = :recruitPartSeq " +
            "FOR UPDATE", nativeQuery = true)
    ApplIdSeqProjection findApplId(@Param("schoolCode") String schoolCode,
                                   @Param("enterYear") String enterYear,
                                   @Param("recruitPartSeq") Integer recruitPartSeq,
                                   @Param("ruleNo") String ruleNo);


    @Transactional
    @Modifying
    @Query(value = "UPDATE APPL_ID_SEQ " +
            "SET APPL_ID_SEQ = :newSeq " +
            "WHERE RECR_SCHL_CODE = :schoolCode " +
            "AND ENTR_YEAR = :enterYear " +
            "AND RECR_PART_SEQ = :recruitPartSeq " +
            "AND rule_no = :ruleNo", nativeQuery = true)
    int updateApplIdSeq_Seq(@Param("schoolCode") String schoolCode,
                            @Param("enterYear") String enterYear,
                            @Param("recruitPartSeq") Integer recruitPartSeq,
                            @Param("newSeq") Integer newSeq,
                            @Param("ruleNo") String ruleNo);


    /* 학교관리자 지원현황 조회 for Excel
     * 2020.06.16 최은주
     * TO-DO 공통쿼리를 추가할 필요가 있음 ( 현재는 KDI 전용으로 만들어짐 )
     *  : 현재는 KDI 전용으로 만들어짐 / 추후
     * 1. 중복건 제외 : 아이디 기준으로 여러건의 데이터중 지원프로세스가 가장 많이 진행된건 / 동일한 상태라면 가장 최근건으로 조회함
     * 2. 시스템관리자 및 학교관리자 데이터 제외 ( 테스트용 )
     * 3. 취소건 제외
     * 4. 2020.08.26  시스템에서 하나의 아이디당 하나의 회차에 한 건만 지원할 수 있도록함 중복을 제외하여도되나 결과는 동일하여 유지함
     */
    @Query(value ="SELECT userId, applNo, applId, seqForYear, smstTypeKorName, admsKorName, corsKorName, categoryEngName, majKorName, " +
                  "       korName, engName, applStatusName, phoneNumber, email, recRequestCnt, recSubmitCnt, country, countryGroupCode, countryGroupName " +
                  "       , rgstBornDate, gend, empCat, videoEssay " +
                  "FROM ( " +
                  "  SELECT (IF ( U.STATUS = 'WITHDRAW', '탈퇴회원/A withdrawal member' ,U.USER_ID)  ) as userId,  " +
                  "       appl.APPL_NO  as applNo, IFNULL(appl.APPL_ID, '') as applId, " +
                  "       CONCAT( recrPart.ENTR_YEAR ,'-', recrPart.RECR_PART_SEQ) as seqForYear, " +
                  "       smstType.SMST_TYPE_KOR_NAME as smstTypeKorName, " +
                  "       adms.ADMS_KOR_NAME as admsKorName, cors.CORS_KOR_NAME as corsKorName, category.CATEGORY_ENG_NAME as categoryEngName, maj.MAJ_KOR_NAME as majKorName, " +
                  "       IFNULL(appl.KOR_NAME, '') as korName, IFNULL(appl.ENG_NAME, '') as engName, SYS_CODEVAL('KR', 'APPL_STS', appl.APPL_STS_CODE) as applStatusName, " +
                  "       IFNULL(appl.PHONE_NUM, '') as phoneNumber, IFNULL(appl.EMAIL, '') as email, " +
                  "       ( SELECT count(rec.REC_STS_CODE) from APPL_REC rec where rec.appl_no=appl.appl_no ) as recRequestCnt, " +
                  "       ( SELECT count(if(rec.REC_STS_CODE='00040', rec.REC_STS_CODE, null) ) FROM APPL_REC rec WHERE rec.appl_no=appl.appl_no ) as recSubmitCnt, " +
                  "       ( SELECT CNTR_ENG_NAME FROM  CNTR where CNTR_CODE= appl.CITZ_CNTR_CODE) as country, " +
                  "       ( SELECT CNTR.CNTR_GRP_CD FROM  CNTR where CNTR_CODE= appl.CITZ_CNTR_CODE) as countryGroupCode, " +
                  "       ( SELECT CNTR.CNTR_GRP_NM FROM  CNTR where CNTR_CODE= appl.CITZ_CNTR_CODE) as countryGroupName,  " +
                  "       ( CASE appl.APPL_STS_CODE " +
                  "         WHEN 'APPL_PART_SAVE' THEN '01_APPL_PART_SAVE' " +
                  "         WHEN 'BASIS_SAVE' THEN '02_BASIS_SAVE' " +
                  "         WHEN 'ACAD_SAVE' THEN '03_ACAD_SAVE' " +
                  "         WHEN 'LANG_SAVE' THEN '04_LANG_CAREER_SAVE' " +
                  "         WHEN 'ESSAY_SAVE' THEN '05_ESSAY_SAVE' " +
                  "         WHEN 'FILE_SAVE' THEN '06_FILE_SAVE' " +
                  "         WHEN 'SUBMIT' THEN '07_SUBMIT' " +
                  "         WHEN 'COMPLETE' THEN '08_COMPLETE' " +
                  "         END) as orderApplStatus " +
                  "       , appl.LAST_MODIFIED_DATE as lastModifiedDate " +
                  "       , SCHL_CODEVAL('EN', :schoolCode, :enterYear, :recruitPartSeq, 'EMP_GRP', EMP_CAT) AS empCat " +
                  "       , date_format(RGST_BORN_DATE, '%Y%m%d') as rgstBornDate " +
                  "       , appl.gend as gend " +
                  "       , ( SELECT applForn.VIDEO_ESSAY FROM  APPL_FORN applForn where applForn.APPL_NO =  appl.CITZ_CNTR_CODE) as videoEssay " +
                  "       FROM APPL appl " +
                  "       INNER JOIN APPL_PART applPart ON ( appl.RECR_SCHL_CODE=applPart.RECR_SCHL_CODE and appl.ENTR_YEAR=applPart.ENTR_YEAR and appl.RECR_PART_SEQ=applPart.RECR_PART_SEQ and appl.APPL_PART_NO=applPart.APPL_PART_NO ) " +
                  "       INNER JOIN RECR_PART recrPart ON ( appl.RECR_SCHL_CODE=recrPart.RECR_SCHL_CODE and appl.ENTR_YEAR=recrPart.ENTR_YEAR and appl.RECR_PART_SEQ=recrPart.RECR_PART_SEQ ) " +
                  "       INNER JOIN SMST_TYPE smstType ON ( appl.RECR_SCHL_CODE=smstType.RECR_SCHL_CODE and applPart.SMST_TYPE_CODE=smstType.SMST_TYPE_CODE ) " +
                  "       INNER JOIN ADMS adms ON ( appl.RECR_SCHL_CODE=adms.RECR_SCHL_CODE and applPart.ADMS_CODE=adms.ADMS_CODE ) " +
                  "       INNER JOIN CORS cors ON ( appl.RECR_SCHL_CODE=cors.RECR_SCHL_CODE and applPart.CORS_CODE=cors.CORS_CODE )  " +
                  "       INNER JOIN CATEGORY category ON ( appl.RECR_SCHL_CODE=category.RECR_SCHL_CODE and applPart.CATEGORY_CODE=category.CATEGORY_CODE ) " +
                  "       INNER JOIN MAJ maj ON (appl.RECR_SCHL_CODE=maj.RECR_SCHL_CODE and appl.ENTR_YEAR=maj.ENTR_YEAR and appl.RECR_PART_SEQ=maj.RECR_PART_SEQ and applPart.MAJ_CODE=maj.MAJ_CODE)  " +
                  "       INNER JOIN USER U ON U.USER_NO = appl.USER_NO " +
                  "  WHERE appl.RECR_SCHL_CODE=:schoolCode  " +
                  "  AND   appl.ENTR_YEAR=:enterYear " +
                  "  AND   appl.RECR_PART_SEQ=:recruitPartSeq " +
                  "  AND   appl.APPL_STS_CODE != 'CANCEL' " +
                  "  AND   appl.USER_NO NOT IN (SELECT USER_NO FROM USER_ROLES WHERE (ROLE_ID = '200' OR ROLE_ID = '210')) " +
                  "  order by U.USER_ID, orderApplStatus DESC, appl.LAST_MODIFIED_DATE DESC " +
                  "  ) A " +
                  "GROUP BY A.userId", nativeQuery = true)
    List<ApplicationListForExcelResponse> findApplDeduplicateList(@Param("schoolCode") String schoolCode,
                                                                  @Param("enterYear") String enterYear,
                                                                  @Param("recruitPartSeq") Integer recruitPartSeq);




    Long countByPart_SchoolCodeAndPart_EnterYearAndPart_RecruitPartSeqAndPart_ApplPartNoAndApplicant_IdAndStatusNot(@Param("schoolCode") String schoolCode,
                                                                                                                     @Param("enterYear") String enterYear,
                                                                                                                     @Param("recruitPartSeq") Integer recruitPartSeq,
                                                                                                                     @Param("applPartNo") Long applPartNo,
                                                                                                                     @Param("id") Long id,
                                                                                                                    @Param("Status") Appl.Status Status);

    List<Appl> findByStatusAndApplDateBetweenOrderByApplDateDesc(@Param("Status") Appl.Status Status,
                                                                 @Param("start") LocalDateTime start,
                                                                 @Param("end") LocalDateTime end);


    /*
     * 학교관리자 지원완료후 전달파일 - KDIS 지원자정보
     * 2021.12.17 최은주
     * CAT00001(국내/일반)     : '04'
     * CAT00002(국내/기업체)   : '02'
     * CAT00003(국내/공무원)   : '01
     * CAT00004(국내/국가정책학) : '33'
     * CAT00005(국내/K-Water) : '26'
     * CAT00011 : '05' -- 국제/GAS
     * CAT00013 : '19' -- 국제/Seoul G20
     * CAT00007 : '31'  -- 국제/KOICA
     * CAT00012 : '10'  -- 국제/Colombo
     * CAT00014 : '08'  -- 국제/POSCO
     * CAT00015 : '25'  -- 국제/IBK
     * CAT00016 : '32'  -- 국제/KNOC
     * CAT00017 : '24'  -- 국제/FSS-KFB-KDIS
     * CAT00018 : '29'  -- 국제/MIPD Scholarship
     * CAT00019 : '28'  -- 국제/PT Bank KEB Hana Indonesia
     * CAT00020 : '30'  -- 국제/The Hyundai Motor Chung Mong-Koo Foundation Scholarship
     * CAT00021 : '14'  -- 국제/NIIED KGSP Designated Application
     * CAT00022 : '16'  -- 국제/NIIED KGSP Applicant
     * (01, 11) 석사 / (02, 12) 박사 / (01,02) 주간 / (11, 12) 야간
     * 이름 : 성, 이름, 미들네임순(성은 모두 대문자, 나머지는 앞글자 대문자
     */
    @Query(value =
            " SELECT appl.APPL_ID as 'APPLICANT_NO', " +
            "       appl.ENTR_YEAR as ADM_YEAR, right(part.SMST_TYPE_CODE, 2) as ADM_TERM " +
//            "       (CASE part.CATEGORY_CODE " +
//            "            WHEN 'CAT00001' THEN '04'    WHEN 'CAT00002' THEN '02'    WHEN 'CAT00003' THEN '01' " +
//            "            WHEN 'CAT00004' THEN '33'    WHEN 'CAT00005' THEN '26' " +
//            "            WHEN 'CAT00011' THEN '05'    WHEN 'CAT00013' THEN '19'    WHEN 'CAT00012' THEN '10' " +
//            "            WHEN 'CAT00014' THEN '08'    WHEN 'CAT00015' THEN '25'    WHEN 'CAT00016' THEN '32' " +
//            "            WHEN 'CAT00017' THEN '24'    WHEN 'CAT00018' THEN '29'    WHEN 'CAT00019' THEN '28' " +
//            "            WHEN 'CAT00020' THEN '30'    WHEN 'CAT00021' THEN '14'    WHEN 'CAT00022' THEN '16' " +
//            "            ELSE 'ERROR_코드확인필요' " +
//            "        END )as CATEGORY, " +
//            "       (IF (part.ADMS_CODE='ADM00001', 'N', 'Y') ) as INTERNATIONAL_YN, " +
//            "       (CASE LPAD(right(part.CORS_CODE, 1), 2, '0') " +
//            "            WHEN '01' THEN '01'     WHEN '02' THEN '10' " +
//            "            ELSE 'ERROR_코드확인필요' " +
//            "        END )  as DEGREE, " +
//            "       (CASE part.MAJ_CODE " +
//            "            WHEN 'MPP' THEN '21'    WHEN 'MDP' THEN '27'    WHEN 'MPM' THEN '28'    WHEN 'MIPD' THEN '29'    WHEN 'MPPM' THEN '31'  " +
//            "            WHEN 'PP' THEN '01'    WHEN 'DP' THEN '03'    WHEN 'PM' THEN '06' " +
//            "            ELSE 'ERROR_코드확인필요' " +
//            "        END )as PROGRAM, " +
//            "       (IF (LEFT(part.CORS_CODE, 1)='0', 'F', 'P') ) as DAY_YN " +
//
//            "       '' as tmp1, '' as tmp2, "+
//            "       date_format(appl.APPL_DATE, '%Y%m%d') as APPLY_DATE, " +
//            "        '' as tmp3, "+
//            "        appl.KOR_NAME AS KOR_NAME, " +
//            "       (IF (part.ADMS_CODE='ADM00001', CONCAT( UPPER(gen.ENG_LAST_NAME),  ', ', CONCAT( UCASE(LEFT(gen.ENG_FIRST_NAME, 1)), LCASE(SUBSTRING(gen.ENG_FIRST_NAME, 2))) ) " +
//            "                                      , CONCAT( UPPER(forn.LAST_NAME),  ', ', CONCAT( UCASE(LEFT(forn.FIRST_NAME, 1)), LCASE(SUBSTRING(forn.FIRST_NAME, 2))),   ' ', CONCAT(UCASE(LEFT(forn.MIDDLE_NAME, 1)), LCASE(SUBSTRING(forn.MIDDLE_NAME, 2))) ) " +
//            "        )) as ENG_NAME, " +
//            "       ( IF (part.ADMS_CODE='ADM00001', UPPER(gen.ENG_LAST_NAME), UPPER(forn.LAST_NAME) )) as LAST_NAME, " +
//            "       ( IF (part.ADMS_CODE='ADM00001', CONCAT(UCASE(LEFT(gen.ENG_FIRST_NAME, 1)), LCASE(SUBSTRING(gen.ENG_FIRST_NAME, 2))), " +
//            "                                        CONCAT(UCASE(LEFT(forn.FIRST_NAME, 1)), LCASE(SUBSTRING(forn.FIRST_NAME, 2))) )) as FIRST_NAME, " +
//            "       ( IF (part.ADMS_CODE='ADM00001', '', CONCAT(UCASE(LEFT(forn.MIDDLE_NAME, 1)),LCASE(SUBSTRING(forn.MIDDLE_NAME, 2))) )) as MIDDLE_NAME, " +
//            "       appl.EMAIL as EMAIL, appl.PHONE_NUM as MOBILE_NO, " +
//            "       appl.RGST_BORN_DATE, RGST_ENCR, "+
//            "       forn.FORN_RGST_NO as FOREIGN_NO, "+
//            "       forn.PASSPORT_NO as PASSPORT_NO, "+
//            "       appl.CITZ_CNTR_CODE  as NATIONAL_CD, " +
//            "       forn.OVERSEAS_KOREAN as OVERSEAS_KOREAN, " +
//            "       ( CASE appl.GEND WHEN 'M' THEN '1' WHEN 'F' THEN '2' END) as GENDER, " +
//            "       date_format(appl.RGST_BORN_DATE, '%Y%m%d') as DOB, " +
//            "       '' as NoTitle1, '' as NoTitle2, " +
//            "       gen.GMP_YN as GMP_YN, " +
//            "       '' as NoTitle3, '' as NoTitle4, " +
//            "       appl.TEL_NUM as PHONENO_HOME, " +
//            "       '' as NoTitle5, " +
//            "       (IF (part.ADMS_CODE='ADM00001', appl.POST_NO, '' ) ) as ZIPNO_HOME, " +
//            "       (IF (part.ADMS_CODE='ADM00001', concat(appl.ADDR, ' ', appl.DETL_ADDR), '' ) ) as ADDRESS_HOME, " +
//            "       (IF (part.ADMS_CODE='ADM00002', appl.ADDR, '' ) ) as ADDRESS_EHOME, " +
//            "       '' as NoTitle6, '' as NoTitle7, '' as NoTitle8, " +
//            "       '' as ZIPNO_WORK, '' as ADDRESS_WORK, " +
//            "       career.COMPANY_ADDRESS  as ADDRESS_EWORK, " +
//            "       '' as NoTitle10, '' as NoTitle11, '' as NoTitle12, '' as NoTitle13, '' as NoTitle14, '' as NoTitle15, '' as NoTitle16, "+
//            "       (CASE appl.CONTACT_CODE  WHEN '01' THEN '1' WHEN '02' THEN '2' ELSE 'ERROR_코드확인필요' END )as CONTACT_CD, " +
//            "       forn.RESIDENCY_TYPE as RESIDENCY_YN, " +
//            "       appl.EMP_CAT as SUB_CAT, " +
//            "       '' as NoTitle17, " +
//            "       DATE_FORMAT(appl.appl_date, '%Y-%m-%d %H:%i:%s') as RECEIPT_DTIME, " +
//            "       'Y' as ONLINE_YN, 'Y' as APPLICATION_YN, 'Y' as DOCUMENT_SUBMIT_YN, 'Y' as DOCUMENT_YN, "+
//            "       '' as NoTitle18, 'Y' as PASS_YN, '' as NoTitle19, '' as NoTitle20, '' as NoTitle21, '' as NoTitle22, '' as NoTitle23, '' as NoTitle24, '' as NoTitle25, "+
//            "       appl.CREATED_BY as INPUT_ID,  DATE_FORMAT(appl.CREATED_DATE, '%Y-%m-%d %H:%i:%s') as INPUT_DTIME, " +
//            "       '' as 'NoTitle26', " +
//            "       appl.LAST_MODIFIED_BY as UPDATE_ID, DATE_FORMAT(appl.LAST_MODIFIED_DATE, '%Y-%m-%d %H:%i:%s') as UPDATE_DTIME, " +
//            "       '' as 'NoTitle27', " +
//            "       forn.SKYPE_ID as SKYPE_ID, " +
//            "       '' as 'NoTitle28', '' as NATIONAL_PHONE, 'Y' as AGREEMENT_YN, '' as 'NoTitle29', '' as 'NoTitle30', "+
//            "       career.CURR_YN as EMPLOYED_YN, " +
//            "       '' as 'NoTitle31', '' as 'NoTitle32', '' as 'NoTitle33', '' as 'NoTitle34', '' as 'NoTitle35', '' as 'NoTitle36',  "+
//            "       forn.VIDEO_ESSAY as VIDEO_ESSAY " +
            " FROM  APPL appl " +
            "      INNER JOIN APPL_PART part ON (appl.RECR_SCHL_CODE = part.RECR_SCHL_CODE  and appl.ENTR_YEAR = part.ENTR_YEAR and appl.RECR_PART_SEQ = part.RECR_PART_SEQ and appl.APPL_PART_NO = part.APPL_PART_NO) " +
            "      LEFT JOIN APPL_GEN gen ON (appl.APPL_NO = gen.APPL_NO ) " +
            "      LEFT JOIN APPL_FORN forn ON (appl.APPL_NO = forn.APPL_NO ) " +
            "      LEFT JOIN APPL_CAREER career ON (appl.APPL_NO = career.APPL_NO and career.CURR_YN = 'Y') " +
            "      LEFT JOIN CNTR cntr ON (appl.CITZ_CNTR_CODE = cntr.CNTR_CODE) " +
            " WHERE appl.APPL_STS_CODE = 'COMPLETE' and appl.RECR_SCHL_CODE = :schoolCode and appl.ENTR_YEAR = :enterYear and appl.RECR_PART_SEQ  = :recruitPartSeq " +
            " ORDER BY appl.APPL_ID", nativeQuery = true)
    List<Object[]> findKidsApplicantList(@Param("schoolCode") String schoolCode,
                                         @Param("enterYear") String enterYear,
                                         @Param("recruitPartSeq") Integer recruitPartSeq);
}

