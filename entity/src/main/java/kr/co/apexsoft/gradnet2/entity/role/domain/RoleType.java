package kr.co.apexsoft.gradnet2.entity.role.domain;

public enum RoleType {
    ROLE_USER(100L),
    ROLE_SYS_ADMIN(200L),
    ROLE_SCHL_ADMIN(210L),
    ROLE_TIME_INDEPENDENT(300L);

    private Long value;

    private RoleType(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return this.value;
    }

    public static class NAME {
        public static final String USER = "ROLE_USER";
        public static final String SYS_ADMIN = "ROLE_SYS_ADMIN";
        public static final String SCHL_ADMIN = "ROLE_SCHL_ADMIN";
        public static final String ROLE_TIME_INDEPENDENT = "ROLE_TIME_INDEPENDENT";

    }
}
