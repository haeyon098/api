package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * 수험표 생성시 필요한 데이터
 */
public interface ApplSlipProjection {

    String getApplId();

    Long getApplNo();

    String getKorName();

    String getUserId();

    String getEngName();


    String getEnterYear();

    String getAdmissionCode();

    String getAdmsKorName();

    String getAdmsEngName();

    String getCourseCode();

    String getSchoolCode();

    Integer getRecruitPartSeq();

    String getCorsKorName();

    String getCorsEngName();

    String getCategoryCodeCode();

    String getCategoryKorName();

    String getCategoryEngName();

    String getMajorCode();

    String getMajorKorName();

    String getMajorEngName();

    String getPhotoFilePath();





}
