package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;


import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class TimeDto {

   private  LocalDateTime stratDay ;
   private  LocalDateTime endDay ;



   public TimeDto(LocalDateTime stratDay , LocalDateTime endDay){
       this.stratDay=stratDay;
       this.endDay=endDay;
   }


}
