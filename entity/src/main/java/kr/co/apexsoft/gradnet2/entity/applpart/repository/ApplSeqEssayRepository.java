package kr.co.apexsoft.gradnet2.entity.applpart.repository;

import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplSeqEssay;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-20
 */
public interface ApplSeqEssayRepository extends JpaRepository<ApplSeqEssay, Long> {
    List<ApplSeqEssay> findBySchoolCodeAndEnterYearAndRecruitPartSeqOrderBySeq(String schoolCode,
                                                                               String enterYear,
                                                                               Integer recruitPartSeq);
}
