package kr.co.apexsoft.gradnet2.entity.applicant.pay.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-10
 */
@Embeddable
public class VBank {
    @Column(name = "VBANK_NAME")
    private String name;

    @Column(name = "VBANK_NUM")
    private String num;

    /* 가상계좌 예금주 */
    @Column(name = "VBANK_HOLDER")
    private String holder;

    /* 가상계좌 만료일시 */
    @Column(name = "VBANK_DUE_DATE")
    private LocalDateTime dueDate;

    public void initDueDate(String dueDate) {
        this.dueDate = LocalDateTime.parse(dueDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }
}
