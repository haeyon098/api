package kr.co.apexsoft.gradnet2.entity.applpart.domain;

import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Formula;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-07-02
 */
@Entity
@Table(name = "APPL_PART_LANG")

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ApplPartLanguage {

    @Id
    @Column(name = "NODE_NO")
    @Getter
    private String id;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "RECR_SCHL_CODE", referencedColumnName = "RECR_SCHL_CODE"),
            @JoinColumn(name = "ENTR_YEAR", referencedColumnName = "ENTR_YEAR"),
            @JoinColumn(name = "RECR_PART_SEQ", referencedColumnName = "RECR_PART_SEQ"),
            @JoinColumn(name = "APPL_PART_NO", referencedColumnName = "APPL_PART_NO")
    })
    private ApplPart part;


    @Column(name = "LEVEL")
    @Getter
    private Integer level;

    @Column(name = "NODE_KOR_NAME")
    @Getter
    private String nodeKorName;

    @Column(name = "NODE_ENG_NAME")
    @Getter
    private String nodeEngName;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_NODE_NO")
    private ApplPartLanguage parentNode;

    @Enumerated(EnumType.STRING)
    @Column(name = "OPTION_CODE")
    @Getter
    private OptionType optionType;

    @OneToMany(mappedBy = "parentNode", targetEntity = ApplPartLanguage.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Getter
    private List<ApplPartLanguage> subNode = new ArrayList<>();

    @Column(name = "LEAF_YN")
    @Getter
    private Boolean isLeaf;


    @Getter
    @Enumerated(EnumType.STRING)
    @Column(name = "LANG_TYPE_CODE")
    private Language.LangType type;

    @Getter
    @Column(name = "EXAM_YN")
    private Boolean isExam;

    @Getter
    @Column(name = "LANG_INFO_TYPE_CODE")
    private String infoTypeCode;

    @Getter
    @Column(name = "DETAIL_GRP_CODE")
    private String detailGrpCode;

    @Getter
    @Column(name = "MDT_YN")
    private Boolean mandatory;

    @Getter
    @Column(name = "UPLOAD_YN")
    private Boolean uploaded;

    @Getter
    @Column(name = "MSG_NO")
    private String mgsNo;

    @Getter
    @Formula("(SELECT M.MSG_ENG_VAL FROM MSG M WHERE M.MSG_NO = MSG_NO)")
    private String msgEng;

    @Getter
    @Formula("(SELECT M.MSG_KOR_VAL FROM MSG M WHERE M.MSG_NO = MSG_NO)")
    private String msgKor;


    public enum OptionType {
        MO, // 1
        MM, // 1 or Many
        OO, //  0 or 1
        OM, // 0 or Many
        NONE

    }


}
