package kr.co.apexsoft.gradnet2.entity.comcode.repository;

import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCodeIds;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface SysCommCodeRepository extends JpaRepository<SysCommCode, SysCommCodeIds> {

    List<SysCommCode> findAllByCodeGrpAndUsedTrueOrderByCodeAsc(String codeGrp);
}
