package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-07
 */
public interface ApplPartResponse {
    Long getId();

    String getStatus();

    String getUserId();

    String getSchoolCode();

    String getSchoolEngName();

    String getSchoolKorName();

    String getEnterYear();

    Integer getRecruitPartSeq();

    String getSemesterTypeCode();

    String getSemesterTypeKorName();

    String getSemesterTypeEngName();

    String getAdmissionCode();

    String getAdmissionKorName();

    String getAdmissionEngName();

    String getCourseCode();

    String getCourseKorName();

    String getCourseEngName();

    String getCategoryCode();

    String getCategoryKorName();

    String getCategoryEngName();

    String getMajorCode();

    String getMajorKorName();

    String getMajorEngName();
    String getEngName();
    String getKorName();

}
