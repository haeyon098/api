package kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository;

import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.JPQLQuery;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendSimpleForListProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.util.StringUtils;

import java.util.List;

import static kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.QRecommend.recommend;
import static kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain.QRecommendInformation.recommendInformation;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QAdms.adms;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QCategory.category;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QCors.cors;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QMajor.major;
import static kr.co.apexsoft.gradnet2.entity.adms.domain.QSemesterType.semesterType;
import static kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.QAppl.appl;
import static kr.co.apexsoft.gradnet2.entity.applpart.domain.QApplPart.applPart;
import static kr.co.apexsoft.gradnet2.entity.recpart.domain.QRecruitPart.recruitPart;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-08-04
 */
public class RecommedRepositoryImpl extends QuerydslRepositorySupport implements RecommedRepositoryCustum {

    public RecommedRepositoryImpl() {
        super(Recommend.class);
    }


    @Override
    public Page<RecommendSimpleForListProjection> findAllBySearchCondition(
            String schoolCode,
            String enterYear,
            Integer recruitPartSeq,
            String admissionCode,
            String courseCode,
            String categoryCode,
            String majorCode,
            String applId,
            String applicantName,
            String email,
            String applicantId,
            String profName,
            String profEmail,
            String profPhoneNumber,
            String recStatus,
            Pageable pageable) {
        List<RecommendSimpleForListProjection> contents = from(appl)
                .join(recommend)
                .on(
                        appl.id.eq(recommend.appl.id)
                )
                .leftJoin(recommendInformation)
                .on(
                        recommend.id.eq(recommendInformation.recommend.id)
                )
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .innerJoin(recruitPart)
                .on(
                        appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                        appl.part.enterYear.eq(recruitPart.enterYear),
                        appl.part.recruitPartSeq.eq(recruitPart.seq)
                )
                .innerJoin(semesterType)
                .on(
                        appl.part.semesterTypeCode.eq(semesterType.id),
                        appl.part.schoolCode.eq(semesterType.recruitSchoolCode)
                )
                .innerJoin(adms)
                .on(
                        appl.part.admissionCode.eq(adms.id),
                        appl.part.schoolCode.eq(adms.recruitSchoolCode)
                )
                .innerJoin(cors)
                .on(
                        appl.part.courseCode.eq(cors.id),
                        appl.part.schoolCode.eq(cors.recruitSchoolCode)
                )
                .innerJoin(category)
                .on(

                        appl.part.categoryCode.eq(category.id),
                        appl.part.schoolCode.eq(category.recruitSchoolCode)
                )
                .innerJoin(major)
                .on(
                        appl.part.schoolCode.eq(major.recruitSchoolCode),
                        appl.part.enterYear.eq(major.enterYear),
                        appl.part.recruitPartSeq.eq(major.recruitPartSeq),
                        appl.part.majorCode.eq(major.id)
                )
                .where(
                        eqSchoolCode(schoolCode),
                        eqEnterYear(enterYear),
                        eqRecruitPartSeq(recruitPartSeq),
                        eqAdmissionCode(admissionCode),
                        eqCourseCode(courseCode),
                        eqCategoryCode(categoryCode),
                        eqMajorCode(majorCode),
                        containsApplId(applId),
                        containsApplicantName(applicantName),
                        containsEmail(email),
                        containsApplicantId(applicantId),
                        containsProfEmail(profEmail),
                        containsProfName(profName),
                        eqRecStsCodee(recStatus)
                )
                .groupBy(recommend, recommend.id)
                .orderBy(recommend.id.asc())
                .offset(pageable.getOffset())
                .limit(pageable.getPageSize())
                .select(Projections.constructor(RecommendSimpleForListProjection.class,
                        appl.applicant.userId,
                        appl.applId,
                        recruitPart.seq, recruitPart.korName, recruitPart.engName,
                        adms.id, adms.korName, adms.engName,
                        cors.id, cors.korName, cors.engName,
                        category.id, category.korName, category.engName,
                        major.id, major.korName, major.engName,
                        appl.applicantInfo.korName, appl.applicantInfo.engName,  appl.applicantInfo.email,
                        recommend.stsCode, recommend.name, recommend.email, recommend.phoneNum,
                        appl.status
                        )
                )
                .fetch();
        JPQLQuery<Appl> countQuery = from(recommend)
                .join(appl)
                .on(
                        recommend.appl.id.eq(appl.id)
                )
                .leftJoin(recommendInformation)
                .on(
                        recommend.id.eq(recommendInformation.recommend.id)
                )
                .innerJoin(applPart)
                .on(
                        appl.part.schoolCode.eq(applPart.schoolCode),
                        appl.part.enterYear.eq(applPart.enterYear),
                        appl.part.recruitPartSeq.eq(applPart.recruitPartSeq),
                        appl.part.applPartNo.eq(applPart.applPartNo)
                )
                .innerJoin(recruitPart)
                .on(
                        appl.part.schoolCode.eq(recruitPart.recruitSchoolCode),
                        appl.part.enterYear.eq(recruitPart.enterYear),
                        appl.part.recruitPartSeq.eq(recruitPart.seq)
                )
                .innerJoin(semesterType)
                .on(
                        appl.part.semesterTypeCode.eq(semesterType.id),
                        appl.part.schoolCode.eq(semesterType.recruitSchoolCode)
                )
                .innerJoin(adms)
                .on(
                        appl.part.admissionCode.eq(adms.id),
                        appl.part.schoolCode.eq(adms.recruitSchoolCode)
                )
                .innerJoin(cors)
                .on(
                        appl.part.courseCode.eq(cors.id),
                        appl.part.schoolCode.eq(cors.recruitSchoolCode)
                )
                .innerJoin(category)
                .on(

                        appl.part.categoryCode.eq(category.id),
                        appl.part.schoolCode.eq(category.recruitSchoolCode)
                )
                .innerJoin(major)
                .on(
                        appl.part.schoolCode.eq(major.recruitSchoolCode),
                        appl.part.enterYear.eq(major.enterYear),
                        appl.part.recruitPartSeq.eq(major.recruitPartSeq),
                        appl.part.majorCode.eq(major.id)
                )
                .where(
                        eqSchoolCode(schoolCode),
                        eqEnterYear(enterYear),
                        eqRecruitPartSeq(recruitPartSeq),
                        eqAdmissionCode(admissionCode),
                        eqCourseCode(courseCode),
                        eqCategoryCode(categoryCode),
                        eqMajorCode(majorCode),
                        containsApplId(applId),
                        containsApplicantName(applicantName),
                        containsEmail(email),
                        containsApplicantId(applicantId),
                        containsProfEmail(profEmail),
                        containsProfName(profName),
                        eqRecStsCodee(recStatus)
                )
                .select(recommend.appl);
        return PageableExecutionUtils.getPage(contents, pageable, countQuery::fetchCount);

    }

    private BooleanExpression containsProfName(String profName) {
        return StringUtils.isEmpty(profName) ? null : recommend.name.containsIgnoreCase(profName);
    }
    private BooleanExpression containsProfEmail(String profEmail) {
        return StringUtils.isEmpty(profEmail) ? null : recommend.email.containsIgnoreCase(profEmail);
    }
    private BooleanExpression containsApplicantId(String applicantId) {
        return StringUtils.isEmpty(applicantId) ? null : appl.applicant.userId.contains(applicantId);
    }
    private BooleanExpression containsEmail(String email) {
        return StringUtils.isEmpty(email) ? null : appl.applicantInfo.email.containsIgnoreCase(email);
    }
    private BooleanExpression containsApplicantName(String applicantName) {
        return StringUtils.isEmpty(applicantName) ? null :
                (appl.applicantInfo.korName.contains(applicantName).or(appl.applicantInfo.engName.containsIgnoreCase(applicantName)));
    }
    private BooleanExpression containsApplId(String applId) {
        return applId == null ? null : appl.applId.contains(applId);
    }
    private BooleanExpression eqSchoolCode(String schoolCode) {
        return StringUtils.isEmpty(schoolCode) ? null : appl.part.schoolCode.eq(schoolCode);
    }
    private BooleanExpression eqEnterYear(String enterYear) {
        return StringUtils.isEmpty(enterYear) ? null : appl.part.enterYear.eq(enterYear);
    }
    private BooleanExpression eqRecruitPartSeq(Integer recruitPartSeq) {
        return recruitPartSeq == null ? null : appl.part.recruitPartSeq.eq(recruitPartSeq);
    }
    private BooleanExpression eqAdmissionCode(String admissionCode) {
        return StringUtils.isEmpty(admissionCode) ? null : appl.part.admissionCode.eq(admissionCode);
    }
    private BooleanExpression eqCourseCode(String courseCode) {
        return StringUtils.isEmpty(courseCode) ? null : appl.part.courseCode.eq(courseCode);
    }
    private BooleanExpression eqCategoryCode(String categoryCode) {
        return StringUtils.isEmpty(categoryCode) ? null : appl.part.categoryCode.eq(categoryCode);
    }
    private BooleanExpression eqMajorCode(String majorCode) {
        return StringUtils.isEmpty(majorCode) ? null : appl.part.majorCode.eq(majorCode);
    }
    private BooleanExpression eqRecStsCodee(String recStsCode) {
        return StringUtils.isEmpty(recStsCode) ? null : recommend.stsCode.eq(recStsCode);
    }
}
