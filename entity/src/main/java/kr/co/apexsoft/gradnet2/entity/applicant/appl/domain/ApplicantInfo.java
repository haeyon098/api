package kr.co.apexsoft.gradnet2.entity.applicant.appl.domain;

import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-02
 */
@Getter
@Embeddable
public class ApplicantInfo {
    @Column(name = "KOR_NAME")
    private String korName;

    @Column(name = "ENG_NAME")
    private String engName;

    @Column(name = "EMAIL")
    private String email;

    @Column(name = "RGST_BORN_DATE")
    private LocalDate registrationBornDate;

    @Column(name = "RGST_ENCR")
    private String registrationEncr;

    @Embedded
    private Address address;

    @Embedded
    private Country cntr;

    @Column(name = "GEND")
    private String gend;

    @Column(name = "TEL_NUM")
    private String telNum;

    @Column(name = "PHONE_NUM")
    private String phoneNum;

    @Column(name = "EMP_CAT")
    private String empCat;

    @Column(name = "CONTACT_CODE")
    private String contactCode;

    public void encryptRegistrationEncr(String registrationEncr) {
        this.registrationEncr = registrationEncr;
    }
    public String engKorName(){return this.getEngName()+"("+this.getKorName()+")";}

    public void initContactCode(String contactCode) {
        this.contactCode = contactCode;
    }
}
