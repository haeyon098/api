package kr.co.apexsoft.gradnet2.entity.standard.repository;

import kr.co.apexsoft.gradnet2.entity.standard.domain.Country;
import kr.co.apexsoft.gradnet2.entity.standard.projection.SearchDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface CountryRepository extends JpaRepository<Country, String> {

    @Transactional
    @Query(value = "select new kr.co.apexsoft.gradnet2.entity.standard.projection.SearchDto(c.id,c.korName,c.engName) from Country c order by c.id")
    List<SearchDto> findAllCustomList();


}
