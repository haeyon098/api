package kr.co.apexsoft.gradnet2.entity.recpart.domain;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-05
 */
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class RecruitPartId implements Serializable {
    @Id
    @Column(name = "RECR_SCHL_CODE")
    private String recruitSchoolCode;

    @Column(name = "ENTR_YEAR")
    private String enterYear;

    @Column(name = "RECR_PART_SEQ")
    private Integer seq;
}
