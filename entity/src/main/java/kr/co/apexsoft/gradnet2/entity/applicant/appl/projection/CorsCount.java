package kr.co.apexsoft.gradnet2.entity.applicant.appl.projection;

import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-20
 */
@Getter
public class CorsCount {

    private final String corsName;
    private final Long counts;

    public CorsCount(String corsName, Long counts) {
        this.corsName = corsName;
        this.counts = counts;
    }
}
