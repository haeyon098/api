package kr.co.apexsoft.gradnet2.entity.applicant.pay.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 가맹점 번호 키테이블
 *
 * @author 박지환
 * @since 2020-09-23
 */
@Entity
@Table(name="PAY_REQ")
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PayReq extends AbstractBaseEntity {

    @Id
    @Column(name = "MERCHANT_UID")
    private String merchantUid;

    @Column(name = "APPL_NO")
    private Long applNo;

    @Column(name = "AMOUNT")
    private Double amount;

    public void initMerchantUid(String merchantUid) {
        this.merchantUid = merchantUid;
    }
}
