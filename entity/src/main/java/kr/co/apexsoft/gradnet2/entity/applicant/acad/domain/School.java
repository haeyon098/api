package kr.co.apexsoft.gradnet2.entity.applicant.acad.domain;

import lombok.Getter;
import org.hibernate.annotations.Formula;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-03
 */
@Embeddable
@Getter
public class School {
    @Column(name = "SCHL_CNTR_CODE")
    private String countryCode;

    @Column(name = "ETC_SCHL_CNTR_NAME")
    private String etcCountryName;

    @Column(name = "SCHL_CODE")
    private String code;

    @Column(name = "SCHL_NAME")
    private String name;

    @Column(name = "COLL_NAME")
    private String collegeName;

    @Column(name = "MAJ_NAME")
    private String majorName;

    @Column(name = "DEGR_NO")
    private String degreeNo;

    @Formula("IF(SCHL_CODE = '999', SCHL_NAME, (SELECT S.SCHL_KOR_NAME FROM SCHL S WHERE S.SCHL_CODE = SCHL_CODE))")
    private String korName;

    @Formula("IF(SCHL_CODE = '999', SCHL_NAME, (SELECT S.SCHL_ENG_NAME FROM SCHL S WHERE S.SCHL_CODE = SCHL_CODE))")
    private String engName;
}
