package kr.co.apexsoft.gradnet2.entity.applpart.domain;

import kr.co.apexsoft.gradnet2.entity._common.AbstractBaseEntity;
import lombok.*;

import javax.persistence.*;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-30
 */
@Entity
@Table(name="APPL_PART")
@Getter
@IdClass(ApplPartId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Builder
public class ApplPart extends AbstractBaseEntity {
    @Id
    private String schoolCode;

    @Id
    private String enterYear;

    @Id
    private Integer recruitPartSeq;

    @Id
    private Long applPartNo;

    @Column(name = "SMST_TYPE_CODE")
    private String semesterTypeCode;

    @Column(name = "ADMS_CODE")
    private String admissionCode;

    @Column(name = "CORS_CODE")
    private String courseCode;

    @Column(name = "CATEGORY_CODE")
    private String categoryCode;

    @Column(name = "MAJ_CODE")
    private String majorCode;

    @Column(name = "APPL_PART_YN")
    private Boolean used;

    @Embedded
    private Fee fee;

    @Column(name = "REMK")
    private String remk;

}
