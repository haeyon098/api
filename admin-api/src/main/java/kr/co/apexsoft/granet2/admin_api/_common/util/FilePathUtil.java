package kr.co.apexsoft.granet2.admin_api._common.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FilePathUtil {

    private static final String SLASH_WINDOW = "/";
    private static final String SLASH_UNIX = "\\";
    private static final String DOUBLE_SLASH = "//";
    private static final String ENCODED_AMP = "&amp;";
    private static final String RECOVERED_AMP = "&";
    private static final String ENCODED_SINGLEQUOTE = "&#39;";
    private static final String RECOVERED_SINGLEQUOTE = "'";

    private FilePathUtil() {
    }

    /**
     * 추천서 zip 파일명
     *
     * @param enterYear
     * @param seq
     * @param applId
     * @return
     */
    public static String getRecZipFileName(String enterYear, Integer seq, String applId) {
        return new StringBuilder().append("rec_")
                .append(enterYear).append("_")
                .append(seq).append("_")
                .append(applId).append(".zip").toString();
    }

    /**
     * 전체 zip 파일패스 + 파일명
     *
     * @param enterYear
     * @param seq
     * @param applId
     * @return
     */
    public static String getAllZipFileName(String enterYear, Integer seq, String applId) {
        return new StringBuilder()
                .append(enterYear).append("_")
                .append(seq).append("_")
                .append(applId).append(".zip").toString();
    }
}
