package kr.co.apexsoft.granet2.admin_api.download.controller;

import kr.co.apexsoft.gradnet2.common.file.FileDto;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.CompleteDocumentProjection;
import kr.co.apexsoft.granet2.admin_api.download.dto.CompleteDownloadCondition;
import kr.co.apexsoft.granet2.admin_api.download.service.ExcelService;
import kr.co.apexsoft.granet2.admin_api.download.service.ZipDownloadService;
import kr.co.apexsoft.granet2.admin_api.download.service.complete.CompleteDownloadService;
import kr.co.apexsoft.granet2.admin_api.search.dto.ApplSearchCondition;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-04-13
 */
@RequestMapping("/admin/download")
@RestController
@RequiredArgsConstructor
public class DownloadController {
    private final ZipDownloadService zipDownloadService;

    private final ExcelService excelService;


    /**
     * 추천서 zip 다운로드
     *
     * @param applNo
     */
    @GetMapping("/rec/{applNo}")
    public ResponseEntity<InputStreamResource> makeRecommendationZip(@PathVariable("applNo") Long applNo) {
        FileDto recommendationZip = zipDownloadService.makeRecommendationZip(applNo);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=" + recommendationZip.getFileName())
                .body(recommendationZip.getResource());
    }

    /**
     * 전체 zip 다운로드
     *
     * @param applNo
     */
    @GetMapping("/all/{applNo}")
    public ResponseEntity<InputStreamResource> makeAllZip(@PathVariable("applNo") Long applNo) {
        FileDto zip = zipDownloadService.makeRenameFileAllZip(applNo);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=" + zip.getFileName())
                .body(zip.getResource());
    }


    /**
     * 지원자 정보 엑셀 다운로드
     *
     * @param condition
     * @return
     */
    @PostMapping("/excel/applicants")
    public ResponseEntity<InputStreamResource> applicantsExcelFileDown(@RequestBody ApplSearchCondition condition) {
        FileDto excel = excelService.applicantsExcelFileDown(condition);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + excel.getFileName() + ".xlsx")
                .body(excel.getResource());
    }



    /**
     * 지원자 현황 엑셀 다운로드(for KDI - 아이디 기준 중복제외)
     *
     * @param condition
     * @return
     */
    @PostMapping("/excel/applicantsListExcelFileDown")
    public ResponseEntity<InputStreamResource> applicantsAllListExcelFileDown(@RequestBody ApplSearchCondition condition) {
        FileDto excel = excelService.applicantsAllListExcelFileDown(condition);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + excel.getFileName() + ".xlsx")
                .body(excel.getResource());
    }


    /**
     * 지원완료 엑셀 데이터 전달
     * @param condition
     * @return
     */
    @PostMapping("/excel/applicants/complete")
    public ResponseEntity<InputStreamResource> applicantsExcelAcademy(@RequestBody CompleteDownloadCondition condition) {
        FileDto excel = excelService.applicantsCompleteFileDown(condition);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + excel.getFileName() + ".xlsx")
                .body(excel.getResource());
    }

}
