package kr.co.apexsoft.granet2.admin_api.search.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplDetailProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplNoZipPrintedProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.CompleteDocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.DocumentEtcNameProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository.RecommedRepository;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SysCommCodeRepository;
import kr.co.apexsoft.granet2.admin_api.search.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.lang.String;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Service
@Transactional
@RequiredArgsConstructor
public class ApplicationSearchServiceImpl implements ApplicationSearchService {

    private final ApplRepository applRepository;

    private final RecommedRepository recommedRepository;

    @Autowired
    private DocumentRepository documentRepository;

    private final SysCommCodeRepository sysCommCodeRepository;

    @Transactional(readOnly = true)
    @Override
    public ApplicantDetailOut getApplicantDetail(Long applicationId) {
        return ApplicantDetailOut.from(applRepository.findById(applicationId)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Appl ID [%d] 없음", applicationId))));
    }

    @Transactional(readOnly = true)
    @Override
    public ApplicationSimpleOut getApplicationSimple(Long applicationId) {
        return ApplicationSimpleOut.from(applRepository.findById(applicationId)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Appl ID [%d] 없음", applicationId))));
    }

    @Transactional(readOnly = true)
    @Override
    public ApplicationDetailOut getApplicationDetail(Long applNo) {
        ApplDetailProjection ap = applRepository.findApplDetailById(applNo)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Appl ID [%d] 없음", applNo)));

        //        List<ApplDocProjection> dp = applRepository.findApplDocsById(applNo);
        List<DocumentEtcNameProjection> docList = documentRepository.findUploadDocEtcName(ap.getApplNo(), DocItem.ETC.getValue(),
                 DocItem.APPL_FORM.getValue(), DocItem.SLIP.getValue(),DocItem.ZIP.getValue());

        List<CompleteDocumentProjection> completeDocuments = documentRepository.findCompleteDocuments(applNo, DocItem.APPL_FORM.getValue(), DocItem.SLIP.getValue(),DocItem.ZIP.getValue(),DocItem.EMAIL_REC.getValue());

        List<SysCommCode>  recStsCodes =  sysCommCodeRepository.findAllByCodeGrpAndUsedTrueOrderByCodeAsc("REC_STS");
        List<Recommend> recommendList = recommedRepository.findByAppl_Id(applNo);
        List<RecSimpleOut> recSimpleOuts = new ArrayList<>();
        for(Recommend recommend :recommendList ) {
            RecSimpleOut recSimpleOut = new RecSimpleOut(recommend, recStsCodes);
            recSimpleOuts.add(recSimpleOut);
        }


        return ApplicationDetailOut.from(ap, docList, completeDocuments, recSimpleOuts);
    }

    @Transactional(readOnly = true)
    @Override
    public Page<ApplicationSimpleForListOut> getApplicationSimplesForList(ApplSearchCondition condition, Pageable pageable) {
        return applRepository.findAllBySearchCondition(
                condition.getSchoolCode(),
                condition.getEnterYear(),
                condition.getRecruitPartSeq(),
                condition.getAdmissionCode(),
                condition.getCourseCode(),
                condition.getCategoryCode(),
                condition.getMajorCode(),
                condition.getApplId(),
                condition.getApplNo(),
                condition.getApplicantName(),
                condition.getEmail(),
                condition.getPhoneNumber(),
                condition.getApplicantId(),
                condition.getStatusCode(),
                pageable)
                .map(ApplicationSimpleForListOut::from);
    }

    @Transactional
    @Override
    public ApplZipPrintedOut patchZipPrinted(Long applNo, Boolean zipPrinted) {

        applRepository.updateApplNoZipPrintedById(applNo, zipPrinted);

        ApplNoZipPrintedProjection applNoZipPrinted = applRepository.findApplNoZipPrintedById(applNo)
                .orElseThrow(() -> new IllegalArgumentException(String.format("Appl ID [%d] 없음", applNo)));

        return new ApplZipPrintedOut(applNoZipPrinted.getApplNo(), applNoZipPrinted.getZipPrinted());
    }
}
