package kr.co.apexsoft.granet2.admin_api.basic.service;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.*;

import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-01
 */
public interface BasicService {

    List<StatusAndCount> getReceiptStatsSimpleOut(String schoolCode, String enterYear, Integer recruitPartSeq);

    RecruitOut getAllRecruitParts(User user);
    List<CodeNameOut> getAllAdmissions(String schoolCode, String enterYear, Integer recruitPartSeq);
    List<CodeNameOut> getAllCourses(String schoolCode, String enterYera, Integer recruitPartSeq, String admissionCode);
    List<CodeNameOut> getAllMajors(String schoolCode, String enterYera, Integer recruitPartSeq, String admissionCode, String courseCode, String categoryCode);
    List<CodeNameOut> getAllCategories(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode, String courseCode);
    List<CodeNameOut> getAllApplStatuses(String schoolCode);

    List<CodeNameOut> getAllRecStatuses();

    DatesAndCounts getReceiptCountVariations(String schoolCode, String enterYear, Integer recruitPartSeq);

    List<CompletedApplCountByCors> getCompletedApplCountByCors(String schoolCode, String enterYear, Integer recruitPartSeq);
    List<ApplStatsOut> getApplStatsByAdmission(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode);
}
