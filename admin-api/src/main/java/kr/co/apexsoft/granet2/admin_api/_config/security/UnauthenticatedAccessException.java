package kr.co.apexsoft.granet2.admin_api._config.security;

/**
 * created by hanmomhanda@gmail.com
 */
public class UnauthenticatedAccessException extends RuntimeException {

    public UnauthenticatedAccessException() {
        super("인증 받지 못한 사용자입니다.");
    }

    public UnauthenticatedAccessException(String message) {
        super(message);
    }

    public UnauthenticatedAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthenticatedAccessException(Throwable cause) {
        super(cause);
    }

    protected UnauthenticatedAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
