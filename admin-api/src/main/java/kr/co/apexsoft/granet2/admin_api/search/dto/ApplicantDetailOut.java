package kr.co.apexsoft.granet2.admin_api.search.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.ApplicantInfo;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.GeneralInfo;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicantDetailOut {

    private String fullName;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    private String gender;
    private String telNumber;
    private String phoneNumber;
    private String address;
    private String email;
    private String emergencyContactName;
    private String emergencyContactPhoneNumber;


    public static ApplicantDetailOut from(Appl appl) {
        ApplicantInfo applicantInfo = appl.getApplicantInfo();
        GeneralInfo generalInfo = appl.getGeneralInfo();
        return new ApplicantDetailOut(
                applicantInfo.getKorName(),
                applicantInfo.getEngName(),
                applicantInfo.getRegistrationBornDate(),
                applicantInfo.getGend(),
                applicantInfo.getTelNum(),
                applicantInfo.getPhoneNum(),
                applicantInfo.getAddress().getFullAddress(),
                applicantInfo.getEmail(),
                generalInfo.getEmergencyName(),
                generalInfo.getEmergencyTelNum()
        );
    }
}
