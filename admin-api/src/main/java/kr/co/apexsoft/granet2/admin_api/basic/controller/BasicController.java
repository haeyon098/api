package kr.co.apexsoft.granet2.admin_api.basic.controller;

import kr.co.apexsoft.granet2.admin_api._config.security.UserPrincipal;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.*;
import kr.co.apexsoft.granet2.admin_api.basic.service.BasicService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-01
 */
@RequestMapping
@RestController
@RequiredArgsConstructor
public class BasicController {

    private final BasicService basicService;

    @RequestMapping("/admin/basic/receipt-status")
    public ResponseEntity<List<StatusAndCount>> getReceiptStatsSimpleOut(@RequestParam("schoolCode") String schoolCode,
                                                                         @RequestParam("enterYear") String enterYear,
                                                                         @RequestParam("recruitPartSeq") Integer recruitPartSeq) {
        return ResponseEntity.ok(basicService.getReceiptStatsSimpleOut(schoolCode, enterYear, recruitPartSeq));
    }

    @RequestMapping("/admin/recruit-parts")
    public ResponseEntity<RecruitOut> getRecruitParts(@AuthenticationPrincipal UserPrincipal userPrincipal){
        return ResponseEntity.ok(basicService.getAllRecruitParts(userPrincipal.getUser()));
    }



    @RequestMapping("/admin/basic/admissions")
    public ResponseEntity<List<CodeNameOut>> getAdmissions(@RequestParam("schoolCode") String schoolCode,
                                                           @RequestParam("enterYear") String enterYear,
                                                           @RequestParam("recruitPartSeq") Integer recruitPartSeq) {
        return ResponseEntity.ok(basicService.getAllAdmissions(schoolCode, enterYear, recruitPartSeq));
    }

    @RequestMapping("/admin/basic/courses")
    public ResponseEntity<List<CodeNameOut>> getCourses(@RequestParam("schoolCode") String schoolCode,
                                                        @RequestParam("enterYear") String enterYear,
                                                        @RequestParam("recruitPartSeq") Integer recruitPartSeq,
                                                        @RequestParam("admissionCode") String admissionCode) {
        return ResponseEntity.ok(basicService.getAllCourses(schoolCode, enterYear, recruitPartSeq, admissionCode));
    }

    @RequestMapping("/admin/basic/categories")
    public ResponseEntity<List<CodeNameOut>> getCategories(@RequestParam("schoolCode") String schoolCode,
                                                           @RequestParam("enterYear") String enterYear,
                                                           @RequestParam("recruitPartSeq") Integer recruitPartSeq,
                                                           @RequestParam("admissionCode") String admissionCode,
                                                           @RequestParam("courseCode") String courseCode) {
        return ResponseEntity.ok(basicService.getAllCategories(schoolCode, enterYear, recruitPartSeq, admissionCode, courseCode));
    }

    @RequestMapping("/admin/basic/majors")
    public ResponseEntity<List<CodeNameOut>> getMajors(@RequestParam("schoolCode") String schoolCode,
                                                       @RequestParam("enterYear") String enterYear,
                                                       @RequestParam("recruitPartSeq") Integer recruitPartSeq,
                                                       @RequestParam("admissionCode") String admissionCode,
                                                       @RequestParam("courseCode") String courseCode,
                                                       @RequestParam("categoryCode") String categoryCode) {
        return ResponseEntity.ok(basicService.getAllMajors(schoolCode, enterYear, recruitPartSeq, admissionCode, courseCode, categoryCode));
    }

    @RequestMapping("/admin/basic/appl-statuses")
    public ResponseEntity<List<CodeNameOut>> getApplStatuses(@RequestParam("schoolCode") String schoolCode) {
        return ResponseEntity.ok(basicService.getAllApplStatuses(schoolCode));
    }

    @RequestMapping("/admin/basic/rec-statuses")
    public ResponseEntity<List<CodeNameOut>> getAllRecStatuses() {
        return ResponseEntity.ok(basicService.getAllRecStatuses());
    }

    @RequestMapping("/admin/basic/receipt-variations")
    public ResponseEntity<DatesAndCounts> getReceiptVariations(@RequestParam("schoolCode") String schoolCode,
                                                               @RequestParam("enterYear") String enterYear,
                                                               @RequestParam("recruitPartSeq") Integer recruitPartSeq) {
        return ResponseEntity.ok(basicService.getReceiptCountVariations(schoolCode, enterYear, recruitPartSeq));
    }

    @RequestMapping("/admin/basic/completed-appl-count-by-course")
    public ResponseEntity<List<CompletedApplCountByCors>> getCompletedApplCountByCourse(@RequestParam("schoolCode") String schoolCode,
                                                                                        @RequestParam("enterYear") String enterYear,
                                                                                        @RequestParam("recruitPartSeq") Integer recruitPartSeq) {
        return ResponseEntity.ok(basicService.getCompletedApplCountByCors(schoolCode, enterYear, recruitPartSeq));
    }

    @RequestMapping("/admin/basic/appl-stats-by-admission")
    public ResponseEntity<List<ApplStatsOut>> getApplStatsByAdmission(@RequestParam("schoolCode") String schoolCode,
                                                                      @RequestParam("enterYear") String enterYear,
                                                                      @RequestParam("recruitPartSeq") Integer recruitPartSeq,
                                                                      @RequestParam("admissionCode") String admissionCode) {
        return ResponseEntity.ok(basicService.getApplStatsByAdmission(schoolCode, enterYear, recruitPartSeq, admissionCode));
    }
}
