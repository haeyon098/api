package kr.co.apexsoft.granet2.admin_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages =
        {"kr.co.apexsoft.granet2.admin_api", "kr.co.apexsoft.gradnet2.common"})
@EntityScan("kr.co.apexsoft.gradnet2.entity")
@EnableJpaRepositories("kr.co.apexsoft.gradnet2.entity")
public class AdminApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdminApiApplication.class, args);
    }

}
