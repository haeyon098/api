package kr.co.apexsoft.granet2.admin_api.user.exception;


import kr.co.apexsoft.granet2.admin_api._support.exception.CustomException;

/**
 * 사용자 잠김 예외처리
 */
public class UserLockException extends CustomException {

    public UserLockException() {
        super();
    }

    public UserLockException(String warningMessage) {
        super(warningMessage);
    }

    public UserLockException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
