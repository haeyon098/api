package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CodeNameOut {

    private final String code;
    private final String name;
    private final String engName;


    public static CodeNameOut from(SysCommCode s) {
        return new CodeNameOut(s.getCode(), s.getCodeKrValue(), s.getCodeEnValue());
    }

    public static CodeNameOut from(AdmsProjection p) {
        return new CodeNameOut(p.getCode(), p.getKorName(), p.getEngName());
    }

    public static CodeNameOut from(Appl.Status s) {
        return new CodeNameOut(s.name(), s.getKorName(), s.getEngName());
    }

    public static CodeNameOut from(RecruitPart rp) {
        return new CodeNameOut(String.valueOf(rp.getSeq()), rp.getKorName(), rp.getEngName());
    }

    public static CodeNameOut of(String code, String name, String engName) {
        return new CodeNameOut(code, name, engName);
    }
}
