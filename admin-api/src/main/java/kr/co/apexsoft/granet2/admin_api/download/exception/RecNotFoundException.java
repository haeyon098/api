package kr.co.apexsoft.granet2.admin_api.download.exception;


import kr.co.apexsoft.granet2.admin_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class RecNotFoundException extends CustomException {

    public RecNotFoundException() {
        super();
    }

    public RecNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public RecNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public RecNotFoundException(String warningMessage, Long applNo) {
        super("[ applNo : "+applNo+" ]", warningMessage);
    }
}
