package kr.co.apexsoft.granet2.admin_api.search.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplicationSimpleForListProjection;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.CodeNameOut;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-18
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationSimpleForListOut {

    private String applicantId;
    private Long applNo;
    private String applId;
    private CodeNameOut recruitPart;
    private CodeNameOut admission;
    private CodeNameOut course;
    private CodeNameOut category;
    private CodeNameOut major;
    private String applicantKorName;
    private String applicantEngName;
    private String phoneNumber;
    private String email;
    private String dateOfBirth;
    private Boolean zipPrinted;
    private CodeNameOut status;
    private Long recLetterCount;
    private Long recLetterReqCount;

    public static ApplicationSimpleForListOut from(ApplicationSimpleForListProjection projection) {
        String dateOfBirth = projection.getDateOfBirth() == null ? "" : projection.getDateOfBirth().toString();
        return new ApplicationSimpleForListOut(
                projection.getApplicantId(),
                projection.getApplNo(),
                projection.getApplId(),
                CodeNameOut.of(String.valueOf(projection.getRecruitPartSeq()), projection.getRecruitPartName(), projection.getRecruitPartEngName()),
                CodeNameOut.of(projection.getAdmissionCode(), projection.getAdmissionName(), projection.getAdmissionEngName()),
                CodeNameOut.of(projection.getCourseCode(), projection.getCourseName(), projection.getCourseEngName()),
                CodeNameOut.of(projection.getCategoryCode(), projection.getCategoryName(), projection.getCategoryEngName()),
                CodeNameOut.of(projection.getMajorCode(), projection.getMajorName(), projection.getMajorEngName()),
                projection.getApplicantKorName(),
                projection.getApplicantEngName(),
                projection.getPhoneNumber(),
                projection.getEmail(),
                dateOfBirth,
                projection.getZipPrinted(),
                CodeNameOut.of(projection.getStatus().name(), projection.getStatus().getKorName(), projection.getStatus().getEngName()),
                projection.getRecLetterCount(),
                projection.getRecLetterReqCount()
        );
    }
}
