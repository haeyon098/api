package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplStatsProjection;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-22
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplStatsOut {

    private String recruitPartName;
    private String admissionName;
    private String courseName;
    private String majorName;
    private String categoryName;
//    private Long masterCount;
//    private Long doctorCount;
    private Long totalCount;

    public static ApplStatsOut from(ApplStatsProjection p) {
        return new ApplStatsOut(
                p.getRecruitPartName(),
                p.getAdmissionName(),
                p.getCourseName(),
                p.getMajorName(),
                p.getCategoryName(),
                p.getTotalCount()
        );
    }
}
