package kr.co.apexsoft.granet2.admin_api._config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.apexsoft.gradnet2.entity.log.domain.PersonalInfoAccessLog;
import kr.co.apexsoft.gradnet2.entity.log.repository.PersonalInfoAccessLogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-11-16
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class AdminInterceptor implements HandlerInterceptor {
    private final ObjectMapper objectMapper = new ObjectMapper();

    private final PersonalInfoAccessLogRepository personalInfoAccessLogRepository;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws JsonProcessingException {
        personalInfoAccessLogRepository.save(new PersonalInfoAccessLog(request.getUserPrincipal().getName(),
                request.getRemoteHost(), request.getMethod(), request.getRequestURI(), objectMapper.writeValueAsString(request.getParameterMap())));
    }

}
