package kr.co.apexsoft.granet2.admin_api.download.service.complete;

import kr.co.apexsoft.gradnet2.common.file.FileDto;
import kr.co.apexsoft.granet2.admin_api._common.ApexSchoolSupportable;
import kr.co.apexsoft.granet2.admin_api.download.dto.CompleteDownloadCondition;

public interface CompleteDownloadService extends ApexSchoolSupportable {

    FileDto download(CompleteDownloadCondition condition);

}
