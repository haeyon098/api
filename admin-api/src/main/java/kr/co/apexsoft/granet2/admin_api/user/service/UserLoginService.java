package kr.co.apexsoft.granet2.admin_api.user.service;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import kr.co.apexsoft.granet2.admin_api._config.security.UnauthenticatedAccessException;
import kr.co.apexsoft.granet2.admin_api._config.security.UserPrincipal;
import kr.co.apexsoft.granet2.admin_api._config.security.jwt.JwtAuthenticationResponse;
import kr.co.apexsoft.granet2.admin_api._config.security.jwt.JwtTokenProvider;
import kr.co.apexsoft.granet2.admin_api.user.dto.UserDto;
import kr.co.apexsoft.granet2.admin_api.user.exception.InvalidUserException;
import kr.co.apexsoft.granet2.admin_api.user.exception.UserLockException;
import kr.co.apexsoft.granet2.admin_api.user.exception.UserNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-04-03
 */
@Service
@RequiredArgsConstructor
public class UserLoginService {
    @NonNull
    private UserRepository userRepository;

    @NonNull
    private AuthenticationManager authenticationManager;

    @NonNull
    private JwtTokenProvider tokenProvider;

    @Value("${login.failMaxCnt}")
    private Integer failMaxCnt;

    public JwtAuthenticationResponse login(String userId, String password) {
        UserPrincipal userPrincipal = null;
        Authentication authentication = null;

        try {
            authentication = this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userId,
                            password
                    )
            );
        } catch (InternalAuthenticationServiceException e){ // 존재하지 않는 사용자
            throw new UserNotFoundException("존재하지 않는 회원입니다.");
        } catch(DisabledException e) {  // 유효한 회원이 아님
            throw new InvalidUserException("로그인을 실패했습니다.");
        } catch (BadCredentialsException e) {
            this.failPassword(userId);
            throw new BadCredentialsException("비밀번호를 틀렸습니다.");
        } catch(LockedException e) {    // 계정 잠김
            throw new UserLockException(userId, "추가 인증이 필요합니다.");
        } catch (UnauthenticatedAccessException e) {
            throw new UnauthenticatedAccessException();
        }

        userPrincipal = (UserPrincipal) authentication.getPrincipal();

        if(!userPrincipal.getUser().isSchoolAdmin()) throw new UnauthenticatedAccessException();

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = this.tokenProvider.generateToken(authentication);

        return new JwtAuthenticationResponse(jwt, userId, userPrincipal.getRoles());
    }

    /**
     * 비밀번호 실패 횟수 확인 (lock)
     * @param userId
     */
    @Transactional
    void failPassword(String userId) {
        this.userRepository.updateFailCnt(userId);
        this.userRepository.updateLock(userId, failMaxCnt);
    }

    @Transactional
    public UserDto unlockUser(String userId) {
        User user = this.userRepository.findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(userId, "존재하지 않는 회원입니다."));
        user.unlock();
        return UserDto.builder()
                .id(user.getId())
                .userId(user.getUserId())
                .email(user.getEmail())
                .build();
    }
}
