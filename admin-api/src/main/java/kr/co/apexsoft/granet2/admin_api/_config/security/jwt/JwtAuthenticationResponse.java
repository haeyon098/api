package kr.co.apexsoft.granet2.admin_api._config.security.jwt;

import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * created by hanmomhanda@gmail.com
 */
@Getter
public class JwtAuthenticationResponse {

    private final String tokenType = "Bearer";

    private String accessToken;

    private String userId;

    private Set<Role> roles = new HashSet<>();

    public JwtAuthenticationResponse(String accessToken,
                                     String userId,
                                     Set<Role> roles) {
        this.accessToken = accessToken;
        this.userId = userId;
        this.roles = roles;
    }

    private Set<String> getRoleSet(Collection<? extends GrantedAuthority> roles) {
        return roles.stream().map(auth -> auth.getAuthority()).collect(Collectors.toSet());
    }
}
