package kr.co.apexsoft.granet2.admin_api.download.service;

import kr.co.apexsoft.gradnet2.common.file.FileDto;
import kr.co.apexsoft.gradnet2.common.file.FileService;
import kr.co.apexsoft.gradnet2.common.file.FileUtils;
import kr.co.apexsoft.gradnet2.common.zip.ZIPService;
import kr.co.apexsoft.gradnet2.common.zip.domain.ZipInfo;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplInfoProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.ZipInDocumentsProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.granet2.admin_api._common.util.FilePathUtil;
import kr.co.apexsoft.granet2.admin_api.download.exception.RecNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-04-13
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ZipDownloadService {
    private final ApplRepository applRepository;
    private final DocumentRepository documentRepository;
    private final FileService fileService;
    private final ZIPService zipService;
    @Value("${file.baseDir}")
    private String FILE_BASE_DIR;

    /**
     * 파일 이름 변경 후 zip
     *
     * @param applNo
     * @return
     */
    public FileDto makeRenameFileAllZip(Long applNo) {
        ApplInfoProjection part = applRepository.findMyApplInfo(applNo);

        // 수험표, 사진, 지원서, 제출파일 조회
        List<ZipInDocumentsProjection> documents = documentRepository.findZipInDocuments(applNo, part.getSchoolCode(),
                part.getEnterYear(), part.getRecruitPartSeq(), DocItem.ZIP.getValue(), DocItem.EMAIL_REC.getValue());

        List<File> files = new ArrayList<>();
        for (ZipInDocumentsProjection document : documents) {
            files.add(fileService.getFileFromFileServer(applNo, document.getFilePath(), document.getNewFilePath()));
        }

        // zip 만들기
        String zipFileName = FilePathUtil.getAllZipFileName(part.getEnterYear(), part.getRecruitPartSeq(), part.getApplId());
        ZipInfo zipInfo = new ZipInfo(part.getApplNo(), part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq(),
                FILE_BASE_DIR, zipFileName);
        File zip = zipService.getZippedFileFromPdf(files, zipInfo);

        InputStreamResource resource = FileUtils.retrieveInputStreamResource(zip);
        zip.delete();

        return new FileDto(zipFileName, resource);
    }

    /**
     * 추천서 다운로드
     *
     * @param applNo
     * @return
     */
    public FileDto makeRecommendationZip(Long applNo) {
        // APPL 정보 & 추천서 문서 조회
        List<Document> recDocuments = documentRepository.findByAppl_IdAndItemCode(applNo, DocItem.EMAIL_REC.getValue());
        if (recDocuments.isEmpty())
            throw new RecNotFoundException("추천서가 존재하지 않습니다.", applNo);
        ApplInfoProjection part = applRepository.findMyApplInfo(applNo);

        // 파일 다운로드
        List<File> files = fileService.getFileListFromFileServer(applNo, recDocuments.stream().map(d -> d.getFilePath()).toArray(size -> new String[size]));

        // 추천서 zip 만들기
        String zipFileName = FilePathUtil.getRecZipFileName(part.getEnterYear(), part.getRecruitPartSeq(), part.getApplId());
        ZipInfo zipInfo = new ZipInfo(part.getApplNo(), part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq(),
                FILE_BASE_DIR, zipFileName);
        File zip = zipService.getZippedFileFromPdf(files, zipInfo);

        InputStreamResource resource = FileUtils.retrieveInputStreamResource(zip);
        zip.delete();

        return new FileDto(zipFileName, resource);
    }

    /**
     * ZIP파일 다운로드
     * 수험표 ,지원서, 제출ZIP
     *
     * @param applNo
     * @return
     */
    public FileDto makeAllZip(Long applNo) {
        // 수험표, 사진, 지원서, 제출 묶음 ZIP 조회
        ApplInfoProjection part = applRepository.findMyApplInfo(applNo);
        List<Document> documents = documentRepository.findByAppl_IdAndItemCodeIn(applNo,
                DocItem.PHOTO.getValue(), DocItem.SLIP.getValue(), DocItem.APPL_FORM.getValue(), DocItem.ZIP.getValue());


        List<Document> otherDocuments = new ArrayList<>();
        Document zipDocument = null;
        for (Document document : documents) {
            if (document.getItemCode().equals(DocItem.ZIP.getValue())) {
                zipDocument = document;
            } else {
                otherDocuments.add(document);
            }
        }

        // 파일 다운로드
        List<File> files = fileService.getFileListFromFileServer(applNo, otherDocuments.stream().map(d -> d.getFilePath()).toArray(size -> new String[size]));
        File zipFile = fileService.getFileFromFileServer(applNo, zipDocument.getFilePath());

        // zip 만들기
        File zip = zipService.appendFilesToZipFile(files, zipFile);
        String zipFileName = FilePathUtil.getAllZipFileName(part.getEnterYear(), part.getRecruitPartSeq(), part.getApplId());
        ZipInfo zipInfo = new ZipInfo(part.getApplNo(), part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq(),
                FILE_BASE_DIR, zipFileName);
        File newZipFile = new File(zipInfo.getTargeDir() + "/" + zipFileName);
        if (zip.exists()) zip.renameTo(newZipFile); // 파일명 변경

        InputStreamResource resource = FileUtils.retrieveInputStreamResource(newZipFile);
        newZipFile.delete();
        return new FileDto(zipFileName, resource);
    }

}
