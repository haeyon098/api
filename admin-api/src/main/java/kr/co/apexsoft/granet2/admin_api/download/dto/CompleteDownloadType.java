package kr.co.apexsoft.granet2.admin_api.download.dto;

/**
 * 지원완료 다운로드 구분 
 * 추가필요
 */
public enum CompleteDownloadType {

    MASTER("마스터정보"),
    ACAD("학력정보"),
    CAREER("경력정보"),
    LANG("어학정보"),
    SUBMIT_FILE("제출파일리스트"),
    REC_INFO("추천인정보"),
    REC_ANSWER("추천서답변"),
    KDI_SURVEY("KDIS_설문"),
    KDI_ALUMNI("KDIS_동문");

    private final String name; //프론트 버튼명 사용

    private CompleteDownloadType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
