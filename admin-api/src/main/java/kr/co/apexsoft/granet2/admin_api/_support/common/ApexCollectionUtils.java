package kr.co.apexsoft.granet2.admin_api._support.common;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-07
 */
public class ApexCollectionUtils {

    private ApexCollectionUtils() {}

    public static Map<LocalDate, Long> getZeroFilledMap(Map<LocalDate, Long> map) {
        TreeMap<LocalDate, Long> inMap = new TreeMap<>(map);
        TreeMap<LocalDate, Long> resultMap = new TreeMap<>();

        Set<LocalDate> localDates = inMap.keySet();
        int inMapCount = 0;
        for (LocalDate localDate : localDates) {
            inMapCount++;
            resultMap.put(localDate, inMap.get(localDate));
            for (LocalDate nextDate = localDate.plusDays(1L) ; !inMap.containsKey(nextDate) && inMapCount < inMap.size() ; nextDate = nextDate.plusDays(1L)) {
                resultMap.put(nextDate, 0L);
            }
        }
        return resultMap;
    }

    public static Map<LocalDate, Long> getZeroFilledMap(LocalDate start, LocalDate end, Map<LocalDate, Long> map) {
        LocalDate minInMap = null;
        LocalDate maxInMap = null;

        LocalDate realStart = null;
        LocalDate realEnd = null;

        if ( map.size() > 0 ){
            minInMap = map.keySet().stream().min(Comparator.naturalOrder()).orElseThrow(() -> new IllegalArgumentException("해당 회차의 데이터가 존재하지 않습니다."));
            maxInMap = map.keySet().stream().max(Comparator.naturalOrder()).orElseThrow(() -> new IllegalArgumentException("해당 회차의 데이터가 존재하지 않습니다."));

             realStart = start.isBefore(minInMap) ? start : minInMap;
             realEnd = end.isAfter(maxInMap) ? end : maxInMap;
        } else {
             realStart = start;
             realEnd = end;
        }

        Map<LocalDate, Long> mergedMap = new TreeMap<>(map);
        mergedMap.put(realStart, map.getOrDefault(realStart, 0L));
        mergedMap.put(realEnd, map.getOrDefault(realEnd, 0L));

        return getZeroFilledMap(mergedMap);
    }
}
