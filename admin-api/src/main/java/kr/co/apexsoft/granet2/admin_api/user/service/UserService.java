package kr.co.apexsoft.granet2.admin_api.user.service;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import kr.co.apexsoft.granet2.admin_api._config.security.UnauthenticatedAccessException;
import kr.co.apexsoft.granet2.admin_api._config.security.UserPrincipal;
import kr.co.apexsoft.granet2.admin_api._config.security.jwt.JwtAuthenticationResponse;
import kr.co.apexsoft.granet2.admin_api._config.security.jwt.JwtTokenProvider;
import kr.co.apexsoft.granet2.admin_api.user.dto.UserDto;
import kr.co.apexsoft.granet2.admin_api.user.exception.InvalidUserException;
import kr.co.apexsoft.granet2.admin_api.user.exception.UserLockException;
import kr.co.apexsoft.granet2.admin_api.user.exception.UserNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService {
    @NonNull
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public User findById(@NonNull Long id) {
        return this.userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("존재하지 않는 회원입니다.", id));
    }

    @Transactional(readOnly = true)
    public User findByUserId(@NonNull String userId) {
        return this.userRepository.findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(userId, "존재하지 않는 회원입니다."));
    }


}
