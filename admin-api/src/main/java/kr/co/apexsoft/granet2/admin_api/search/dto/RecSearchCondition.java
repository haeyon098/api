package kr.co.apexsoft.granet2.admin_api.search.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * @author 박지환
 * created on 2020-08-04
 */
@Getter
@Setter
public class RecSearchCondition {

    private String schoolCode;
    private String enterYear;
    private Integer recruitPartSeq;
    private String semesterTypeCode;
    private String admissionCode;
    private String courseCode;
    private String categoryCode;
    private String majorCode;
    private String applId;
    private String applicantName;
    private String email;
    private String applicantId;
    private String recStatus;
    private String profName;
    private String profPhoneNumber;
    private String profEmail;
}
