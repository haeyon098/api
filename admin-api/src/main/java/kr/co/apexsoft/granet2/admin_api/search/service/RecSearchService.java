package kr.co.apexsoft.granet2.admin_api.search.service;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendSimpleForListProjection;
import kr.co.apexsoft.granet2.admin_api.search.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author 박지환
 * created on 2020-08-04
 */
public interface RecSearchService {

    Page<RecSimpleForListOut> getRecSimplesForList(RecSearchCondition condition, Pageable pageable);

}
