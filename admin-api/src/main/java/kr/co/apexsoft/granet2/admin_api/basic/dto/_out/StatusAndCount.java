package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-21
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class StatusAndCount {

    private final String statusName;
    private final Long count;

    public static StatusAndCount of(String statusName, Long count) {
        return new StatusAndCount(statusName, count);
    }
}
