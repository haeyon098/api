package kr.co.apexsoft.granet2.admin_api.search.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendSimpleForListProjection;
import kr.co.apexsoft.granet2.admin_api.search.dto.*;
import kr.co.apexsoft.granet2.admin_api.search.service.ApplicationSearchService;
import kr.co.apexsoft.granet2.admin_api.search.service.RecSearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author 박지환
 * created on 2020-08-04
 */
@RestController
@RequestMapping("/admin/recommedations")
@RequiredArgsConstructor
public class SearchRecController {

    private final RecSearchService getRecSimplesForList;


    @GetMapping
    public ResponseEntity<Page<RecSimpleForListOut>> getRecSimplesForList(RecSearchCondition condition,
                                                                                       @PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(getRecSimplesForList.getRecSimplesForList(condition, pageable));
    }

}
