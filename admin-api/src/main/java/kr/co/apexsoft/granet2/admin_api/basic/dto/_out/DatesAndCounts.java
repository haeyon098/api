package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-21
 */
@Getter
public class DatesAndCounts {

    private List<String> dates = new ArrayList<>();
    private List<Long> counts = new ArrayList<>();
}
