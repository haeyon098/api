package kr.co.apexsoft.granet2.admin_api._config.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-03-11
 */
@Component
@Slf4j
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       AccessDeniedException e) throws IOException, ServletException {
        log.error("Access Denied Error: {}", e.getMessage());
        response.sendError(
                HttpServletResponse.SC_FORBIDDEN,
                "허가되지 않은 접근입니다. 다시 로그인해주세요."
        );
    }
}
