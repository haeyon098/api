package kr.co.apexsoft.granet2.admin_api.download.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 지원자 정보 데이터
 * TODO: 추가예정
 */
@Getter
@AllArgsConstructor
public class ApplicationsOut {

    private String applId;
    private String rgstNo;
    private String fornRgstNo;
    private String passportNo;
    private String visaNo;

}
