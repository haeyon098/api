package kr.co.apexsoft.granet2.admin_api.basic.service;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.adms.repository.*;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SchoolCommCodeRepository;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SysCommCodeRepository;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import kr.co.apexsoft.gradnet2.entity.recpart.repository.RecruitPartRepository;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.granet2.admin_api._support.common.ApexCollectionUtils;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-01
 */
@Service
@Transactional
@RequiredArgsConstructor
public class BasicServiceImpl implements BasicService {

    private final ApplRepository applRepository;
    private final SemesterTypeRepository semesterTypeRepository;
    private final AdmsRepository admsRepository;
    private final CorsRepository corsRepository;
    private final MajorRepository majorRepository;
    private final CategoryRepository categoryRepository;
    private final RecruitPartRepository recruitPartRepository;

    private final RecruitSchoolRepository recruitSchoolRepository;
    private final SysCommCodeRepository sysCommCodeRepository;
    /**
     * 모집 학교, 모집 학기의 접수 현황 조회
     *
     * @param schoolCode
     * @param enterYear
     * @param recruitPartSeq
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public List<StatusAndCount> getReceiptStatsSimpleOut(String schoolCode, String enterYear, Integer recruitPartSeq) {
        long currentCount = applRepository.countBySchoolEnterYearRecruitPartSeqAndStatusIn(schoolCode, enterYear, recruitPartSeq, Arrays.asList(Appl.Status.COMPLETE));
//        KDI는 미결제 없음
//        long unPaidCount = applRepository.countBySchoolEnterYearRecruitPartSeqAndStatusIn(schoolCode, enterYear, recruitPartSeq, Arrays.asList(Appl.Status.SUBMIT));
        long unSubmittedCount = applRepository.countBySchoolEnterYearRecruitPartSeqAndStatusIn(schoolCode, enterYear, recruitPartSeq, Arrays.asList(
                Appl.Status.APPL_PART_SAVE,
                Appl.Status.BASIS_SAVE,
                Appl.Status.ACAD_SAVE,
                Appl.Status.LANG_SAVE,
                Appl.Status.ESSAY_SAVE,
                Appl.Status.FILE_SAVE,
                Appl.Status.SUBMIT));
//        long cancelledCount = applRepository.countBySchoolEnterYearRecruitPartSeqAndStatusIn(schoolCode, enterYear, recruitPartSeq, Arrays.asList(Appl.Status.CANCEL));
        List<StatusAndCount> statusAndCounts = new ArrayList<>();
        statusAndCounts.add(StatusAndCount.of("현재 접수현황", currentCount));
        statusAndCounts.add(StatusAndCount.of("원서 작성 중 인원", unSubmittedCount));
//        statusAndCounts.add(StatusAndCount.of("지원 취소 인원", cancelledCount));
        return statusAndCounts;
    }

    /**
     * @param user
     * @return
     */
    @Transactional(readOnly = true)
    @Override
    public RecruitOut getAllRecruitParts(User user) {
        List<RecruitSchoolOut>  schools = getAllRecruitSchools(user);

        String[] schoolCodes = schools.stream().map(RecruitSchoolOut::getSchoolCode).toArray(String[]::new);

        return new RecruitOut(schools,recruitPartRepository.findByRecruitSchoolCodeInOrderByRecruitSchoolCodeAscEnterYearDescSeqDesc(schoolCodes)
                .stream()
                .map(recruitPart -> RecruitPartOut.from(recruitPart, schools.stream().filter(school -> school.getSchoolCode().equals(recruitPart.getRecruitSchoolCode())).findFirst().get()))
                .collect(Collectors.toList()));
    }


    private List<RecruitSchoolOut> getAllRecruitSchools(User user) {
        List<RecruitSchool> recruitSchools;
        if(user.isSysAdmin()){
            recruitSchools =recruitSchoolRepository.findAllBy();
        }else{
            recruitSchools = recruitSchoolRepository.findByIdIn(user.getSchoolCode());
        }

       return  recruitSchools.stream().map(RecruitSchoolOut::from).collect(Collectors.toList());
    }



    @Transactional(readOnly = true)
    @Override
    public List<CodeNameOut> getAllAdmissions(String schoolCode, String enterYear, Integer recruitPartSeq) {
        return admsRepository.findBySchoolAndEnterYearAndRecruitPartSeq(schoolCode, enterYear, recruitPartSeq)
                .stream().map(CodeNameOut::from).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<CodeNameOut> getAllCourses(String schoolCode, String enterYera, Integer recruitPartSeq, String admissionCode) {
        return corsRepository.findBySchoolAndEnterYearAndRecruitPartSeqAndAdmission(
                schoolCode, enterYera, recruitPartSeq, admissionCode)
                .stream().map(CodeNameOut::from).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<CodeNameOut> getAllCategories(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode, String courseCode) {
        return categoryRepository.findBy(schoolCode, enterYear, recruitPartSeq, admissionCode, courseCode)
                .stream().map(CodeNameOut::from).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<CodeNameOut> getAllMajors(String schoolCode, String enterYera, Integer recruitPartSeq, String admissionCode, String courseCode, String categoryCode) {
        return majorRepository.findBy(schoolCode, enterYera, recruitPartSeq, admissionCode, courseCode, categoryCode)
                .stream().map(CodeNameOut::from).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<CodeNameOut> getAllApplStatuses(String schoolCode) {
        Appl.Status[] statuses = RecruitSchool.SchoolCodeType.getSteps(schoolCode);
        return Arrays.stream(statuses).map(CodeNameOut::from).collect(Collectors.toList());
    }


    @Transactional(readOnly = true)
    @Override
    public List<CodeNameOut> getAllRecStatuses() {
        return sysCommCodeRepository.findAllByCodeGrpAndUsedTrueOrderByCodeAsc("REC_STS")
                .stream().map(CodeNameOut::from).collect(Collectors.toList());

    }


    @Transactional(readOnly = true)
    @Override
    public DatesAndCounts getReceiptCountVariations(String schoolCode, String enterYear, Integer recruitPartSeq) {
        Map<LocalDate, Long> dateCountMap = applRepository
                .countByStatusAndDate(schoolCode, enterYear, recruitPartSeq, Appl.Status.COMPLETE);
        RecruitPart recruitPart = recruitPartRepository
                .findByRecruitSchoolCodeAndEnterYearAndSeq(schoolCode, enterYear, recruitPartSeq)
                .orElseThrow(() -> new RuntimeException(
                        String.format("학교 [%s], 연도 [%s], 회차 [%d] 인 RecruitPart 가 없습니다.", schoolCode, enterYear, recruitPartSeq)));
        Map<LocalDate, Long> zeroFilledMap = ApexCollectionUtils.getZeroFilledMap(
                recruitPart.getStartDate().toLocalDate(),
                recruitPart.getEndDate().toLocalDate(),
                dateCountMap);
        DatesAndCounts datesAndCounts = new DatesAndCounts();
        List<String> dates = datesAndCounts.getDates();
        List<Long> counts = datesAndCounts.getCounts();
        zeroFilledMap.forEach((k, v) -> {
            dates.add(k.toString().replace("T", ""));
            counts.add(v);
        });
        return datesAndCounts;
    }

    @Transactional(readOnly = true)
    @Override
    public List<CompletedApplCountByCors> getCompletedApplCountByCors(String schoolCode, String enterYear, Integer recruitPartSeq) {
        return applRepository.findCompletedApplCountsByCors(schoolCode, enterYear, recruitPartSeq)
                .stream()
                .map(CompletedApplCountByCors::from)
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<ApplStatsOut> getApplStatsByAdmission(String schoolCode, String enterYear, Integer recruitPartSeq, String admissionCode) {
        return applRepository.findApplStatsByAdmission(schoolCode, enterYear, recruitPartSeq, admissionCode)
                .stream()
                .map(ApplStatsOut::from)
                .collect(Collectors.toList());
    }
}
