package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-02
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplStatusesOut {

    private List<String> applStatuses;

    public static ApplStatusesOut fromApplStatus() {
        List<String> applStatuses = new ArrayList<>();
        for (Appl.Status status : Appl.Status.values()) {
            applStatuses.add(status.name());
        }
        return new ApplStatusesOut(applStatuses);
    }
}
