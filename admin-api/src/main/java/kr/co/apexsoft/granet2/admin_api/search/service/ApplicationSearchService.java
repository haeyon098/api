package kr.co.apexsoft.granet2.admin_api.search.service;

import kr.co.apexsoft.granet2.admin_api.search.dto.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
public interface ApplicationSearchService {

    ApplicantDetailOut getApplicantDetail(Long applicationId);

    ApplicationSimpleOut getApplicationSimple(Long applicationId);

    ApplicationDetailOut getApplicationDetail(Long applicationId);

    Page<ApplicationSimpleForListOut> getApplicationSimplesForList(ApplSearchCondition condition, Pageable pageable);

    ApplZipPrintedOut patchZipPrinted(Long applNo, Boolean zipPrinted);
}
