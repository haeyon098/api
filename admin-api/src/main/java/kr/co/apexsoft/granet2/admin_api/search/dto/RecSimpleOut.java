package kr.co.apexsoft.granet2.admin_api.search.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.CodeNameOut;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @author 박지환
 * created on 2020-08-10
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RecSimpleOut {

    String profName;
    String profEmail;
    CodeNameOut recStatus;


    public RecSimpleOut(Recommend recommend, List<SysCommCode> recCodes) {
        Predicate<SysCommCode> findRecCommCode = p -> p.getCode().equals(recommend.getStsCode());
        Optional<SysCommCode> recCommCode = recCodes.stream().filter(findRecCommCode).findAny();

        this.profEmail = recommend.getEmail();
        this.profName = recommend.getName();
        this.recStatus = CodeNameOut.of(recCommCode.get().getCode(), recCommCode.get().getCodeKrValue(), recCommCode.get().getCodeEnValue());
    }
}
