package kr.co.apexsoft.granet2.admin_api.download.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 지원현황 - KDI
 * TODO: 추가예정
 */
@Getter
@AllArgsConstructor
public class ApplicationStatusOut {
//    private int lineNo;
    private String userId;
    private Long applNo;
    private String seqForYear;
    private String applId;
    private String smstTypeKorName;
    private String admsKorName;
    private String corsKorName;
    private String categoryEngName;
    private String majKorName;

    private String korName;
    private String engName;
    private String applStatusName;
    private String phoneNumber;
    private String email;

    private String recRequestCnt;
    private String recSubmitCnt;

    private String country;
    private String countryGroupCode;
    private String countryGroupName;

    private String empCat;
    private String rgstBornDate;
    private String gend;
    private String videoEssay;
}
