package kr.co.apexsoft.granet2.admin_api.search.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-01
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationSimpleOut {

    private final Long applNo;
    private final String userId;
    private final String status;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private final LocalDateTime applicationDate;
    private final String semesterTypeCode;
    private final String admissionCode;
    private final String courseCode;
    private final String categoryCode;

    public static ApplicationSimpleOut from(Appl appl) {
        ApplPart applPart = appl.getPart();
        return new ApplicationSimpleOut(
                appl.getId(),
                appl.getApplicant().getUserId(),
                appl.getStatus().name(),
                appl.getApplDate(),
                applPart.getSemesterTypeCode(),
                applPart.getAdmissionCode(),
                applPart.getCourseCode(),
                applPart.getCategoryCode()
        );
    }
}
