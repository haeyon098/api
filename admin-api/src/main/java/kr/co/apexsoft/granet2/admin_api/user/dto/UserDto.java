package kr.co.apexsoft.granet2.admin_api.user.dto;

import lombok.*;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-25
 */
@Getter
@Builder
public class UserDto {

    private Long id;

    private String userId;

    private String email;

    private Status status = Status.ACTIVE;  // 사용자 상태 코드

    private Boolean locked = false; // 계정 잠김

    public enum Status {
        ACTIVE, INACTIVE, WITHDRAW
    }
}
