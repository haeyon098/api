package kr.co.apexsoft.granet2.admin_api.download.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * 지원완료건
 * 엑셀 파일 다운로드
 * 조건
 */
@Getter
@Setter
public class CompleteDownloadCondition {

    private String schoolCode;
    private String enterYear;
    private Integer recruitPartSeq;
    private String semesterTypeCode;
    private String admissionCode;
    private CompleteDownloadType type;

}
