package kr.co.apexsoft.granet2.admin_api.search.service;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendSimpleForListProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository.RecommedRepository;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SysCommCodeRepository;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.CodeNameOut;
import kr.co.apexsoft.granet2.admin_api.search.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 박지환
 * created on 2020-08-04
 */
@Service
@Transactional
@RequiredArgsConstructor
public class RecSearchServiceImpl implements RecSearchService {

    private final RecommedRepository recommedRepository;

    private final SysCommCodeRepository sysCommCodeRepository;

    @Transactional(readOnly = true)
    @Override
    public Page<RecSimpleForListOut> getRecSimplesForList(RecSearchCondition condition, Pageable pageable) {
        List<SysCommCode>  recStsCodes =  sysCommCodeRepository.findAllByCodeGrpAndUsedTrueOrderByCodeAsc("REC_STS");
        Page<RecommendSimpleForListProjection> projections = recommedRepository.findAllBySearchCondition(
                condition.getSchoolCode(),
                condition.getEnterYear(),
                condition.getRecruitPartSeq(),
                condition.getAdmissionCode(),
                condition.getCourseCode(),
                condition.getCategoryCode(),
                condition.getMajorCode(),
                condition.getApplId(),
                condition.getApplicantName(),
                condition.getEmail(),
                condition.getApplicantId(),
                condition.getProfName(),
                condition.getProfEmail(),
                condition.getProfPhoneNumber(),
                condition.getRecStatus(),
                pageable);


        List<RecSimpleForListOut> recs = new ArrayList<>();

        for (RecommendSimpleForListProjection projection: projections) {
            RecSimpleForListOut rec = new RecSimpleForListOut(projection, recStsCodes);
            recs.add(rec);
        }

        return new PageImpl<>(recs, pageable, projections.getTotalElements());

    }

}
