package kr.co.apexsoft.granet2.admin_api.search.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.Getter;
import lombok.Setter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-20
 */
@Getter
@Setter
public class ApplSearchCondition {

    private String schoolCode;
    private String enterYear;
    private Integer recruitPartSeq;
    private String semesterTypeCode;
    private String admissionCode;
    private String courseCode;
    private String categoryCode;
    private String majorCode;
    private String applId;
    private Long applNo;
    private String applicantName;
    private String email;
    private String phoneNumber;
    private String applicantId;
    private Appl.Status statusCode;
}
