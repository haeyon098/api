package kr.co.apexsoft.granet2.admin_api.download.service.complete;


import kr.co.apexsoft.gradnet2.common.crypto.CryptoService;
import kr.co.apexsoft.gradnet2.common.excel.ExcelGenerator;
import kr.co.apexsoft.gradnet2.common.excel.ExcelIOException;
import kr.co.apexsoft.gradnet2.common.file.FileDto;
import kr.co.apexsoft.gradnet2.common.file.FileUtils;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.repository.AcademyRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.repository.LanguageRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository.RecommedRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository.AlumniRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository.SrvyAnsRepository;
import kr.co.apexsoft.granet2.admin_api.download.dto.CompleteDownloadCondition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class KdisDownloadService implements CompleteDownloadService {

    @NonNull
    private CryptoService cryptoService;

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private AcademyRepository acadRepository;

    @NonNull
    private CareerRepository careerRepository;

    @NonNull
    private DocumentRepository docmentRepository;

    @NonNull
    private LanguageRepository langRepository;

    @NonNull
    private RecommedRepository recRepository;

    @NonNull
    private AlumniRepository alumniRepository;

    @NonNull
    private SrvyAnsRepository srvyRepository;


    @Override
    public boolean supports(String schoolCode) {
        return RecruitSchool.SchoolCodeType.KDI.getValue().equals(schoolCode);
    }


    @Override
    public FileDto download(CompleteDownloadCondition condition) {
        switch (condition.getType()) {
            case MASTER: {
                return completeDownloadMaster(condition);
            }
            case ACAD: {
                return completeDownloadAcademy(condition);
            }
            case CAREER: {
                return completeExcelCareer(condition);
            }
            case LANG: {
                return completeDownloadLanguage(condition);
            }
            case SUBMIT_FILE: {
                return completeDownloadSubmitFile(condition);
            }
            case REC_INFO: {
                return completeDownloadRecommender(condition);
            }
            case REC_ANSWER: {
                return completeDownloadRecommenderAnswer(condition);
            }
            case KDI_SURVEY: {
                return completeDownloadSurvey(condition); //수정필요
            }
            case KDI_ALUMNI: {
                return completeDownloadAlumni(condition);
            }
            default:
                throw new ExcelIOException("구분을 다시 확인해주세요.");
        }
    }

    public FileDto completeDownloadMaster(CompleteDownloadCondition condition) {
        List<Object[]> applicantsList = applRepository.findKidsApplicantList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());

        System.err.println("applicantsList.size() : " +  applicantsList.size() );
        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < applicantsList.size(); i++) {
            Object[] applicantsInfo = applicantsList.get(i);

            //총 87컬럼의 엑셀 생성
            Object[] tempOut = {
                    applicantsInfo[0], applicantsInfo[1], (String)applicantsInfo[2] };
//                    , applicantsInfo[3], applicantsInfo[4],
//                    applicantsInfo[5], applicantsInfo[6], applicantsInfo[7], "", ""};
//                    applicantsInfo[8], applicantsInfo[9]};
//                    applicantsInfo[10], applicantsInfo[11], applicantsInfo[12], applicantsInfo[13], applicantsInfo[14],
//                    applicantsInfo[15], applicantsInfo[16], applicantsInfo[17], applicantsInfo[18],

//                    getRegistNo( (Date)applicantsInfo[19], (String)applicantsInfo[20]), // 주민번호 복호화
//                    cryptoService.decrypt( (String)applicantsInfo[21] ), // 외국인등록번호 복호화
//                    cryptoService.decrypt( (String)applicantsInfo[22] ), // 여권번호 복호화

//                    applicantsInfo[19],
//                    applicantsInfo[21], applicantsInfo[22],
//
//                    applicantsInfo[23], applicantsInfo[24], applicantsInfo[25],
//                    applicantsInfo[26], applicantsInfo[27], applicantsInfo[28], applicantsInfo[29], applicantsInfo[30],
//                    applicantsInfo[31], applicantsInfo[32], applicantsInfo[33], applicantsInfo[34], applicantsInfo[35],
//                    applicantsInfo[36], applicantsInfo[37], applicantsInfo[38], applicantsInfo[39], applicantsInfo[40],
//                    applicantsInfo[41], applicantsInfo[42], applicantsInfo[43], applicantsInfo[44], applicantsInfo[45],
//                    applicantsInfo[46], applicantsInfo[47], applicantsInfo[48], applicantsInfo[49], applicantsInfo[50],
//                    applicantsInfo[51], applicantsInfo[52], applicantsInfo[53], applicantsInfo[54], applicantsInfo[55],
//                    applicantsInfo[56], applicantsInfo[57], applicantsInfo[58], applicantsInfo[59], applicantsInfo[60],
//                    applicantsInfo[61], applicantsInfo[62], applicantsInfo[63], applicantsInfo[64], applicantsInfo[65],
//                    applicantsInfo[66], applicantsInfo[67], applicantsInfo[68], applicantsInfo[69], applicantsInfo[70],
//                    applicantsInfo[71], applicantsInfo[72], applicantsInfo[73], applicantsInfo[74], applicantsInfo[75],
//                    applicantsInfo[76], applicantsInfo[77], applicantsInfo[78], applicantsInfo[79], applicantsInfo[80],
//                    applicantsInfo[81], applicantsInfo[82], applicantsInfo[83], applicantsInfo[84], applicantsInfo[85],
//                    applicantsInfo[86], applicantsInfo[87]};

            results.add(tempOut);
        }



        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "ADM_YEAR", "ADM_TERM" };
//                , "CATEGORY", "INTERNATIONAL_YN", "DEGREE", "PROGRAM", "DAY_YN", " ", " " };
//                "APPLY_DATE", " ", "KOR_NAME", "ENG_NAME", "LAST_NAME", "FIRST_NAME", "MIDDLE_NAME", "EMAIL", "MOBILE_NO", "RESIDENT_NO",
//                "FOREIGN_NO", "PASSPORT_NO", "NATIONAL_CD", "OVERSEAS_KOREAN", "GENDER", "DOB", " ", " ", "GMP_YN", " ",
//                " ", "PHONENO_HOME", " ", "ZIPNO_HOME", "ADDRESS_HOME", "ADDRESS_EHOME", " ", " ", " ", "ZIPNO_WORK",
//                "ADDRESS_WORK", "ADDRESS_EWORK", " ", " ", " ", " ", " ", " ", " ", "CONTACT_CD",
//                "RESIDENCY_YN", "SUB_CAT", " ", "RECEIPT_DTIME", "ONLINE_YN", "APPLICATION_YN", "DOCUMENT_SUBMIT_YN", "DOCUMENT_YN", " ", "PASS_YN",
//                " ", " "," ", " ", " ", " ", " ", "INPUT_ID", "INPUT_DTIME", " ",
//                "UPDATE_ID", "UPDATE_DTIME", " ", "SKYPE_ID", " ", "NATIONAL_PHONE", "AGREEMENT_YN", " ", " ", "EMPLOYED_YN",
//                " ", " ", " ", " ", " ", " ", "VIDEO_ESSAY"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.dataToExcel(excelHeader, results));

        return new FileDto("AD_MASTER", resource);
    }



    /**
     * 학력
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadAcademy(CompleteDownloadCondition condition) {
        List<Object[]> academyList = acadRepository.findKidsAcademyList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());
        String beApplId = "";
        String currApplId = "";
        int seqByApplid = 1;

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < academyList.size(); i++) {
            Object[] academy = academyList.get(i);
            currApplId = (String) academy[0];

            if (currApplId.equals(beApplId)) {
                seqByApplid = seqByApplid + 1;
            } else {
                seqByApplid = 1;
                beApplId = currApplId;
            }

            Object[] tempOut = {
                    academy[0],
                    seqByApplid,
                    academy[1],
                    academy[2],
                    academy[3],
                    academy[4],
                    academy[5],
                    academy[6],
                    academy[7],
                    academy[8],
                    "", "", "",
                    academy[9],
                    academy[10],
                    academy[11],
                    "", "",
                    academy[12],
                    "",
                    academy[13],
                    "", "", "", "", "", "", "", "", "", "", "", "",
                    academy[14],
                    academy[15],
                    academy[16],
                    academy[17]};
            results.add(tempOut);
        }

        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "SEQ_NO", "SCHOOL_NAME", "SCHOOL_ENAM", "MAJOR_NAME", "MAJOR_ENAME", "DEGREE", "FROM_DATE", "TO_DATE", "ACAD_TYPE",
                " ", " ", " ", "REMARK", "INPUT_ID", "INPUT_DTIME", " ", " ", "UPDATE_DTIME", " ", "LOCATION",
                " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ",
                "SCHOOL_NATION", "GRAD_TYPE", "GRAD_AVR", "GARD_FULL"};


        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, results));
        return new FileDto("AD_ACADEMY", resource);
    }


    /**
     * 경력
     *
     * @param condition
     * @return
     */
    private FileDto completeExcelCareer(CompleteDownloadCondition condition) {
        List<Object[]> careerList = careerRepository.findKidsCareerList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());
        String beApplId = "";
        String currApplId = "";
        int seqByApplid = 1;

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < careerList.size(); i++) {
            Object[] career = careerList.get(i);
            currApplId = (String) career[0];

            if (currApplId.equals(beApplId)) {
                seqByApplid = seqByApplid + 1;
            } else {
                seqByApplid = 1;
                beApplId = currApplId;
            }

            Object[] tempOut = {
                    career[0],
                    seqByApplid,
                    career[1],
                    career[2],
                    career[3],
                    career[4],
                    career[5],
                    career[6],
                    career[7],
                    career[8],
                    career[9],
                    career[10],
                    career[11],
                    career[12],
                    career[13],
                    career[14],
                    career[15],
                    career[16],
                    career[17]};
            results.add(tempOut);
        }

        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "SEQ_NO", "COMPANY_NAME", "COMPANY_ENAME", "FROM_DATE", "TO_DATE",
                "CURRENT_YN", "DEPT_KNAME", "DEPT_ENAME", "POSITION_NAME", "POSITION_ENAME", "REMARK",
                "INPUT_ID", "INPUT_DTIME", "INPUT_IP", "UPDATE_ID", "UPDATE_DTIME", "UPDATE_IP", "EMP_CAT"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, results));
        return new FileDto("AD_CAREER", resource);
    }
    /**
     *  어학
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadLanguage(CompleteDownloadCondition condition) {
        List<Object[]> langList = langRepository.findKidsLanguageList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < langList.size(); i++) {
            Object[] lang = langList.get(i);

            Object[] tempOut = {
                    lang[0],
                    lang[1],
                    lang[2],
                    lang[3],
                    lang[4],
                    lang[5],
                    lang[6],
                    lang[7],
                    lang[8],
                    lang[9],
                    lang[10],
                    lang[11]};
            results.add(tempOut);
        }

        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "LANG_CODE", "OBTAIN_SCORE", "OBTAIN_DATE", " ",
                "INPUT_ID", "INPUT_DTIME", "INPUT_IP", "UPDATE_ID", "UPDATE_DTIME", "UPDATE_IP", "OPIC_SCORE"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, langList));
        return new FileDto("AD_LANG", resource);
    }

    /**
     * 설문내용중 - 동문정보 리스트
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadAlumni(CompleteDownloadCondition condition) {
        List<Object[]> alumniList = alumniRepository.findKidsAlumniList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());
        String beApplId = "";
        String currApplId = "";
        int seqByApplid = 1;

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < alumniList.size(); i++) {
            Object[] alumni = alumniList.get(i);
            currApplId = (String) alumni[0];

            if (currApplId.equals(beApplId)) {
                seqByApplid = seqByApplid + 1;
            } else {
                seqByApplid = 1;
                beApplId = currApplId;
            }

            Object[] tempOut = {
                    alumni[0],
                    seqByApplid,
                    alumni[1],
                    alumni[2],
                    alumni[3],
                    alumni[4],
                    alumni[5],
                    alumni[6],
                    alumni[7],
                    alumni[8],
                    alumni[9],
                    alumni[10],
                    alumni[11],
                    alumni[12],
                    alumni[13]};
            results.add(tempOut);
        }


        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "SEQ_NO", "NAME", "ADM_YEAR", "PROGRAM", "REMARK",
                "INPUT_ID", "INPUT_DTIME", "INPUT_IP",  "UPDATE_ID", "UPDATE_DTIME", "UPDATE_IP", "RECOMMENDER"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, results));
        return new FileDto("AD_ALUMNI", resource);
    }

    /**
     * 제출파일 리스트
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadSubmitFile(CompleteDownloadCondition condition) {
        List<Object[]> submitFileList = docmentRepository.findKidsSubmitFileList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());
        String beApplId = "";
        String currApplId = "";
        int seqByApplid = 1;

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < submitFileList.size(); i++) {
            Object[] submitFile = submitFileList.get(i);
            currApplId = (String) submitFile[0];

            if (currApplId.equals(beApplId)) {
                seqByApplid = seqByApplid + 1;
            } else {
                seqByApplid = 1;
                beApplId = currApplId;
            }

            Object[] tempOut = {
                    submitFile[0],
                    seqByApplid,
                    submitFile[1],
                    submitFile[2],
                    "Y",
                    submitFile[3],
                    "", "", "", "", "", "",
                    submitFile[4],
                    submitFile[5],
                    submitFile[6],
                    submitFile[7],
                    submitFile[8],
                    submitFile[9],
                    submitFile[10],
                    ""};
            results.add(tempOut);
        }

        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "SEQ", "SUBMIT_DOC", "제출파일 종류", "SUBMIT_YN", "SUBMIT_DATE",
                "ONLINE_YN", "REQUIRED_YN", "FILE_PATH", "FILE_NAME", "RFILE_NAME", "REMARK",
                "INPUT_ID", "INPUT_DTIME", "INPUT_IP",  "UPDATE_ID", "UPDATE_DTIME", "UPDATE_IP", "SORT", "SUBMIT_SELF_YN"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, results));
        return new FileDto("AD_SUBMIT_DOC", resource);
    }


    /**
     * 추천서 정보 - 추천인 정보를 포함한 추천서 정보
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadRecommender(CompleteDownloadCondition condition) {
        List<Object[]> recommenderList = recRepository.findKidsRecommenderList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());
        String beApplId = "";
        String currApplId = "";
        int seqByApplid = 1;

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < recommenderList.size(); i++) {
            Object[] recommender = recommenderList.get(i);
            currApplId = (String) recommender[0];

            if (currApplId.equals(beApplId)) {
                seqByApplid = seqByApplid + 1;
            } else {
                seqByApplid = 1;
                beApplId = currApplId;
            }

             Object[] tempOut = {
                    recommender[0],
                    seqByApplid,
                    recommender[1],
                    recommender[2],
                    recommender[3],
                    recommender[4],
                    "", "", "Y", "", "",
                    recommender[5],
                    recommender[6],
                    recommender[7],
                    recommender[8],
                    recommender[9],
                    recommender[10],
                    recommender[11],
                    "",
                    recommender[12],
                    recommender[13],
                    "Y"};
            results.add(tempOut);
        }
        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "SEQ_NO", "NAME", "ORGANIZATION", "POSITION", "EMAIL",
                "ALUMNII_YN", "MAIL_YN", "ONLINE_YN", "EMARK", "USER_ID",
                "INPUT_ID", "INPUT_DTIME", "INPUT_IP",  "UPDATE_ID", "UPDATE_DTIME", "UPDATE_IP",
                "PHONE_NO", "ACCESS_KEY", "AGREEMENT", "ADDRESS", "SIGNATURE", "SUBMIT_YN"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, results));
        return new FileDto("AD_RECOMMENDER", resource);
    }

    /**
     * 추천서 답변
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadRecommenderAnswer(CompleteDownloadCondition condition) {
        List<Object[]> recommenderAnswerList = recRepository.findKidsRecommenderAnswerList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());
        String beApplId = "";
        String currApplId = "";
        int seqByApplid = 1;

        List<Object[]> results = new ArrayList<>();
        for (int i = 0; i < recommenderAnswerList.size(); i++) {
            Object[] recommenderAnswer = recommenderAnswerList.get(i);
            currApplId = (String) recommenderAnswer[0];

            if (currApplId.equals(beApplId)) {
                seqByApplid = seqByApplid + 1;
            } else {
                seqByApplid = 1;
                beApplId = currApplId;
            }

            Object[] tempOut = {
                    recommenderAnswer[0],
                    seqByApplid,
                    "", "",
                    recommenderAnswer[1],
                    recommenderAnswer[2],
                    recommenderAnswer[3],
                    recommenderAnswer[4],
                    recommenderAnswer[5],
                    recommenderAnswer[6],
                    recommenderAnswer[7],
                    recommenderAnswer[8],
                    recommenderAnswer[9],
                    recommenderAnswer[10],
                    recommenderAnswer[11],
                    recommenderAnswer[12],
                    recommenderAnswer[13],
                    recommenderAnswer[14],
                    recommenderAnswer[15],
                    recommenderAnswer[16],
                    recommenderAnswer[17],
                    recommenderAnswer[18],
                    recommenderAnswer[19]};
            results.add(tempOut);
        }
        // 헤더
        String[] excelHeader = {"APPLICANT_NO", "SEQ_NO", "ACCESS_KEY", "VERSION", "CREATE_DTIME", "UPDATE_DTIME",
                "ANSWER_01", "ANSWER_02", "ANSWER_03", "ANSWER_04", "ANSWER_05", "ANSWER_06", "ANSWER_07", "ANSWER_08", "ANSWER_09", "ANSWER_10",
                "ANSWER_11", "ANSWER_12", "ANSWER_13", "ANSWER_14", "ANSWER_15", "ANSWER_16", "ANSWER_17", "ANSWER_18", "ANSWER_19", "ANSWER_20"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, results));
        return new FileDto("AD_RECOMMENDATION_ANSWER", resource);
    }


    /**
     * 설문답변
     *
     * @param condition
     * @return
     */
    private FileDto completeDownloadSurvey(CompleteDownloadCondition condition) {
        List<Object[]> surveyList = srvyRepository.findKidsSurveyList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());

        // 헤더
        String[] excelHeader = {"APPLICANT_NO",	"ANS_1_CD", "ANS_1_VAL", "ANS_1_1_CD", "ANS_1_1_VAL", "ANS_2_CD", "ANS_2_VAL", "ANS_2_1_CD", "ANS_2_1_VAL",
                "ANS_3_CD", "ANS_3_VAL", "ANS_3_1_CD", "ANS_3_1_VAL", "ANS_4_CD", "ANS_4_VAL", "ANS_5_CD", "ANS_5_VAL", "ANS_6_VAL", "ANS_7_CD", "ANS_7_VAL",
                "ANS_8_CD", "ANS_8_VAL", "ANS_Interested_CD", "ANS_Interested_VAL", "ANS_TopicStudy", "ANS_EmpPubSec_CD", "ANS_EmpPubSec_VAL", "ANS_9_CD", "ANS_9_VAL",
                "ANS_10_CD", "ANS_10_VAL", "ANS_Supervisor", "ANSWER_11_Visted", "ANSWER_12_Subscribed", "INPUT_ID", "INPUT_DTIME"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.arrayDataToExcel(excelHeader, surveyList));
        return new FileDto("AD_SURVEY", resource);
    }

    /**
     * 주민번호
     * @param borndate
     * @param regstEncr
     * @return
     */
    private String getRegistNo(Date borndate, String regstEncr){
        SimpleDateFormat fmt = new SimpleDateFormat("yyMMdd");

        return StringUtils.isBlank(regstEncr)?"": fmt.format(borndate) +"-"+cryptoService.decrypt(regstEncr);
    }
}
