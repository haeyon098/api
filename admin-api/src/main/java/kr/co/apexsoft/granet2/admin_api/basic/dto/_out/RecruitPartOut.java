package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-23
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RecruitPartOut {


    private final String schoolCode;
    private final String schoolName;
    private final String schoolEngName;

    private final String enterYear;
    private final String partSeq;
    private final String name;
    private final String engName;

    @JsonFormat(pattern="yyyy-MM-dd")
    private final LocalDateTime startDate;

    @JsonFormat(pattern="yyyy-MM-dd")
    private final LocalDateTime endDate;


    public static RecruitPartOut from(RecruitPart rp,RecruitSchoolOut school) {

        return new RecruitPartOut(rp.getRecruitSchoolCode(),school.getSchoolName(),school.getSchoolEngName(),rp.getEnterYear(),
                String.valueOf(rp.getSeq()), rp.getKorName(), rp.getEngName(), rp.getSubmit().getStartDate(), rp.getCmplete().getEndDate() );
    }

}
