package kr.co.apexsoft.granet2.admin_api.search.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-24
 */
@Getter @Setter
@AllArgsConstructor
public class ApplZipPrintedOut {

    private Long applNo;
    private Boolean zipPrinted;
}
