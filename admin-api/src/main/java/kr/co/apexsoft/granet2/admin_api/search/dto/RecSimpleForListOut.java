package kr.co.apexsoft.granet2.admin_api.search.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendSimpleForListProjection;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.CodeNameOut;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-18
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RecSimpleForListOut {

    private String applicantId;
    private String applId;
    private CodeNameOut recruitPart;
    private CodeNameOut admission;
    private CodeNameOut course;
    private CodeNameOut category;
    private CodeNameOut major;
    private String applicantKorName;
    private String applicantEngName;
    private String email;
    private CodeNameOut recStatus;
    private String profName;
    private String profEmail;
    private String profPhoneNumber;
    private CodeNameOut applStatus;

    public  RecSimpleForListOut (RecommendSimpleForListProjection projection, List<SysCommCode> recCodes) {
        Predicate<SysCommCode> findRecCommCode = p -> p.getCode().equals(projection.getRecStsCode());
        Optional<SysCommCode> recCommCode = recCodes.stream().filter(findRecCommCode).findAny();

        this.applicantId = projection.getApplicantId();
        this.applId = projection.getApplId();
        this.recruitPart =  CodeNameOut.of(String.valueOf(projection.getRecruitPartSeq()), projection.getRecruitPartName(), projection.getRecruitPartEngName());
        this.admission = CodeNameOut.of(projection.getAdmissionCode(), projection.getAdmissionName(), projection.getAdmissionEngName());
        this.course = CodeNameOut.of(projection.getCourseCode(), projection.getCourseName(), projection.getCourseEngName());
        this.category = CodeNameOut.of(projection.getCategoryCode(), projection.getCategoryName(), projection.getCategoryEngName());
        this.major = CodeNameOut.of(projection.getMajorCode(), projection.getMajorName(), projection.getMajorEngName());
        this.applicantKorName = projection.getApplicantKorName();
        this.applicantEngName = projection.getApplicantEngName();
        this.email = projection.getEmail();
        this.recStatus = CodeNameOut.of(recCommCode.get().getCode(), recCommCode.get().getCodeKrValue(), recCommCode.get().getCodeEnValue());
        this.profEmail = projection.getProfEmail();
        this.profName = projection.getProfName();
        this.profPhoneNumber = projection.getProfPhoneNumber();
        this.applStatus = CodeNameOut.of(projection.getStatus().name(), projection.getStatus().getKorName(), projection.getStatus().getEngName());
    }
}
