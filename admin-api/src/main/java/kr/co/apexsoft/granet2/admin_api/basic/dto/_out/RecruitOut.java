package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


@Getter
@AllArgsConstructor
public class RecruitOut {

    private List<RecruitSchoolOut> schools = new ArrayList<>();
    private List<RecruitPartOut> parts = new ArrayList<>();
}
