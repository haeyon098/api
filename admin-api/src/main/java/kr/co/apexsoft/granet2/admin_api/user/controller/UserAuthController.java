package kr.co.apexsoft.granet2.admin_api.user.controller;

import kr.co.apexsoft.granet2.admin_api._config.security.jwt.JwtAuthenticationResponse;
import kr.co.apexsoft.granet2.admin_api.user.dto.LoginDto;
import kr.co.apexsoft.granet2.admin_api.user.dto.UserDto;
import kr.co.apexsoft.granet2.admin_api.user.service.UserLoginService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class UserAuthController {
    @NonNull
    private UserLoginService userLoginService;

    /**
     * 로그인
     *
     * @param loginDto
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginDto loginDto) {
        return ResponseEntity.ok(this.userLoginService.login(loginDto.getUserId(), loginDto.getPassword()));
    }

    /**
     * 캡챠 성공 후처리
     *
     * @param userId
     * @return
     */
    @GetMapping("/captcha/success")
    public ResponseEntity<UserDto> resetPasswordCnt(@RequestParam("userId") String userId) {
        return ResponseEntity.ok(this.userLoginService.unlockUser(userId));
    }
}
