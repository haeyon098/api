package kr.co.apexsoft.granet2.admin_api.download.service;

import kr.co.apexsoft.gradnet2.common._common.util.DateUtil;
import kr.co.apexsoft.gradnet2.common.crypto.CryptoService;
import kr.co.apexsoft.gradnet2.common.excel.ExcelGenerator;
import kr.co.apexsoft.gradnet2.common.file.FileDto;
import kr.co.apexsoft.gradnet2.common.file.FileUtils;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplicationListForExcelResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.granet2.admin_api.download.dto.ApplicationStatusOut;
import kr.co.apexsoft.granet2.admin_api.download.dto.ApplicationsOut;
import kr.co.apexsoft.granet2.admin_api.download.dto.CompleteDownloadCondition;
import kr.co.apexsoft.granet2.admin_api.download.service.complete.CompleteDownloadService;
import kr.co.apexsoft.granet2.admin_api.search.dto.ApplSearchCondition;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-04-21
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ExcelService {
    private final ApplRepository applRepository;

    private final CryptoService cryptoService;

    @NonNull
    private List<CompleteDownloadService> completeDownloadServices;

    /**
     * 지원자 정보 엑셀 다운로드
     * 항목정의 &확정 필요
     *
     * @param condition
     * @return
     */
    public FileDto applicantsExcelFileDown(ApplSearchCondition condition) {
        List<Appl> list =
                applRepository.findApplicantsDetail(
                        condition.getSchoolCode(),
                        condition.getEnterYear(),
                        condition.getRecruitPartSeq(),
                        condition.getAdmissionCode(),
                        condition.getCourseCode(),
                        condition.getCategoryCode(),
                        condition.getMajorCode(),
                        condition.getApplId());

        List<ApplicationsOut> results = list.stream().map(appl ->
                new ApplicationsOut(
                        appl.getApplId(),
                        getRegistNo(appl.getApplicantInfo().getRegistrationBornDate(), appl.getApplicantInfo().getRegistrationEncr()),
                        cryptoService.decrypt(appl.getForeignerInfo().getForeignerRegistrationNo()),
                        cryptoService.decrypt(appl.getForeignerInfo().getPassportNo()),
                        cryptoService.decrypt(appl.getForeignerInfo().getVisaNo())
                )
        ).collect(Collectors.toList());

        // 헤더
        String[] excelHeader = {"수험번호", "주민등록번호", "외국인등록번호", "여권번호", "비자번호"};

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.dataToExcel(excelHeader, results));

        return new FileDto("복호화정보", resource);
    }

    /**
     * 주민번호
     * @param borndate
     * @param regstEncr
     * @return
     */
    private String getRegistNo(LocalDate borndate,String regstEncr){
        return StringUtils.isBlank(regstEncr)?"": DateUtil.convertFormat(borndate,"yyMMdd") +"-"+cryptoService.decrypt(regstEncr);
    }

    /**
     * 학교관리자 지원현황 조회 for Excel
     * KDI 전용으로, 학교공통으로 확대 필요
     * 2020.06.16 최은주
     *
     * @param condition
     * @return
     */
    public FileDto applicantsAllListExcelFileDown(ApplSearchCondition condition) {

        List<ApplicationListForExcelResponse> applList = applRepository.findApplDeduplicateList(condition.getSchoolCode(), condition.getEnterYear(), condition.getRecruitPartSeq());

        List<ApplicationStatusOut> results = applList.stream().map(list ->
                new ApplicationStatusOut(
                        list.getUserId(),
                        list.getApplNo(),
                        list.getSeqForYear(),
                        list.getApplId(),
                        list.getSmstTypeKorName(),
                        list.getAdmsKorName(),
                        list.getCorsKorName(),
                        list.getCategoryEngName(),
                        list.getMajKorName(),
                        list.getKorName(),
                        list.getEngName(),
                        list.getApplStatusName(),
                        list.getPhoneNumber(),
                        list.getEmail(),
                        list.getRecRequestCnt(),
                        list.getRecSubmitCnt(),
                        list.getCountry(),
                        list.getCountryGroupCode(),
                        list.getCountryGroupName(),
                        list.getEmpCat(),
                        list.getRgstBornDate(),
                        list.getGend(),
                        list.getVideoEssay()
                )
        ).collect(Collectors.toList());


        // 헤더
        String[] excelHeader = {"사용자ID", "입학지원번호", "회차", "수험번호", "학기구분", "모집전형", "모집과정", "지원구분", "전공",
                                "한글이름", "영문이름", "지원상태", "휴대폰번호", "이메일", "추천서 요청수", "추천서 제출수", "국가", "대륙코드", "대륙명",
                		            "Emp.Cat", "DOB", "GENDER", "Video Essay" };

        // data to Excel
        InputStreamResource resource = FileUtils.retrieveInputStreamResource(ExcelGenerator.dataToExcel(excelHeader, results));


        LocalDate currentDate = LocalDate.now();
        String toDay = currentDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));

        //2020.08.26 기본적으로 시스템이 한 회차당 하나의 아이디로 한건의 지원을 하도록 하였음
        //추후 중복쿼리부분 삭제해도되나 현재 결과는 동일하여 유지함
        //return new FileDto(toDay+"_지원현황_중복제외", resource);
       return new FileDto(toDay+"_지원현황", resource);
    }


    /**
     * 학교별 지원완료 데이터
     * @param condition
     * @return
     */
    public FileDto applicantsCompleteFileDown(CompleteDownloadCondition condition){
        return completeDownloadServices.stream().filter(it -> it.supports(condition.getSchoolCode())).findFirst().get().download(condition);

    }
}

