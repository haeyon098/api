package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.CorsCount;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-20
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class CompletedApplCountByCors {

    private final String courseName;
    private final Long count;

    public static CompletedApplCountByCors from(CorsCount corsCount) {
        return new CompletedApplCountByCors(corsCount.getCorsName(), corsCount.getCounts());
    }
}
