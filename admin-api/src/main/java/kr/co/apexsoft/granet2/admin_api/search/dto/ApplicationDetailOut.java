package kr.co.apexsoft.granet2.admin_api.search.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplDetailProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.CompleteDocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.DocumentEtcNameProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.granet2.admin_api.basic.dto._out.CodeNameOut;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-18
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationDetailOut {

    private ApplSimple applSimple;
    private Adms adms;
    private ApplicantInfo applicantInfo;
    private List<DocItem> docItems;
    private List<DocItem> completeDocItems;
    private List<RecommendItem> recommendItems;

    public static ApplicationDetailOut from(ApplDetailProjection ap, List<DocumentEtcNameProjection> dps,
                                            List<CompleteDocumentProjection> cps, List<RecSimpleOut> recList ) {
        String applDate = ap.getApplDate() == null ? "" : ap.getApplDate().toLocalDate().toString();
        String gender = ap.getGender() == null ? "" : ap.getGender().equals("F") ? "여성" : "남성";
        String dateOfBirth = ap.getDateOfBirth() == null ? "" : ap.getDateOfBirth().toString();
        String address = ap.getAddress() == null ? "" : ap.getAddress().getFullAddress();
        return new ApplicationDetailOut(
                new ApplSimple(ap.getApplNo(), ap.getApplicantId(), CodeNameOut.of(ap.getStatus().name(), ap.getStatus().getKorName(), ap.getStatus().getEngName())),
                new Adms(ap.getApplId(), applDate,
                        CodeNameOut.of(String.valueOf(ap.getRecruitPartSeq()), ap.getRecruitPartName(), ap.getRecruitPartEngName()),
                        CodeNameOut.of(ap.getAdmissionCode(), ap.getAdmissionName(), ap.getAdmissionEngName()),
                        CodeNameOut.of(ap.getCourseCode(), ap.getCourseName(), ap.getCourseEngName()),
                        CodeNameOut.of(ap.getCategoryCode(), ap.getCategoryName(), ap.getCategoryEngName()),
                        CodeNameOut.of(ap.getMajorCode(), ap.getMajorName(), ap.getMajorEngName())),
                new ApplicantInfo(ap.getApplicantName(), ap.getApplicantEngName(), dateOfBirth, gender,
                        ap.getTelNumber(), ap.getPhoneNumber(), address, ap.getEmail(),
                        ap.getEmergencyName(), ap.getEmergencyTel()),
                dps.stream().map(dp -> new DocItem(dp.getEngItemName(), dp.getOriginFileName(), dp.getFilePath())).collect(Collectors.toList()),
                cps.stream().map(cp -> new DocItem(cp.getKorItemName(), cp.getOriginFileName(), cp.getFilePath())).collect(Collectors.toList()),
                recList.stream().map(rp -> new RecommendItem(rp.getProfName(), rp.getProfEmail(), rp.getRecStatus())).collect(Collectors.toList())
        );
    }

    @Getter
    @AllArgsConstructor
    static class ApplSimple {
        Long applNo;
        String applicantId;
        CodeNameOut status;
    }

    @Getter
    @AllArgsConstructor
    static class Adms {
        String applId;
        String applDate;
        CodeNameOut recruitPart;
        CodeNameOut admission;
        CodeNameOut course;
        CodeNameOut category;
        CodeNameOut major;
    }

    @Getter
    @AllArgsConstructor
    static class ApplicantInfo {
        String name;
        String engName;
        String dateOfBirth;
        String gender;
        String telNumber;
        String phoneNumber;
        String address;
        String email;
        String emergencyName;
        String emergencyTel;
    }

    @Getter
    @AllArgsConstructor
    static class DocItem {
        String type;
        String name;
        String link;
    }

    @Getter
    @AllArgsConstructor
    static class RecommendItem {
        String profName;
        String profEmail;
        CodeNameOut recStatus;
    }
}
