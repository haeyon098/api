package kr.co.apexsoft.granet2.admin_api._config.security;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import kr.co.apexsoft.granet2.admin_api.user.exception.UserNotFoundException;
import kr.co.apexsoft.granet2.admin_api.user.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by hanmomhanda@gmail.com
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomUserDetailsService implements UserDetailsService {
    @NonNull
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String userId) {
        User user = this.userRepository.findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(userId, "존재하지 않는 회원입니다."));

        return new UserPrincipal(user.getId(),
                user.getUserId(), user.getPassword(), user.getRoles(), !user.getLocked(), user.checkActiveUser(), user);
    }
}
