package kr.co.apexsoft.granet2.admin_api.basic.dto._out;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;


@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RecruitSchoolOut {


    private final String schoolCode;
    private final String schoolName;
    private final String schoolEngName;




    public static RecruitSchoolOut from( RecruitSchool school) {

        return new RecruitSchoolOut(school.getId(),school.getKorName(),school.getEngName() );
    }

}
