package kr.co.apexsoft.granet2.admin_api.search.controller;

import kr.co.apexsoft.granet2.admin_api.search.dto.ApplSearchCondition;
import kr.co.apexsoft.granet2.admin_api.search.dto.ApplZipPrintedOut;
import kr.co.apexsoft.granet2.admin_api.search.dto.ApplicantDetailOut;
import kr.co.apexsoft.granet2.admin_api.search.dto.ApplicationSimpleForListOut;
import kr.co.apexsoft.granet2.admin_api.search.service.ApplicationSearchService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-27
 */
@RestController
@RequestMapping("/admin/applications")
@RequiredArgsConstructor
public class SearchController {

    private final ApplicationSearchService applicationSearchService;

    @GetMapping("/{id}/applicant")
    public ResponseEntity<ApplicantDetailOut> getApplicantDetail(@PathVariable("id") Long applicationId) {
        return ResponseEntity.ok(applicationSearchService.getApplicantDetail(applicationId));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getApplicationSimple(
            @PathVariable("id") Long applicationId,
            @RequestParam(value = "simple", defaultValue = "false") boolean simple) {
        if (simple) {
            return ResponseEntity.ok(applicationSearchService.getApplicationSimple(applicationId));
        } else {
            return ResponseEntity.ok(applicationSearchService.getApplicationDetail(applicationId));
        }

    }

    @GetMapping
    public ResponseEntity<Page<ApplicationSimpleForListOut>> getApplicationSimplesForList(ApplSearchCondition condition,
                                                                                          @PageableDefault(size = 20) Pageable pageable) {
        return ResponseEntity.ok(applicationSearchService.getApplicationSimplesForList(condition, pageable));
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ApplZipPrintedOut> patchZipPrinted(@PathVariable("id") Long applNo, @RequestParam("zipPrinted") Boolean zipPrinted) {
        return ResponseEntity.ok(applicationSearchService.patchZipPrinted(applNo, zipPrinted));
    }
}
