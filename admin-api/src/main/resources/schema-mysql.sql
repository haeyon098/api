-- --------------------------------------------------------
-- 호스트:                          apexsoft.iptime.org
-- 서버 버전:                        5.5.50-0ubuntu0.14.04.1 - (Ubuntu)
-- 서버 OS:                        debian-linux-gnu
-- HeidiSQL 버전:                  10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 참조되지 않는 테이블부터 delete
DROP TABLE IF EXISTS `USER_ROLES`;
DROP TABLE IF EXISTS `SYS_COMM_CODE`;
DROP TABLE IF EXISTS `SMST_TYPE`;
DROP TABLE IF EXISTS `SCHL`;
DROP TABLE IF EXISTS `ROLE`;
DROP TABLE IF EXISTS `RECR_SCHL_COMM_CODE`;
DROP TABLE IF EXISTS `RECR_SCHL`;
DROP TABLE IF EXISTS `RECR_PART_TIME`;
DROP TABLE IF EXISTS `RECR_PART`;
DROP TABLE IF EXISTS `old_RECR_SCHL_COMM_CODE`;
DROP TABLE IF EXISTS `old_CORS`;
DROP TABLE IF EXISTS `old_CATEGORY`;
DROP TABLE IF EXISTS `old_ADMS`;
DROP TABLE IF EXISTS `MAJ`;
DROP TABLE IF EXISTS `CORS`;
DROP TABLE IF EXISTS `CNTR`;
DROP TABLE IF EXISTS `CATEGORY`;
DROP TABLE IF EXISTS `APPL_REC_PROF_ANS`;
DROP TABLE IF EXISTS `APPL_REC_PROF_INFO`;
DROP TABLE IF EXISTS `APPL_REC`;
DROP TABLE IF EXISTS `APPL_PART_DOC`;
DROP TABLE IF EXISTS `APPL_PART`;
DROP TABLE IF EXISTS `APPL_LANG`;
DROP TABLE IF EXISTS `APPL_GEN`;
DROP TABLE IF EXISTS `APPL_FORN`;
DROP TABLE IF EXISTS `APPL_ESSAY`;
DROP TABLE IF EXISTS `APPL_DOC`;
DROP TABLE IF EXISTS `APPL_CAREER`;
DROP TABLE IF EXISTS `APPL_ACAD`;
DROP TABLE IF EXISTS `APPL`;
DROP TABLE IF EXISTS `USER`;
DROP TABLE IF EXISTS `ADMS`;

-- 테이블 gradnet2_dev.ADMS 구조 내보내기
CREATE TABLE `ADMS` (
                                      `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                      `ADMS_CODE` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '모집전형코드',
                                      `ADMS_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집전형한글명',
                                      `ADMS_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집전형영문명',
                                      `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                      `CREATED_DATE` datetime DEFAULT NULL,
                                      `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                      `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                      PRIMARY KEY (`RECR_SCHL_CODE`,`ADMS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='모집전형';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.USER 구조 내보내기
CREATE TABLE `USER` (
                                      `USER_NO` bigint(20) NOT NULL AUTO_INCREMENT,
                                      `USER_ID` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                      `CONFIRM_YN` char(1) COLLATE utf8_bin DEFAULT NULL,
                                      `EMAIL` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                      `FAIL_CNT` int(11) DEFAULT NULL,
                                      `LOCK_YN` char(1) COLLATE utf8_bin DEFAULT NULL,
                                      `PASSWORD` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                      `STATUS` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                      `CREATED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                      `CREATED_DATE` datetime DEFAULT NULL,
                                      `LAST_MODIFIED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                      `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                      PRIMARY KEY (`USER_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL 구조 내보내기
CREATE TABLE `APPL` (
                                      `APPL_NO` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '입학지워번호',
                                      `USER_NO` bigint(20) DEFAULT NULL COMMENT '회원번호',
                                      `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집학교코드',
                                      `ENTR_YEAR` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '입학연도',
                                      `RECR_PART_SEQ` int(11) DEFAULT NULL COMMENT '모집회차',
                                      `APPL_PART_NO` bigint(20) DEFAULT NULL COMMENT '지원단위 번호',
                                      `KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '한글이름',
                                      `ENG_NAME` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '영문이름',
                                      `APPL_STS_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '지원상태코드',
                                      `APPL_DATE` datetime DEFAULT NULL COMMENT '지원일시',
                                      `CITZ_CNTR_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '국적 국가코드',
                                      `ETC_CITZ_CNTR_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '직접입력국적 국가명',
                                      `RGST_BORN_DATE` date DEFAULT NULL COMMENT '주민등록번호 앞자리',
                                      `RGST_ENCR` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '주민등록번호 뒷자리',
                                      `APPL_ID` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '수험번호',
                                      `EMAIL` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '이메일',
                                      `GEND` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '성별',
                                      `ADDR` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '주소',
                                      `DETL_ADDR` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '상세주소',
                                      `POST_NO` varchar(18) COLLATE utf8_bin DEFAULT NULL COMMENT '우편번호',
                                      `PHONE_NUM` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '휴대폰번호',
                                      `TEL_NUM` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '전화번호',
                                      `DOC_CHK_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '첨부파일 안내 확인여부',
                                      `PRIV_INFO_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '개인정보 제3자 동의여부',
                                      `ZIP_PRINT_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT 'zip출력여부',
                                      `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                      `CREATED_DATE` datetime DEFAULT NULL,
                                      `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                      `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                      PRIMARY KEY (`APPL_NO`),
                                      KEY `FK1v9dkh65uunb5m382fbs2gu15` (`USER_NO`),
                                      KEY `FKhrmlimf5qp8twejy2x5y0cxsx` (`RECR_SCHL_CODE`,`ENTR_YEAR`,`RECR_PART_SEQ`,`APPL_PART_NO`),
                                      CONSTRAINT `FK1v9dkh65uunb5m382fbs2gu15` FOREIGN KEY (`USER_NO`) REFERENCES `USER` (`USER_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='입학지원';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_ACAD 구조 내보내기
CREATE TABLE `APPL_ACAD` (
                                           `APPL_ACAD_NO` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '지원자학력번호',
                                           `APPL_NO` bigint(20) DEFAULT NULL COMMENT '입학지원번호',
                                           `ACAD_SEQ` int(11) DEFAULT NULL COMMENT '학력정보 순번',
                                           `ACAD_TYPE_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '학력 구분코드',
                                           `ACAD_STS_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '학력 상태코드',
                                           `LAST_SCHL_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '최종학교 여부',
                                           `SCHL_CNTR_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '학교 소재국가코드',
                                           `ETC_SCHL_CNTR_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '직접입력학교 소재 국가명',
                                           `SCHL_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '학교코드',
                                           `SCHL_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '학교명',
                                           `COLL_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '단과대학명',
                                           `MAJ_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '전공학과명',
                                           `ENTR_DAY` date DEFAULT NULL COMMENT '입학일자',
                                           `DEGR_NO` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '학위등록번호',
                                           `GRDA_DAY` date DEFAULT NULL COMMENT '졸업일자',
                                           `GRAD_FORM_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '평점형식코드',
                                           `GRAD_AVR` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '평점평균',
                                           `GARD_FULL` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '만점',
                                           `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `CREATED_DATE` datetime DEFAULT NULL,
                                           `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                           PRIMARY KEY (`APPL_ACAD_NO`),
                                           KEY `FKflflwial0p0jb0hceh5ehoj90` (`APPL_NO`),
                                           CONSTRAINT `FKflflwial0p0jb0hceh5ehoj90` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=522 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자학력정보';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_CAREER 구조 내보내기
CREATE TABLE `APPL_CAREER` (
                                             `APPL_CAREER_NO` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '지원자경력번호',
                                             `APPL_NO` bigint(20) DEFAULT NULL COMMENT '입학지원번호',
                                             `CAREER_SEQ` int(11) DEFAULT NULL COMMENT '경력순번',
                                             `COMPANY_NAME` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '기관명',
                                             `COMPANY_ADDRESS` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '기관주소',
                                             `COMPANY_DEPARTMENT` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '부서',
                                             `COMPANY_POSITION` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '직급',
                                             `COMPANY_TYPE_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '기관구분코드',
                                             `CURR_YN` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '현재 재직여부',
                                             `CAREER_DESC` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '경력 설명 ',
                                             `JOIN_DAY` date DEFAULT NULL COMMENT '입사일자',
                                             `RETR_DAY` date DEFAULT NULL COMMENT '퇴사일자',
                                             `CREATED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                             `CREATED_DATE` datetime DEFAULT NULL,
                                             `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                             `LAST_MODIFIED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                             PRIMARY KEY (`APPL_CAREER_NO`),
                                             KEY `FKcafsxcql1qcppsk8p83vaj3h9` (`APPL_NO`),
                                             CONSTRAINT `FKcafsxcql1qcppsk8p83vaj3h9` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자 경력';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_DOC 구조 내보내기
CREATE TABLE `APPL_DOC` (
                                          `APPL_DOC_NO` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '지원자제출서류번호',
                                          `APPL_NO` bigint(20) DEFAULT NULL COMMENT '입학지원번호',
                                          `DOC_GRP_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '제출서류 그룹코드',
                                          `DOC_GRP_NO` int(11) DEFAULT NULL COMMENT '제출서류 그룹순번',
                                          `DOC_ITEM_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '제출서류 항목코드',
                                          `DOC_ITEM_NAME` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '제출서류 항목명',
                                          `ORG_FILE_NAME` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT '원본 파일명',
                                          `FILE_PATH` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '파일 경로',
                                          `PDF_PAGE_CNT` int(11) DEFAULT NULL COMMENT 'PDF 페이지 수',
                                          `ETC_SEQ` int(11) DEFAULT NULL COMMENT '기타파일 순번',
                                          `IMG_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '이미지 여부',
                                          `PDF_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT 'PDF 파일 여부',
                                          `FILE_EXT` varchar(5) COLLATE utf8_bin DEFAULT NULL COMMENT '파일 확장자',
                                          `CREATED_DATE` datetime DEFAULT NULL,
                                          `CREATED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                          `LAST_MODIFIED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                          `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                          PRIMARY KEY (`APPL_DOC_NO`),
                                          KEY `FKfnk2jut2fiasxara0g4yn6ks8` (`APPL_NO`),
                                          CONSTRAINT `FKfnk2jut2fiasxara0g4yn6ks8` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자 제출서류';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_ESSAY 구조 내보내기
CREATE TABLE `APPL_ESSAY` (
                                            `ESSAY_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '서술입력 항목 구분 코드',
                                            `APPL_NO` bigint(20) NOT NULL COMMENT '입학지원 번호',
                                            `DATA` mediumtext COLLATE utf8_bin COMMENT '데이터',
                                            `CREATED_DATE` datetime DEFAULT NULL,
                                            `CREATED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                            `LAST_MODIFIED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                            `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                            PRIMARY KEY (`ESSAY_TYPE_CODE`,`APPL_NO`),
                                            KEY `FKabfkm58sm6n2i920e34smd1` (`APPL_NO`),
                                            CONSTRAINT `FKabfkm58sm6n2i920e34smd1` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자 서술입력 항목';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_FORN 구조 내보내기
CREATE TABLE `APPL_FORN` (
                                           `APPL_NO` bigint(20) NOT NULL COMMENT '입학지원번호',
                                           `FORN_TYPE_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '외국인 구분코드',
                                           `FORN_RGST_NO` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '외국인 등록번호',
                                           `PASSPORT_NO` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '여권번호',
                                           `VISA_NO` varchar(1000) COLLATE utf8_bin DEFAULT NULL COMMENT '비자번호',
                                           `VISA_TYPE_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '비자종류코드',
                                           `VISA_TYPE_ETC` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '비자유형 직접입력값',
                                           `VISA_EXPR_DAY` date DEFAULT NULL COMMENT '비자 만료일',
                                           `HOME_TEL_NUM` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '본국 연락처',
                                           `HOME_ADDR` varchar(500) COLLATE utf8_bin DEFAULT NULL COMMENT '본국 주소',
                                           `HOME_EMRG_NAME` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '본국 비상연락처 이름',
                                           `HOME_EMRG_RELA` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '본국 비상연락처 관계',
                                           `HOME_EMRG_TEL` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '본국 비상연락처 전화번호',
                                           `KOR_EMRG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '국내 비상연락처 이름',
                                           `KOR_EMRG_RELA` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '국내 비상연락처 관계',
                                           `KOR_EMRG_TEL` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '국내 비상연락처 전화번호',
                                           PRIMARY KEY (`APPL_NO`),
                                           CONSTRAINT `FKevu083kijw0a0lp046girl1rn` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자외국인정보';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_GEN 구조 내보내기
CREATE TABLE `APPL_GEN` (
                                          `APPL_NO` bigint(20) NOT NULL COMMENT '입학지원번호',
                                          `EMGR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '비상연락처 이름',
                                          `EMGR_RELA` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '비상연락처 관계',
                                          `EMRG_TEL` varchar(20) COLLATE utf8_bin DEFAULT NULL COMMENT '비상연락처 전화번호',
                                          `HNDC_GRAD` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '장애등급',
                                          `HNDC_TYPE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '장애유형',
                                          PRIMARY KEY (`APPL_NO`),
                                          CONSTRAINT `FKqu8x1e490o3scjx7g8xamwwi6` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자내국인정보';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_LANG 구조 내보내기
CREATE TABLE `APPL_LANG` (
                                           `APPL_LANG_NO` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '지원자어학정보번호',
                                           `APPL_NO` bigint(20) DEFAULT NULL COMMENT '입학 지원번호',
                                           `LANG_SEQ` int(11) DEFAULT NULL COMMENT '어학정보 순번',
                                           `LANG_TYPE_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '어학 타입코드',
                                           `LANG_INFO_TYPE_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '어학 정보 구분코드',
                                           `LANG_SUB_GRP_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '어학 하위 그룹코드',
                                           `SUB_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '어학 서브코드',
                                           `EXAM_DAY` date DEFAULT NULL COMMENT '시험 일자',
                                           `EXPR_DAY` date DEFAULT NULL COMMENT '만료 일자',
                                           `SCORE` int(11) DEFAULT NULL COMMENT '점수',
                                           `CREATED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                           `CREATED_DATE` datetime DEFAULT NULL,
                                           `LAST_MODIFIED_BY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                           `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                           PRIMARY KEY (`APPL_LANG_NO`),
                                           KEY `FKcxnydhmut4nc754cgsrx8hdw3` (`APPL_NO`),
                                           CONSTRAINT `FKcxnydhmut4nc754cgsrx8hdw3` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=296 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원자 어학정보';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_PART 구조 내보내기
CREATE TABLE `APPL_PART` (
                                           `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                           `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '입학년도',
                                           `RECR_PART_SEQ` int(11) NOT NULL COMMENT '모집회차',
                                           `APPL_PART_NO` bigint(20) NOT NULL COMMENT '지원단위번호',
                                           `SMST_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '학기구분코드',
                                           `ADMS_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집전형코드',
                                           `CATEGORY_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '지원구분코드',
                                           `CORS_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집과정코드',
                                           `MAJ_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '전공코드',
                                           `APPL_PART_YN` char(1) COLLATE utf8_bin DEFAULT NULL COMMENT '지원가능여부',
                                           `REMK` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '비고',
                                           `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `CREATED_DATE` datetime DEFAULT NULL,
                                           `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                           PRIMARY KEY (`RECR_SCHL_CODE`,`ENTR_YEAR`,`RECR_PART_SEQ`,`APPL_PART_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원단위';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_PART_DOC 구조 내보내기
CREATE TABLE `APPL_PART_DOC` (
                                               `APPL_PART_DOC_NO` bigint(20) NOT NULL AUTO_INCREMENT,
                                               `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                               `ENTR_YEAR` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                               `RECR_PART_SEQ` int(11) DEFAULT NULL,
                                               `APPL_PART_NO` bigint(20) DEFAULT NULL,
                                               `DOC_GRP_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                               `DOC_ITEM_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                               `UPLOAD_YN` char(50) COLLATE utf8_bin DEFAULT NULL,
                                               `MDT_YN` char(50) COLLATE utf8_bin DEFAULT NULL,
                                               `SEND_CNT` int(11) DEFAULT NULL,
                                               `MSG_NO` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                               `TMPLT_YN` char(50) COLLATE utf8_bin DEFAULT NULL,
                                               PRIMARY KEY (`APPL_PART_DOC_NO`),
                                               KEY `FK_APPL_PART_DOC_APPL_PART` (`RECR_SCHL_CODE`,`ENTR_YEAR`,`RECR_PART_SEQ`,`APPL_PART_NO`),
                                               CONSTRAINT `FK_APPL_PART_DOC_APPL_PART` FOREIGN KEY (`RECR_SCHL_CODE`, `ENTR_YEAR`, `RECR_PART_SEQ`, `APPL_PART_NO`) REFERENCES `APPL_PART` (`RECR_SCHL_CODE`, `ENTR_YEAR`, `RECR_PART_SEQ`, `APPL_PART_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원단위별 제출서류';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.APPL_REC 구조 내보내기
CREATE TABLE `APPL_REC` (
                                          `APPL_REC_NO` bigint(20) NOT NULL AUTO_INCREMENT,
                                          `APPL_NO` bigint(20) DEFAULT NULL,
                                          `PROF_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `PROF_EMAIL` varchar(100) COLLATE utf8_bin DEFAULT NULL,
                                          `PROF_INST` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `PROF_DEPT` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `PROF_PHONE_NUM` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `REC_TITLE` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `REC_KEY` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                          `REC_STS_CODE` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `CREATED_DATE` datetime DEFAULT NULL,
                                          `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                          PRIMARY KEY (`APPL_REC_NO`),
                                          KEY `FKa7jykg8219fhjm62lv68i2fcg` (`APPL_NO`),
                                          CONSTRAINT `FKa7jykg8219fhjm62lv68i2fcg` FOREIGN KEY (`APPL_NO`) REFERENCES `APPL` (`APPL_NO`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='추천서 요청';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 gradnet2_dev.APPL_REC_PROF_INFO 구조 내보내기
CREATE TABLE `APPL_REC_PROF_INFO` (
                                                    `APPL_REC_NO` bigint(20) NOT NULL,
                                                    `PROF_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `PROF_EMAIL` varchar(100) COLLATE utf8_bin DEFAULT NULL,
                                                    `PROF_INST` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `PROF_DEPT` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `PROF_PHONE_NUM` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `PROF_SIGN` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `REC_Day` datetime DEFAULT NULL,
                                                    `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `CREATED_DATE` datetime DEFAULT NULL,
                                                    `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                    `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                                    PRIMARY KEY (`APPL_REC_NO`),
                                                    CONSTRAINT `FKglfcxruabhrsslxy3lf8mpl3p` FOREIGN KEY (`APPL_REC_NO`) REFERENCES `APPL_REC` (`APPL_REC_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='추천서 작성';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 gradnet2_dev.APPL_REC_PROF_ANS 구조 내보내기
CREATE TABLE `APPL_REC_PROF_ANS` (
                                                   `ID` bigint(20) NOT NULL AUTO_INCREMENT,
                                                   `ANSWER` varchar(100) COLLATE utf8_bin DEFAULT NULL,
                                                   `QST_NO` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                   `APPL_REC_NO` bigint(20) DEFAULT NULL,
                                                   `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                   `CREATED_DATE` datetime DEFAULT NULL,
                                                   `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                   `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                                   PRIMARY KEY (`ID`),
                                                   KEY `FKosjmjnbyj3mlhd97vvc4433ci` (`APPL_REC_NO`),
                                                   CONSTRAINT `FKosjmjnbyj3mlhd97vvc4433ci` FOREIGN KEY (`APPL_REC_NO`) REFERENCES `APPL_REC_PROF_INFO` (`APPL_REC_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='추천서 답변';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.CATEGORY 구조 내보내기
CREATE TABLE `CATEGORY` (
                                          `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                          `CATEGORY_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '지원구분코드',
                                          `CATEGORY_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '지원구분한글명',
                                          `CATEGORY_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '지원구분영문명',
                                          `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `CREATED_DATE` datetime DEFAULT NULL,
                                          `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                          `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                          PRIMARY KEY (`RECR_SCHL_CODE`,`CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='지원구분';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.CNTR 구조 내보내기
CREATE TABLE `CNTR` (
                                      `CNTR_CODE` varchar(5) NOT NULL DEFAULT '' COMMENT '국가코드',
                                      `CNTR_KOR_NAME` varchar(100) DEFAULT NULL COMMENT '한글국가명',
                                      `CNTR_ENG_NAME` varchar(100) DEFAULT NULL COMMENT '영문국가명',
                                      `CNTR_ALPHA_TWO` varchar(2) DEFAULT NULL,
                                      `CNTR_ALPHA_THREE` varchar(3) DEFAULT NULL,
                                      `CNTR_GRP_CD` varchar(2) DEFAULT NULL,
                                      `CNTR_GRP_NM` varchar(20) DEFAULT NULL,
                                      `CREATED_BY` varchar(100) DEFAULT NULL COMMENT '생성자',
                                      `CREATED_DATE` datetime DEFAULT NULL COMMENT '생성일시',
                                      `LAST_MODIFIED_BY` varchar(100) DEFAULT NULL COMMENT '수정자',
                                      `LAST_MODIFIED_DATE` datetime DEFAULT NULL COMMENT '수정일시',
                                      PRIMARY KEY (`CNTR_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='국가';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.CORS 구조 내보내기
CREATE TABLE `CORS` (
                                      `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                      `CORS_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집과정코드',
                                      `CORS_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집과정한글명',
                                      `CORS_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집과정영문명',
                                      `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                      `CREATED_DATE` datetime DEFAULT NULL,
                                      `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                      `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                      PRIMARY KEY (`RECR_SCHL_CODE`,`CORS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='모집과정';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.MAJ 구조 내보내기
CREATE TABLE `MAJ` (
                                     `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL,
                                     `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL,
                                     `RECR_PART_SEQ` int(11) NOT NULL,
                                     `MAJ_CODE` varchar(50) COLLATE utf8_bin NOT NULL,
                                     `MAJ_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                     `MAJ_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                     `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                     `CREATED_DATE` datetime DEFAULT NULL,
                                     `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                     `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                     PRIMARY KEY (`RECR_SCHL_CODE`,`ENTR_YEAR`,`RECR_PART_SEQ`,`MAJ_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='전공';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.old_ADMS 구조 내보내기
CREATE TABLE `old_ADMS` (
                                          `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                          `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '학기구분코드',
                                          `SMST_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '입학년도',
                                          `ADMS_CODE` varchar(50) COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '모집전형코드',
                                          `ADMS_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집전형한글명',
                                          `ADMS_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집전형영문명',
                                          PRIMARY KEY (`RECR_SCHL_CODE`,`ENTR_YEAR`,`SMST_TYPE_CODE`,`ADMS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='모집전형';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.old_CATEGORY 구조 내보내기
CREATE TABLE `old_CATEGORY` (
                                              `RECR_SCR_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                              `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '입학년도',
                                              `SMST_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '학기구분코드',
                                              `CATEGORY_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '지원구분코드',
                                              `CATEGORY_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '지원구분한글명',
                                              `CATEGORY_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '지원구분영문명',
                                              PRIMARY KEY (`RECR_SCR_CODE`,`ENTR_YEAR`,`SMST_TYPE_CODE`,`CATEGORY_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='지원구분';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.old_CORS 구조 내보내기
CREATE TABLE `old_CORS` (
                                          `RECR_SCR_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                          `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '학기구분코드',
                                          `SMST_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '입학년도',
                                          `CORS_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집과정코드',
                                          `CORS_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집과정한글명',
                                          `CORS_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집과정영문명',
                                          PRIMARY KEY (`RECR_SCR_CODE`,`ENTR_YEAR`,`SMST_TYPE_CODE`,`CORS_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='모집과정';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.old_RECR_SCHL_COMM_CODE 구조 내보내기
CREATE TABLE `old_RECR_SCHL_COMM_CODE` (
                                                         `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                                         `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '입학연도',
                                                         `RECR_PART_SEQ` int(11) NOT NULL COMMENT '학기코드',
                                                         `CODE_GRP` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '공통코드 그룹',
                                                         `CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '코드',
                                                         `CODE_KOR_VAL` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '한글 코드값',
                                                         `CODE_ENG_VAL` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '영문 코드값',
                                                         `USE_YN` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '사용여부'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.RECR_PART 구조 내보내기
CREATE TABLE `RECR_PART` (
                                           `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL,
                                           `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL,
                                           `RECR_PART_SEQ` int(11) NOT NULL,
                                           `RECR_PART_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `RECR_PART_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `START_DATE` datetime DEFAULT NULL,
                                           `END_DATE` datetime DEFAULT NULL,
                                           `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `CREATED_DATE` datetime DEFAULT NULL,
                                           `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                           PRIMARY KEY (`RECR_SCHL_CODE`,`ENTR_YEAR`,`RECR_PART_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='모집단위';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.RECR_PART_TIME 구조 내보내기
CREATE TABLE `RECR_PART_TIME` (
                                                `ID` bigint(20) NOT NULL AUTO_INCREMENT,
                                                `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL,
                                                `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL,
                                                `RECR_PART_SEQ` int(11) NOT NULL,
                                                `TIME_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL,
                                                `START_DATE` datetime DEFAULT NULL,
                                                `END_DATE` datetime DEFAULT NULL,
                                                `TIME_DESC` varchar(255) COLLATE utf8_bin DEFAULT NULL,
                                                `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                `CREATED_DATE` datetime DEFAULT NULL,
                                                `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                                `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                                PRIMARY KEY (`ID`),
                                                KEY `RECR_PART_KEY` (`RECR_SCHL_CODE`,`ENTR_YEAR`,`RECR_PART_SEQ`),
                                                CONSTRAINT `FK_RECR_PART_TIME_RECR_PART` FOREIGN KEY (`RECR_SCHL_CODE`, `ENTR_YEAR`, `RECR_PART_SEQ`) REFERENCES `RECR_PART` (`RECR_SCHL_CODE`, `ENTR_YEAR`, `RECR_PART_SEQ`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='모집단위별시간';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.RECR_SCHL 구조 내보내기
CREATE TABLE `RECR_SCHL` (
                                           `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                           `RECR_SCHL_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집학교한글명',
                                           `RECR_SCHL_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '모집학교영문명',
                                           `NOTICE` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '공지사항',
                                           `USE_YN` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '사용여부',
                                           `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `CREATED_DATE` datetime DEFAULT NULL,
                                           `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                           PRIMARY KEY (`RECR_SCHL_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='모집학교';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.RECR_SCHL_COMM_CODE 구조 내보내기
CREATE TABLE `RECR_SCHL_COMM_CODE` (
                                                     `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                                     `ENTR_YEAR` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '입학연도',
                                                     `RECR_PART_SEQ` int(11) NOT NULL COMMENT '학기코드',
                                                     `CODE_GRP` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '공통코드 그룹',
                                                     `CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '코드',
                                                     `CODE_KOR_VAL` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '한글 코드값',
                                                     `CODE_ENG_VAL` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '영문 코드값',
                                                     `USE_YN` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '사용여부',
                                                     PRIMARY KEY (`CODE`,`CODE_GRP`,`ENTR_YEAR`,`RECR_SCHL_CODE`,`RECR_PART_SEQ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='학교별공통코드';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.ROLE 구조 내보내기
CREATE TABLE `ROLE` (
                                      `ROLE_ID` bigint(20) NOT NULL AUTO_INCREMENT,
                                      `ROLE_TYPE` varchar(60) COLLATE utf8_bin DEFAULT NULL,
                                      PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.SCHL 구조 내보내기
CREATE TABLE `SCHL` (
                                      `SCHL_CODE` varchar(10) NOT NULL COMMENT '학교코드',
                                      `CNTR_CODE` varchar(5) DEFAULT NULL COMMENT '국가코드',
                                      `SCHL_KOR_NAME` varchar(100) DEFAULT NULL COMMENT '학교명_한글',
                                      `SCHL_ENG_NAME` varchar(100) DEFAULT NULL COMMENT '학교명_영문',
                                      `SCHL_TYPE` varchar(5) DEFAULT NULL COMMENT '학교구분코드',
                                      `CREATED_BY` varchar(255) DEFAULT NULL COMMENT '생성자',
                                      `CREATED_DATE` datetime DEFAULT NULL COMMENT '생성일시',
                                      `LAST_MODIFIED_BY` varbinary(255) DEFAULT NULL COMMENT '수정자',
                                      `LAST_MODIFIED_DATE` datetime DEFAULT NULL COMMENT '수정일시',
                                      PRIMARY KEY (`SCHL_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='출신학교';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.SMST_TYPE 구조 내보내기
CREATE TABLE `SMST_TYPE` (
                                           `RECR_SCHL_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '모집학교코드',
                                           `SMST_TYPE_CODE` varchar(50) COLLATE utf8_bin NOT NULL COMMENT '학기구분코드',
                                           `SMST_TYPE_KOR_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '학기구분한글명',
                                           `SMST_TYPE_ENG_NAME` varchar(50) COLLATE utf8_bin DEFAULT NULL COMMENT '학기구분영문명',
                                           `CREATED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `CREATED_DATE` datetime DEFAULT NULL,
                                           `LAST_MODIFIED_BY` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                           `LAST_MODIFIED_DATE` datetime DEFAULT NULL,
                                           PRIMARY KEY (`RECR_SCHL_CODE`,`SMST_TYPE_CODE`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='학기구분';

-- 내보낼 데이터가 선택되어 있지 않습니다.

-- 테이블 gradnet2_dev.SYS_COMM_CODE 구조 내보내기
CREATE TABLE `SYS_COMM_CODE` (
                                               `CODE_GRP` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '코드 그룹',
                                               `CODE` varchar(255) COLLATE utf8_bin NOT NULL COMMENT '코드',
                                               `CODE_KOR_VAL` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '한글 코드값',
                                               `CODE_ENG_VAL` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '영문 코드값',
                                               `USE_YN` char(1) COLLATE utf8_bin DEFAULT '1' COMMENT '사용여부',
                                               `REMARKS` varchar(50) COLLATE utf8_bin DEFAULT NULL,
                                               PRIMARY KEY (`CODE`,`CODE_GRP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='시스템공통코드';

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- 테이블 gradnet2_dev.USER_ROLES 구조 내보내기
CREATE TABLE `USER_ROLES` (
                                            `USER_NO` bigint(20) NOT NULL,
                                            `ROLE_ID` bigint(20) NOT NULL,
                                            PRIMARY KEY (`USER_NO`,`ROLE_ID`),
                                            KEY `FKjwdu523d0aq0788e0r5euax0x` (`ROLE_ID`),
                                            CONSTRAINT `FKjwdu523d0aq0788e0r5euax0x` FOREIGN KEY (`ROLE_ID`) REFERENCES `ROLE` (`ROLE_ID`),
                                            CONSTRAINT `FK8gy8wgt34rjse0shbrqjwjp34` FOREIGN KEY (`USER_NO`) REFERENCES `USER` (`USER_NO`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- 내보낼 데이터가 선택되어 있지 않습니다.


-- delimiter 지정만 해도 아래와 같은 에러 발생함
-- Caused by: java.sql.SQLSyntaxErrorException: You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near 'delimiter // // delimiter' at line 1

-- mysql shell 에서 gradnet2 계정으로 다음과 같이 간단한 function create 해도 권한 에러 남
-- mysql> create function hello (s char(20)) returns char(50) deterministic return concat('Hello, ', s);
-- ERROR 1419 (HY000): You do not have the SUPER privilege and binary logging is enabled (you *might* want to use the less safe log_bin_trust_function_creators variable)

-- 따라서 그냥 mysql shell 에서 아래와 같이 root 로 아래 함수를 만들고,

-- mysql> DELIMITER //
-- mysql> CREATE FUNCTION `SCHL_CODEVAL`(
--     ->     `v_lang` VARCHAR(50),
--     ->     `v_schoolCode` VARCHAR(50),
--     ->     `v_enterYear` VARCHAR(50),
--     ->     `v_seq` VARCHAR(50),
--     ->     `v_codeGrp` VARCHAR(50),
--     ->     `v_code` VARCHAR(50)
--     ->
--     -> ) RETURNS varchar(50) CHARSET utf8 deterministic
--     -> BEGIN
--     ->     DECLARE return_value VARCHAR(50) DEFAULT '';
--     ->     SELECT IF(v_lang = 'EN', CODE_ENG_VAL, CODE_KOR_VAL) INTO return_value FROM RECR_SCHL_COMM_CODE
--     ->     WHERE RECR_SCHL_CODE = v_schoolCode AND ENTR_YEAR = v_enterYear AND RECR_PART_SEQ = v_seq
--     ->       AND CODE_GRP=v_codeGrp AND CODE=v_code;
--     ->     RETURN return_value;
--     -> END//
-- Query OK, 0 rows affected, 1 warning (0.00 sec)

-- mysql> delimiter ;

-- 아래 내용은 주석 처리함

-- 함수 gradnet2_dev.SCHL_CODEVAL 구조 내보내기
-- DELIMITER //
-- CREATE FUNCTION SCHL_CODEVAL (
--     v_lang VARCHAR(50),
--     v_schoolCode VARCHAR(50),
--     v_enterYear VARCHAR(50),
--     v_seq VARCHAR(50),
--     v_codeGrp VARCHAR(50),
--     v_code VARCHAR(50)
-- ) RETURNS varchar(50) CHARSET utf8
-- BEGIN
--     DECLARE return_value VARCHAR(50) DEFAULT '';
--     SELECT IF(v_lang = 'EN', CODE_ENG_VAL, CODE_KOR_VAL) INTO return_value FROM RECR_SCHL_COMM_CODE
--     WHERE RECR_SCHL_CODE = v_schoolCode AND ENTR_YEAR = v_enterYear AND RECR_PART_SEQ = v_seq
--       AND CODE_GRP=v_codeGrp AND CODE=v_code;
--     RETURN return_value;
-- END //
-- DELIMITER ;



/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
