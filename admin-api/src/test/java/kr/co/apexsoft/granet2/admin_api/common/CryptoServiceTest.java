package kr.co.apexsoft.granet2.admin_api.common;

import kr.co.apexsoft.gradnet2.common.crypto.CryptoService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
public class CryptoServiceTest {

    @Autowired
    private CryptoService cryptoService;


    @DisplayName("AWS SES 암복호 테스트")
    @Test
    public void _Test() {
        String visa ="비자번호1234";

        String encData1 =cryptoService.encrypt(visa);
        String encData2= cryptoService.encrypt(visa);
        String decData1 = cryptoService.decrypt(encData1);
        String decData2 = cryptoService.decrypt(encData2);

        assertThat(visa).isEqualTo(decData1);

        assertThat(decData1).isEqualTo(decData2);

        assertThat(encData1.equals(encData2)).isFalse();



    }
}
