package kr.co.apexsoft.granet2.admin_api.basic.controller;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Connection;

import static kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl.Status.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-01
 */
@SpringBootTest
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
@AutoConfigureMockMvc
public class BasicControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private DataSource dataSource;

//    @BeforeAll
//    public void beforeAll() throws Exception {
//        try (Connection conn = dataSource.getConnection()) {
//            ScriptUtils.executeSqlScript(conn, new ClassPathResource("schema-mysql.sql"));
//            ScriptUtils.executeSqlScript(conn, new ClassPathResource("data-mysql.sql"));
//        }
//    }

    @DisplayName("학교/연도/회차 기준 접수 현황 조회")
    @Test
    void getReceiptStatsSimple() throws Exception {
        mvc.perform(
                get("/admin/basic/receipt-status?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].statusName").value("현재 접수현황"))
                .andExpect(jsonPath("$[0].count").value(25))
                .andExpect(jsonPath("$[1].statusName").value("원서 작성 중 인원"))
                .andExpect(jsonPath("$[1].count").value(96))
                .andExpect(jsonPath("$[2].statusName").value("지원 취소 인원"))
                .andExpect(jsonPath("$[2].count").value(107))
        ;
    }

    @DisplayName("학교 기준 전체 회차 조회")
    @Test
    void getAllRecruitPart() throws Exception {
        mvc.perform(
                get("/admin/ecruit-parts?schoolCodes=SCL00001")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].enterYear").value("2020"))
                .andExpect(jsonPath("$[0].schoolCode").value("SCL00001"))
                .andExpect(jsonPath("$[0].partSeq").value("1"))
                .andExpect(jsonPath("$[0].name").value("2020 가을학기 모집"))

        ;
    }

    @DisplayName("학교/연도/회차/학기 기준 전체 모집 전형 조회")
    @Test
    void getAllAdmissions() throws Exception {
        mvc.perform(
                get("/admin/basic/admissions?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$[0].code").value("ADM00001"))
            .andExpect(jsonPath("$[0].name").value("국내"))
            .andExpect(jsonPath("$[0].engName").value("KOREAN"))
            .andExpect(jsonPath("$[1].code").value("ADM00002"))
            .andExpect(jsonPath("$[1].name").value("국제"))
            .andExpect(jsonPath("$[1].engName").value("INTERNATIONAL"))
        ;
    }

    @DisplayName("학교/연도/회차/전형 기준 전체 모집 과정 조회")
    @Test
    void getAllCourses() throws Exception {
        mvc.perform(get("/admin/basic/courses?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1&admissionCode=ADM00001")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].code").value("01"))
                .andExpect(jsonPath("$[0].name").value("주간 석사과정"))
                .andExpect(jsonPath("$[0].engName").value("Master's Program( Full-Time)"))
                // APPL_PART 에는 없는 과정 정보 제외
//                .andExpect(jsonPath("$[1].code").value("02"))
//                .andExpect(jsonPath("$[1].name").value("주간 박사과정"))
//                .andExpect(jsonPath("$[1].engName").value("Ph.D. Program( Full-Time)"))
//                .andExpect(jsonPath("$[2].code").value("11"))
//                .andExpect(jsonPath("$[2].name").value("야간 석사과정"))
//                .andExpect(jsonPath("$[2].engName").value("Master's Program( Part-Time)"))
        ;
    }

    @DisplayName("학교/연도/회차/학기/전형/과정 기준 전체 지원구분 조회")
    @Test
    void getAllCategories() throws Exception {
        mvc.perform(get("/admin/basic/categories?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1&admissionCode=ADM00001&courseCode=01")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                // APPL_PART 에는 없는 과정 정보 제외
//                .andExpect(jsonPath("categories[0].code").value("CAT00001"))
//                .andExpect(jsonPath("categories[0].name").value("일반전형"))
//                .andExpect(jsonPath("categories[1].code").value("CAT00002"))
//                .andExpect(jsonPath("categories[1].name").value("기업체특별전형"))
//                .andExpect(jsonPath("categories[2].code").value("CAT00011"))
//                .andExpect(jsonPath("categories[2].name").value("Global Ambassador Scholaship(GAS)"))
//                .andExpect(jsonPath("categories[3].code").value("CAT00012"))
//                .andExpect(jsonPath("categories[3].name").value("Seoul G20"))
                .andExpect(jsonPath("$[0].code").value("CAT00001"))
                .andExpect(jsonPath("$[0].name").value("일반전형"))
                .andExpect(jsonPath("$[0].engName").value("General Applicant"))
                .andExpect(jsonPath("$[1].code").value("CAT00002"))
                .andExpect(jsonPath("$[1].name").value("기업체특별전형"))
                .andExpect(jsonPath("$[1].engName").value("Business & Corporate Applicant"))
        ;
    }


    @DisplayName("학교/연도/회차/전형/과정/지원구분 기준 전체 모집 전공 조회")
    @Test
    void getAllMajors() throws Exception {
        mvc.perform(get("/admin/basic/majors?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1&admissionCode=ADM00001&courseCode=01&categoryCode=CAT00001")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].code").value("MPP"))
                .andExpect(jsonPath("$[0].name").value("정책학(MPP)"))
                .andExpect(jsonPath("$[0].engName").value("Master of Public Policy(MPP)"))
                .andExpect(jsonPath("$[1].code").value("MDP"))
                .andExpect(jsonPath("$[1].name").value("개발정책학(MDP)"))
                .andExpect(jsonPath("$[1].engName").value("Master of Development Policy (MDP)"))
                .andExpect(jsonPath("$[2].code").value("MPM"))
                .andExpect(jsonPath("$[2].name").value("공공관리학(MPM)"))
                .andExpect(jsonPath("$[2].engName").value("Mster of Public Management (MPM)"))
        // APPL_PART 에는 없는 과정 정보 제외
//                .andExpect(jsonPath("$[1].code").value("02"))
//                .andExpect(jsonPath("$[1].name").value("주간 박사과정"))
//                .andExpect(jsonPath("$[1].engName").value("Ph.D. Program( Full-Time)"))
//                .andExpect(jsonPath("$[2].code").value("11"))
//                .andExpect(jsonPath("$[2].name").value("야간 석사과정"))
//                .andExpect(jsonPath("$[2].engName").value("Master's Program( Part-Time)"))
        ;
    }

    // 현재 Appl.Status enum 으로 존재하므로 학교 별로 다르지 않다고 가정
    // 추후 학교 별로 다르게 가져갈 시 지원서상태 테이블 추가 필요
    @DisplayName("전체 지원서 상태 종류 조회")
    @Test
    void getAllApplStatuses() throws Exception {
        mvc.perform(get("/admin/basic/appl-statuses").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].code").value(APPL_PART_SAVE.name()))
                .andExpect(jsonPath("$[0].name").value(APPL_PART_SAVE.getKorName()))
                .andExpect(jsonPath("$[0].engName").value(APPL_PART_SAVE.getEngName()))
                .andExpect(jsonPath("$[1].code").value(BASIS_SAVE.name()))
                .andExpect(jsonPath("$[1].name").value(BASIS_SAVE.getKorName()))
                .andExpect(jsonPath("$[1].engName").value(BASIS_SAVE.getEngName()))
                .andExpect(jsonPath("$[2].code").value(ACAD_SAVE.name()))
                .andExpect(jsonPath("$[2].name").value(ACAD_SAVE.getKorName()))
                .andExpect(jsonPath("$[2].engName").value(ACAD_SAVE.getEngName()))
                .andExpect(jsonPath("$[3].code").value(LANG_SAVE.name()))
                .andExpect(jsonPath("$[3].name").value(LANG_SAVE.getKorName()))
                .andExpect(jsonPath("$[3].engName").value(LANG_SAVE.getEngName()))
                .andExpect(jsonPath("$[4].code").value(ESSAY_SAVE.name()))
                .andExpect(jsonPath("$[4].name").value(ESSAY_SAVE.getKorName()))
                .andExpect(jsonPath("$[4].engName").value(ESSAY_SAVE.getEngName()))
                .andExpect(jsonPath("$[5].code").value(FILE_SAVE.name()))
                .andExpect(jsonPath("$[5].name").value(FILE_SAVE.getKorName()))
                .andExpect(jsonPath("$[5].engName").value(FILE_SAVE.getEngName()))
                .andExpect(jsonPath("$[6].code").value(SUBMIT.name()))
                .andExpect(jsonPath("$[6].name").value(SUBMIT.getKorName()))
                .andExpect(jsonPath("$[6].engName").value(SUBMIT.getEngName()))
                .andExpect(jsonPath("$[7].code").value(COMPLETE.name()))
                .andExpect(jsonPath("$[7].name").value(COMPLETE.getKorName()))
                .andExpect(jsonPath("$[7].engName").value(COMPLETE.getEngName()))
                .andExpect(jsonPath("$[8].code").value(CANCEL.name()))
                .andExpect(jsonPath("$[8].name").value(CANCEL.getKorName()))
                .andExpect(jsonPath("$[8].engName").value(CANCEL.getEngName()))
        ;
    }

    // KDI 는 회차와 학기가 사실 상 다르지 않음, 메인 화면은 회차 기준
    @DisplayName("학교/연도/회차 기준 일자별 접수 건수 추이")
    @Test
    void getReceiptCountVariation() throws Exception {
        mvc.perform(get("/admin/basic/receipt-variations?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("dates").isArray())
                .andExpect(jsonPath("dates[0]").value("2020-02-10"))
                .andExpect(jsonPath("dates[1]").value("2020-02-11"))
                .andExpect(jsonPath("dates[2]").value("2020-02-12"))
                .andExpect(jsonPath("counts").isArray())
                .andExpect(jsonPath("counts[0]").value(0))
                .andExpect(jsonPath("counts[1]").value(0))
                .andExpect(jsonPath("counts[2]").value(0))
        ;
    }

    @DisplayName("학교/연도/회차 기준 과정별 접수 현황 집계")
    @Test
    void getCompletedApplCountByCourse() throws Exception {
        mvc.perform(get("/admin/basic/completed-appl-count-by-course?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].courseName").value("주간 석사과정"))
                .andExpect(jsonPath("$[0].count").value(25))
        ;
    }

    @DisplayName("학교/연도/회차/전형 기준 접수 현황")
    @Test
    void getCompletedApplCountByAdmission() throws Exception {
        mvc.perform(get("/admin/basic/appl-stats-by-admission?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1&admissionCode=ADM00001")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].recruitPartName").value("KDI 2020 가을학기"))
                .andExpect(jsonPath("$[0].admissionName").value("국내"))
                .andExpect(jsonPath("$[0].courseName").value("주간 석사과정"))
                .andExpect(jsonPath("$[0].majorName").value("정책학(MPP)"))
                .andExpect(jsonPath("$[0].categoryName").value("일반전형"))
                .andExpect(jsonPath("$[0].totalCount").value(8L))
                .andExpect(jsonPath("$[1].recruitPartName").value("KDI 2020 가을학기"))
                .andExpect(jsonPath("$[1].admissionName").value("국내"))
                .andExpect(jsonPath("$[1].courseName").value("주간 석사과정"))
                .andExpect(jsonPath("$[1].majorName").value("개발정책학(MDP)"))
                .andExpect(jsonPath("$[1].categoryName").value("일반전형"))
                .andExpect(jsonPath("$[1].totalCount").value(3L))
                .andExpect(jsonPath("$[2].recruitPartName").value("KDI 2020 가을학기"))
                .andExpect(jsonPath("$[2].admissionName").value("국내"))
                .andExpect(jsonPath("$[2].courseName").value("주간 석사과정"))
                .andExpect(jsonPath("$[2].majorName").value("정책학(MPP)"))
                .andExpect(jsonPath("$[2].categoryName").value("기업체특별전형"))
                .andExpect(jsonPath("$[2].totalCount").value(1L))
                .andExpect(jsonPath("$[3].recruitPartName").value("KDI 2020 가을학기"))
                .andExpect(jsonPath("$[3].admissionName").value("국내"))
                .andExpect(jsonPath("$[3].courseName").value("주간 석사과정"))
                .andExpect(jsonPath("$[3].majorName").value("개발정책학(MDP)"))
                .andExpect(jsonPath("$[3].categoryName").value("기업체특별전형"))
                .andExpect(jsonPath("$[3].totalCount").value(1L))
                .andExpect(jsonPath("$[4].recruitPartName").value("KDI 2020 가을학기"))
                .andExpect(jsonPath("$[4].admissionName").value("국내"))
                .andExpect(jsonPath("$[4].courseName").value("주간 석사과정"))
                .andExpect(jsonPath("$[4].majorName").value("공공관리학(MPM)"))
                .andExpect(jsonPath("$[4].categoryName").value("기업체특별전형"))
                .andExpect(jsonPath("$[4].totalCount").value(1L))
        ;
    }
}
