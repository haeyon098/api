package kr.co.apexsoft.granet2.admin_api.search.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.apexsoft.granet2.admin_api.search.dto.ApplicantDetailOut;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.print.attribute.standard.Media;
import javax.sql.DataSource;
import java.sql.Connection;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@SpringBootTest
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
@AutoConfigureMockMvc
public class SearchControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private JacksonTester<ApplicantDetailOut> jsonApplicantDetail;

    @Autowired DataSource dataSource;

//    @BeforeAll
//    public void beforeAll() throws Exception {
//        try (Connection conn = dataSource.getConnection()) {
//            ScriptUtils.executeSqlScript(conn, new ClassPathResource("schema-mysql.sql"));
//            ScriptUtils.executeSqlScript(conn, new ClassPathResource("data-mysql.sql"));
//        }
//    }

    @BeforeEach
    public void setup() {
        JacksonTester.initFields(this, objectMapper);
    }

    @DisplayName("지원자_상세_조회")
    @Test
    void getApplicantDetail() throws Exception {
        mvc.perform(
                get("/admin/applications/8/applicant")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value("JIHWAN PARK"))
                .andExpect(jsonPath("dateOfBirth").value("1992-06-17"))
                .andExpect(jsonPath("emergencyContactPhoneNumber").value("0222222222"))
        ;
    }

    @DisplayName("지원 정보 조회")
    @Test
    void getApplicationInfo() throws Exception {
        mvc.perform(
                get("/admin/applications/195?simple=true")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("applNo").value(195))
                .andExpect(jsonPath("userId").value("wl5407"))
                .andExpect(jsonPath("status").value("CANCEL"))
//                .andExpect(jsonPath("applicationDate").isEmpty())  // isNull
                .andExpect(jsonPath("semesterTypeCode").value("003"))
                .andExpect(jsonPath("admissionCode").value("ADM00002"))
                .andExpect(jsonPath("courseCode").value("01"))
                .andExpect(jsonPath("categoryCode").value("CAT00011"))

//                .andExpect(jsonPath("userNo").value(21))
//                .andExpect(jsonPath("recruitSchoolCode").value("SCL00001"))
//                .andExpect(jsonPath("entranceYear").value("2020"))
//                .andExpect(jsonPath("applicationStatusCode").value("LANG_SAVE"))

        ;
    }

    @DisplayName("지원 상세 조회")
    @Test
    void getApplicationDetailInfo() throws Exception {
        mvc.perform(
                get("/admin/applications/12")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
        ;
    }

    @DisplayName("지원 내역 검색 - 학교+연도+회차 만 선택 + 페이징")
    @Test
    void getApplicationSearchResults() throws Exception {
        mvc.perform(
                get("/admin/applications?schoolCode=SCL00001&enterYear=2020&recruitPartSeq=1&page=0&size=20")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        ).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("content").isArray())
                .andExpect(jsonPath("content[0].applicantId").value("test12333"))  // user_no = 1
                .andExpect(jsonPath("content[0].applNo").value("8"))
                .andExpect(jsonPath("content[0].applId").isEmpty())
                .andExpect(jsonPath("content[0].recruitPart.code").value("1"))
                .andExpect(jsonPath("content[0].recruitPart.name").value("KDI 2020 가을학기"))
                .andExpect(jsonPath("content[0].recruitPart.engName").value("Fall 2020 for KDI"))
                .andExpect(jsonPath("content[0].admission.code").value("ADM00001"))
                .andExpect(jsonPath("content[0].admission.name").value("국내"))
                .andExpect(jsonPath("content[0].admission.engName").value("KOREAN"))
                .andExpect(jsonPath("content[0].course.code").value("01"))
                .andExpect(jsonPath("content[0].course.name").value("주간 석사과정"))
                .andExpect(jsonPath("content[0].course.engName").value("Master's Program( Full-Time)"))
                .andExpect(jsonPath("content[0].category.code").value("CAT00001"))
                .andExpect(jsonPath("content[0].category.name").value("일반전형"))
                .andExpect(jsonPath("content[0].category.engName").value("General Applicant"))
                .andExpect(jsonPath("content[0].major.code").value("MPP"))
                .andExpect(jsonPath("content[0].major.name").value("정책학(MPP)"))
                .andExpect(jsonPath("content[0].major.engName").value("Master of Public Policy(MPP)"))
                .andExpect(jsonPath("content[0].applicantName").value("박지환"))
                .andExpect(jsonPath("content[0].phoneNumber").value("01047436248"))
                .andExpect(jsonPath("content[0].email").value("wl5407@apexsoft.co.kr"))
                .andExpect(jsonPath("content[0].dateOfBirth").value("1992-06-17"))
                .andExpect(jsonPath("content[0].zipPrinted").value(false))
                .andExpect(jsonPath("content[0].status.code").value("BASIS_SAVE"))
                .andExpect(jsonPath("content[0].status.name").value("기본정보 저장"))
                .andExpect(jsonPath("content[0].status.engName").value("Basic Info Saved"))
                .andExpect(jsonPath("first").value(true))
                .andExpect(jsonPath("last").value(false))
                .andExpect(jsonPath("number").value(0))
                .andExpect(jsonPath("numberOfElements").value(20))
                .andExpect(jsonPath("totalPages").value(12))
                .andExpect(jsonPath("totalElements").value(233))
        ;
    }

    @DisplayName("출력 여부 단건 저장")
    @Test
    void saveZipPrinted() throws Exception {
        mvc.perform(patch("/admin/applications/213?zipPrinted=true").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("applNo").value(213L))
                .andExpect(jsonPath("zipPrinted").value(true))
        ;
    }
}

