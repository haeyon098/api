package kr.co.apexsoft.granet2.admin_api.download;

import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.ZipInDocumentsProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-04-23
 */
@SpringBootTest
@Transactional
public class DownloadServiceTests {

    @Autowired
    private DocumentRepository documentRepository;

    @DisplayName("전체 ZIP 안 파일과 새로운 파일명 조회")
    @Test
    void getZipInDocuments() {
        List<ZipInDocumentsProjection> list = documentRepository.findZipInDocuments(1L,
                "SCLOOOO1", "2020", 1);
    }
}
