package kr.co.apexsoft.granet2.admin_api.etc;

import com.querydsl.jpa.impl.JPAQueryFactory;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.repository.AcademyRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.repository.LanguageRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.QRole;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-03-02
 */
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Transactional
public class SlowQueryTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private AcademyRepository academyRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private CareerRepository careerRepository;

    @Autowired
    private EntityManager em;

//    @BeforeAll
//    public void beforeAll() throws Exception {
//        try (Connection conn = dataSource.getConnection()) {
//            ScriptUtils.executeSqlScript(conn, new ClassPathResource("schema-mysql.sql"));
//            ScriptUtils.executeSqlScript(conn, new ClassPathResource("data-mysql.sql"));
//        }
//    }

    @DisplayName("querydsl 설정 확인 테스트")
    @Test
    public void querydsl() throws Exception {
        JPAQueryFactory jpaQueryFactory = new JPAQueryFactory(em);
        QRole qRole = QRole.role;
        Role role = jpaQueryFactory
                .selectFrom(qRole)
                .fetchFirst();

        assertThat(role.getId()).isEqualTo(100);
        assertThat(role.getType()).isEqualTo(RoleType.ROLE_USER);
    }

    @DisplayName("academyRepository.findByAppl_IdAndTypeOrderBySeq()")
    @Test
    public void 학력_findByAppl_IdAndTypeOrderBySeq() throws Exception {
        List<Academy> academies = academyRepository.findByAppl_IdAndTypeOrderBySeq(93L, Academy.AcademyType.UNIV);

        for (Academy academy : academies) {
            System.out.println(academy.getId());
        }
    }

    @DisplayName("languageRepository.findByAppl_Id()")
    @Test
    public void 어학_findByAppl_Id() throws Exception {
        List<Language> langList = languageRepository.findByAppl_Id(119L);

        for (Language lang : langList) {
            System.out.println(lang.getId());
        }
    }

    @DisplayName("careerRepository.findByAppl_Id()")
    @Test
    public void 경력_findByAppl_Id() throws Exception {
        List<Career> careerList = careerRepository.findByAppl_IdOrderBySeq(195L);

        for (Career career : careerList) {
            System.out.println(career.getId());
        }
    }
}
