package kr.co.apexsoft.granet2.admin_api._support.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ApexCollectionUtilsTest {

    @DisplayName("값이 없는 날짜는 0으로 채운다.")
    @Test
    public void test1() {
        // given
        Map<LocalDate, Long> map = new HashMap<>();
        LocalDate first = LocalDate.of(2020, 3, 1);
        LocalDate mid1 = LocalDate.of(2020, 3, 8);
        LocalDate mid2 = LocalDate.of(2020, 3, 22);
        LocalDate mid3 = LocalDate.of(2020, 3, 23);
        LocalDate last = LocalDate.of(2020, 4, 3);
        map.put(first, 1L);
        map.put(mid1, 8L);
        map.put(mid2, 22L);
        map.put(mid3, 33L);
        map.put(last, 99L);

        // when
        Map<LocalDate, Long> zeroFilledMap = ApexCollectionUtils.getZeroFilledMap(map);

        // then
        assertThat(zeroFilledMap.size()).isEqualTo(Duration.between(first.atStartOfDay(), last.atStartOfDay()).toDays() + 1);
        assertThat(zeroFilledMap.get(first)).isEqualTo(1L);
        assertThat(zeroFilledMap.get(first.plusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid1.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid1)).isEqualTo(8L);
        assertThat(zeroFilledMap.get(mid1.plusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid2.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid2)).isEqualTo(22L);
        assertThat(zeroFilledMap.get(mid2.plusDays(1L))).isEqualTo(33L);
        assertThat(zeroFilledMap.get(mid3.minusDays(1L))).isEqualTo(22L);
        assertThat(zeroFilledMap.get(mid3)).isEqualTo(33L);
        assertThat(zeroFilledMap.get(mid3.plusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(last.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(last)).isEqualTo(99L);
    }

    @DisplayName("하나짜리 맵은 원래 맵과 같다.")
    @Test
    public void test2() {
        // given
        Map<LocalDate, Long> map = new HashMap<>();
        LocalDate first = LocalDate.of(2020, 3, 1);
        map.put(first, 111L);

        // when
        Map<LocalDate, Long> zeroFilledMap = ApexCollectionUtils.getZeroFilledMap(map);

        // then
        assertThat(zeroFilledMap.size()).isEqualTo(1);
        assertThat(zeroFilledMap.get(first)).isEqualTo(111L);
    }

    @DisplayName("시작 날짜, 종료 날짜를 기준으로 다른 날짜맵을 0 채우면서 병합한다.")
    @Test
    public void test3() {
        // given
        LocalDate start = LocalDate.of(2020, 3, 1);
        LocalDate end = LocalDate.of(2020, 3, 20);
        Map<LocalDate, Long> map = new HashMap<>();
        LocalDate mid1 = LocalDate.of(2020, 3, 5);
        LocalDate mid2 = LocalDate.of(2020, 3, 7);
        LocalDate mid3 = LocalDate.of(2020, 3, 9);
        LocalDate mid4 = LocalDate.of(2020, 3, 10);
        LocalDate mid5 = LocalDate.of(2020, 3, 11);
        LocalDate mid6 = LocalDate.of(2020, 3, 14);
        LocalDate mid7 = LocalDate.of(2020, 3, 15);
        map.put(mid1, 1L);
        map.put(mid2, 2L);
        map.put(mid3, 3L);
        map.put(mid4, 4L);
        map.put(mid5, 5L);
        map.put(mid6, 6L);
        map.put(mid7, 7L);

        // when
        Map<LocalDate, Long> zeroFilledMap = ApexCollectionUtils.getZeroFilledMap(start, end, map);

        // then
        assertThat(zeroFilledMap.size()).isEqualTo(20);
        assertThat(zeroFilledMap.get(start)).isEqualTo(0L);

        assertThat(zeroFilledMap.get(mid1.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid1)).isEqualTo(1L);
        assertThat(zeroFilledMap.get(mid1.plusDays(1L))).isEqualTo(0L);

        assertThat(zeroFilledMap.get(mid2.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid2)).isEqualTo(2L);
        assertThat(zeroFilledMap.get(mid2.plusDays(1L))).isEqualTo(0L);

        assertThat(zeroFilledMap.get(mid3.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid3)).isEqualTo(3L);
        assertThat(zeroFilledMap.get(mid3.plusDays(1L))).isEqualTo(4L);

        assertThat(zeroFilledMap.get(mid4.minusDays(1L))).isEqualTo(3L);
        assertThat(zeroFilledMap.get(mid4)).isEqualTo(4L);
        assertThat(zeroFilledMap.get(mid4.plusDays(1L))).isEqualTo(5L);

        assertThat(zeroFilledMap.get(mid5.minusDays(1L))).isEqualTo(4L);
        assertThat(zeroFilledMap.get(mid5)).isEqualTo(5L);
        assertThat(zeroFilledMap.get(mid5.plusDays(1L))).isEqualTo(0L);

        assertThat(zeroFilledMap.get(mid6.minusDays(1L))).isEqualTo(0L);
        assertThat(zeroFilledMap.get(mid6)).isEqualTo(6L);
        assertThat(zeroFilledMap.get(mid6.plusDays(1L))).isEqualTo(7L);

        assertThat(zeroFilledMap.get(mid7.minusDays(1L))).isEqualTo(6L);
        assertThat(zeroFilledMap.get(mid7)).isEqualTo(7L);
        assertThat(zeroFilledMap.get(mid7.plusDays(1L))).isEqualTo(0L);

        assertThat(zeroFilledMap.get(end)).isEqualTo(0L);

    }

}
