# Gradnet2 Admin

## DB initialization

계정 및 DB 생성, Custom Funcion 추가를 위해 다음 내용을 MySQL 셸에서 root 계정으로 실행

```sql
mysql> create user gradnet2;
mysql> create database gradnet2_dev character set utf8;
mysql> show databases;
mysql> grant all privileges on gradnet2_dev.* to gradnet2@localhost;
mysql> show grants for gradnet2;
mysql> use gradnet2_dev;
mysql> DELIMITER //
mysql> CREATE FUNCTION `SCHL_CODEVAL`(     `v_lang` VARCHAR(50),     `v_schoolCode` VARCHAR(50),     `v_enterYear` VARCHAR(50),     `v_seq` VARCHAR(50),     `v_codeGrp` VARCHAR(50),     `v_code` VARCHAR(50)  ) RETURNS varchar(50) CHARSET utf8 deterministic BEGIN     DECLARE return_value VARCHAR(50) DEFAULT '';     SELECT IF(v_lang = 'EN', CODE_ENG_VAL, CODE_KOR_VAL) INTO return_value FROM RECR_SCHL_COMM_CODE     WHERE RECR_SCHL_CODE = v_schoolCode AND ENTR_YEAR = v_enterYear AND RECR_PART_SEQ = v_seq       AND CODE_GRP=v_codeGrp AND CODE=v_code;     RETURN return_value; END//
mysql> DELIMITER ;
```
