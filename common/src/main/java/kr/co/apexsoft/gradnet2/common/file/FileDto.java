package kr.co.apexsoft.gradnet2.common.file;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.io.InputStreamResource;

import java.nio.charset.Charset;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-05-28
 */
@Getter
@Setter
public class FileDto {
    private String fileName;
    private InputStreamResource resource;

    public FileDto(String fileName, InputStreamResource resource) {
        this.fileName = encodeStrToUTF8(fileName);
        this.resource = resource;
    }

    public void setFileName(String fileName) {
        this.fileName = encodeStrToUTF8(fileName);
    }

    private String encodeStrToUTF8(String str){
        StringBuilder sb = new StringBuilder();

        byte[] btArray = str.getBytes(Charset.forName("UTF-8"));

        for(int i = 0 ; i < btArray.length ; i++)
        {
            short c = (short) ((short)btArray[i] & 0x00ff);

            if(c == '%' || c == ' ' || c == ' ')
            {
                sb.append(String.format("%%%02x", c));
            }
            else if(c < 128)
            {
                sb.append(String.format("%c", c));
            }
            else
            {
                if(i+2 < btArray.length)
                {
                    sb.append(String.format("%%%02X%%%02X%%%02X", btArray[i], btArray[i+1], btArray[i+2]));
                    i+=2;
                }
            }
        }
        return sb.toString();
    }
}
