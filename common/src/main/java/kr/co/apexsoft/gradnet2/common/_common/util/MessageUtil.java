package kr.co.apexsoft.gradnet2.common._common.util;

import org.springframework.context.support.MessageSourceAccessor;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-05-16
 */
public class MessageUtil {
    static MessageSourceAccessor messageSourceAccessor;

    public static void setMessageSourceAccessor(MessageSourceAccessor messageSourceAccessor) {
        MessageUtil.messageSourceAccessor = messageSourceAccessor;
    }

    public static MessageSourceAccessor getMessageSourceAccessor() {
        return messageSourceAccessor;
    }

    public static String getMessage(String key) {
        return messageSourceAccessor.getMessage(key);
    }

    public static String getMessage(String key, Object[] objs) {
        return messageSourceAccessor.getMessage(key, objs);
    }
}
