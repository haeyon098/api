package kr.co.apexsoft.gradnet2.common.file.exception;

import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 */
public class FileException extends CustomException {
    public FileException(String warningMessage, Long applNo) {
        super("[applNo:" + applNo + "]",warningMessage);
    }
}
