package kr.co.apexsoft.gradnet2.common.excel;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ExcelGenerator {

    public static <E> ByteArrayInputStream dataToExcel(String[] columns, List<E> dataList) {
        try(
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            Workbook workbook = new XSSFWorkbook();
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet();


            Font headerFont = workbook.createFont();
//            headerFont.setColor(IndexedColors.BLUE.getIndex());   // Header 폰트 색 변경
            headerFont.setFontName("맑은 고딕");
            headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            // Background 색 채움
            headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < columns.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(columns[col]);
                cell.setCellStyle(headerCellStyle);
            }

            ObjectMapper oMapper = new ObjectMapper();

            int rowIdx = 1;
            for (E data : dataList) {
                Row row = sheet.createRow(rowIdx++);

                Map<String, Object> map = oMapper.convertValue(data, Map.class);

                int columnIdx = 0;
                for (String key : map.keySet()){
                    if (map.get(key) == null)
                        row.createCell(columnIdx++).setCellValue("");
                    else
                        row.createCell(columnIdx++).setCellValue(map.get(key).toString());
                }
            }

            for (int col = 0; col < columns.length; col++) {
                sheet.autoSizeColumn(col);
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new ExcelIOException("엑셀 파일 생성을 실패했습니다.");
        }
    }

    public static <E> ByteArrayInputStream arrayDataToExcel(String[] columns, List<Object[]> dataList) {
        try (
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ) {
            Workbook workbook = new XSSFWorkbook();
            CreationHelper createHelper = workbook.getCreationHelper();

            Sheet sheet = workbook.createSheet();


            Font headerFont = workbook.createFont();
//            headerFont.setColor(IndexedColors.BLUE.getIndex());   // Header 폰트 색 변경
            headerFont.setFontName("맑은 고딕");
            headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);

            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            // Background 색 채움
            headerCellStyle.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
            headerCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);

            // Row for Header
            Row headerRow = sheet.createRow(0);

            // Header
            for (int col = 0; col < columns.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(columns[col]);
                cell.setCellStyle(headerCellStyle);
            }

            ObjectMapper oMapper = new ObjectMapper();

            int rowIdx = 1;
            for (Object[] rows : dataList) {
                Row row = sheet.createRow(rowIdx++);


                int columnIdx = 0;
                for (Object value : rows) {
                    row.createCell(columnIdx++).setCellValue(String.valueOf(value));
                }
            }

            for (int col = 0; col < columns.length; col++) {
                sheet.autoSizeColumn(col);
            }

            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new ExcelIOException("엑셀 파일 생성을 실패했습니다.");
        }
    }


    public static List<Map<String, Object>> excelToData(InputStream is) {
        try{
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheetAt(0);
            Iterator<Row> rows = sheet.iterator();
            List<Map<String, Object>> maps = new ArrayList<>();

            // header
            String[] header = getHeader(sheet.getRow(0));

            int rowIdx = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                // skip header
                if(rowIdx == 0) {
                    rowIdx++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();
                Map<String, Object> map = new HashMap<>();

                int cellIdx = 0;
                while (cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    map.put(header[cellIdx], currentCell.getStringCellValue());
                    cellIdx++;
                }

                maps.add(map);
            }

            return maps;
        } catch (IOException e) {
            throw new ExcelIOException("엑셀 파일 읽기를 실패했습니다.");
        }
    }

    /**
     * 엑셀 에서 헤더 추출
     *
     * @param row
     * @return
     */
    private static String[] getHeader(Row row) {
        List<String> headers = new ArrayList<>();
        for(Cell cell : row) {
            headers.add(cell.getStringCellValue());
        }
        return headers.toArray(new String[headers.size()]);
    }

}
