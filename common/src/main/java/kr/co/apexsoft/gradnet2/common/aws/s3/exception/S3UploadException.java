package kr.co.apexsoft.gradnet2.common.aws.s3.exception;

/**
 * s3업로드
 */
public class S3UploadException extends S3CommException {

    public S3UploadException(String keyName,String keyValue, String warningMessage) {
        super(keyName,keyValue,warningMessage);
    }

}
