package kr.co.apexsoft.gradnet2.common.file;


import kr.co.apexsoft.gradnet2.common.aws.s3.domain.FileWrapper;

import java.net.URL;

public interface FileRepository {

    FileResponse upload(FileRequest fileRequest);

    void delete(String fileKey);

    void deleteObjects(String[] fileKey);

    void copy(String sourceKey, String destinationKey);

    URL getGeneratePresignedUrl(String fileKey);

    FileWrapper getFileWrapperFromFileRepo(String fileKey);
}
