package kr.co.apexsoft.gradnet2.common.aws.s3.exception;

/**
 * s3 데이터 다운로드
 */
public class S3GetDataException extends S3CommException {


    public S3GetDataException(String keyName,String keyValue, String warningMessage) {
        super(keyName,keyValue,warningMessage);
    }

}
