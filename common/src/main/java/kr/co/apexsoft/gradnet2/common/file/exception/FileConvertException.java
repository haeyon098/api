package kr.co.apexsoft.gradnet2.common.file.exception;

import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 * MultipartFile -> file 전환
 */
public class FileConvertException extends CustomException {

    public FileConvertException() {
        super();
    }

    public FileConvertException(String warningMessage) {
        super(warningMessage);
    }

    public FileConvertException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
