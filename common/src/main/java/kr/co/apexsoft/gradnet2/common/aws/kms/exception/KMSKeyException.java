package kr.co.apexsoft.gradnet2.common.aws.kms.exception;
import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 *  KMS 암호화
 */
public class KMSKeyException extends CustomException {

    public KMSKeyException() {
        super();
    }

    public KMSKeyException(String warningMessage) {
        super(warningMessage);
    }

    public KMSKeyException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
