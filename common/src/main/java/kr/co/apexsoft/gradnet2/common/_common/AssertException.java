package kr.co.apexsoft.gradnet2.common._common;


import kr.co.apexsoft.gradnet2.common._support.CustomException;

public class AssertException extends CustomException {

    public AssertException() {
        super();
    }

    public AssertException(String warningMessage) {
        super(warningMessage);
    }

    public AssertException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
