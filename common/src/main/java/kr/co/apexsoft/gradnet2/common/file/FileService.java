package kr.co.apexsoft.gradnet2.common.file;

import kr.co.apexsoft.gradnet2.common._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.common.file.exception.FileException;
import kr.co.apexsoft.gradnet2.common.aws.s3.domain.FileWrapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    @NonNull
    private FileRepository fileRepository;


    @Value("${file.baseDir}")
    private String FILE_BASE_DIR;


    public InputStream getInputStreamFromFileRepo(String filePath) {
        FileWrapper fileWrapper = fileRepository.getFileWrapperFromFileRepo(filePath);
        return fileWrapper.getInputStream();
    }

    public byte[] getBytesFromFileRepo(String filePath) throws IOException {
        InputStream inputStream = getInputStreamFromFileRepo(filePath);
        byte[] bytes = IOUtils.toByteArray(inputStream);
        return bytes;
    }


    /**
     * file서버에서 files 다운로드
     *
     * @param filePaths
     * @param applNo
     * @return
     */
    public List<File> getFileListFromFileServer(Long applNo, String[] filePaths) {
        List<File> uploadedFiles = new ArrayList<>();

        for (String path : filePaths) {
            uploadedFiles.add(getFileFromFileRepo(applNo, FILE_BASE_DIR, path));
        }

        return uploadedFiles;
    }

    /**
     * file서버에서 file 한건 다운로드
     *
     * @param applNo
     * @param filePaths
     * @return
     */
    public File getFileFromFileServer(Long applNo, String... filePaths) {
        return getFileFromFileRepo(applNo, FILE_BASE_DIR, filePaths);
    }


    /**
     * target 위치파일 다운로드 후 파일생성
     *
     * @param baseDir
     * @param filePaths
     * @param applNo
     * @return
     */
    private File getFileFromFileRepo(Long applNo, String baseDir, String... filePaths) {
        String downFilePath = filePaths[0];
        String reFilePath = filePaths.length > 1 ? filePaths[1] : downFilePath;
        File file = null;
        try {
            byte[] bytes = getBytesFromFileRepo(downFilePath);
            file = new File(baseDir, reFilePath);
            FileUtils.writeByteArrayToFile(file, bytes);
        } catch (Exception e) {
            throw new FileException(MessageUtil.getMessage("FILE_DOWN_FAIL"), applNo);
        }

        return file;
    }
}
