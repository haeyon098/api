package kr.co.apexsoft.gradnet2.common.crypto;

public interface CryptoService {
    String encrypt(String plainText);

    String decrypt(String ciphertext);

}
