package kr.co.apexsoft.gradnet2.common.zip.domain;

import kr.co.apexsoft.gradnet2.common._common.util.FilePathUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class ZipInfo {
    private Long applNo;
    private String schoolCode;
    private String enterYear;
    private Integer recruitPartSeq;
    private String fileBaseDir;
    private String zipFileName;

    public Long getApplNo() {
        return this.applNo;
    }

    public String getTargeDir() {
        return FilePathUtil.getMakeDirectoryFullPath(this.fileBaseDir,this.schoolCode, this.enterYear, this.recruitPartSeq,this.applNo );
    }
}
