package kr.co.apexsoft.gradnet2.common.file;

import kr.co.apexsoft.gradnet2.common._common.util.FilePathUtil;
import lombok.Builder;
import lombok.Getter;

@Getter
public class FileResponse {
    private Long applDocNo;

    private String fileName;

    private String originalFileName;

    private Long fileSize;

    private String fileDirPath;

    private String fileKey;

    @Builder
    public FileResponse(String fileDirPath, String fileName, String originalFileName, Long fileSize, String fileKey) {
        this.fileDirPath = FilePathUtil.recoverSingleQuote(FilePathUtil.recoverAmpersand(fileDirPath));
        this.fileName = FilePathUtil.recoverSingleQuote(FilePathUtil.recoverAmpersand(fileName));
        this.originalFileName = FilePathUtil.recoverSingleQuote(FilePathUtil.recoverAmpersand(originalFileName));
        this.fileSize = fileSize;
        this.fileKey = fileKey;
    }

    public void setApplDocNo(Long applDocNo) {
        this.applDocNo = applDocNo;
    }
}
