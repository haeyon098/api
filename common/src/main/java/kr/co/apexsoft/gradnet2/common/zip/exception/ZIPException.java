package kr.co.apexsoft.gradnet2.common.zip.exception;


import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 * ZIP
 */
public class ZIPException extends CustomException {

    public ZIPException() {
        super();
    }

    public ZIPException(String warningMessage) {
        super(warningMessage);
    }

    public ZIPException(String warningMessage, Long applNo) {
        super("[applNo:" + applNo + "]",warningMessage);
    }


}
