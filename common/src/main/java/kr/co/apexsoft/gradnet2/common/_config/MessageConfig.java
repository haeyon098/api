package kr.co.apexsoft.gradnet2.common._config;

import kr.co.apexsoft.gradnet2.common._common.util.MessageUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import java.util.Locale;

/**
 * Class Description
 * 다국어 설정
 *
 * @author 김혜연
 * @since 2019-05-16
 */
@Configuration
public class MessageConfig {

    @Bean
    public LocaleResolver localeResolver() {
        AcceptHeaderLocaleResolver localeResolver = new AcceptHeaderLocaleResolver();
        localeResolver.setDefaultLocale(Locale.KOREA);  // 디폴트 한국
        return localeResolver;
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:/messages/message");
        messageSource.setDefaultEncoding("utf-8");
        messageSource.setCacheSeconds(180); // 리로딩 간격
        Locale.setDefault(Locale.KOREA);
        return messageSource;
    }

    @Bean
    public MessageSourceAccessor messageSourceAccessor() {
        return new MessageSourceAccessor(this.messageSource());
    }

    @Bean
    public void messageUtils() {
        MessageUtil.setMessageSourceAccessor(this.messageSourceAccessor());
    }
}
