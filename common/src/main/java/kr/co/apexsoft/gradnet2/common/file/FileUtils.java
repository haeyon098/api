package kr.co.apexsoft.gradnet2.common.file;

import kr.co.apexsoft.gradnet2.common._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.common.file.exception.FileByteConvertException;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-06-27
 */
public class FileUtils {
    /**
     * request에서 multipart 파일 1개 추출
     * @param request
     * @return
     */
    public static FileRequest getFileRequest(HttpServletRequest request,String maxImgSize,String maxPdfSize) {
        StandardMultipartHttpServletRequest multipartReq = null;
        if (StandardMultipartHttpServletRequest.class.isAssignableFrom(request.getClass())) {
            multipartReq = (StandardMultipartHttpServletRequest) request;
        }

        Objects.requireNonNull(multipartReq);
        return new FileRequest(multipartReq,maxImgSize,maxPdfSize);
    }

    public static InputStreamResource retrieveInputStreamResource(File file) {
        try {
            return new InputStreamResource(new ByteArrayInputStream(org.apache.commons.io.FileUtils.readFileToByteArray(file)));
        } catch (IOException e) {
            throw new FileByteConvertException(MessageUtil.getMessage("FILE_TO_BYTE_CONVERT_FAIL"));
        }
    }

    public static InputStreamResource retrieveInputStreamResource(ByteArrayInputStream byteArrayInputStream) {
        return new InputStreamResource(byteArrayInputStream);
    }
}
