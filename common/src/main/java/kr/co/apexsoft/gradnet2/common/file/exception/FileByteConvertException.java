package kr.co.apexsoft.gradnet2.common.file.exception;


import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 * 파일 Byte[] 컨버트
 */
public class FileByteConvertException extends CustomException {

    public FileByteConvertException() {
        super();
    }

    public FileByteConvertException(String warningMessage) {
        super(warningMessage);
    }

    public FileByteConvertException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
