package kr.co.apexsoft.gradnet2.common.file.exception;


import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 * 파일 not found exception
 * FileInputStream 추출시
 *
 */
public class FileNotFoundException extends CustomException {

    public FileNotFoundException() {
        super();
    }

    public FileNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public FileNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
