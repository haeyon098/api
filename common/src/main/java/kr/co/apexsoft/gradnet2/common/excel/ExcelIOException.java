package kr.co.apexsoft.gradnet2.common.excel;

import kr.co.apexsoft.gradnet2.common._support.CustomException;

/**
 */
public class ExcelIOException extends CustomException {
    public ExcelIOException(String warningMessage) {
        super(warningMessage);
    }
}
