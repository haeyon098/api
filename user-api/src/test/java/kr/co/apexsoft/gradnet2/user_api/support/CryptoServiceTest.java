package kr.co.apexsoft.gradnet2.user_api.support;

import kr.co.apexsoft.gradnet2.user_api.SpringTestSupport;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.CryptoService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CryptoServiceTest extends SpringTestSupport {



    @Autowired
    private CryptoService cryptoService;

    @Test
    public void _Test() {
        String visa ="비자번호1234";

        String encData1 =cryptoService.encrypt(visa);
        String encData2= cryptoService.encrypt(visa);
        String decData1 = cryptoService.decrypt(encData1);
        String decData2 = cryptoService.decrypt(encData2);

        assertTrue(!encData1.equals(encData2));
        assertTrue(decData1.equals(decData2));

    }
}
