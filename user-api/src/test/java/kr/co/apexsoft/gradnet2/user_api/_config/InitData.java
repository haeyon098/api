package kr.co.apexsoft.gradnet2.user_api._config;

import kr.co.apexsoft.gradnet2.entity.role.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class InitData implements CommandLineRunner {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {

//        this.roleRepository.saveAll(Arrays.asList(
//                new Role(RoleType.ROLE_USER), new Role(RoleType.ROLE_ADMIN)));
    }
}
