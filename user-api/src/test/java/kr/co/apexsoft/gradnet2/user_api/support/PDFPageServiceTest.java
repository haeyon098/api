package kr.co.apexsoft.gradnet2.user_api.support;

import kr.co.apexsoft.gradnet2.user_api.SpringTestSupport;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.PDFPageService;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.domain.PDFPageInfo;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;


public class PDFPageServiceTest extends SpringTestSupport {

    @Autowired
    private PDFPageService pdfPageService;

    @Test
    public void pdf에_정보삽입_테스트() {
        PDFPageInfo pdfPageInfo =new PDFPageInfo();
       pdfPageInfo.setApplNo((long) 1);
       pdfPageInfo.setLeftBottom("왼쪽아래");
       pdfPageInfo.setLeftTop("왼쪽위");
        pdfPageService.insertPDFPageInfo(new File("/opt/gradnet/upload", "/1.pdf"), 1, 2, pdfPageInfo);
    }

}
