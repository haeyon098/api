package kr.co.apexsoft.gradnet2.user_api;

import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.BDDMockito.given;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-11
 */
public class UserSignupTest {
    @Rule
    public MockitoRule mockito = MockitoJUnit.rule();   // @Mock 등 어노테이션 붙은 field 초기화

    @Mock
    private UserRepository userRepository;

    @Test
    public void 회원가입_Test() {
        // given
        // 회원정보 input
        Role userRole = new Role(1L, RoleType.ROLE_USER);
        Set<Role> roles = new HashSet<>();
        roles.add(userRole);
        User user = new User("test", "test1234!", roles);

        // 논리 검증 - 중복검사
        given(userRepository.findByUserId("test")).willThrow(NullPointerException.class);

        // when
        // 회원가입 구현
        userRepository.save(user);

        // then
        // 회원가입 성공 확인
//        verify()
    }
}
