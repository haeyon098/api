package kr.co.apexsoft.gradnet2.user_api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import org.springframework.boot.test.json.JacksonTester;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author hanmomhanda@gmail.com
 * created on 2018-11-17
 */
public class TestHelper {


    public static void setupGeneralTest(Object testInstance) {
        ObjectMapper objectMapper = new ObjectMapper();
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        LocalDateDeserializer localDateDeserializer =  new LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        javaTimeModule.addDeserializer(LocalDate.class, localDateDeserializer);
        objectMapper.registerModule(javaTimeModule);
        JacksonTester.initFields(testInstance, objectMapper);
    }

    public static void resetAutoIncrement(EntityManager em, String tableName, String idColumnName) {
        em.createNativeQuery(
                String.format("ALTER TABLE `%s` ALTER COLUMN `%s` RESTART WITH 1",
                tableName, idColumnName))
            .executeUpdate();
    }
}
