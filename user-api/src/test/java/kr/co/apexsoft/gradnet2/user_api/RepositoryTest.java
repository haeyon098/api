package kr.co.apexsoft.gradnet2.user_api;

import kr.co.apexsoft.gradnet2.entity.adms.repository.*;
import kr.co.apexsoft.gradnet2.entity.applicant.career.projection.CareerProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.recpart.repository.RecruitPartRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-06
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RepositoryTest {
    @Autowired
    private RecruitPartRepository recruitPartRepository;

    @Autowired
    private RecruitSchoolRepository recruitSchoolRepository;

    @Autowired
    private SemesterTypeRepository semesterTypeRepository;

    @Autowired
    private AdmsRepository admsRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private MajorRepository majorRepository;

    @Autowired
    private CareerRepository careerRepository;

    @Test
    public void findAll() {
//        recruitPartRepository.findAll();
//        recruitSchoolRepository.findAll();
//        semesterTypeRepository.findAll();
//        admsRepository.findAll();
        categoryRepository.findAll();
        majorRepository.findAll();
    }

    @Test
    public  void 경력정보(){

        List<CareerProjection> list =careerRepository.findCareerInfo(223L);
        assertThat(list.get(0).getName().equals("tt"));
        assertThat(list.get(0).getCompTypeName().equals("private"));
    }
}
