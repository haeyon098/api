package kr.co.apexsoft.gradnet2.user_api;

import kr.co.apexsoft.gradnet2.user_api._support.birt.service.BirtService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplFormFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplSlipFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplFormBirtService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplSlipBirtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-11
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BirtServiceTest {

    @Autowired
    private ApplFormBirtService applFormBirtService;

    @Autowired
    private ApplSlipBirtService  applSlipBirtService;

    @Autowired
    private BirtService birtService;



    @Value("${file.baseDir}")
    private String fileBaseDir;


    @Test
    public void 버트생성_로컬저장_Test() {
        birtService.generateBirtFile();
    }



//    @Test
//    public void study_plan_testKr() {
//        birtService.generateSelfIntroductionFile();
//    }

//    @Test
//    public void study_plan_testEn() {
//        birtService.generateStudyPlanFileEn();
//    }

    @Test
    public void 지원서생성_국내() {
        ApplFormFileInfo applFormFileInfo =new ApplFormFileInfo();
        applFormFileInfo.setPartSchoolCode("SCL00001");
        applFormFileInfo.setPartEnterYear("2020");
        applFormFileInfo.setPartRecruitPartSeq(2);
        applFormFileInfo.setPartAdmissionCode("ADM00002");
        applFormFileInfo.setApplicantUserId("hskim");
        applFormFileInfo.setId(53L);
        applFormFileInfo.setPartMajorCode("MDP");
       applFormBirtService.generateBirtFile(applFormFileInfo);
    }

    @Test
    public void 수험표생성() {
        ApplSlipFileInfo applFormFileInfo =new ApplSlipFileInfo();
        applFormFileInfo.setPartSchoolCode("SCL00001");
        applFormFileInfo.setPartEnterYear("2020");
        applFormFileInfo.setPartRecruitPartSeq(2);
        applFormFileInfo.setPartAdmissionCode("ADM00001");
        applFormFileInfo.setApplicantUserId("hskim");
        applFormFileInfo.setId(223L);
        applFormFileInfo.setPartMajorCode("MAJ00001");
        applSlipBirtService.generateBirtFile(applFormFileInfo);
    }


    @Test
    public void 지원서생성_국제() {
        ApplFormFileInfo applFormFileInfo =new ApplFormFileInfo();
        applFormFileInfo.setPartSchoolCode("SCL00001");
        applFormFileInfo.setPartEnterYear("2020");
        applFormFileInfo.setPartRecruitPartSeq(1);
        applFormFileInfo.setPartAdmissionCode("ADM00002");
        applFormFileInfo.setApplicantUserId("hskim");
        applFormFileInfo.setId(237L);
        applFormFileInfo.setPartMajorCode("MAJ00001");
        applFormBirtService.generateBirtFile(applFormFileInfo);
    }
}
