package kr.co.apexsoft.gradnet2.user_api._support.file;

import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-06-27
 */
public class FileUtils {
    /**
     * request에서 multipart 파일 1개 추출
     * @param request
     * @return
     */
    public static FileRequest getFileRequest(HttpServletRequest request,String maxImgSize,String maxPdfSize) {
        StandardMultipartHttpServletRequest multipartReq = null;
        if (StandardMultipartHttpServletRequest.class.isAssignableFrom(request.getClass())) {
            multipartReq = (StandardMultipartHttpServletRequest) request;
        }

        Objects.requireNonNull(multipartReq);
        return new FileRequest(multipartReq,maxImgSize,maxPdfSize);
    }
}
