package kr.co.apexsoft.gradnet2.user_api.user.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 사용자 정보 에러처리
 * @author 김혜연
 */
@Getter
public class InvalidUserException extends CustomException {
    public InvalidUserException() {
        super();
    }

    public InvalidUserException(String warningMessage) {
        super(warningMessage);
    }

    public InvalidUserException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
