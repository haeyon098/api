package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
public class ForeignerInfoRequest {
    @NotBlank
    private String foreignerTypeCode;
    @Size(max = 50)
    private String foreignerRegistrationNo;
    @NotBlank
    @Size(max = 50)
    private String passportNo;
    @Size(max = 50)
    private String visaNo;
    @NotBlank
    private String visaTypeCode;
    @Size(max = 50)
    private String visaTypeEtc;
    private LocalDate visaExprDay;
    private String homeAddress;
    private String homeTelNum;
    @Size(max = 50)
    private String korEmergencyName;
    private String korEmergencyRelation;
    @Size(max = 30)
    private String korEmergencyTel;
    @NotBlank
    @Size(max = 50)
    private String homeEmergencyName;
    @NotBlank
    @Size(max = 30)
    private String homeEmergencyTel;
    @NotBlank
    private String homeEmergencyRelation;
    private Boolean overseasKorean;
    private String skype;
    private String residencyType;
    private String videoEssay;
    @NotBlank
    @Size(max = 25)
    private String firstName;
    @Size(max = 25)
    private String middleName;
    @NotBlank
    @Size(max = 50)
    private String lastName;

}


