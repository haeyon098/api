package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-28
 */
@Getter
public class RecommendProfResponse {

    private Long id;

    private Long applNo;

    private String name;

    private String email;

    private String institute;

    private String address;

    private String position;

    private String phoneNum;

    private String sign;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate recDay;

    private List<AnswerDto> answerList;


}
