package kr.co.apexsoft.gradnet2.user_api.adms.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class RecruitSchoolNotFoundException extends CustomException {

    public RecruitSchoolNotFoundException() {
        super();
    }

    public RecruitSchoolNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public RecruitSchoolNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public RecruitSchoolNotFoundException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
