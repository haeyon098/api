package kr.co.apexsoft.gradnet2.user_api.adms.conrtroller;

import kr.co.apexsoft.gradnet2.user_api.adms.dto.AdmsRequest;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import kr.co.apexsoft.gradnet2.user_api.adms.service.AdmsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-06
 */
@RestController
@RequestMapping("/adms")
@RequiredArgsConstructor
public class AdmsController {
    @NonNull
    private AdmsService admsService;

    /**
     * 지원가능 학기구분 조회
     *
     * @param admsRequest
     * @return
     */
    @PostMapping("/semester-type")
    public ResponseEntity<List<AdmsResponse>> findSemesterType(@Valid @RequestBody AdmsRequest admsRequest) {
        return ResponseEntity.ok(admsService.findSemesterType(admsRequest.getSchoolCode(), admsRequest.getEnterYear(),
                admsRequest.getRecruitPartSeq()));
    }

    /**
     * 지원가능 모집전형 조회
     *
     * @param admsRequest
     * @return
     */
    @PostMapping("/adms-code")
    public ResponseEntity<List<AdmsResponse>> findAdms(@Valid @RequestBody AdmsRequest admsRequest) {
        return ResponseEntity.ok(admsService.findAdms(admsRequest.getSchoolCode(), admsRequest.getEnterYear(),
                admsRequest.getRecruitPartSeq(), admsRequest.getSemesterTypeCode()));
    }

    /**
     * 지원가능 모집과정 조회
     *
     * @param admsRequest
     * @return
     */
    @PostMapping("/cors")
    public ResponseEntity<List<AdmsResponse>> findCors(@Valid @RequestBody AdmsRequest admsRequest) {
        return ResponseEntity.ok(admsService.findCors(admsRequest.getSchoolCode(), admsRequest.getEnterYear(),
                admsRequest.getRecruitPartSeq(), admsRequest.getSemesterTypeCode(), admsRequest.getAdmissionCode()));
    }

    /**
     * 지원가능 지원구분 조회
     *
     * @param admsRequest
     * @return
     */
    @PostMapping("/category")
    public ResponseEntity<List<AdmsResponse>> findCategory(@Valid @RequestBody AdmsRequest admsRequest) {
        return ResponseEntity.ok(admsService.findCategory(admsRequest.getSchoolCode(), admsRequest.getEnterYear(),
                admsRequest.getRecruitPartSeq(), admsRequest.getSemesterTypeCode(), admsRequest.getAdmissionCode(),
                admsRequest.getCourseCode()));
    }

    /**
     * 지원가능 전공 조회
     *
     * @param admsRequest
     * @return
     */
    @PostMapping("/major")
    public ResponseEntity<List<AdmsResponse>> findMajor(@Valid @RequestBody AdmsRequest admsRequest) {
        return ResponseEntity.ok(admsService.findMajor(admsRequest.getSchoolCode(), admsRequest.getEnterYear(),
                admsRequest.getRecruitPartSeq(), admsRequest.getSemesterTypeCode(), admsRequest.getAdmissionCode(),
                admsRequest.getCourseCode(), admsRequest.getCategoryCode()));
    }
}
