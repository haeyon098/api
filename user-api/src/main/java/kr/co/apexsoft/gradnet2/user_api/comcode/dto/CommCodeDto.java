package kr.co.apexsoft.gradnet2.user_api.comcode.dto;

import lombok.Getter;

@Getter
public class CommCodeDto {

    /* 공통코드 그룹 */
    private String codeGrp;

    /* 코드값 */
    private String code;

    /*코드 한글명*/
    private String codeKrValue;

    /*코드 영문명*/
    private String codeEnValue;
}
