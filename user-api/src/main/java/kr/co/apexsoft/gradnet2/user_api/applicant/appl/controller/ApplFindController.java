package kr.co.apexsoft.gradnet2.user_api.applicant.appl.controller;

import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
public class ApplFindController {
    @NonNull
    private ApplService applService;

    /***
     * 수험번호 찾기
     * 비회원 용으로 url 분리
     * @param email
     * @param userPrincipal
     */
    @GetMapping("/find/applid")
    public void findApplIdByEmail(@RequestParam String email,
                                  @AuthenticationPrincipal UserPrincipal userPrincipal) {
        applService.findApplIdByEmail(email);
    }

}
