package kr.co.apexsoft.gradnet2.user_api.standard.controller;

import kr.co.apexsoft.gradnet2.entity.standard.projection.SearchDto;
import kr.co.apexsoft.gradnet2.user_api.standard.service.StandardService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 기준정보성 데이터
 *
 * @author 김효숙
 * @since 2020-01-02
 */
@RestController
@RequestMapping("/standard")
@RequiredArgsConstructor
public class StandardController {
    @NonNull
    private StandardService standardService;

    /**
     * 학교검색
     *
     * @return
     */
    @GetMapping("/schl")
    public ResponseEntity<List<SearchDto>> findAllSchool() {
        return ResponseEntity.ok(standardService.findAllSchool());
    }

    /**
     * 나라검색
     *
     * @return
     */
    @GetMapping("/country")
    public ResponseEntity<List<SearchDto>> findAllCountry() {
        return ResponseEntity.ok(standardService.findAllCountry());
    }
}
