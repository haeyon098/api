package kr.co.apexsoft.gradnet2.user_api._support.aws.s3;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.aws.s3.domain.FileMeta;
import kr.co.apexsoft.gradnet2.user_api._support.aws.s3.domain.FileWrapper;
import kr.co.apexsoft.gradnet2.user_api._support.aws.s3.exception.*;
import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRepository;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.Date;

@Service
public class S3FileRepositoryImpl implements FileRepository {

    private static final Logger logger = LoggerFactory.getLogger(S3FileRepositoryImpl.class);

    @Value("${aws.s3.bucketName}")
    private String bucketName;

    @Value("${aws.s3.URL}")
    private String s3URL;

    @Autowired
    private AmazonS3 amazonS3;

    @Override
    public FileResponse upload(FileRequest fileRequest) {
        try {
            PutObjectRequest request = createPutObjectRequest(fileRequest);
            TransferManager tm = createTransferManager();

            Upload upload = tm.upload(request);
            upload.waitForCompletion();
        } catch (Exception e) {

            throw new S3UploadException("FileRequest",fileRequest.getFileName(), MessageUtil.getMessage("S3_UPLOAD_FAIL"));
        }

        return
                FileResponse.builder()
                        .fileDirPath(fileRequest.getFileDirPath())
                        .fileName(fileRequest.getFileName())
                        .originalFileName(fileRequest.getOriginalFileName())
                        .fileSize(fileRequest.getFileSize())
                        .fileKey(fileRequest.getFileKey())
                        .build();
    }

    private TransferManager createTransferManager() {
        return TransferManagerBuilder
                .standard()
                .withS3Client(amazonS3)
                .withMultipartUploadThreshold((long) (5 * 1024 * 1025))
                .build();
    }

    private PutObjectRequest createPutObjectRequest(FileRequest fileRequest) {
        return new PutObjectRequest(bucketName, fileRequest.getFileKey(),
                fileRequest.getFile())
                .withCannedAcl(CannedAccessControlList.Private)
                .withMetadata(createObjectMetadata(fileRequest));
    }

    private ObjectMetadata createObjectMetadata(FileRequest fileRequest) {
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentLength(fileRequest.getFileSize());
        meta.setContentEncoding("UTF-8");
        return meta;
    }

    @Override
    public void delete(String fileKey) {
        try {
            amazonS3.deleteObject(bucketName, fileKey);
        } catch (Exception e) {
            throw new S3DeleteException("fileKey",fileKey, MessageUtil.getMessage("S3_DELETE_FAIL"));
        }
    }

    @Override
    public void deleteObjects(String... fileKeys) {
        try {
            amazonS3.deleteObjects(new DeleteObjectsRequest(bucketName).withKeys(fileKeys).withQuiet(false));
        } catch (RuntimeException e) {
            throw new S3DeleteException("fileKey",fileKeys.toString(), MessageUtil.getMessage("S3_DELETE_FAIL"));
        }
    }

    @Override
    public void copy(String sourceKey, String destinationKey) { //TODO: 에디터 디벨롭 이후 메소드 수정
        try {
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, sourceKey, bucketName, destinationKey);
            amazonS3.copyObject(copyObjRequest);
        } catch (Exception e) {
            throw new S3CopyException("sourceKey",sourceKey, MessageUtil.getMessage("S3_COPY_FAIL"));
        }
    }

    @Override
    public URL getGeneratePresignedUrl(String fileKey) {
        try {
            Date expiration = new Date();
            long expTimeMillis = expiration.getTime();
            expTimeMillis += 1000 * 60 * 5; //FIXME: 5분
            expiration.setTime(expTimeMillis);

            GeneratePresignedUrlRequest generatePresignedUrlRequest =
                    new GeneratePresignedUrlRequest(bucketName, fileKey)
                            .withMethod(HttpMethod.GET)
                            .withExpiration(expiration);
            return amazonS3.generatePresignedUrl(generatePresignedUrlRequest);
        } catch (Exception e) {
            throw new S3CopyException("fileKey",fileKey, MessageUtil.getMessage("S3_COPY_FAIL"));
        }
    }

    private FileMeta createFileMeta(Object object) {
        FileMeta fileMeta = null;
        if (object instanceof ObjectMetadata) {
            fileMeta = new FileMeta();
            ObjectMetadata metadata = (ObjectMetadata) object;
            fileMeta.setContentType(metadata.getContentType());
            fileMeta.setContentEncoding(metadata.getContentEncoding());
            fileMeta.setETag(metadata.getETag());
            fileMeta.setLastModified(metadata.getLastModified().toString());
            fileMeta.setContentLength(metadata.getContentLength());
        }
        return fileMeta;
    }

    @Override
    public FileWrapper getFileWrapperFromFileRepo(String fileKey) {
        S3Object s3Object = null;
        try {
            s3Object = amazonS3.getObject(new GetObjectRequest(bucketName, fileKey));
        } catch (AmazonS3Exception e) {
            throw new S3GetDataException("fileKey",fileKey, MessageUtil.getMessage("S3_DOWN_FAIL"));
        }
        FileWrapper fileWrapper = new FileWrapper(s3Object.getObjectContent(),
                createFileMeta(s3Object.getObjectMetadata()));
        return fileWrapper;
    }



}
