package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.Alumni;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.SrvyAns;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.SrvyQstAns;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository.AlumniRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository.SrvyAnsRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository.SrvyQstAnsRepository;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SchoolCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SchoolCommCodeRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.CompleteService;
import kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.srvy.exception.SrvyAnsNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.srvy.exception.SrvyOfficePostalFailException;
import kr.co.apexsoft.gradnet2.user_api.comcode.dto.SchoolCommCodeDto;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author aran
 * @since 2020-02-26
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SrvyService {
    @NonNull
    private SrvyAnsRepository srvyAnsRepository;

    @NonNull
    private SrvyQstAnsRepository srvyQstAnsRepository;

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private CareerRepository careerRepository;

    @NonNull
    private AlumniRepository alumniRepository;

    @NonNull
    private SchoolCommCodeRepository schoolCommCodeRepository;

    private final ApexModelMapper modelMapper;

    @NonNull
    private CompleteService completeService;

    /**
     * 서베이저장 & 지원완료 처리
     * @param ansRequest
     * @param srvyNo
     * @param alumniList
     * @param applNo
     * @param roles
     */
    public void saveSurveyAnswerAndComplete(SrvyQstAnsRequest ansRequest, Long srvyNo, List<Alumni> alumniList, Long applNo, Set<Role> roles) {
        //서베이 저장
        saveSurveyAnswer(ansRequest, srvyNo, alumniList, applNo, roles);

        //지원완료 가능한 상태 체크
        Appl appl = completeService.checkApplCompleteSataus(applNo, roles);

        //지원완료 처리
        completeService.complete(appl);
    }

    private void saveSurveyAnswer(SrvyQstAnsRequest ansRequest, Long srvyNo, List<Alumni> alumniList, Long applNo, Set<Role> roles) {
        //지원완료 가능한 상태 체크
        Appl dbAppl = completeService.checkApplCompleteSataus(applNo, roles);
        SrvyAns srvyAns = srvyAnsRepository.findById(srvyNo)
                .orElseThrow(() -> new SrvyAnsNotFoundException(MessageUtil.getMessage("NOT_FOUND_SRVY_ANS"), srvyNo));


        dbAppl.initContactCode(ansRequest.getContactCode());

        for (Alumni alumni : alumniList) {
            alumni.initAppl(dbAppl);
        }
        alumniRepository.saveAll(alumniList);

        List<SrvyQstAns> srvyQstAnsList = new ArrayList<>();

        // TODO: 질문답변 관련 모두 하드코딩. 추후 리팩토링 필요
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns1(), srvyAns, 100L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns1_1(), srvyAns, 101L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns2(), srvyAns, 200L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns2_1(), srvyAns, 201L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns3(), srvyAns, 300L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns3_1(), srvyAns, 301L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns4(), srvyAns, 400L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns5(), srvyAns, 500L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns6(), srvyAns, 600L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns7(), srvyAns, 700L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns8(), srvyAns, 800L));
        // FIXME: 학교 코드 저장. [학교코드값#코드한글명]으로 변경(2020-03-10) ex)007코드명
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns9().getCode() + ")%" + ansRequest.getAns9().getCodeKrValue(),
                srvyAns, 900L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns10(), srvyAns, 1000L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns11(), srvyAns, 1100L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns12(), srvyAns, 1200L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns13(), srvyAns, 1300L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns14(), srvyAns, 1400L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns15(), srvyAns, 1500L));
        srvyQstAnsList.add(saveSurveyQuestionAnswer(ansRequest.getAns16(), srvyAns, 1600L));
        srvyQstAnsRepository.saveAll(srvyQstAnsList);

        srvyAns.srvyAnsClose();
    }

    private SrvyQstAns saveSurveyQuestionAnswer(String answer, SrvyAns srvyAns, Long qstNo) {
        // 답변 항목 하나마다 저장
        return new SrvyQstAns(qstNo, srvyAns.getRecruitSchoolCode(), srvyAns.getId(), srvyAns.getAppl(),
                false, answer);
    }

    public SrvyAnsResponse saveSurveyAnswer(Long applNo) {
        Appl dbAppl = applRepository.findById(applNo).get();
        SrvyAns dbSrvyAns = srvyAnsRepository.findByAppl_Id(applNo)
                .orElseGet(() -> srvyAnsRepository.save(modelMapper.convert(
                        new SrvyAnsSaveDto(
                                dbAppl.getPart().getSchoolCode(), dbAppl, false, null
                        ),
                        SrvyAns.class
                        )
                ));

        int srvyNo = dbSrvyAns.getId().intValue();
        String schoolCode = dbSrvyAns.getRecruitSchoolCode();
        boolean answered = dbSrvyAns.getAnswered();

        return new SrvyAnsResponse(srvyNo, schoolCode, applNo.intValue(), answered, getAlumni(applNo), getRecruitSchoolCodeList(applNo), getRecruitSchoolCodeListforDoctor(applNo)  );
    }

    public void checkContactCode(Long applNo) {
        List<Career> careers = careerRepository.findByAppl_IdOrderBySeq(applNo);
        int addrCount = 0;
        for (Career career : careers) {
            if (career.getAddress() != null) {
                addrCount++;
            }
        }
        if (addrCount == 0) {
            throw new SrvyOfficePostalFailException(MessageUtil.getMessage("SRVY_OFFICE_POSTAL_FAIL"), applNo);
        }

    }

    private List<AlumniDTO> getAlumni(Long applNo) {
        List<Alumni> alumni = alumniRepository.findByAppl_id(applNo);
        List<AlumniDTO> alumniList = modelMapper.convert(alumni, new TypeToken<List<AlumniDTO>>() {
        }.getType());
        return alumniList;
    }

    private List<SchoolCommCodeDto> getRecruitSchoolCodeList(Long applNo) {
        // 코드명 하드코딩:석사용. KDI용 추가설문에 답변으로 선택되는 내용임
        Appl dbAppl = applRepository.findById(applNo).get();

        List<SchoolCommCode> schoolCommCodeList = schoolCommCodeRepository.findAllByCodeGrpAndSchoolCodeAndEnterYearAndRecruitPartSeqAndUsedTrueOrderByCodeAsc("SRVY_INTER_TYPE", dbAppl.getPart().getSchoolCode(), dbAppl.getPart().getEnterYear(), dbAppl.getPart().getRecruitPartSeq());
        return modelMapper.convert(schoolCommCodeList, new TypeToken<List<SchoolCommCode>>() {
        }.getType());
    }

    private List<SchoolCommCodeDto> getRecruitSchoolCodeListforDoctor(Long applNo) {
        // 코드명 하드코딩:박사용. KDI용 추가설문에 답변으로 선택되는 내용임
        Appl dbAppl = applRepository.findById(applNo).get();

        List<SchoolCommCode> schoolCommCodeList = schoolCommCodeRepository.findAllByCodeGrpAndSchoolCodeAndEnterYearAndRecruitPartSeqAndUsedTrueOrderByCodeAsc("SRVY_INTER_TYPE_D", dbAppl.getPart().getSchoolCode(), dbAppl.getPart().getEnterYear(), dbAppl.getPart().getRecruitPartSeq());
        return modelMapper.convert(schoolCommCodeList, new TypeToken<List<SchoolCommCode>>() {
        }.getType());
    }

}
