package kr.co.apexsoft.gradnet2.user_api.user.service;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import kr.co.apexsoft.gradnet2.user_api._config.role.expeption.RoleNotFoundException;
import kr.co.apexsoft.gradnet2.entity.role.repository.RoleRepository;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._config.security.jwt.JwtAuthenticationResponse;
import kr.co.apexsoft.gradnet2.user_api._config.security.jwt.JwtTokenProvider;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.HashUtil;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api.user.exception.InvalidUserException;
import kr.co.apexsoft.gradnet2.user_api.user.exception.InvalidUserInfoException;
import kr.co.apexsoft.gradnet2.user_api.user.exception.UserLockException;
import kr.co.apexsoft.gradnet2.user_api.user.exception.UserNotFoundException;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 김혜연
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserRegisterService {

    @NonNull
    private UserRepository userRepository;

    @NonNull
    private UserService userService;

    @NonNull
    private RoleRepository roleRepository;

    @NonNull
    private PasswordEncoder passwordEncoder;

    @NonNull
    private AuthenticationManager authenticationManager;

    @NonNull
    private JwtTokenProvider tokenProvider;

    @Value("${front.url}")
    private String frontUrl;

    @NonNull
    private MailService mailService;

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${login.failMaxCnt}")
    private Integer failMaxCnt;

    @Transactional
    public User register(@NonNull User user, String[] roles, Errors errors) {
        checkDuplicateId(user.getUserId(), errors);
        checkDuplicateEmail(user.getEmail(), errors);

        for(String role : roles) {
            try {
                user.addRole(new Role(RoleType.valueOf(role).getValue(), RoleType.valueOf(role)));
            } catch (IllegalArgumentException e) {
                throw new RoleNotFoundException(MessageUtil.getMessage("ROLE_NOT_FOUND"));
            }
        }

        if (errors.hasErrors())
            throw new InvalidUserInfoException(MessageUtil.getMessage("INVALID_USER_INFO"), errors);

        user.initUser();
        user.changePassword(this.passwordEncoder.encode(user.getPassword()));

        return this.userRepository.save(user);
    }

    private void checkDuplicateId(String userId, Errors errors) {
        if (this.userRepository.findByUserId(userId).isPresent())
            errors.rejectValue("userId", "userId.duplicated", MessageUtil.getMessage("USERID_ALREADY_REGISTERED"));
    }

    private void checkDuplicateEmail(String email, Errors errors) {
        if(this.userRepository.findByEmail(email).isPresent())
            errors.rejectValue("email", "email.duplicated", MessageUtil.getMessage("EMAIL_ALREADY_REGISTERED"));
    }

    private void checkUserDuplicateEmail(Long userNo, String email, Errors errors) {
        if(this.userRepository.findByEmailAndIdNot(email, userNo).isPresent())
            errors.rejectValue("email", "email.duplicated", MessageUtil.getMessage("EMAIL_ALREADY_REGISTERED"));
    }

    /**
     * 회원가입 시 이메일 인증 메일 전송
     *
     * @param email
     */
    private void sendRegisterEmail(String userId, String email) {
        String hash = HashUtil.generateHash(userId+";"+email);
        Map<String, String> replacements = new HashMap<>();
        String expireDate = LocalDateTime.now().plusMinutes(30).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        replacements.put("url", frontUrl+ "/user/confirm/email"+ userId + "/" + expireDate+ "/" + hash);
        // TODO: 이메일 인증할 임시 화면 필요

        List<String> mailList = new ArrayList<String>();
        mailList.add(email);

        MailDto dto = MailDto.builder()
                .to(mailList)
                .subject("이메일 인증을 해주세요. Certificate your email.")
                .templateId("register-user.html")
                .replacements(replacements)
                .resourceLoader(resourceLoader)
                .build();

        mailService.send(dto);

    }

    public Boolean confirmRegisterHash(String userId, String hash) {
        User user = userService.findByUserId(userId);
        String hashconfirm = HashUtil.generateHash(user.getUserId()+";"+user.getEmail());

        if(hash.equals(hashconfirm))
            return true;
        return false;
    }

    @Transactional(readOnly = true)
    public Boolean isUse(String type, String id) {
        if(type.equals("userId"))
            return !this.userRepository.findByUserId(id).isPresent();
        else if(type.equals("email"))
            return !this.userRepository.findByEmail(id).isPresent();

        return true;
    }

    public JwtAuthenticationResponse login(String userId, String password) {
        UserPrincipal userPrincipal = null;
        Authentication authentication = null;

        try {
            authentication = this.authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            userId,
                            password
                    )
            );

        } catch (InternalAuthenticationServiceException e){ // 존재하지 않는 사용자
            throw new UserNotFoundException(MessageUtil.getMessage("USER_NOT_FOUND"));
        } catch(DisabledException e) {  // 유효한 회원이 아님
            throw new InvalidUserException(MessageUtil.getMessage("LOGIN_FAIL"));
        } catch (BadCredentialsException e) {
            this.failPassword(userId);
            throw new BadCredentialsException(MessageUtil.getMessage("FAIL_PASSWORD"));
        } catch(LockedException e) {    // 계정 잠김
            throw new UserLockException(userId, MessageUtil.getMessage("REQUIRED_CAPTCHA"));
        }

        userPrincipal = (UserPrincipal) authentication.getPrincipal();

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = this.tokenProvider.generateToken(authentication);

        return new JwtAuthenticationResponse(jwt, userId, userPrincipal.getRoles());
    }

    /**
     * 비밀번호 실패 횟수 확인 (lock)
     * @param userId
     */
    @Transactional
    void failPassword(String userId) {
        this.userRepository.updateFailCnt(userId);
        this.userRepository.updateLock(userId, failMaxCnt);
    }

    @Transactional
    public User changePassword(String userId, String newPassword) {
        User user = this.userService.findByUserId(userId);
        user.changePassword(passwordEncoder.encode(newPassword));
        return user;
    }

    @Transactional(readOnly = true)
    public boolean isChangeEmail(String email, Long userNo) {
        boolean isPresent = this.userRepository.findByEmailAndIdNot(email, userNo).isPresent();
        return !isPresent;
    }

    @Transactional
    public User changeInfo(User dbUser, String email, String password, Errors errors) {
        checkUserDuplicateEmail(dbUser.getId(), email, errors);

        if (errors.hasErrors())
            throw new InvalidUserInfoException(MessageUtil.getMessage("EMAIL_ALREADY_REGISTERED"), errors);

        dbUser.changeEmail(email);
        if(!password.isEmpty()) {
            dbUser.changePassword(passwordEncoder.encode(password));
        }
        return userRepository.save(dbUser);
    }

    @Transactional(readOnly = true)
    public Boolean confirmPassword(String userId, String password) {
        User user = userRepository.findByUserId(userId).orElseThrow(() -> new UserNotFoundException(MessageUtil.getMessage("USER_NOT_FOUND")));
        return passwordEncoder.matches(password, user.getPassword());
    }
}
