package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.BizException;
import lombok.Getter;

import java.util.HashMap;

/**
 * 저장시 appl status 체크
 * 취소 상태
 * @author 김효숙
 */
@Getter
public class ApplSaveStatusCancelBizException extends BizException {


    public ApplSaveStatusCancelBizException(String warningMessage, Long applNo, HashMap<String,Object> returnMap) {
        super(warningMessage, "applNo:" + applNo,returnMap);
    }

    public ApplSaveStatusCancelBizException(String warningMessage, Long applNo) {
        super(warningMessage, "applNo:" + applNo);
    }

}

