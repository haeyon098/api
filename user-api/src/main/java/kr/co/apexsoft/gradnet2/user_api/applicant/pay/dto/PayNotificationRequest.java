package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import lombok.Getter;
import lombok.ToString;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-18
 */
@Getter
@ToString
public class PayNotificationRequest {
    private String imp_uid;
    private String merchant_uid;
    private String status;
}
