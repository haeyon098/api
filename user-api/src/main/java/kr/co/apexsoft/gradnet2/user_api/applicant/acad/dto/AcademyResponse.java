package kr.co.apexsoft.gradnet2.user_api.applicant.acad.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Grade;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;

@Getter
public class AcademyResponse {
    private Long applId;
    private Long id;
    private Academy.AcademyType type;
    private Integer seq;
    private Boolean lasted;
    private String statusCode;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate enterDay;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate graduateDay;

    private Grade grade;

    private String schoolCollegeName;
    private String schoolMajorName;
    private String schoolDegreeNo;


    private Cntr cntr;
    private Schl schl;

    public AcademyResponse(Long applNo,Long id, Academy.AcademyType type, Integer seq, Boolean lasted, String statusCode, LocalDate enterDay, LocalDate graduateDay,
                           Grade grade,
                           String schoolCollegeName, String schoolMajorName, String schoolDegreeNo,
                           String schoolCountryCode, String schoolCode, String schoolName, String schoolEtcCountryName) {
        this.applId=applNo;
        this.id = id;
        this.type = type;
        this.seq = seq;
        this.lasted = lasted;
        this.statusCode = statusCode;
        this.enterDay = enterDay;
        this.graduateDay = graduateDay;
        this.grade = grade;
        this.schoolCollegeName = schoolCollegeName;
        this.schoolMajorName = schoolMajorName;
        this.schoolDegreeNo = schoolDegreeNo;

        this.cntr = new Cntr(schoolCountryCode, schoolEtcCountryName);

        this.schl = new Schl(schoolCode, schoolName);

    }

    @Getter
    @AllArgsConstructor
    private class Cntr {
        private String id;


        private String name;
    }

    @Getter
    @AllArgsConstructor
    private class Schl {
        private String id;

        private String name;
    }

}
