package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.Getter;

import javax.validation.constraints.NotBlank;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-10-23
 */
@Getter
public class ApplResponse {
    private Long id;

    private String applicantUserId;

    private String partSchoolCode;
    private String partEnterYear;
    private String partRecruitPartSeq;

    private String partRemk;
    private Integer partFeeKrw;
    private Double partFeeUsd;

    private String applicantInfoKorName;
    private String applicantInfoEngName;
    private String applicantInfoEmail;
    private String applicantInfoTelNum;
}
