package kr.co.apexsoft.gradnet2.user_api.user.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * created by hanmomhanda@gmail.com
 */
public class UserNotFoundException extends CustomException {

    public UserNotFoundException() {
        super();
    }

    public UserNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public UserNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public UserNotFoundException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
