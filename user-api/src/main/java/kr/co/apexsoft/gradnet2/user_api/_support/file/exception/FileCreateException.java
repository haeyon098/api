package kr.co.apexsoft.gradnet2.user_api._support.file.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 */
public class FileCreateException extends CustomException {

    public FileCreateException() {
        super();
    }

    public FileCreateException(String warningMessage) {
        super(warningMessage);
    }

    public FileCreateException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
