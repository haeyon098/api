package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto;


import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-25
 */
@Getter
public class RecommendRequest {
    @NotNull
    private Long applNo;

    @NotBlank
    @Size(max = 50)
    private String name;

    @NotBlank
    @Size(max = 100)
    @Email
    private String email;

    @NotBlank
    @Size(max = 50)
    private String institute;

    @NotBlank
    @Size(max = 50)
    private String position;

    @NotBlank
    @Size(max = 30)
    private String phoneNum;

}
