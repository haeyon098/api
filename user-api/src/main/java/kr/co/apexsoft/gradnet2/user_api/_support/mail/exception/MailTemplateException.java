package kr.co.apexsoft.gradnet2.user_api._support.mail.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * 메일
 */
public class MailTemplateException extends CustomException {

    public MailTemplateException() {
        super();
    }

    public MailTemplateException(String warningMessage) {
        super(warningMessage);
    }

    public MailTemplateException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
