package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class ApplNotFoundException extends CustomException {

    public ApplNotFoundException() {
        super();
    }

    public ApplNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public ApplNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public ApplNotFoundException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
