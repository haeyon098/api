package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
*
*
* @author aran
* @since 2020-03-02
*/
public class SrvyAnsNotFoundException extends CustomException {
    public SrvyAnsNotFoundException()  {
        super();
    }

    public SrvyAnsNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public SrvyAnsNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public SrvyAnsNotFoundException(String warningMessage, Long id) { super("[ srvyNo: "+id+" ]", warningMessage); }
}
