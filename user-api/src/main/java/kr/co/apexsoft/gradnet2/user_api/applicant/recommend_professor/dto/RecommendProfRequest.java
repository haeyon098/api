package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto;

import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-28
 */
@Getter
public class RecommendProfRequest {

    @NotBlank
    private String key;

    @NonNull
    private Long applNo;

    @NotBlank
    @Size(max = 50)
    private String name;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 50)
    private String institute;

    @NotBlank
    @Size(max = 100)
    private String address;

    @NotBlank
    @Size(max = 50)
    private String position;

    @NotBlank
    @Size(max = 30)
    private String phoneNum;

    @NotBlank
    @Size(max = 50)
    private String sign;

    @NonNull
    private LocalDate recDay;

    @NotNull
    private List<AnswerDto> answerList;



}
