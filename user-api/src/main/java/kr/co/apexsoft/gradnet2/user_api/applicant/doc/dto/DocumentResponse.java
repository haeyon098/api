package kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto;

import lombok.Getter;

@Getter
public class DocumentResponse {

    private Long id;

    private Integer seq;

    private String groupCode;

    private Integer groupNo;

    private String itemCode;

    private String itemName;

    private Integer pdfPageCount;

    private String originFileName;

    private String filePath;

    private Boolean isImage;

    private String extension;

}
