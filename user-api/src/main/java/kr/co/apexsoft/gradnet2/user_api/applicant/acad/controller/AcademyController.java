package kr.co.apexsoft.gradnet2.user_api.applicant.acad.controller;

import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.user_api.applicant.acad.dto.AcademyRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.acad.service.AcademyService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-12
 */
@RestController
@RequestMapping("/appl/academy")
@RequiredArgsConstructor
public class AcademyController {

    @NonNull
    private AcademyService academyService;

    private final ApexModelMapper modelMapper;

    private final ApexCollectionValidator apexCollectionValidator;


    /**
     * 학력 정보 조회
     *
     * @param id
     * @param userPrincipal
     * @return
     */
    @GetMapping("/{applNo}")
    public ResponseEntity<Map<Object,Object>> findAcad(@PathVariable("applNo") Long id,
                                                       @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(academyService.findAcademyByApplNo(id));

    }

    /**
     * 학력 저장
     *
     * @param academyRequestList
     * @param errors
     * @param id
     * @param userPrincipal
     * @return
     */
    @PostMapping("/{applNo}")
    public ResponseEntity<Map<Object,Object>> createAcad(@Valid @RequestBody List<AcademyRequest> academyRequestList,
                                                         Errors errors,
                                                         @PathVariable("applNo") Long id,
                                                         @AuthenticationPrincipal UserPrincipal userPrincipal) {

        apexCollectionValidator.validate(academyRequestList,errors);

        if (errors.hasErrors()) {
            throw new InvalidException(MessageUtil.getMessage("APPL_ACADEMY_INVALID_INFO"), errors);
        }

        return ResponseEntity.ok(academyService.createAcad(modelMapper.convert(academyRequestList, new TypeToken<List<Academy>>() {
        }.getType()), id,userPrincipal.getRoles()));
    }


}
