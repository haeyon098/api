package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.Getter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
public class AddressDto {
    private String postNo;
    private String address;
    private String detailAddress;
}
