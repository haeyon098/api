package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 수정 appl status 체크
 * 취소 상태
 * @author 박지환
 */
@Getter
public class ApplUpdateStatusCancelException extends CustomException {



    public ApplUpdateStatusCancelException(String warningMessage, Long applNo) {
        super( "[applNo:" + applNo + "]", warningMessage);
    }

}

