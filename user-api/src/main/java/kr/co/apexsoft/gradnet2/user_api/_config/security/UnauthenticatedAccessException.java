package kr.co.apexsoft.gradnet2.user_api._config.security;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;

/**
 * created by hanmomhanda@gmail.com
 */
public class UnauthenticatedAccessException extends RuntimeException {

    public UnauthenticatedAccessException() {
        super(MessageUtil.getMessage("UNAUTHENTICATED_USER"));
    }

    public UnauthenticatedAccessException(String message) {
        super(message);
    }

    public UnauthenticatedAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnauthenticatedAccessException(Throwable cause) {
        super(cause);
    }

    protected UnauthenticatedAccessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
