package kr.co.apexsoft.gradnet2.user_api._support.iamport.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessToken {

    String access_token;

    int expired_at;

    int now;


}