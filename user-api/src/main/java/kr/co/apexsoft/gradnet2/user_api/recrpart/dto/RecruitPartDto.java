package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-10-19
 */
@Getter
@Setter
public class RecruitPartDto {
    @NotBlank
    private String schoolCode;
    private String schoolName;
    @NotBlank
    private String enterYear;
    @NotNull
    private Integer recruitPartSeq;

    @NotBlank
    private String korName;
    @NotBlank
    private String engName;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime startDate;
    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime endDate;

    private List<RecruitPartTimeForSysadminDto> time;
}
