package kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-14
 */
@Getter
@Setter
public class LanguageResponse {


    private String id; //node_id

    private Long applId;

    private Language.LangType type;

    private Boolean isExam;

    private String infoTypeCode;

    private String detailGrp;

    private String detailCode;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate examDay;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expirationDay;

    private String score;

    private Boolean mandatory;

    private Boolean uploaded;


}
