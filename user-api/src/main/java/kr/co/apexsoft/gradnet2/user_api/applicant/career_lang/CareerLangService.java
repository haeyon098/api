package kr.co.apexsoft.gradnet2.user_api.applicant.career_lang;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.repository.LanguageRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartLanguage;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.applpart.repository.ApplPartLanguageRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import kr.co.apexsoft.gradnet2.user_api.applicant.career.dto.CareerResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.service.DocumentService;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResquest;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageValidation;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.ApplPartLanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.service.ApplPartService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CareerLangService {
    @NonNull
    private CareerRepository careerRepository;

    @NonNull
    private LanguageRepository languageRepository;

    @NonNull
    private ApplPartLanguageRepository applPartLanguageRepository;

    @NonNull
    private ApplRepository applRepository;

    private final ApexModelMapper modelMapper;

    @NonNull
    private ApplService applService;

    @NonNull
    private DocumentService documentService;


    @NonNull
    private ApplPartService applPartService;

    public  void validationLanguage(Long applNo, List<LanguageResquest> resquestList ) {
        //지원단위
        ApplPart part = applRepository.findById(applNo).get().getPart();

        //최상위 조건 부모노드==NULL
        List<ApplPartLanguage> list = applPartLanguageRepository.findByPartAndParentNode_IdIsNull(part);
        for(ApplPartLanguage lang:list){
            LanguageValidation.check(lang,resquestList);
        }

    }

    public CareerLangResponse save(Long id, List<Language> languages,
                                   List<Career> careers, Set<Role> roles) {
        Appl appl = applService.checkApplSave(id, Appl.Status.LANG_SAVE, roles);
        setApplCareerList(appl, careers);

        Long[] groupNos = ArrayUtils.addAll(this.getUpdateLangRow(id, languages), this.getUpdateCareerRow(id, careers));

        // 경력, 어학 관련 APPL_DOC, S3 파일 있으면 삭제
        documentService.documentDeleteByGroupCode(id, groupNos, DocGrpType.CAREER.getValue(), DocGrpType.LANG.getValue());

        List<ApplPartLanguageResponse> resultLang = saveLanguage(id, languages);
        List<Career> resultCareer = saveCareer(id, careers);

        appl.changeStatus(Appl.Status.LANG_SAVE);

        return new CareerLangResponse(appl.getStatus(),
                modelMapper.convert(resultCareer, new TypeToken<List<CareerResponse>>() {
                }.getType()),
                resultLang);
    }

    private List<Career> saveCareer(Long id, List<Career> careerList) {
        Long[] ids = careerList.stream().filter(c -> c.getId() != null).map(c -> c.getId()).toArray(Long[]::new);
        if(ids.length == 0) careerRepository.deleteByAppl_Id(id);
        else careerRepository.deleteByAppl_IdAndIdNotIn(id, ids);

        careerRepository.saveAll(careerList);
        return careerRepository.findByAppl_IdOrderBySeq(id);
    }

    private List<ApplPartLanguageResponse> saveLanguage(Long id, List<Language> languageList) {
        Long[] ids = languageList.stream().filter(l -> l.getId() != null).map(l -> l.getId()).toArray(Long[]::new);
        if(ids.length == 0) languageRepository.deleteByAppl_Id(id);
        else languageRepository.deleteByAppl_IdAndIdNotIn(id, ids);

        languageRepository.saveAll(languageList);
        return applPartService.getLanguageList(id);
    }


    private List<Career> setApplCareerList(Appl appl, List<Career> careerList) {
        for (Career career : careerList) {
            career.initAppl(appl);
        }
        return careerList;
    }

    /**
     * 어학정보코드 변경 건만 applLangNo 추출
     * @param applNo
     * @param reuqests
     * @return
     */
    private Long[] getUpdateLangRow(Long applNo, List<Language> reuqests) {
        List<Language> savedList = languageRepository.findByAppl_Id(applNo);

        for(Language request : reuqests) {
            if(request.getId() != null) savedList.removeIf(l -> l.notChangeLangInfoType(request.getId(), request.getInfoTypeCode()));
        }
        return savedList.stream().map(l -> l.getId() ).toArray(Long[]::new);
    }

    /**
     * 기관명 변경 건만 applCareerNo 추출
     * @param applNo
     * @param reuqests
     * @return
     */
    private Long[] getUpdateCareerRow(Long applNo, List<Career> reuqests) {
        List<Career> savedList = careerRepository.findByAppl_IdOrderBySeq(applNo);

        for(Career request : reuqests) {
            if(request.getId() != null) savedList.removeIf(c -> c.notChangeLangInfoType(request.getId(), request.getName()));
        }
        return savedList.stream().map(c -> c.getId() ).toArray(Long[]::new);
    }
}
