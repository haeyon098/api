package kr.co.apexsoft.gradnet2.user_api.recrpart.controller;

import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.RecruitPartCreateRequest;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.RecruitPartDto;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.RecruitPartTimeForSysadminDto;
import kr.co.apexsoft.gradnet2.user_api.recrpart.service.RecruitPartService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Class Description
 * 시스템관리자 - 모집회차 관리
 *
 * @author 김혜연
 * @since 2020-10-19
 */
@RestController
@RequestMapping("/sysadmin/recruit-part")
@RequiredArgsConstructor
public class SysadminRecruitPartController {

    @NonNull
    private RecruitPartService recruitPartService;

    private final ApexModelMapper modelMapper;

    private final ApexCollectionValidator apexCollectionValidator;

    /**
     * 모집회차 목록
     *
     * @param schoolCode
     * @param pageable
     * @return
     */
    @GetMapping
    public ResponseEntity<Page<RecruitPartDto>> findAllRecruitPart(@RequestParam(required = false) String schoolCode,
                                                                   @PageableDefault Pageable pageable) {
        return ResponseEntity.ok(modelMapper.convert(recruitPartService.findAllRecruitPart(schoolCode, pageable),
                new TypeToken<List<RecruitPartDto>>() {}.getType(), pageable));
    }

    /**
     * 모집회차 상세보기
     *
     * @param schoolCode
     * @param enterYear
     * @param recruitPartSeq
     * @return
     */
    @GetMapping("/{schoolCode}/{enterYear}/{recruitPartSeq}")
    public ResponseEntity<RecruitPartDto> findRecrujitPartOne (@PathVariable("schoolCode") String schoolCode,
                                                               @PathVariable("enterYear") String enterYear,
                                                               @PathVariable("recruitPartSeq") Integer recruitPartSeq) {
        return ResponseEntity.ok(recruitPartService.findRecruitPartOne(schoolCode, enterYear, recruitPartSeq));
    }

    /**
     * 모집회차 생성
     *
     * @param recruitPartRequest
     * @return
     */
    @PostMapping
    public ResponseEntity<RecruitPartDto> create(@Valid @RequestBody RecruitPartCreateRequest recruitPartRequest, Errors errors) {
        apexCollectionValidator.validate(recruitPartRequest.getTime(), errors);
        if (errors.hasErrors())
            throw new InvalidException("전체 필수 입력입니다.", errors);

        RecruitPart recruitPart = modelMapper.convert(recruitPartRequest, RecruitPart.class);
        return ResponseEntity.ok(modelMapper.convert(recruitPartService.create(recruitPart), RecruitPartDto.class));
    }

    /**
     * 모집회차 정보 수정
     *
     * @param recruitPartDto
     * @return
     */
    @PostMapping("/part")
    public ResponseEntity<RecruitPartDto> saveRecruitPart(@Valid @RequestBody RecruitPartDto recruitPartDto, Errors errors) {
        if (errors.hasErrors())
            throw new InvalidException("전체 필수 입력입니다.", errors);

        RecruitPart recruitPart = modelMapper.convert(recruitPartDto, RecruitPart.class);
        modelMapper.convert(recruitPart.getTime(), new TypeToken<List<RecruitPartTime>>(){}.getType());

        return ResponseEntity.ok(recruitPartService.saveRecruitPart(recruitPart));
    }

    /**
     * 모집회차 - 시간 정보 수정
     *
     * @param list
     * @return
     */
    @PostMapping("/time")
    public ResponseEntity<List<RecruitPartTimeForSysadminDto>> saveRecruitPartTime(
            @Valid @RequestBody List<RecruitPartTimeForSysadminDto> list, Errors errors) {
        apexCollectionValidator.validate(list, errors);
        if (errors.hasErrors())
            throw new InvalidException("전체 필수 입력입니다.", errors);

        List<RecruitPartTime> times = modelMapper.convert(list, new TypeToken<List<RecruitPartTime>>(){}.getType());
        return ResponseEntity.ok(recruitPartService.saveRecruitPartTime(times));
    }
}
