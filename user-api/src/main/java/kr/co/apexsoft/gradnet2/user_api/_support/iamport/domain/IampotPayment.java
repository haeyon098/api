package kr.co.apexsoft.gradnet2.user_api._support.iamport.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IampotPayment {

    String imp_uid;
    String merchant_uid;
    String currency;
    String buyer_name;
    String buyer_email;
    String buyer_tel_num;
    String buyer_tel;

    String vbank_name;
    String vbank_num;
    String vbank_holder;
    Long vbank_date;

    String status;


    String card_number;  //cardApplyNum
    Long paid_at;//payDate
    Double amount;//paid_amount
    String pg_provider; //pgType
    String pay_method;//payType


    String card_apply_num;  //cardApplyNum
    LocalDateTime pay_date;//payDate
    Double paid_amount;//paid_amount
    String pg_type; //pgType
    String pay_type;//payType
    LocalDateTime vbank_due_date;

    Long appl_Id;
    Long id;

    public String getCard_apply_num() {
        return card_number;
    }

    public LocalDateTime getVbank_due_date() {
        return getLocalDateTime(vbank_date);
    }

    public LocalDateTime getPay_date() {
        return getLocalDateTime(paid_at);
    }

    public Double getPaid_amount() {
        return amount;
    }

    public String getPg_type() {
        return pg_provider;
    }

    public String getPay_type() {
        return pay_method;
    }

    public String getBuyer_tel_num() {
        return buyer_tel;
    }

    private LocalDateTime getLocalDateTime(Long date) {
        if (date != 0) return new Timestamp(date * 1000L).toLocalDateTime();
        return null;
    }

    public void setApplNo(Long applNo) {
        this.appl_Id = applNo;
    }

    public Long getId() {
        return this.id;
    }
}
