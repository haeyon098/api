package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 아임포트
 */
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class IamportCheckResponse {

    private String impUid;
    private String merchantUid;

    private String pgType;
    private String payType;
    private Double amount;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime payDate;

    private String buyerName;
    private String buyerEmail;
    private String buyerTelNum;

    private String applNo;

    public String getApplNo(){
        return this.merchantUid.split("_")[3];
    }


}
