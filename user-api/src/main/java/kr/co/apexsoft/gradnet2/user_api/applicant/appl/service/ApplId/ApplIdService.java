package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplId;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-12-02
 */
public interface ApplIdService {

    void makeApplId(Appl appl);
    String getRuleNo(Appl appl) ;
}
