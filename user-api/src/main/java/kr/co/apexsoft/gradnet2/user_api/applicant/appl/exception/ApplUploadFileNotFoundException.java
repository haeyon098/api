package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;


public class ApplUploadFileNotFoundException extends CustomException {

    public ApplUploadFileNotFoundException(Long applNo, String warningMessage) {
        super("[applNo:" + applNo + "]",warningMessage);
    }

}
