package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto;

import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.AppBirtFileInfo;
import lombok.Getter;

/**
 * 추천서파일 정보
 * 학교,전형 공통으로 사용
 *
 */
@Getter
public class RecProfFileInfo extends AppBirtFileInfo {

    @Override
    public String getBirtFileName() {
        return FilePathUtil.getRecBirtFileName(this.getPartSchoolCode());
    }

    @Override
    public String getFileName() {
        return FilePathUtil.getApplFormFileName(this.getPartEnterYear(), this.getPartMajorCode(), this.getApplicantUserId());
    }

}
