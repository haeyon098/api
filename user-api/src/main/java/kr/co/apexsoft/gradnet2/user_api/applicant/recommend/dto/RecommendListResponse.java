package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-04-07
 */
@Getter
@AllArgsConstructor
public class RecommendListResponse {
    List<RecommendResponse> recommendResponses;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime requestStartDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime requestEndDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime recommendEndDate;
}
