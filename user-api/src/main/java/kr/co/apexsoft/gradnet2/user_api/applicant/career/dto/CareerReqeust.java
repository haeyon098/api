package kr.co.apexsoft.gradnet2.user_api.applicant.career.dto;

import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-14
 */
@Getter
public class CareerReqeust {
    private Long id;

    private Integer seq;

    @NonNull
    private LocalDate joinDay;

    @NonNull
    private LocalDate retireDay;

    private Boolean currently; //현재 재직여부

    @Size(max = 80)
    @NotBlank
    private String name;

    @NotBlank
    private String compTypeCode;

    @Size(max = 80)
    private String address;

    @Size(max = 50)
    @NotBlank
    private String department;

    @Size(max = 50)
    @NotBlank
    private String position;

    @Size(max = 255)
    private String description;
}
