package kr.co.apexsoft.gradnet2.user_api.recrpart.controller;

import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.RecruitPartForBannerResponse;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.RecruitPartPossibleForViewResponse;
import kr.co.apexsoft.gradnet2.user_api.recrpart.service.RecruitPartService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-12
 */
@RestController
@RequestMapping("/recruit")
@RequiredArgsConstructor
public class RecruitPartController {
    @NonNull
    private RecruitPartService recruitPartService;

    /**
     * 모집전형 조회
     *
     * @param schoolCode
     * @return
     */
    @GetMapping("/{schoolCode}")
    public ResponseEntity<List<RecruitPartPossibleForViewResponse>> findAllByRecruitPart(@PathVariable String schoolCode) {
               return ResponseEntity.ok(recruitPartService.findAllForViewByRecruitPart(schoolCode));
        //       return ResponseEntity.ok(recruitPartService.findAllByRecruitPart(schoolCode));
    }

    /**
     * 배너 광고용 모집전형 전체 조회
     *
     * @return
     */
    @GetMapping("/banner")
    public ResponseEntity<List<RecruitPartForBannerResponse>> findAllByRecruitPartForBanner() {
        return ResponseEntity.ok(recruitPartService.findAllByRecruitPartForBanner());
    }
}


