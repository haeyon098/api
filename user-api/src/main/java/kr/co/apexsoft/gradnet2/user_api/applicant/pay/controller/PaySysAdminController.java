package kr.co.apexsoft.gradnet2.user_api.applicant.pay.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.Pay;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.ManualPayDto;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayNotCompleteResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.service.PayAdminService;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.service.PayService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * SYSTEM ADMIN
 */
@RestController
@RequestMapping("/sysadmin/pay")
@RequiredArgsConstructor
public class PaySysAdminController {

    @NonNull
    private PayAdminService payAdminService;

    @NonNull
    private PayService payService;

    private final ApexModelMapper modelMapper;


    /**
     * 수동 결제처리
     *
     * @param applNo
     * @param manualPayDto
     * @param errors
     * @return
     */
    @PostMapping("/manual/{applNo}")
    public ResponseEntity<Pay> manualPay(@PathVariable Long applNo, @Valid @RequestBody ManualPayDto manualPayDto, Errors errors) {
        if (errors.hasErrors())
            throw new InvalidException("필수입력을 확인해주세요.", errors);

        return ResponseEntity.ok(payService.manualPay(applNo, modelMapper.convert(manualPayDto, Pay.class)));
    }


    /**
     * 정산처리 체크
     * @param type
     * @param from
     * @param to
     * @return
     */
    @GetMapping("/check/{type}")
    public ResponseEntity<PayNotCompleteResponse> check(@PathVariable String type,
                                                        @RequestParam("from") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime from,
                                                        @RequestParam("to") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") LocalDateTime to) {

        if (type.equals("incomplete"))
            return ResponseEntity.ok(payAdminService.getIncompleteList(from, to));
        else
            return ResponseEntity.ok(payAdminService.getUnPayList(from, to));
    }


}
