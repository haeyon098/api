package kr.co.apexsoft.gradnet2.user_api._support.aws.s3.exception;

/**
 * s3 객체간 복사
 */
public class S3CopyException extends S3CommException {


    public S3CopyException(String keyName,String keyValue, String warningMessage) {
        super(keyName,keyValue,warningMessage);
    }

}
