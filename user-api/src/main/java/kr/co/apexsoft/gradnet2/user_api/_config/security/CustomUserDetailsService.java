package kr.co.apexsoft.gradnet2.user_api._config.security;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api.user.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * created by hanmomhanda@gmail.com
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CustomUserDetailsService implements UserDetailsService {
    @NonNull
    private UserService userService;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String userId) {
        User user = this.userService.findByUserId(userId);

        return new UserPrincipal(user.getId(),
                user.getUserId(), user.getPassword(), user.getRoles(), !user.getLocked(), user.checkActiveUser(), user);
    }
}
