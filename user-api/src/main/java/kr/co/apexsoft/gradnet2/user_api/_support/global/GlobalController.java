package kr.co.apexsoft.gradnet2.user_api._support.global;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-03-05
 */
@RestController
@RequestMapping("/global")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class GlobalController {
}
