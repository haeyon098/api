package kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto;

import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.Size;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-08-04
 */
@Getter
public class EssayRequest {
    private Long id;        // APPL_ESSAY_NO

    @NonNull
    private Long applId;    // APPL_NO

    @NonNull
    private Integer seq;

    @Size(max=15000)
    private String data;
}
