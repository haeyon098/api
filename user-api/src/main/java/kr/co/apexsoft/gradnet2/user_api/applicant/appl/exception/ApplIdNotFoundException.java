package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;


public class ApplIdNotFoundException extends CustomException {

    public ApplIdNotFoundException(String warningMessage) {
        super(warningMessage);
    }

}
