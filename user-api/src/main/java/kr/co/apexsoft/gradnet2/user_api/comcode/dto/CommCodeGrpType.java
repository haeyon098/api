package kr.co.apexsoft.gradnet2.user_api.comcode.dto;

public enum CommCodeGrpType {
    SYSTEM("시스템공통"),
    SCHOOL("학교별공통코드");

    private final String name;

    private CommCodeGrpType(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
