package kr.co.apexsoft.gradnet2.user_api._support.aws.s3.domain;

import java.io.InputStream;

public class FileWrapper {
    private InputStream inputStream;
    private FileMeta fileMeta;

    public FileWrapper() {}

    public FileWrapper(InputStream inputStream, FileMeta fileMeta) {
        this.inputStream = inputStream;
        this.fileMeta = fileMeta;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public FileMeta getFileMeta() {
        return fileMeta;
    }

    public void setFileMeta(FileMeta fileMeta) {
        this.fileMeta = fileMeta;
    }
}
