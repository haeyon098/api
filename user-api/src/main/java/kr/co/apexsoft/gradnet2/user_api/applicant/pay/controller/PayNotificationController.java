package kr.co.apexsoft.gradnet2.user_api.applicant.pay.controller;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.service.PayService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.xerces.impl.io.Latin1Reader;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-10
 */
@RestController
@RequestMapping
@RequiredArgsConstructor
public class PayNotificationController {
    @NonNull
    private PayService payService;

    private final String[] IAMPORT_IP = {"52.78.100.19", "52.78.48.223"};

    /**
     * 아임포트 웹훅 (Notification)
     *
     * @param payNotificationRequest
     */
    @PostMapping("/pay/notification")
    public void notification(@RequestBody PayNotificationRequest payNotificationRequest) {
        try {
            checkIamportIp();

            if (payNotificationRequest.getStatus().equals("paid")) // 가상계좌에 입금되었을 때
                payService.notification(payNotificationRequest.getMerchant_uid());
        } catch (Exception e) {
            payService.sendFailMail(null,"","가상계좌 입금통보", e.getClass().getName(), e.getMessage(),
                    payNotificationRequest.toString());
            throw e;
        }
    }

    /**
     * 아임포트 고정 ip 확인
     */
    private void checkIamportIp() {
        String ip = IpUtil.getUserIp();

        if(!Arrays.asList(IAMPORT_IP).contains(ip))
            throw new AccessDeniedException("IAMPORT IP가 아닙니다. 잘못된 접근입니다. / IP : "+ip);
    }

}
