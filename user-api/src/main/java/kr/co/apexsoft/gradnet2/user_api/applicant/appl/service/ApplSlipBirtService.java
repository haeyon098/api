
package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplSlipProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.AppBirtFileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 수험표
 */
@Service
@Transactional
public class ApplSlipBirtService extends ApplBirtService {
    @Autowired
    private ApplRepository applRepository;

    @Override
    public Map<String, Object> setBirtInfo(AppBirtFileInfo fileInfo) {
        ApplSlipProjection applSlipProjection=applRepository.findMyApplSlip(fileInfo.getApplNo(), DocItem.PHOTO.getValue());
        ObjectMapper oMapper = new ObjectMapper();
        return oMapper.convertValue(applSlipProjection, Map.class);
    }

}

