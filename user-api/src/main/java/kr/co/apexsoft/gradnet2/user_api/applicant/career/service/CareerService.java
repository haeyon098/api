package kr.co.apexsoft.gradnet2.user_api.applicant.career.service;

import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.user_api.applicant.career.dto.CareerResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CareerService {
    @NonNull
    private CareerRepository careerRepository;

    private final ApexModelMapper modelMapper;

    @Transactional(readOnly = true)
    public List<CareerResponse> findAllCareer(Long id) {
        List<Career> careers = careerRepository.findByAppl_IdOrderBySeq(id);
        List<CareerResponse> responses = modelMapper.convert(careers, new TypeToken<List<CareerResponse>>(){}.getType());
        return responses;
    }
}
