package kr.co.apexsoft.gradnet2.user_api._support.birt.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Birt 생성
 */
public class BirtGenerateException extends CustomException {

    public BirtGenerateException() {
        super();
    }

    public BirtGenerateException(String warningMessage) {
        super(warningMessage);
    }

    public BirtGenerateException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
