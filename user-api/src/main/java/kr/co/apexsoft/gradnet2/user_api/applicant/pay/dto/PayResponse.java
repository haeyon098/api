package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-17
 */
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayResponse {
    private Long id;
    private String impUid;
    private String merchantId;

    private String status;

    private String vbankNum;
    private String vbankName;
    private String vbankHolder;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime vbankDueDate;
}
