package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 추천서 요청시 지원사항 저장 상태에서는 불가
 * @author 박지환
 */
@Getter
public class RecReqeustStatusFailException extends CustomException {


    public RecReqeustStatusFailException(String warningMessage, Long applNO) {
        super("[applNO:" + applNO + "]", warningMessage);
    }


}

