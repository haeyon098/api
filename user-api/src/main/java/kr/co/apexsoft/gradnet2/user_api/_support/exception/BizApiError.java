package kr.co.apexsoft.gradnet2.user_api._support.exception;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.springframework.http.HttpStatus;

import java.util.HashMap;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class BizApiError extends ApiError {

    private HashMap<String,Object> returnMap;


    public BizApiError(@NonNull HttpStatus status, @NonNull String timestamp, String debugMessage,
                       String warningMessage, String exceptionName, HashMap<String,Object> map) {
        super(status,timestamp,debugMessage,warningMessage,exceptionName);
        this.returnMap=map;
    }



}
