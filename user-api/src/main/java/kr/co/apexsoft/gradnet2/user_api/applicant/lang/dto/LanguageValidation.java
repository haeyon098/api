package kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto;

import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartLanguage;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.exception.LanguageValidationFailException;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 어학 validation용
 *
 * @author 김효숙
 * @since 2020-07-09
 */

@Getter
public class LanguageValidation {

    private String id;
    private Boolean isPresent;
    private Boolean optionTypeCheckResult; //subNode 옵션규칙 체크 결과
    private List<LanguageValidation> subNode;

    private LanguageValidation(String id, ApplPartLanguage.OptionType optionType, Boolean isLeaf,
                               Boolean isPresent, List<LanguageValidation> subNode) {
        this.id = id;
        this.isPresent = isLeaf ? isPresent : subNode.stream().anyMatch(LanguageValidation::getIsPresent);
        this.subNode = subNode;
        this.optionTypeCheckResult = checkOptionTypeCode(optionType, subNode);


    }

    private static LanguageValidation form(ApplPartLanguage language,
                                           List<LanguageResquest> resquestList) {
        return new LanguageValidation(
                language.getId(), language.getOptionType(), language.getIsLeaf(),
                resquestList.stream().anyMatch(languageResquest -> languageResquest.getNodeId().equals(language.getId())),
                language.getSubNode().stream().map((ApplPartLanguage language1) -> form(language1, resquestList)).collect(Collectors.toList()));
    }

    public static void check(ApplPartLanguage language,
                             List<LanguageResquest> resquestList) {

        LanguageValidation reform = LanguageValidation.form(language, resquestList); //validation필한 정보로 세팅
        LanguageValidation.valid(reform);
    }

    private static  void valid(LanguageValidation languageValidation){
        //최 상단부터 체크필요
        if (!languageValidation.getOptionTypeCheckResult()){
            throw new LanguageValidationFailException(MessageUtil.getMessage("LANGUAGE_INVALID_INFO"));
        }else {
            languageValidation.getSubNode().forEach(languageValidationConsumer -> {
                if (languageValidationConsumer.getIsPresent()) {
                    LanguageValidation.valid(languageValidationConsumer);
                }});
        }
    }
    private Boolean checkOptionTypeCode(ApplPartLanguage.OptionType optionType, List<LanguageValidation> subNode) {
        long count = subNode.stream().filter(LanguageValidation::getIsPresent).count();
        Boolean result = true;
        switch (optionType) {
            case MO:
                result = count == 1;
                break;
            case MM:
                result = count >= 1;
                break;
            case OO:
                result = (count == 0 || count == 1);
                break;
            case OM:
                //result = count >= 0;
                break;
        }

        return result;

    }


}
