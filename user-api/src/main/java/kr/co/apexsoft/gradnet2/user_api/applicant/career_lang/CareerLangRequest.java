package kr.co.apexsoft.gradnet2.user_api.applicant.career_lang;

import kr.co.apexsoft.gradnet2.user_api.applicant.career.dto.CareerReqeust;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResquest;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CareerLangRequest {
    private List<CareerReqeust> careerList;

    private List<LanguageResquest> languageList;
}
