package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class RecProfInfoNotlException extends CustomException {

    public RecProfInfoNotlException() {
        super();
    }

    public RecProfInfoNotlException(String warningMessage) {
        super(warningMessage);
    }

    public RecProfInfoNotlException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public RecProfInfoNotlException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
