package kr.co.apexsoft.gradnet2.user_api.applicant.appl.controller;

import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.BasisService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-01-08
 */
@RestController
@RequestMapping("/appl/basis")
@RequiredArgsConstructor
public class BasisController {
    @NonNull
    private BasisService basisService;

    private final ApexModelMapper modelMapper;

    private final ApexCollectionValidator apexCollectionValidator;

    /**
     * 기본정보 조회
     *
     * @param id
     * @param userPrincipal
     * @return
     */
    @GetMapping("/{applNo}")
    public ResponseEntity<BasisInfoResponse> findBasisInfo(@PathVariable("applNo") Long id,
                                                           @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(basisService.findBasisInfo(id));
    }

    /**
     * 내국인 기본정보 저장/수정
     *
     * @param basisInfoGenRequest
     * @param errors
     * @param userPrincipal
     * @return
     */
    @PostMapping("/gen/{applNo}")
    public ResponseEntity<BasisInfoResponse> createBasisGen(@Valid @RequestBody BasisInfoGenRequest basisInfoGenRequest, @PathVariable(name = "applNo") Long id,
                                                             @AuthenticationPrincipal UserPrincipal userPrincipal,Errors errors) {
        apexCollectionValidator.validateBasisGen(basisInfoGenRequest, errors);
        if(errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("BASIS_INFO_INVALID_INFO"),errors);

        return ResponseEntity.ok(modelMapper.convert(
                basisService.createBasisInfo(modelMapper.convert(basisInfoGenRequest, Appl.class), id, true,userPrincipal.getRoles()), BasisInfoResponse.class));
    }

    /**
     * 외국인 기본정보 저장/수정
     *
     * @param basisInfoFornRequest
     * @param errors
     * @param userPrincipal
     * @return
     */
    @PostMapping("/forn/{applNo}")
    public ResponseEntity<BasisInfoResponse> createBasisForn(@Valid @RequestBody BasisInfoFornRequest basisInfoFornRequest, Errors errors, @PathVariable(name = "applNo") Long id,
                                                             @AuthenticationPrincipal UserPrincipal userPrincipal) {
        apexCollectionValidator.validateBasisForn(basisInfoFornRequest, errors);
        if (errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("BASIS_INFO_INVALID_INFO"),errors);

        return ResponseEntity.ok(modelMapper.convert(
                basisService.createBasisInfo(modelMapper.convert(basisInfoFornRequest, Appl.class), id, false,userPrincipal.getRoles()), BasisInfoResponse.class));
    }
}
