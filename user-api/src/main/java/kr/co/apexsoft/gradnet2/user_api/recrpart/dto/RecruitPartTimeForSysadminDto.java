package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-08
 */
@Getter
@Setter
public class RecruitPartTimeForSysadminDto {
    private Long id;

    @NotBlank
    private String recruitPartSchoolCode;
    @NotBlank
    private String recruitPartEnterYear;
    @NotNull
    private Integer recruitPartSeq;

    @NotNull
    private RecruitPartTime.TimeType type;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime startDate;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd'T'HH:mm")
    private LocalDateTime endDate;

    @NotNull
    private String description;
}
