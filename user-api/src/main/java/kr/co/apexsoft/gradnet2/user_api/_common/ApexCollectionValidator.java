package kr.co.apexsoft.gradnet2.user_api._common;

import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.BasisInfoFornRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.BasisInfoGenRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.career_lang.CareerLangRequest;
import lombok.val;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.validation.Validation;
import java.util.Collection;

@Component
public class ApexCollectionValidator implements Validator {

    private SpringValidatorAdapter validator;

    public ApexCollectionValidator() {
        this.validator = new SpringValidatorAdapter(
                Validation.buildDefaultValidatorFactory().getValidator()
        );
    }

    @Override
    public boolean supports(Class clazz) {
        return true;
    }

    @Override
    public void validate(Object target, Errors errors) {
        if(target instanceof Collection){
            Collection collection = (Collection) target;

            for (Object object : collection) {
                validator.validate(object, errors);
            }
        } else {
            validator.validate(target, errors);
        }

    }

    public void validateCareerLang(CareerLangRequest target, Errors errors) {
        Collection careerCollection = target.getCareerList();
        for(Object career: careerCollection) {
            validator.validate(career, errors);
        }
        Collection langCollection = target.getLanguageList();
        for(Object language: langCollection) {
            validator.validate(language, errors);
        }

    }

    public void validateBasisGen(BasisInfoGenRequest genRequest, Errors errors) {
        validator.validate(genRequest, errors);
        validator.validate(genRequest.getApplicantInfo(), errors);
        validator.validate(genRequest.getGeneralInfo(), errors);
    }

    public void validateBasisForn(BasisInfoFornRequest fornRequest, Errors errors) {
        validator.validate(fornRequest, errors);
        validator.validate(fornRequest.getApplicantInfo(), errors);
        validator.validate(fornRequest.getForeignerInfo(), errors);
    }
}
