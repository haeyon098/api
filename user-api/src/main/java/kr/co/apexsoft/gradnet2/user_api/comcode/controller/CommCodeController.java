package kr.co.apexsoft.gradnet2.user_api.comcode.controller;

import kr.co.apexsoft.gradnet2.entity._common.EnumMapper;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayEnums;
import kr.co.apexsoft.gradnet2.user_api.comcode.dto.CommCodeDto;
import kr.co.apexsoft.gradnet2.user_api.comcode.dto.CommCodeGrpType;
import kr.co.apexsoft.gradnet2.entity._common.EnumValue;
import kr.co.apexsoft.gradnet2.user_api.comcode.dto.SchoolCommRequest;
import kr.co.apexsoft.gradnet2.user_api.comcode.service.CommCodeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-03-14
 */
@RestController
@RequestMapping("/commcode")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CommCodeController {

    @NonNull
    private CommCodeService commonCodeService;

    @NonNull
    private EnumMapper enumMapper;

    /**
     * 코드그룹 전체 조회
     * codeType  : SYSTEM , SCHOOL
     */
    @GetMapping("/groups/{codeType}/{codeGroups}")
    public ResponseEntity<Map<String, List<? extends CommCodeDto>>> findByCodeGroups(@PathVariable("codeGroups") String codeGroups,
                                                                                     @PathVariable("codeType") CommCodeGrpType codeType,
                                                                                    SchoolCommRequest SchoolCommRequest) {
        return ResponseEntity.ok(commonCodeService.findByCodeGroups(codeGroups.split(","),codeType,SchoolCommRequest));
    }

    /**
     * enum 리스트 조회
     * @param names
     * @return
     */
    @GetMapping("/enums/{names}")
    public ResponseEntity<Map<String, List<EnumValue>>> findByEnums(@PathVariable String names) {
        return ResponseEntity.ok(enumMapper.get(names));
    }
}
