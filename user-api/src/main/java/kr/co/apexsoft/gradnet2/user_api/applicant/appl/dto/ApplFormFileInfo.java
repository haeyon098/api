package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import lombok.Getter;

/**
 * 지원서파일 정보
 * 학교,전형 공통으로 사용
 *
 */
@Getter
public class ApplFormFileInfo extends  AppBirtFileInfo{

    @Override
    public String getBirtFileName() {
        return FilePathUtil.getApplBirtFileName(this.getPartSchoolCode(),this.getPartAdmissionCode());
    }

    @Override
    public String getFileName() {
        return FilePathUtil.getApplFormFileName(this.getPartEnterYear(), this.getPartMajorCode(), this.getApplicantUserId());
    }

}
