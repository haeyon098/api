package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-25
 */

public class RecruitPartTimeResponse {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startDate;

    private String dday;
    private boolean isWriteable;

    public boolean getIsWriteable() {
        return LocalDateTime.now().isAfter(this.startDate) && LocalDateTime.now().isBefore(this.endDate);
    }

    public String getDday() {

        StringBuffer label = new StringBuffer("D ");
        //시간이 아닌 날짜로 계산함
        long day = ChronoUnit.DAYS.between(LocalDate.now(), this.endDate.toLocalDate());
        if (day == 0) {
            label.append("DAY");
        } else if (day > 0) {
            label.append("-").append(day);
        } else {
            label.append("+").append(Math.abs(day));

        }
        return label.toString();
    }


}
