package kr.co.apexsoft.gradnet2.user_api._common.util;


import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;

public class PayNameUtil {

    private PayNameUtil() {
    }


    public static String getmerchantUidName(String schoolCode, String enterYear, Integer recruitPartSeq, Long applNo) {
        return new StringBuilder()
                .append(RecruitSchool.SchoolCodeType.getUpperCaseName(schoolCode)).append("_")
                .append(enterYear).append("_")
                .append(recruitPartSeq).append("_")
                .append(String.valueOf(applNo)).append("_")
                .append((int)(Math.random()*99999))
                .toString();

    }

    public static String getProudctName(String schoolCode, String enterYear, String majorKorName,
                                        String courseKorName, Integer RecruitPartSeq) {
        return new StringBuilder()
                .append(enterYear).append(" ")
                .append(RecruitSchool.SchoolCodeType.getUpperCaseName(schoolCode)).append(" ")
                .append(RecruitPartSeq).append(" ")
                .append(courseKorName).append(" ")
                .append(majorKorName).append(" ")
                .toString();
    }




}
