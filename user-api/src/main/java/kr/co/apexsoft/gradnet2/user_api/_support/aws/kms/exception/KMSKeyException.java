package kr.co.apexsoft.gradnet2.user_api._support.aws.kms.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 *  KMS 암호화
 */
public class KMSKeyException extends CustomException {

    public KMSKeyException() {
        super();
    }

    public KMSKeyException(String warningMessage) {
        super(warningMessage);
    }

    public KMSKeyException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
