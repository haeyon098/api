package kr.co.apexsoft.gradnet2.user_api._support.zip;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.zip.domain.ZipInfo;
import kr.co.apexsoft.gradnet2.user_api._support.zip.exception.ZIPException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.compress.archivers.zip.ZipFile;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.Deflater;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ZIPService {

    @Value("${file.baseDir}")
    private String fileBaseDir;

    private static final Logger logger = LoggerFactory.getLogger(ZIPService.class);

    /**
     * pdf 파일들 zip으로 만들어서 , zip파일 리턴
     *
     * @param pdfFiles
     * @param zipInfo
     * @return
     */
    public File getZippedFileFromPdf(List<File> pdfFiles, ZipInfo zipInfo) {
        String targetDir = zipInfo.getTargeDir();
        String zipFileName = zipInfo.getZipFileName();
        File zipFile = null;

        try {
            zipFile = makeZipFileFromPDF(targetDir, zipFileName, pdfFiles);
        } catch (Exception e) {
            throw new ZIPException(MessageUtil.getMessage("ZIP_FAIL"), zipInfo.getApplNo());
        } finally {
            for (File aFile : pdfFiles) { //zip으로 만들어진 파일 삭제 필수
                if (aFile.exists())
                    aFile.delete();
            }
        }
        return zipFile;
    }

    /**
     * 기존 zip 파일의 내용과 fileList에 있는 파일을 합친 새 zip 파일 생성
     *
     * @param fileList
     * @param zipFile
     * @return
     * @throws IOException
     * @throws InterruptedException
     */
    public File appendFilesToZipFile(List<File> fileList, File zipFile) throws ZIPException{ //TODO: 추후 사용에따라서, 익셉션처리 옮길수도 있음(applNo 로그..)

        String zipFilePath = zipFile.getAbsolutePath();
        String zipFileDir = zipFilePath.substring(0, zipFilePath.lastIndexOf(File.separator));
        File tempZipFile = new File(zipFileDir, "temp.zip");
        try {
            FileUtils.copyFile(zipFile, tempZipFile);
            zipFile.delete();

            File newZipFile = new File(zipFilePath);

            ZipArchiveOutputStream zos = addFilesToZipArchive(fileList, newZipFile);

            // 기존 zip 파일에 있던 파일을 새 zip 파일에 추가
            ZipFile zFile = new ZipFile(tempZipFile);
            Enumeration<ZipArchiveEntry> enumZe = zFile.getEntries();
            int length;
            byte[] buf = new byte[8 * 1024];
            InputStream is = null;
            while (enumZe.hasMoreElements()) {
                ZipArchiveEntry ze = enumZe.nextElement();
                try {
                    is = zFile.getInputStream(ze);
                    zos.putArchiveEntry(ze);
                    while ((length = is.read(buf, 0, buf.length)) >= 0) {
                        zos.write(buf, 0, length);
                    }
                    zos.closeArchiveEntry();
                } finally {
                    if (is != null)
                        try {
                            is.close();
                        } catch (IOException e) {
                        }
                }
            }

            closeZipFile(zFile);
            closeZipArchiveOutputStream(zos);


        } catch (Exception e) {
            throw new ZIPException( MessageUtil.getMessage("ZIP_FAIL"));
        }finally {
            if (tempZipFile.exists()) tempZipFile.delete();
        }

        return zipFile;
    }

    /**
     * PDF파일들을 ZIP으로 만들기
     *
     * @param targetDir
     * @param zipFileName
     * @param pdfFiles
     * @return
     */

    public File makeZipFileFromPDF(String targetDir, String zipFileName, List<File> pdfFiles) throws IOException, InterruptedException {

        File zippedFile = new File(targetDir, zipFileName);
        ZipArchiveOutputStream zos = addFilesToZipArchive(pdfFiles, zippedFile);

        closeZipArchiveOutputStream(zos);
        return zippedFile;
    }


    private ZipArchiveOutputStream addFilesToZipArchive(List<File> fileList, File zipFile) throws IOException {
        OutputStream os = null;
        try {
            os = new FileOutputStream(zipFile);
        } catch (FileNotFoundException e) {

        }

        ZipArchiveOutputStream zos = new ZipArchiveOutputStream(os);
        zos.setEncoding("UTF-8");
        zos.setUseLanguageEncodingFlag(true);
        zos.setFallbackToUTF8(true);
        zos.setLevel(Deflater.NO_COMPRESSION);

        ZipArchiveEntry ze;
        int length;
        byte[] buf = new byte[8 * 1024];
        FileInputStream fis = null;

        for (File file : fileList) {
            try {
                String fileName = file.getName();
                ze = new ZipArchiveEntry(fileName);
                zos.putArchiveEntry(ze);
                fis = new FileInputStream(file);
                while ((length = fis.read(buf, 0, buf.length)) >= 0) {
                    zos.write(buf, 0, length);
                }
                fis.close();
                zos.closeArchiveEntry();
            } finally {
                if (fis != null)
                    try {
                        fis.close();
                    } catch (IOException e) {
                    }
            }
        }

        return zos;
    }

    private void closeZipFile(ZipFile zipFile) throws IOException {
        try {
            zipFile.close();
        } finally {
            if (zipFile != null)
                try {
                    zipFile.close();
                } catch (IOException e) {
                }
        }
    }

    private void closeZipArchiveOutputStream(ZipArchiveOutputStream zos) throws IOException {
        try {
            zos.close();
        } finally {
            if (zos != null)
                try {
                    zos.close();
                } catch (IOException e) {
                }
        }
    }


}
