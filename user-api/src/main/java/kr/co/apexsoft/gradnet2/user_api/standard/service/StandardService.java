package kr.co.apexsoft.gradnet2.user_api.standard.service;

import kr.co.apexsoft.gradnet2.entity.standard.projection.SearchDto;
import kr.co.apexsoft.gradnet2.entity.standard.repository.CountryRepository;
import kr.co.apexsoft.gradnet2.entity.standard.repository.SchoolRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class StandardService {
    @NonNull
    private CountryRepository countryRepository;

    @NonNull
    private SchoolRepository schoolRepository;

    @Transactional(readOnly = true)
    public List<SearchDto> findAllSchool() {
        return schoolRepository.findAllCustomList();
    }


    @Transactional(readOnly = true)
    public List<SearchDto> findAllCountry() {
        return countryRepository.findAllCustomList();
    }
}
