package kr.co.apexsoft.gradnet2.user_api._support.pdf.exception;


import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * pdf 메세지 용 공통
 */
public class PDFCommException extends CustomException {
    public PDFCommException(String warningMessage, Long applNo,String fileName) {

        super("[applNo:" + applNo + "] [fileName:" + fileName + "]",warningMessage);
    }
}