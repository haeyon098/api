package kr.co.apexsoft.gradnet2.user_api.applicant.career_lang;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.user_api.applicant.career.dto.CareerResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.ApplPartLanguageResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-13
 */
@Getter
@AllArgsConstructor
public class CareerLangResponse {

    private Appl.Status status;

    private List<CareerResponse> careerList;

    private List<ApplPartLanguageResponse> languageList;

}
