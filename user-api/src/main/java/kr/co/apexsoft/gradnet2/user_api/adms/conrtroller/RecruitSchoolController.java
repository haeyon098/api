package kr.co.apexsoft.gradnet2.user_api.adms.conrtroller;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.user_api.adms.service.AdmsService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-06
 */
@RestController
@RequestMapping("/recruit-school")
@RequiredArgsConstructor
public class RecruitSchoolController {
    @NonNull
    private AdmsService admsService;

    /**
     * 모집학교 전체 조회
     *
     * @return
     */
    @GetMapping
    public ResponseEntity<List<RecruitSchool>> findAllRecruitSchool() {
        return ResponseEntity.ok(admsService.findAllRecruitSchool());
    }

    /**
     * 학교 한건 조회
     *
     * @param id
     * @return
     */
    @GetMapping("/{schoolCode}")
    public ResponseEntity<RecruitSchool> findOne(@PathVariable("schoolCode") String id) {
        return ResponseEntity.ok(admsService.findSchoolOne(id));
    }
}
