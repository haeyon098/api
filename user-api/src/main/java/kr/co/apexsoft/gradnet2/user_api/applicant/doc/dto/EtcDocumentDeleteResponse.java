package kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.EtcDocumentProjection;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class EtcDocumentDeleteResponse {
    String status;
    List<EtcDocumentProjection> list;
}
