package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.service;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendInfoResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository.RecommedRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain.RecommendInformation;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.projection.RecommendProfProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.repository.RecommendProfessorRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.user_api._common.ApexAssert;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.birt.service.BirtService;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRepository;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.user_api._support.mail.exception.MailValidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplFormFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.service.DocumentService;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception.RecCancelException;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception.RecPeriodException;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto.RecProfFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto.RecommendProfResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.exception.RecProfInfoNotlException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RecommendProfessorService {

    @NonNull
    private RecommendProfessorRepository recommendProfessorRepository;

    @NonNull
    private RecommedRepository recommedRepository;

    @NonNull
    private ApplRepository applRepository;

    private final ApexModelMapper modelMapper;

    @NonNull
    public ApplService applService;

    @NonNull
    public BirtService birtService;

    @NonNull
    private FileRepository fileRepository;

    @Value("${file.baseDir}")
    private String fileBaseDir;

    @Value("${front.url}")
    private String frontUrl;

    @NonNull
    private MailService mailService;

    @Autowired
    private ResourceLoader resourceLoader;


    @NonNull
    private DocumentService documentService;



    /**
     * 추천서 임시저장
     *
     *
     * @param recommendInformation
     * @param applNo
     * @param key
     * @return
     */
    public RecommendProfResponse tempSaveRecommendProf(RecommendInformation recommendInformation, Long applNo, String key) {
        Recommend recommend = recommedRepository.findByKey(key).get();
        //추천서 마감시간 종료여부 체크
        if(!applService.isPossiblePeriod(applNo, RecruitPartTime.TimeType.RECOMMEND)) {
            throw new RecPeriodException(MessageUtil.getMessage("REC_PERIOD_FAIL"), recommend.getId());
        }
        RecommendInformation dbInformation;
        if(recommendProfessorRepository.findById(recommend.getId()).isPresent()) {
            dbInformation =  recommendProfessorRepository.findById(recommend.getId())
                    .orElseThrow(() -> new RecProfInfoNotlException(MessageUtil.getMessage("REC_PROF_INFO_NOT_FOUND")));
            recommendInformation.initId(dbInformation.getId());
        }

        recommendInformation.initRecommend(recommend);
        recommendInformation = recommendProfessorRepository.save(recommendInformation);

        return modelMapper.convert(recommendInformation, RecommendProfResponse.class);

    }


    /**
     * 추천서 저장
     * 버트 생성
     * 메일 전송
     *
     *
     * @param recommendInformation
     * @param applNo
     * @param key
     * @return
     */
    public void saveRecommendProf(RecommendInformation recommendInformation, Long applNo, String key) {
        RecommendProfResponse recommendProfResponse = tempSaveRecommendProf(recommendInformation, applNo, key);
        RecommendInfoResponse recommendInfoResponse = recommedRepository.findRecInfo(recommendProfResponse.getId());
        RecommendInformation dbInfo = recommendProfessorRepository.findById(recommendProfResponse.getId())
                .orElseThrow(() -> new RecProfInfoNotlException("REC_PROF_INFO_NOT_FOUND"));
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));
        RecommendProfProjection profProjection = recommendProfessorRepository.findRecProfInfo(recommendProfResponse.getId());

        //파일생성 (PDF, ZIP)
        saveRecFile(recommendInfoResponse, appl.getPart().getSchoolCode(), dbInfo, profProjection);
        //완료 메일 전송
        sendCompleteMail(dbInfo,recommendInfoResponse.getRecrShclEngName());
    }


    /**
     * 버트 생성 Zip 생성
     * Doc 에 저장
     *
     *
     * @param recommendInfoResponse
     * @param recommendInformation
     * @return
     */
    public void saveRecFile( RecommendInfoResponse recommendInfoResponse, String schoolCode,
                             RecommendInformation recommendInformation, RecommendProfProjection recommendProfProjection ) {
        // 버트 파일 생성
        Map<String, Object> map  = birtService.generateRecProfFile(recommendInfoResponse,
                getRecBirtFileName( recommendInformation.getRecommend().getAppl()),  recommendInformation, recommendProfProjection);

        String filePath = (String)map.get("filePath");
        //PDF 객체 생성&업로드
        FileRequest fileRequest = new FileRequest((String) map.get("pdfDirectoryFullPath"), (String) map.get("pdfFileName"));
        fileRequest.setCustomFileDirPath(filePath);

        this.documentService.uploadAndSaveGradnetCustomFile
                (fileRequest, recommendInformation.getRecommend().getAppl(), DocGrpType.ADMISSION.getValue(),
                        recommendInformation.getRecommend().getId() ,DocItem.EMAIL_REC.getValue(),true);

    }

    /**
     * 추천서 버트 파일 선택
     *
     * @param appl
     * @return
     */
    private String getRecBirtFileName (Appl appl) {
        RecProfFileInfo fileInfo = modelMapper.convert(appl, RecProfFileInfo.class);
        return fileInfo.getBirtFileName();
    }

    /**
     * 추천서 내용
     * 조회
     *
     * @param key
     * @return
     */
    @Transactional(readOnly = true)
    public RecommendProfResponse findRecommendProf(String key) {
        Recommend recommend = recommedRepository.findByKey(key).get();


        RecommendInformation dbInformation;
        if(recommendProfessorRepository.findById(recommend.getId()).isPresent()) {
            dbInformation =  recommendProfessorRepository.findById(recommend.getId())
                    .orElseThrow(() -> new RecProfInfoNotlException("REC_PROF_INFO_NOT_FOUND"));
        } else {
            return null;
        }

        return modelMapper.convert(dbInformation, RecommendProfResponse.class);
    }

    /**
     * 추천서 완료 메일전송
     *
     * @param recommendInformation
     */
    public void sendCompleteMail (RecommendInformation recommendInformation,String schlName) {
        //메일전송 정보 저장
        List<String> mailList = new ArrayList<String>();
        Map<String, String> replacements = new HashMap<>();
        replacements.put("url", frontUrl);
        replacements.put("recName",recommendInformation.getRecommend().getAppl().getApplicantInfo().getEngName());
        replacements.put("profName",recommendInformation.getName());
        mailList.add(recommendInformation.getRecommend().getAppl().getApplicant().getEmail());

        // 이메일 유효한지 확인
        for (String mailAddr : mailList) {
            ApexAssert.notMailPattern(mailAddr, MessageUtil.getMessage("MAIL_ADDRESS_CHECK_FAIL"), MailValidException.class);
        }
        //메일전송
        MailDto dto = MailDto.builder()
                .to(mailList)
                .subject("Notice of "+ schlName +" recommendation letter status")
                .templateId("recommend-complete.html")
                .replacements(replacements)
                .resourceLoader(resourceLoader)
                .build();

        mailService.send(dto);
    }
}
