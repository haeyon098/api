package kr.co.apexsoft.gradnet2.user_api.applicant.lang.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * 어학저장시 벨리데이션체크
 *
 * @author 김효숙
 * @since 2020-07-13
 */
public class LanguageValidationFailException extends CustomException {


    public LanguageValidationFailException(String warningMessage) {
        super(warningMessage);
    }


}
