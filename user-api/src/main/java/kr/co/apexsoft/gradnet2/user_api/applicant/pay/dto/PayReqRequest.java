package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import lombok.Getter;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-09-23
 */
@Getter
public class PayReqRequest {

    private Long applNo;
    private Double amount;
}
