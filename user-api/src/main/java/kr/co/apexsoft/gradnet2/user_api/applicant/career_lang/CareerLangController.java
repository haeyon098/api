package kr.co.apexsoft.gradnet2.user_api.applicant.career_lang;

import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResquest;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@RestController
@RequestMapping("/appl/career-lang")
@RequiredArgsConstructor
public class CareerLangController {
    @NonNull
    private CareerLangService careerLangService;

    private final ApexCollectionValidator apexCollectionValidator;

    private final ApexModelMapper modelMapper;

    /**
     * 어학, 경력 저장
     *
     * @param id
     * @param careerLangRequest
     * @param userPrincipal
     * @param errors
     * @return
     */
    @PostMapping("/{applNo}")
    public ResponseEntity<CareerLangResponse> save(@PathVariable("applNo") Long id,
                                                   @Valid @RequestBody CareerLangRequest careerLangRequest,
                                                   @AuthenticationPrincipal UserPrincipal userPrincipal,
                                                   Errors errors) {
        //form 값
        apexCollectionValidator.validateCareerLang(careerLangRequest, errors);

        if (errors.hasErrors()) {
            throw new InvalidException(MessageUtil.getMessage("APPL_LANG_CAREER_INVALID_INFO"));
        }

        //어학 OptionType체크
        careerLangService.validationLanguage(id,careerLangRequest.getLanguageList());


        List<Career> careers= modelMapper.convert(careerLangRequest.getCareerList(), new TypeToken<List<Career>>(){}.getType());
        List<Language> languages= modelMapper.convert(careerLangRequest.getLanguageList(), new TypeToken<List<Language>>(){}.getType());

        return ResponseEntity.ok(careerLangService.save(id, languages, careers, userPrincipal.getRoles()));
    }


    @PostMapping("/validation/{applNo}")
    public  void findLanguageList(@PathVariable("applNo") Long applNo, @RequestBody List<LanguageResquest> languageList) {
         careerLangService.validationLanguage(applNo,languageList);
    }

}
