package kr.co.apexsoft.gradnet2.user_api.applicant.pay.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * 가상계좌 발급취소 실패
 */
public class VBankCancelException extends CustomException {

    public VBankCancelException() {
        super();
    }

    public VBankCancelException(String warningMessage) {
        super(warningMessage);
    }

    public VBankCancelException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
