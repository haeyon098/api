package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
*
*
* @author aran
* @since 2020-03-05
*/
@Getter
@Setter
public class RecruitPartForBannerResponse {
    private String schoolCode;
    private String schoolName;
    private String enterYear;

    private String korName;
    private String engName;

    @JsonFormat(pattern = "yyyy-MM-dd ")
    private LocalDateTime startDate;
    @JsonFormat(pattern = "yyyy-MM-dd ")
    private LocalDateTime endDate;
}
