package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendListResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendProfResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.service.RecommendService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
@RestController
@RequestMapping("/appl/rec")
@RequiredArgsConstructor
public class RecomemmdController {

    @NonNull
    private RecommendService recommendService;

    private final ApexModelMapper modelMapper;

    /**
     * 추천서 저장 및
     * 이메일 전송
     *
     * @param RecommendRequest
     * @param errors
     * @param userPrincipal
     * @return
     */
    @PostMapping("/send/{applNo}")
    public ResponseEntity<RecommendResponse> sendRecommend(@Valid @RequestBody RecommendRequest RecommendRequest,
                                                           Errors errors,
                                                           @PathVariable("applNo") Long id,
                                                           @AuthenticationPrincipal UserPrincipal userPrincipal) {
        if (errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("RECOMMEND_INVALID_INFO"), errors);

        return ResponseEntity.ok(recommendService.sendRecommend(
                modelMapper.convert(RecommendRequest, Recommend.class), id, userPrincipal.getRoles()));
    }


    /**
     * 추천서 요청내역 조회
     *
     * @param applNo
     * @param userPrincipal
     * @return
     */
    @GetMapping("/list/{applNo}")
    public ResponseEntity<RecommendListResponse> findRecommends(@PathVariable("applNo") Long applNo, @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(recommendService.findRecommends(applNo));
    }

    /**
     * 추천서 요청 취소
     *
     * @param recNo
     * @param userPrincipal
     * @return
     */
    @DeleteMapping("/{applNo}/{recNo}")
    public ResponseEntity<List<RecommendResponse>> cancelRecommend(
            @PathVariable("applNo") Long id, @PathVariable("recNo") Long recNo, @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(recommendService.cancelRecommend(recNo, userPrincipal.getRoles()));
    }


}
