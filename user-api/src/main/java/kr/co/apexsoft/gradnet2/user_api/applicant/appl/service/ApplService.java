package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import com.sun.xml.bind.v2.model.core.ID;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.adms.repository.RecruitSchoolRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplPartResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.MyApplResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.TimeDto;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.CompleteDocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.entity.recpart.repository.RecruitPartRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.user_api.adms.exception.RecruitSchoolNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.adms.service.AdmsService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.DuplicateDto;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.MyApplAndInfoResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.MyListResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.Duplicate.DuplicateService;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.MyListRecruitPartResponse;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.MyListRecruitPartTimeResponse;
import kr.co.apexsoft.gradnet2.user_api.recrpart.service.RecruitPartService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.*;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-06
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplService {
    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private AdmsService admsService;


    @NonNull
    private MailService mailService;

    @NonNull
    private RecruitPartService recruitPartService;

    @Autowired
    private ResourceLoader resourceLoader;

    @NonNull
    private DocumentRepository documentRepository;

    @NonNull
    private RecruitPartRepository recruitPartRepository;

    @NonNull
    private RecruitSchoolRepository recruitSchoolRepository;

    private final ApexModelMapper modelMapper;

    @NonNull
    private DuplicateService duplicateService;

    public ApplPartResponse createAppl(Appl appl, User user, Set<Role> roles) {
        if (!isTimeIndependentRole(roles)) {
            recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeqAndTime_TypeAndTime_StartDateBeforeAndTime_EndDateAfter(
                    appl.getPart().getSchoolCode(), appl.getPart().getEnterYear(),
                    appl.getPart().getRecruitPartSeq(), RecruitPartTime.TimeType.SUBMIT, LocalDateTime.now(), LocalDateTime.now()).
                    orElseThrow(() -> new ApplPartSaveTimeException(MessageUtil.getMessage("APPL_SAVE_FAIL_DEADLINE")));

        }
        duplicateService.checkApplication(new DuplicateDto(appl, user, true));
        appl.changeApplicant(user);
        appl.changeStatus(Appl.Status.APPL_PART_SAVE);
        applRepository.save(appl);
        return findApplPart(appl.getId());
    }

    @Transactional(readOnly = true)
    public ApplPartResponse findApplPart(Long id) {
        return applRepository.findMyApplPart(id);
    }

    @Transactional(readOnly = true)
    public Appl findAppl(Long id) {
        return applRepository.findById(id).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));
    }

    @Transactional(readOnly = true)
    public MyListResponse findApplList(Long userNo) {
        List<MyApplAndInfoResponse> all = new ArrayList<>();
        List<MyApplAndInfoResponse> proceeding = new ArrayList<>();
        List<MyApplAndInfoResponse> submit = new ArrayList<>();
        List<MyApplAndInfoResponse> complete = new ArrayList<>();
        List<MyApplAndInfoResponse> cancel = new ArrayList<>();

        List<MyApplResponse> applListResponses = applRepository.fintApplList(userNo);

        for (MyApplResponse appl : applListResponses) {
            // doc, time 조회
            List<CompleteDocumentProjection> documents = documentRepository.findCompleteDocuments(appl.getId(),
                    DocItem.APPL_FORM.getValue(), DocItem.SLIP.getValue(), DocItem.ZIP.getValue());

            RecruitPart savedPart = recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeq(
                    appl.getSchoolCode(), appl.getEnterYear(), appl.getRecruitPartSeq()).orElseThrow(
                    () -> new RecruitSchoolNotFoundException(MessageUtil.getMessage("RECRUIT_PART_NOT_FOUND")));
            MyListRecruitPartResponse part = modelMapper.convert(savedPart, MyListRecruitPartResponse.class);
            part.setPeriods(modelMapper.convert(new ArrayList<>(savedPart.getTime()), new TypeToken<List<MyListRecruitPartTimeResponse>>() {
            }.getType()));

            MyApplAndInfoResponse applAndInfo = new MyApplAndInfoResponse(appl, documents, part);
            all.add(applAndInfo);

            // tab 리스트
            if (isProceeding(appl.getStatus())) {
                proceeding.add(applAndInfo);
            } else if(isFromSubmitToComplete(Appl.Status.valueOf(appl.getStatus()))) {
                submit.add(applAndInfo);
            } else if (appl.getStatus().equals(Appl.Status.COMPLETE.name())) {
                complete.add(applAndInfo);
            } else if (appl.getStatus().equals(Appl.Status.CANCEL.name())) {
                cancel.add(applAndInfo);
            }
        }

        return new MyListResponse(all, proceeding, submit, complete, cancel);
    }

    /**
     * 내원서 화면 기준 진행중 인지 확인
     *
     * @param status
     * @return
     */
    private boolean isProceeding(String status) {
        return status.equals(Appl.Status.APPL_PART_SAVE.name()) || status.equals(Appl.Status.BASIS_SAVE.name()) ||
                status.equals(Appl.Status.ACAD_SAVE.name()) || status.equals(Appl.Status.LANG_SAVE.name()) ||
                status.equals(Appl.Status.ESSAY_SAVE.name()) || status.equals(Appl.Status.FILE_SAVE.name());
    }

    /**
     * 내원서 화면 작성완료 ~ 지원완료 전
     * @param status
     * @return
     */
    private boolean isFromSubmitToComplete(Appl.Status status) {
        Appl.Status[] statusArr = Appl.Status.values();
        int nowIdx = Arrays.asList(statusArr).indexOf(status);
        int submitIdx = Arrays.asList(statusArr).indexOf(Appl.Status.SUBMIT);
        int completeIdx = Arrays.asList(statusArr).indexOf(Appl.Status.COMPLETE);
        return submitIdx <= nowIdx && nowIdx < completeIdx;
    }

    public MyListResponse cancelAppl(Long id, User user) {
        checkApplSaveTime(id, user.getRoles(), RecruitPartTime.TimeType.SUBMIT, "APPL_UPDATE_CANCEL_FAIL_STATUS");
        Appl appl = applRepository.findById(id).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));

        // 지원완료 || 지원취소 체크
        if (appl.isCompleteStatus() || appl.isCancelStatus()) {
            throw new ApplCancelStatusCompleteBizException(MessageUtil.getMessage("APPL_UPDATE_CANCEL_FAIL_STATUS"), appl.getId());
        }

        appl.changeStatus(Appl.Status.CANCEL);
        applRepository.save(appl);
        return this.findApplList(user.getId());
    }


    /**
     * 원서
     * 수정 / 삭제
     * 가능 여부 조회
     *
     * @param timeType
     * @param applNo
     * @return
     */
    @Transactional(readOnly = true)
    public void checkApplCancelUpdate(Long applNo, RecruitPartTime.TimeType timeType, Set<Role> roles) {
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));

        // 지원완료 체크
        if (appl.isCompleteStatus()) {
            throw new ApplUpdateStatusCompleteException(MessageUtil.getMessage("APPL_UPDATE_CANCEL_FAIL_STATUS"), appl.getId());
        }
        // 지원취소 체크
        if (appl.isCancelStatus()) {
            throw new ApplUpdateStatusCancelException(MessageUtil.getMessage("APPL_UPDATE_CANCEL_FAIL_STATUS"), appl.getId());
        }
        checkApplSaveTime(applNo, roles, timeType, "APPL_SAVE_FAIL_DEADLINE");
    }


    /**
     * 현재 가능한 기간인지 확인
     *
     * @param applNo
     * @param timeType
     * @return
     */
    @Transactional(readOnly = true)
    public boolean isPossiblePeriod(Long applNo, RecruitPartTime.TimeType timeType) {
        TimeDto time = applRepository.findRecruitPartPeriod(applNo, timeType);
        LocalDateTime now = LocalDateTime.now();
        return now.isAfter(time.getStratDay()) && now.isBefore(time.getEndDay());

    }

    private boolean isTimeIndependentRole(Set<Role> roles) {
        return roles.stream()
                .anyMatch(role -> (role.getType().equals(RoleType.ROLE_TIME_INDEPENDENT)) || (role.getType().equals(RoleType.ROLE_SYS_ADMIN)));
    }

    /**
     * 원서 작성 기간 체크
     * ROLE_TIME_INDEPENDENT & ROLE_SYS_ADMIN 권한 제외
     *
     * @param applNo
     * @param roles
     */
    @Transactional(readOnly = true)
    public void checkApplSaveTime(Long applNo, Set<Role> roles, RecruitPartTime.TimeType timeType, String mgsCode) {

        if (!isTimeIndependentRole(roles)) {
            if (!isPossiblePeriod(applNo, timeType)) {
                throw new ApplSaveTimeException(MessageUtil.getMessage(mgsCode), applNo);
            }

        }

    }

    /**
     * appl 저장 가능 상태 체크
     * 시간
     * 지원완료 ,지원 취소
     * 현재상태 이하 스탭
     *
     * @param applNo
     * @param tryStatus
     * @param roles
     */
    @Transactional(readOnly = true)
    public Appl checkApplSave(Long applNo, Appl.Status tryStatus, Set<Role> roles) {
        return checkApplSave(applNo, tryStatus, roles, null);
    }


    /**
     * appl 저장 가능 상태 체크
     * 시간
     * 지원완료 ,지원 취소
     * 현재상태 이하 스탭
     * 국내전형, 주민번호 중복
     *
     * @param applNo
     * @param tryStatus
     * @param roles
     * @return
     */
    @Transactional(readOnly = true)
    public Appl checkApplSave(Long applNo, Appl.Status tryStatus, Set<Role> roles, DuplicateDto duplicateDto) {
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));
        HashMap<String, Object> map = new HashMap<>();
        Appl.Status[] statusArr = RecruitSchool.SchoolCodeType.getSteps(appl.getPart().getSchoolCode());
        int dbIndex = Arrays.asList(statusArr).indexOf(appl.getStatus()); //db 저장된 상태

        // 원서작성 기간 체크
        checkApplSaveTime(appl.getId(), roles, RecruitPartTime.TimeType.SUBMIT, "APPL_SAVE_FAIL_DEADLINE");


        map.put("dbStatus", appl.getStatus());

        // 지원완료 체크
        if (appl.isCompleteStatus()) {
            throw new ApplSaveStatusCompleteBizException(MessageUtil.getMessage("APPL_SAVE_FAIL_STATUS_COMPLETE"), appl.getId(), map);
        }
        // 지원취소 체크
        if (appl.isCancelStatus()) {
            throw new ApplSaveStatusCancelBizException(MessageUtil.getMessage("APPL_SAVE_FAIL_STATUS_CANCEL"), appl.getId(), map);
        }

        //TODO이하 상태만 저장가능
        int tryIndex = Arrays.asList(statusArr).indexOf(tryStatus);
        if ((dbIndex + 1) < tryIndex) {
            throw new ApplSaveStatusBizException(MessageUtil.getMessage("APPL_BIZ_NOT_STATUS"), appl.getId(), map);
        }

        //중복지원체크
        DuplicateDto dto = new DuplicateDto(appl, roles, false);
        if (duplicateDto != null) {
            dto.setRegistration(duplicateDto);
        } else {
            dto.setRegistration(appl, true);
        }

        duplicateService.checkApplication(dto);
        return appl;
    }


    /**
     * 이메일로 수험번호 찾기
     *
     * @param email
     */
    public void findApplIdByEmail(String email) {
        List<Appl> appls = applRepository.findAllByApplicantInfo_EmailAndStatus(email, Appl.Status.COMPLETE);

        if (appls.isEmpty()) {
            throw new ApplIdNotFoundException(MessageUtil.getMessage("APPL_ID_NOT_FOUND"));
        }

        List<String> mailList = new ArrayList<String>();
        mailList.add(email);

        // 200228. 최신 10개의 목록만 보여준다.
        Map<String, String> replacements = new HashMap<>();
        RecruitPart recruitPart = null;
        RecruitSchool school = null;

        for (int i = 0; i < 10; i++) {
            String recruitPartName = "", schoolName = "", applId = "";

            if (appls.size() > i) {
                ApplPart applPart = appls.get(i).getPart();
                recruitPart = recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeq(applPart.getSchoolCode(),
                        applPart.getEnterYear(), applPart.getRecruitPartSeq()).get();

                school = recruitSchoolRepository.findById(applPart.getSchoolCode()).orElseThrow(
                        () -> new RecruitSchoolNotFoundException(MessageUtil.getMessage("RECRUIT_PART_NOT_FOUND"))
                );

                schoolName = school.getKorName() + "(" + school.getEngName() + ")";
                recruitPartName = recruitPart.getKorName() + "(" + recruitPart.getEngName() + ")";
                applId = appls.get(i).getApplId();
            }
            replacements.put("schoolName" + i, schoolName);
            replacements.put("recruitPartName" + i, recruitPartName);
            replacements.put("applId" + i, applId);
        }

        MailDto dto = MailDto.builder()
                .to(mailList)
                .subject("수험번호 안내 메일입니다. This is for checking the Application Number.")
                .templateId("find-applId.html")
                .replacements(replacements)
                .resourceLoader(resourceLoader)
                .build();
        mailService.send(dto);
    }


}
