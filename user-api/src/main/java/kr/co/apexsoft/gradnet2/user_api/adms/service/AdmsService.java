package kr.co.apexsoft.gradnet2.user_api.adms.service;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.adms.projection.AdmsResponse;
import kr.co.apexsoft.gradnet2.entity.adms.repository.*;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.adms.exception.RecruitSchoolNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-06
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AdmsService {
    @NonNull
    private RecruitSchoolRepository recruitSchoolRepository;

    @NonNull
    private SemesterTypeRepository semesterTypeRepository;

    @NonNull
    private AdmsRepository admsRepository;

    @NonNull
    private CorsRepository corsRepository;

    @NonNull
    private MajorRepository majorRepository;

    @NonNull
    private CategoryRepository categoryRepository;

    public List<RecruitSchool> findAllRecruitSchool() {
        return recruitSchoolRepository.findByUsed(true);
    }

    // 지원가능 쪽에 동적쿼리 사용할수 있으면 좋을 듯.

    public List<AdmsResponse> findSemesterType(String schoolCode, String enterYear, Integer recruitPartSeq) {
        return semesterTypeRepository.findSemesterType(schoolCode, enterYear, recruitPartSeq);
    }

    public List<AdmsResponse> findAdms(String schoolCode, String enterYear, Integer recruitPartSeq, String semesterTypeCode) {
        return admsRepository.findAdms(schoolCode, enterYear, recruitPartSeq, semesterTypeCode);
    }

    public List<AdmsResponse> findCors(String schoolCode, String enterYear, Integer recruitPartSeq, String semesterTypeCode,
                                       String admissionCode) {
        return corsRepository.findCors(schoolCode, enterYear, recruitPartSeq, semesterTypeCode, admissionCode);
    }

    public List<AdmsResponse> findMajor(String schoolCode, String enterYear, Integer recruitPartSeq, String semesterTypeCode,
                                       String admissionCode, String courseCode, String categoryCode) {
        return majorRepository.findMajor(schoolCode, enterYear, recruitPartSeq, semesterTypeCode, admissionCode, courseCode,
                categoryCode);
    }

    public List<AdmsResponse> findCategory(String schoolCode, String enterYear, Integer recruitPartSeq, String semesterTypeCode,
                                        String admissionCode, String courseCode) {
        return categoryRepository.findCategory(schoolCode, enterYear, recruitPartSeq, semesterTypeCode, admissionCode, courseCode);
    }

    public RecruitSchool findSchoolOne(String id) {
        return recruitSchoolRepository.findById(id).orElseThrow(
                () -> new RecruitSchoolNotFoundException(MessageUtil.getMessage("RECRUIT_PART_NOT_FOUND"))
        );
    }
}
