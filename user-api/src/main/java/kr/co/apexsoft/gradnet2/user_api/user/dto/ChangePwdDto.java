package kr.co.apexsoft.gradnet2.user_api.user.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-09-09
 */
@Getter
public class ChangePwdDto {
    @NotBlank
    private String userId;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;
}
