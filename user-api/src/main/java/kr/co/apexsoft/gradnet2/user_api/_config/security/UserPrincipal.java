package kr.co.apexsoft.gradnet2.user_api._config.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * created by hanmomhanda@gmail.com
 */
public class UserPrincipal implements UserDetails {

    private Long id;

    private String userId;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    private Set<Role> roles;

    private boolean accountNonLocked;

    private boolean enabled;

    private User user;

    public UserPrincipal() {
    }

    public UserPrincipal(Long id,
                         String userId,
                         String password,
                         Set<Role> roles,
                         boolean accountNonLocked,
                         boolean enabled,
                         User user) {
        this.id = id;
        this.userId = userId;
        this.password = password;
        this.authorities = roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getType().name()))
                .collect(Collectors.toList());
        this.roles = roles;
        this.accountNonLocked = accountNonLocked;
        this.enabled = enabled;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public Set<Role> getRoles() { return this.roles; }

    public User getUser() { return this.user; }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return userId;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserPrincipal that = (UserPrincipal) o;

        if (!id.equals(that.id)) return false;
        if (!userId.equals(that.userId)) return false;
        if (!password.equals(that.password)) return false;
        return authorities != null ? authorities.equals(that.authorities) : that.authorities == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + (authorities != null ? authorities.hashCode() : 0);
        return result;
    }
}
