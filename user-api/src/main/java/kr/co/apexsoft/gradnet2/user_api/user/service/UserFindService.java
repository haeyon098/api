package kr.co.apexsoft.gradnet2.user_api.user.service;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.HashUtil;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api.user.exception.UserNotFoundException;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-09-06
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Transactional
public class UserFindService {
    @NonNull
    private UserRepository userRepository;

    @Value("${front.url}")
    private String frontUrl;

    @NonNull
    private MailService mailService;

    @Autowired
    private ResourceLoader resourceLoader;

    @Transactional(readOnly = true)
    public String findUserId(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new UserNotFoundException(MessageUtil.getMessage("USER_NOT_FOUND"))).getUserId();
    }

    @Transactional(readOnly = true)
    public void findPassword(String userId, String email) {
        User user = userRepository.findByUserIdAndEmail(userId, email)
                .orElseThrow(() -> new UserNotFoundException(MessageUtil.getMessage("USER_NOT_FOUND")));

        String hash = HashUtil.generateHash(user.getUserId()+";"+user.getEmail());
        sendEmail(user.getUserId(), user.getEmail(), hash);
    }

    /**
     * 메일로 비밀번호 찾기 링크 전송
     *
     * @param email
     * @param hash
     */
    private void sendEmail(String userId, String email, String hash) {
        Map<String, String> replacements = new HashMap<>();
        String expireDate = LocalDateTime.now().plusMinutes(30).format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        replacements.put("url", frontUrl+ "/user/modify/pwd/"+ userId + "/" + expireDate+ "/" + hash);

        List<String> mailList = new ArrayList<String>();
        mailList.add(email);

        MailDto dto = MailDto.builder()
                .to(mailList)
                .subject("비밀번호 찾기 안내 메일입니다. This is Your password.")
                .templateId("find-password.html")
                .replacements(replacements)
                .resourceLoader(resourceLoader)
                .build();

        mailService.send(dto);
    }

    public Boolean checkPasswordModifyHash(String userId, String hash) {
        User user = userRepository.findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(MessageUtil.getMessage("USER_NOT_FOUND")));

        String newHash = HashUtil.generateHash(user.getUserId()+";"+user.getEmail());

        if(hash.equals(newHash))
            return true;
        else
            return false;
    }
}
