package kr.co.apexsoft.gradnet2.user_api.applicant.appl.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplicationSerachProjection;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplSearchCondition;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.MyListResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplSysadminService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 SYSTEM ADMIN
 */
@RestController
@RequestMapping("/sysadmin/appl")
@RequiredArgsConstructor
public class ApplSysAdminController {

    @NonNull
    private ApplSysadminService applSysadminService;




    /**
     * 전체 지원자 목록 /조회
     *
     * @return
     */
    @PostMapping("/all")
    public ResponseEntity<Page<ApplicationSerachProjection>> getApplAllList(@RequestBody ApplSearchCondition condition, @PageableDefault Pageable pageable) {
        return ResponseEntity.ok(applSysadminService.getApplAllList(condition,pageable));
    }


    /**
     * 지원자 화면 조회
     *
     * @param userId
     * @return
     */
    @GetMapping("/list")
    public ResponseEntity<MyListResponse> getApplListByUserId(@RequestParam("userId") String userId) {
        return ResponseEntity.ok(applSysadminService.getApplListByUserId(userId));
    }


    /**
     * 수험번호 생성
     * 지원서 재생성
     * 수험번호 재생성
     *
     * @return
     */
    @PostMapping("/remake/applId/{applNo}")
    public ResponseEntity<Page<ApplicationSerachProjection>> remakeApplId(@PathVariable("applNo") Long id,@RequestBody ApplSearchCondition condition, @PageableDefault Pageable pageable) {
        return ResponseEntity.ok(applSysadminService.remakeApplId(id,condition,pageable));
    }

    /**
     * 지원서 재생성
     * 수험번호 재생성
     *
     * @return
     */
    @PostMapping("/remake/application/{applNo}")
    public ResponseEntity<Page<ApplicationSerachProjection>> remakeApplication(@PathVariable("applNo") Long id,@RequestBody ApplSearchCondition condition, @PageableDefault Pageable pageable) {
        return ResponseEntity.ok(applSysadminService.remakeApplication(id,condition,pageable));
    }
}
