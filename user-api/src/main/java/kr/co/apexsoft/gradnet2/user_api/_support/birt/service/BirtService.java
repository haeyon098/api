package kr.co.apexsoft.gradnet2.user_api._support.birt.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplPartResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.essay.domain.Essay;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendInfoResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain.RecommendInformation;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.projection.RecommendProfProjection;
import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.birt.core.BirtEngineFactory;
import kr.co.apexsoft.gradnet2.user_api._support.birt.core.BirtPdfType;
import kr.co.apexsoft.gradnet2.user_api._support.birt.core.CustomAbstractSingleFormatBirtProcessor;
import kr.co.apexsoft.gradnet2.user_api._support.birt.core.CustomPdfSingleFormatBirtSaveToFile;
import kr.co.apexsoft.gradnet2.user_api._support.birt.exception.BirtGenerateException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.AppBirtFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.EssayFileInfo;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-10
 */
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BirtService {
    @NonNull
    private BirtEngineFactory birtEngineFactory;

    @NonNull
    private ResourceLoader resourceLoader;

    @Value("${file.baseDir}")
    private String fileBaseDir;


    /**
     * sample - 원하는 포맷의 pdf 생성
     */
    public void generateBirtFile() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("degree", "20191210- test");

        generateBirt(map, "KDITest", "/output/pdf", "pdftest1.pdf");
    }

    /**
     * 자기소개서
     *
     * @param appl
     * @param part
     * @param essay
     * @return
     */
    public Map<String, Object> generateEssayFile(Appl appl, ApplPartResponse part, Essay essay, EssayFileInfo essayFileInfo) {
        String name = appl.getApplicantInfo().getKorName() == null ? appl.getApplicantInfo().getEngName() : appl.getApplicantInfo().getKorName();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("corsName", part.getCourseEngName());
        map.put("catagoryName", part.getCategoryEngName());
        map.put("majorName", part.getMajorEngName());
        map.put("applicantName", name);
        map.put("studyData", essay.getData());


        return generateBirt(map, essayFileInfo.getBirtFileName(), essayFileInfo.getFileBaseDir(), essayFileInfo.getFileName());
    }

    public Map<String, Object> generateApplFormAndSlip( Map<String, Object> map,AppBirtFileInfo applFormFileInfo){

    return  generateBirt(map, applFormFileInfo.getBirtFileName(), applFormFileInfo.getTargeDir(), applFormFileInfo.getFileName());
}

    /**
     * Birt 생성 & 파일 로컬 저장
     *
     * @param map
     * @param birtRptFileName
     * @param pdfDirectoryFullPath
     * @param pdfFileName
     */
    private Map<String, Object> generateBirt(Map<String, Object> map, String birtRptFileName,
                                             String pdfDirectoryFullPath, String pdfFileName) {
        map.put("reportFormat", "PDF");
        map.put("reportName", birtRptFileName);
        map.put("pdfType", BirtPdfType.MULTIPLE.codeVal());
        map.put("pdfDirectoryFullPath", pdfDirectoryFullPath);
        map.put("pdfFileName", pdfFileName);

        IReportEngine reportEngine = birtEngineFactory.getObject();
        CustomAbstractSingleFormatBirtProcessor birtProcessor = new CustomPdfSingleFormatBirtSaveToFile();
        birtProcessor.setBirtEngine(reportEngine);

        String pathToRptdesignFile = "classpath:reports/" + birtRptFileName + ".rptdesign";
        Resource designFile = resourceLoader.getResource(pathToRptdesignFile);
        InputStream designFileInputStream = null;

        try {
            designFileInputStream = designFile.getInputStream();
            birtProcessor.setDesignFileInputStream(designFileInputStream);
            try {
                birtProcessor.createReport(map);
            }catch(Exception e){
                throw new BirtGenerateException(e.getMessage(),MessageUtil.getMessage("BIRT_FAIL"));
            }
            //setPasswordPdf(map); // 편집시 비밀번호 설정 FIXME: KDI봄학기 임시 기능임. 다음학기 해제 필요
        } catch (Exception e) {
            throw new BirtGenerateException(e.getMessage(),MessageUtil.getMessage("BIRT_FAIL"));
        } finally {
            if (designFileInputStream != null) {
                try {
                    designFileInputStream.close();
                } catch (IOException e) {
                    throw new BirtGenerateException(MessageUtil.getMessage("BIRT_FAIL"));
                }
            }
        }
        return map;
    }

    /**
     * 편집시 비밀번호 설정
     *
     * @param map
     * @throws Exception
     */
    private void setPasswordPdf(Map<String, Object> map) throws Exception {
        String fileDir = (String) map.get("pdfDirectoryFullPath");
        String fileName = (String) map.get("pdfFileName");
        File file = new File(fileDir, fileName);

        PDDocument originPdf = PDDocument.load(file);

        AccessPermission ap = new AccessPermission();
        ap.setCanModify(false);
        ap.setReadOnly();
        StandardProtectionPolicy spp = new StandardProtectionPolicy("apexSecret", "", ap);
        originPdf.protect(spp);

        originPdf.save(file.getAbsoluteFile());
        originPdf.close();
    }



    /**
     * 추천서
     *
     * @param recommendInfoResponse
     * @param information
     * @return
     */
    public Map<String, Object> generateRecProfFile(RecommendInfoResponse recommendInfoResponse, String recBirtFileName,
                                                   RecommendInformation information, RecommendProfProjection recommendProfProjection) {



        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> map = oMapper.convertValue(recommendProfProjection, Map.class);

        //답변
        for (int i = 0; i < information.getAnswer().size(); i ++) {
            map.put("col"+(i+1),information.getAnswer().get(i).getAnswer());
        }

        String pdfDirectoryFullPath = FilePathUtil.getMakeDirectoryFullPath(fileBaseDir, recommendInfoResponse.getRecrShclCode(), recommendInfoResponse.getEnterYear(),
                recommendInfoResponse.getRecruitPartSeq(), recommendInfoResponse.getId());


        String pofFileName = "rec"+"_"+information.getId()+"_"+recommendInfoResponse.getEnterYear() +"_"+ recommendInfoResponse.getMajorCode()
                +"_"+ information.getRecommend().getAppl().getApplicant().getId()+".pdf";

        String pdfFilePath =  recommendInfoResponse.getRecrShclCode() +"/"+ recommendInfoResponse.getEnterYear() +"/"+ recommendInfoResponse.getRecruitPartSeq() +"/"
                + recommendInfoResponse.getId();
        map.put("filePath", pdfFilePath);
        return generateBirt(map, recBirtFileName, pdfDirectoryFullPath, pofFileName);
    }
}
