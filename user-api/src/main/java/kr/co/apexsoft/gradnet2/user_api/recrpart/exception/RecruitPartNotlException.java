package kr.co.apexsoft.gradnet2.user_api.recrpart.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class RecruitPartNotlException extends CustomException {

    public RecruitPartNotlException() {
        super();
    }

    public RecruitPartNotlException(String warningMessage) {
        super(warningMessage);
    }

    public RecruitPartNotlException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public RecruitPartNotlException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
