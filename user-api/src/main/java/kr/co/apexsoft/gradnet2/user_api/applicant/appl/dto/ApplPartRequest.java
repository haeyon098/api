package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-06
 */
@Getter
public class ApplPartRequest {
    @NotNull
    private String schoolCode;

    @NotNull
    private String enterYear;

    @NotNull
    private Integer recruitPartSeq;

    @NotNull
    private Long applPartNo;
}
