package kr.co.apexsoft.gradnet2.user_api.applicant.pay.service;

import com.sun.istack.Nullable;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.Pay;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayEnums;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayReq;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.projection.PayInfoProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.repository.PayRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.repository.PayReqRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._common.util.PayNameUtil;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.IamportService;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IampotPayment;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplCompleteFailException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.CompleteService;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayInfoResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.exception.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PayService {
    @NonNull
    private PayRepository payRepository;

    @NonNull
    private CompleteService completeService;

    @NonNull
    private PayReqRepository payReqRepository;

    @NonNull
    private ApplRepository applRepository;

    private final ApexModelMapper modelMapper;

    @NonNull
    private IamportService iamportService;

    @NonNull
    private MailService mailService;

    @Autowired
    private ResourceLoader resourceLoader;

    @Value("${alert.email}")
    private String mailAddress;


    /**
     * 결제 요청 정보 조회
     *
     * @param applNo
     * @return
     */
    public PayInfoResponse findPayInfo(Long applNo) {
        PayInfoProjection payInfoProjection = this.payRepository.findPayInfo(applNo);
        PayInfoResponse payInfoResponse = modelMapper.convert(payInfoProjection, PayInfoResponse.class);
        payInfoResponse.initProductName(PayNameUtil.getProudctName(payInfoProjection.getSchoolCode(),
                payInfoProjection.getEnterYear(), payInfoProjection.getMajorKorName(), payInfoProjection.getCourseKorName(), payInfoProjection.getRecruitPartSeq()));
        return payInfoResponse;
    }

    /**
     * 결제 처리
     *
     * @param applNo
     * @param pay
     * @return
     */
    public Pay paid(Long applNo, ApplPart part, Pay pay) {
        checkAmount(pay.getMerchantUid(), pay.getAmount());
        Appl appl = checkApplSubmitStatus(applNo);

        pay.initSchoolInfo(part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq(), part.getRemk());
        pay.changeStatusComplete();

        //결제 완료 건 - 지원완료 처리
        completeService.complete(appl);

        return save(pay);
    }

    public Pay save(Pay pay) {
        return payRepository.save(pay);
    }

    /**
     * 가상계좌 발급 처리
     *
     * @return
     */
    public Pay issueVbank(Long applNo, ApplPart part, Pay pay) {
        checkAmount(pay.getMerchantUid(), pay.getAmount());
        Appl appl = checkApplSubmitStatus(applNo);

        pay.initSchoolInfo(part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq(), part.getRemk());
        pay.changeStatusIssue();

        appl.changeStatus(Appl.Status.VBANK_ISSUE);

        return save(pay);
    }

    /**
     * 결제요청 검증 및 결제 금액 검증
     *
     * @param amount
     */
    private void checkAmount(String merchantUid, Double amount) {
        if (!amount.equals(payReqRepository.findByMerchantUid(merchantUid).get().getAmount()))
            throw new NoMatchAmountException(MessageUtil.getMessage("NO_MATCH_AMOUNT"));
    }

    /**
     * 작성완료 상태 확인
     *
     * @param applNo
     */
    private Appl checkApplSubmitStatus(Long applNo) {
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));
        if (!appl.isSubmitStatus())
            throw new ApplCompleteFailException(MessageUtil.getMessage("APPL_SAVE_FAIL_STATUS_NOT_SUBMIT"), appl.getId());
        return appl;
    }

    // TODO: 웹훅 url은 일반결제에서도 문제가 생긴경우 paid 상태로 호출이 있으나, 현재는 가상계좌발급 상태에 대해서만 처리
    public void notification(String merchantUid) {
        Pay pay = payRepository.findByMerchantUid(merchantUid).orElseThrow(
                () -> new PayNotFoundException("해당 결제 정보가 존재하지 않습니다.")); // 로그만 찍힘 - 다국어 불필요

        if (!pay.isIssueVBank())
            throw new NotIssueVBankException("가상계좌 발급상태가 아닙니다.");

        Appl appl = pay.getAppl();
        pay.changeStatusComplete();

        //지원완료 처리
        completeService.complete(appl);
    }

    /**
     * 가맹점 번호 키 , 정보저장
     * 결제 요청 검증 (단계, 시간, 결제내역)
     */
    public PayReq savePayReq(PayReq payReq, Set<Role> roles) {
        completeService.checkApplCompleteSataus(payReq.getApplNo(), roles); // 상태 & 결제 시간 체크
        checkPaid(payReq.getApplNo());

        PayInfoProjection payInfoProjection = this.payRepository.findPayInfo(payReq.getApplNo());
        payReq.initMerchantUid(PayNameUtil.getmerchantUidName(
                payInfoProjection.getSchoolCode(), payInfoProjection.getEnterYear(), payInfoProjection.getRecruitPartSeq()
                , payInfoProjection.getId()));
        return payReqRepository.save(payReq);
    }

    /**
     * 결제 내역 검증 (결제완료 | 가상계좌발급)
     *
     * @param applNo
     */
    private void checkPaid(Long applNo) {

        Optional<Pay> pay = payRepository.findByAppl_IdAndStatusNot(applNo, Pay.Status.CANCEL);

        if (pay.isPresent()) {
            if (pay.get().isIssueVBank())
                throw new AlreadyIssueVBankException(MessageUtil.getMessage("ALREADY_VBANK"));
            else if (pay.get().isComplete())
                throw new AlreadyPaidException(MessageUtil.getMessage("ALREADY_PAID"));
        }
    }

    /**
     * 결제정보 테이블 조회
     *
     * @param applNo
     * @return
     */
    public Pay findPayByApplNo(Long applNo) {

        return payRepository.findByAppl_IdAndStatusNot(applNo, Pay.Status.CANCEL).orElseThrow(() -> new PayNotFoundException(MessageUtil.getMessage("PAY_INFO_NOT_FOUND")));

    }

    /**
     * 모바일 결제 후처리
     *
     * @param impuid
     * @return
     */
    public PayResponse paidByImpUid(Long applNo, String impuid, ApplPart part) {
        IampotPayment iampotPayment = iamportService.paymentByImpUid(impuid);
        iampotPayment.setApplNo(applNo);
        Pay pay = modelMapper.convertToCamelCase(iampotPayment, Pay.class);

        pay.initPGAndPayTypeValue(iampotPayment.getPg_provider(), iampotPayment.getPay_method());

        Pay savedPayInfo;

        if (iampotPayment.getPay_method().equals(PayEnums.PayType.VBANK.getInput()))
            savedPayInfo = issueVbank(applNo, part, pay);
        else
            savedPayInfo = paid(applNo, part, pay);
        return modelMapper.convert(savedPayInfo, PayResponse.class);
    }

    /**
     * @param merchantUid
     * @return
     */
    public Long findPayByMerchantUid(String merchantUid) {
        return payReqRepository.findByMerchantUid(merchantUid).orElseThrow(
                () -> new PayNotFoundException(MessageUtil.getMessage("PAY_INFO_NOT_FOUND"))).getApplNo();

    }

    /**
     * 수동 결제 처리
     *
     * @param applNo
     * @param pay
     * @return
     */
    public Pay manualPay(Long applNo, Pay pay) {
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));

        completeService.checkSubmitOver(appl.getPart().getSchoolCode(), appl.getStatus(), applNo);

        if (appl.getStatus().equals(Appl.Status.VBANK_ISSUE)) {
            this.cancelVBank(appl);
        }

        pay.initManual(appl.getPart().getRemk());
        pay.changeStatusComplete();

        //결제 완료 건 - 지원완료 처리
        completeService.completeNotSendEmail(appl);
        return save(pay);
    }


    /**
     * 가상계좌 취소
     */
    private void cancelVBank(Appl appl) {
        Pay pay = payRepository.findByAppl_IdAndApplStatusAndStatus(appl.getId(), Appl.Status.VBANK_ISSUE, Pay.Status.VBANK_ISSUE).orElseThrow(
                    () -> new PayNotFoundException(MessageUtil.getMessage("PAY_INFO_NOT_FOUND")));
        iamportService.cancelVbank(pay.getImpUid());
        pay.cancelPay();
        appl.changeStatus(Appl.Status.SUBMIT); // 작성완료 상태변경
    }


    /**
     * 결제수단 변경
     * 가상계좌 취소처리
     *
     * @param applNo
     */
    public PayInfoResponse changPay(Long applNo) {
        Appl appl = applRepository.findById(applNo).get();

        if (!appl.getStatus().equals(Appl.Status.VBANK_ISSUE)) {
           throw new VBankCancelException(MessageUtil.getMessage("VBANK_CANCLE_FAIL"));
        }
        this.cancelVBank(appl);

        return modelMapper.convert(appl, PayInfoResponse.class);
    }

    /**
     * 결제 실패시 메시지 메일 전송
     *
     * @param applNo
     * @param admsInfo
     * @param message
     */
    public void sendFailMail(Long applNo, String admsInfo, String payType, String exceptionName, String message, @Nullable String detail) {
        Map<String, String> replacements = new HashMap<>();
        String[] mails = mailAddress.split(",");

        if(mails.length > 0) {
            List<String> mailList = Arrays.asList(mails);
            String applNoString = "";

            if(applNo != null) applNoString = applNo.toString();

            replacements.put("applNo", applNoString);
            replacements.put("admsInfo", admsInfo);
            replacements.put("message", exceptionName+":"+message);
            replacements.put("payType", payType);
            replacements.put("detail", detail);

            //메일전송
            MailDto dto = MailDto.builder()
                    .to(mailList)
                    .subject("결제 후처리 실패 알림")
                    .templateId("pay-complete-fail.html")
                    .replacements(replacements)
                    .resourceLoader(resourceLoader)
                    .build();

            mailService.send(dto);
        }
    }
}
