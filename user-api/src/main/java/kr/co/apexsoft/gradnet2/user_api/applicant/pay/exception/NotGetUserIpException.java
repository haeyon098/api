package kr.co.apexsoft.gradnet2.user_api.applicant.pay.exception;


// FIXME: common 모듈 사용하면서 부터는 그쪽으로 옮겨야함

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

public class NotGetUserIpException extends CustomException {

    public NotGetUserIpException() {
        super();
    }

    public NotGetUserIpException(String warningMessage) {
        super(warningMessage);
    }

    public NotGetUserIpException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
