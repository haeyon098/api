package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;


public class ApplFormDataNotFoundException extends CustomException {




    public ApplFormDataNotFoundException(String warningMessage,Long applNo) {
        super("[applNo:"+applNo+"]",warningMessage);
    }

}
