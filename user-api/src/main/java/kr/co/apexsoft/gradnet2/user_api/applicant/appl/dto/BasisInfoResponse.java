package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-01-08
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BasisInfoResponse {
    private Appl.Status status;
    private ApplicantInfoResponse applicantInfo;
    private GeneralInfoDto generalInfo;
    private ForeignerInfoResponse foreignerInfo;
}
