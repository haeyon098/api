package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
public class AlumniDTO {

    private Long id;
    
    private String type;

    private String name;

    private LocalDate admYear;

    private String majorTypeCode;

    private String remark;
}
