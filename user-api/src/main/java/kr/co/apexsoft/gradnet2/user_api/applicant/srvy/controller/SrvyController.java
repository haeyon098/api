package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.srvy.domain.Alumni;
import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.CompleteService;
import kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.srvy.service.SrvyService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import lombok.NonNull;

import java.util.List;

/**
 * @author aran
 * @since 2020-02-26
 */
@RestController
@RequestMapping("/appl/srvy")
@RequiredArgsConstructor
public class SrvyController {

    // TODO: 설문지 공백 체크가 필요한 항목 확인하여 벨리데이션 처리(2020-03-02 현재 전부 미정이므로 디비컬럼과 맞춤)
    // TODO: 이미 답변한 설문지일 경우 처리방안 필요

    @NonNull
    private SrvyService srvyService;

    private final ApexModelMapper modelMapper;

    private final ApexCollectionValidator apexCollectionValidator;


    @NonNull
    private CompleteService completeService;

    /**
     * 설문지 생성
     *
     * @param applNo
     * @return
     */
    @GetMapping("/qst/{applNo}")
    public ResponseEntity<SrvyAnsResponse> createSurveyAnswer(@PathVariable("applNo") Long applNo) {
        return ResponseEntity.ok(srvyService.saveSurveyAnswer(applNo));
    }

    /**
     * 설문에 대한 회원의 답변 저장
     *
     * @param srvyQstAnsRequest
     * @param id
     * @param errors
     * @return
     */
    @PostMapping("/ans/{applNo}/{srvyNo}")
    public void createSurveyAnswer(@Valid @RequestBody SrvyQstAnsRequest srvyQstAnsRequest,
                                   @PathVariable(name = "srvyNo") Long id,
                                   @PathVariable("applNo") Long applNo, @AuthenticationPrincipal UserPrincipal userPrincipal,
                                   Errors errors) {

        apexCollectionValidator.validate(srvyQstAnsRequest.getAlumni(), errors);
        List<Alumni> alumniList = modelMapper.convert(srvyQstAnsRequest.getAlumni(), new TypeToken<List<Alumni>>() {
        }.getType());
        if (errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("SURVEY_ANSWER_INVALID"), errors);

        srvyService.saveSurveyAnswerAndComplete(srvyQstAnsRequest, id, alumniList,applNo,userPrincipal.getRoles());

    }

    /**
     * 설문답변 조회
     *
     * @param applNo
     * @return
     */
    @GetMapping("/check/postal/{applNo}")
    public void checkContactCode(@PathVariable(name = "applNo") Long applNo) {
        srvyService.checkContactCode(applNo);
    }

}
