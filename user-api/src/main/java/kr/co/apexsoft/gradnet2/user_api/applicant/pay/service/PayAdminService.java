package kr.co.apexsoft.gradnet2.user_api.applicant.pay.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.repository.PayRepository;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.IamportService;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IampotPayment;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.CompleteApplResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.IamportCheckResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayNotCompleteResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 결제용
 * 시스템관리자
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PayAdminService {
    @NonNull
    private PayRepository payRepository;

    @NonNull
    private ApplRepository applRepository;


    private final ApexModelMapper modelMapper;

    @NonNull
    private IamportService iamportService;


    /**
     * 아임포트 결제완료 기준으로, 지원완료 미처리 체크
     * 수동결제 처리 필요
     * or
     * 중복결제 환불 필요
     *
     * @return
     */

    public PayNotCompleteResponse getIncompleteList(LocalDateTime from, LocalDateTime to) {
        List<IampotPayment> iamportList = iamportService.getAllPaid(from, to);
        List<String> completeList = payRepository.findByCompleteImpuid(from, to);


        List<IamportCheckResponse> newList = iamportList.stream()
                .filter(iamport -> !completeList.contains(iamport.getImp_uid()))
                .map(iamport -> modelMapper.convertToCamelCase(iamport, IamportCheckResponse.class))
                .collect(Collectors.toList());

        return new PayNotCompleteResponse(iamportList.size(), completeList.size(), newList);
    }

    /**
     * 지원완료 기준, 아임포트 결제 체크
     * 미결제 확인
     *
     * @param from
     * @param to
     * @return
     */
    public PayNotCompleteResponse getUnPayList(LocalDateTime from, LocalDateTime to) {
        List<IampotPayment> iamportList = iamportService.getAllPaid(from, to);
        List<Long> applNoList = iamportList.stream()
                .map(iampotPayment -> Long.parseLong(iampotPayment.getMerchant_uid().split("_")[3]))
                .collect(Collectors.toList());

        //다른부분에서 사용 고려 상태&시간 파라미터로 받음
        List<Appl> applList = applRepository.findByStatusAndApplDateBetweenOrderByApplDateDesc(Appl.Status.COMPLETE, from.minusMinutes(1), to.plusMinutes(1));

        List<CompleteApplResponse> newList =
                applList.stream()
                        .filter(appl -> !applNoList.contains(appl.getId()))
                        .map(appl -> new CompleteApplResponse(appl.getId(),
                                appl.getPart().getSchoolCode(), appl.getPart().getEnterYear(), appl.getPart().getRecruitPartSeq(), appl.getApplDate()))
                        .collect(Collectors.toList());
        return new PayNotCompleteResponse(iamportList.size(), applList.size(), newList);

    }


}
