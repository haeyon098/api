package kr.co.apexsoft.gradnet2.user_api._support.iamport;

import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IamportVBank;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IampotCheckListPage;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IampotPayment;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class IamportService {
    @NonNull
    private IamportServiceImpl iamport;


    /**
     * 아임포트 고유 아이디로 결제 정보 조회
     *
     * @param impUid
     * @return
     */
    public IampotPayment paymentByImpUid(String impUid) {
        return iamport.callGetAuthApi("/payments/" + impUid, IampotPayment.class, "PAY_INFO_NOT_FOUND");
    }


    /**
     * 가상계좌 발급취소
     *
     * @param impUid
     * @return
     */
    public IampotPayment cancelVbank(String impUid) {

        return iamport.callDeleteAuthApi("/vbanks/" + impUid, IampotPayment.class, "VBANK_CANCLE_FAIL");
    }

    /**
     * 결제완료건 페이징 조회
     * 다른 상태 검색 필요시 URL분리필요 (paid:결제완료)
     */
    public IampotCheckListPage getAllPaidPage(LocalDateTime from, LocalDateTime to, int limit, int page) {
        Map<String, Object> params = new HashMap<>();

        params.put("from", getUnixTimestamp(from));
        params.put("to", getUnixTimestamp(to));
        params.put("page", page);
        params.put("limit", limit);

        return iamport.callGetAuthApi("/payments/status/paid", params, IampotCheckListPage.class, "PAY_INFO_NOT_FOUND");
    }


    /*
     * 결제완료건 전체조회
     * TODO: 성능 이슈 있을수 있음
     * */
    public List<IampotPayment> getAllPaid(LocalDateTime from, LocalDateTime to) {


        int page = 1;
        List<IampotPayment> iamportList = new ArrayList<>();

        while (page != 0) {
            IampotCheckListPage pageList = getAllPaidPage(from, to, 100, page); //아임포트 최대 100으로 제한함
            iamportList.addAll((pageList.getList()));
            page = pageList.getNext();
        }
        return iamportList;
    }


    /**
     * API사용 가상게좌 발급
     * TODO: 테스트용으로 사용 (세틀뱅크 PG사 추가필요)
     * 발급시 은행코드 유효한지 확인필요
     * @param merchant_uid
     * @return
     */
    public IampotPayment issue(String merchant_uid,LocalDateTime time) {
        Random random = new Random();
        IamportVBank data = new IamportVBank(merchant_uid + random.nextInt(1000000), (double) 1000, "004", getUnixTimestamp(time), "테스트");
        return iamport.callPostAuthApi("/vbanks", data, IampotPayment.class, "PAY_INFO_NOT_FOUND");

    }


    /**
      * @param time
     * @return
     */
    private long getUnixTimestamp(LocalDateTime time) {
        return time.atZone(ZoneId.of("Asia/Seoul")).toInstant().getEpochSecond();
    }

}
