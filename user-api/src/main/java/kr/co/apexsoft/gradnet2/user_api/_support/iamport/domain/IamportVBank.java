
package kr.co.apexsoft.gradnet2.user_api._support.iamport.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class IamportVBank {

    private String merchant_uid;

    private Double amount;

    private String vbank_code;

    private Long vbank_due;

    private String vbank_holder;


}

