package kr.co.apexsoft.gradnet2.user_api._common;

/**
 * 학교별 모듈 분리위한 사용
 */
public interface ApexSchoolSupportable {
    boolean supports(String schoolCode);
}
