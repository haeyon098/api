package kr.co.apexsoft.gradnet2.user_api.comcode.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SchoolCommRequest
{
    /*모집학교코드*/
    private String schoolCode;

    /*입학연도*/
    private String enterYear;

    /*회차구분분코드*/
    private Integer recruitPartSeq;
}
