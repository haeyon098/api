package kr.co.apexsoft.gradnet2.user_api._config.role.expeption;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

public class RoleNotFoundException extends CustomException {

    public RoleNotFoundException() {
        super();
    }

    public RoleNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public RoleNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

}
