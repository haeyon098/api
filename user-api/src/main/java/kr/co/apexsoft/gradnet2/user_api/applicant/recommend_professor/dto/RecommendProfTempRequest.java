package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto;

import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.List;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-03-16
 */
@Getter
public class RecommendProfTempRequest {

    private String key;

    private Long applNo;

    private String name;

    private String email;

    private String institute;

    private String address;

    private String position;

    private String phoneNum;

    private String sign;

    private LocalDate recDay;

    private List<AnswerDto> answerList;


}
