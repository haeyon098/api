package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 저장시 appl status 체크
 * @author 김효숙
 */
@Getter
public class RecCountException extends CustomException {


    public RecCountException(String warningMessage, Long applNo) {
        super("[applNo:" + applNo + "]", warningMessage);
    }


}

