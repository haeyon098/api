package kr.co.apexsoft.gradnet2.user_api._support.iamport;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.apexsoft.gradnet2.user_api._support.api.ApiCallService;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.AccessToken;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.AuthData;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IamportResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;

@Service
@Transactional
public class IamportServiceImpl {

    @Autowired
    private ApiCallService apiService;

    private String API_URL = "https://api.iamport.kr";

    @Value("${import.restApiKey}")
    private String restApiKey;

    @Value("${import.restApiSecret}")
    private String restApiSecret;


    private <T> T callApi(String path, HttpMethod httpMethod, HttpHeaders httpHeaders, Object body, Class<T> responseClazz, String message) {
        ObjectMapper mapper = new ObjectMapper();

        IamportResponse iamportResponse = (IamportResponse) apiService.callApiEndpoint(API_URL + path, httpMethod, httpHeaders, body, IamportResponse.class,
                IamportException.class, message);

        //FIXME: 아임포트 결과가 LIST로 오는경우 추가 처리 필요함  if(iamportResponse.getResponse().getClass() == ArrayList.class)

        return mapper.convertValue(iamportResponse.getResponse(), responseClazz);
    }


    /**
     * 인증토큰 헤더에 추가
     *
     * @return
     */
    private HttpHeaders getAuthHeaders() {
        AccessToken auth = getAuth();
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(auth.getAccess_token());
        return headers;
    }

    /**
     * 토큰+GET
     *
     * @param path
     * @param responseClazz
     * @param <T>
     * @return
     */
    public <T> T callGetAuthApi(String path, Class<T> responseClazz, String message) {
        return callApi(path, HttpMethod.GET, getAuthHeaders(), null, responseClazz, message);
    }

    /**
     * 토큰+GET
     *
     * @param path
     * @param params
     * @param responseClazz
     * @param <T>
     * @return
     */
    public <T> T callGetAuthApi(String path, Map<String, Object> params, Class<T> responseClazz, String message) {
        UriComponentsBuilder url = UriComponentsBuilder.fromPath(path);
        params.forEach(url::queryParam);
        return callApi(url.build().toUriString(), HttpMethod.GET, getAuthHeaders(), null, responseClazz, message);

    }

    /**
     * 토큰+post
     *
     * @param path
     * @param body
     * @param responseClazz
     * @param <T>
     * @return
     */
    public <T> T callPostAuthApi(String path, Object body, Class<T> responseClazz, String message) {
        return callApi(path, HttpMethod.POST, getAuthHeaders(), body, responseClazz, message);
    }


    /**
     * post
     *
     * @param path
     * @param body
     * @param responseClazz
     * @param <T>
     * @return
     */
    public <T> T postApi(String path, Object body, Class<T> responseClazz, String message) {
        return callApi(path, HttpMethod.POST, HttpHeaders.EMPTY, body, responseClazz, message);
    }


    public <T> T callDeleteAuthApi(String path, Class<T> responseClazz, String message) {
        return callApi(path, HttpMethod.DELETE, getAuthHeaders(), null, responseClazz, message);
    }

    /**
     * API 토큰 요청
     *
     * @return
     */
    public AccessToken getAuth() {
        AuthData data = new AuthData(restApiKey, restApiSecret);
        return postApi("/users/getToken", data, AccessToken.class,"AUTHEICATION_FAILURE");

    }


}
