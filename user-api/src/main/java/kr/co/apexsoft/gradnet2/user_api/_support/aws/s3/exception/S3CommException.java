package kr.co.apexsoft.gradnet2.user_api._support.aws.s3.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * s3 공용 익셉션
 */
public class S3CommException extends CustomException {


    public S3CommException(String keyName,String keyValue,  String warningMessage) {

        super("["+keyName+":"+keyValue+"]", warningMessage);


    }
}
