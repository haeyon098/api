package kr.co.apexsoft.gradnet2.user_api.applicant.pay.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.Pay;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayEnums;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayReq;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.IamportService;
import kr.co.apexsoft.gradnet2.user_api._support.iamport.domain.IampotPayment;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayInfoResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayReqRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto.PayResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.pay.service.PayService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-10
 */
@RestController
@RequestMapping("/appl/pay")
@RequiredArgsConstructor
public class PayController {
    @NonNull
    private PayService payService;

    @NonNull
    private IamportService iamportService;

    private final ApexModelMapper modelMapper;

    /**
     * 결제 요청 정보 조회
     *
     * @param applNo
     * @param userPrincipal
     * @return
     */
    @GetMapping("/info/{applNo}")
    public ResponseEntity<PayInfoResponse> findPayInfo(@PathVariable("applNo") Long applNo, @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(payService.findPayInfo(applNo));
    }

    /**
     * 결제 요청 테이블 조회
     *
     * @param applNo
     * @return
     */
    @GetMapping("/{applNo}")
    public ResponseEntity<PayResponse> findPayByApplNo(@PathVariable("applNo") Long applNo) {
        return ResponseEntity.ok(modelMapper.convert(payService.findPayByApplNo(applNo), PayResponse.class));
    }

    /**
     * 결제 처리
     * 가상계좌 발급 처리
     *
     * @param applNo
     * @param payRequest
     * @return
     */
    @PostMapping("/{payType}/{applNo}")
    public ResponseEntity<PayResponse> paid(@PathVariable String payType,
                                            @PathVariable Long applNo, @Valid @RequestBody PayRequest payRequest, Errors errors,
                                            @RequestAttribute(name = "applPart") ApplPart part) {
        try {
            if (errors.hasErrors())
                throw new InvalidException(MessageUtil.getMessage("PAY_INVALID_INFO"), errors);

            Pay pay = modelMapper.convert(payRequest, Pay.class);
            pay.initDate(payRequest.getPayDate(), payRequest.getVbankDueDate());
            pay.initPGAndPayTypeValue(payRequest.getPgType(), payRequest.getPayType());
            Pay savedPayInfo;

            if (payType.equals(PayEnums.PayType.VBANK.getInput()))
                savedPayInfo = payService.issueVbank(applNo, part, pay);
            else
                savedPayInfo = payService.paid(applNo, part, pay);

            return ResponseEntity.ok(modelMapper.convert(savedPayInfo, PayResponse.class));
        } catch (Exception e) {
            payService.sendFailMail(applNo, part.getRemk(), payType, e.getClass().getName(), e.getMessage(),
                    "결제요청정보 > "+ payRequest.toString());
            throw e;
        }
    }


    /**
     * 결제 요청 정보 저장
     *
     * @param payReqRequest
     */
    @PostMapping("/request")
    public ResponseEntity<PayReq> savePayReq(@RequestBody PayReqRequest payReqRequest,
                                             @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(payService.savePayReq(modelMapper.convert(payReqRequest, PayReq.class), userPrincipal.getRoles()));
    }

    /**
     * merchantUid 로 applNo 추출
     *
     * @param merchantUid
     * @return
     */
    @GetMapping("/merchantUid/{merchantUid}")
    public ResponseEntity<Long> findPayByMerchantUid(@PathVariable("merchantUid") String merchantUid) {
        return ResponseEntity.ok(payService.findPayByMerchantUid(merchantUid));
    }


    /**
     * 모바일 결제처리
     *
     * @param impUid
     * @param applNo
     * @param part
     * @return
     */
    @GetMapping("/mobile/{applNo}/{impUid}")
    public ResponseEntity<PayResponse> paidByImpUid(@PathVariable("impUid") String impUid,
                                                    @PathVariable("applNo") Long applNo,
                                                    @RequestAttribute(name = "applPart") ApplPart part) {
        try {
            return ResponseEntity.ok(payService.paidByImpUid(applNo, impUid, part));
        } catch (Exception e) {
            payService.sendFailMail(applNo, part.getRemk(), "모바일 결제", e.getClass().getName(), e.getMessage(), "impUid > "+impUid);
            throw e;
        }
    }

    /**
     * 가상계좌 발급취소 처리
     *
     * @param applNo
     * @return
     */
    @DeleteMapping("/vbank/{applNo}")
    public ResponseEntity<PayInfoResponse> cancelVbank(@PathVariable Long applNo) {
        return ResponseEntity.ok(payService.changPay(applNo));

    }

    @GetMapping("/test/vbank/issue")
    public ResponseEntity<IampotPayment> vbnak(@RequestParam(required = false)String uid,@RequestParam("duedate") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime duedate) {
        return ResponseEntity.ok(iamportService.issue(uid,duedate));

    }
}
