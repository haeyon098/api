package kr.co.apexsoft.gradnet2.user_api._support.aws.s3.exception;

/**
 * s3 다운로드
 */
public class S3DeleteException extends S3CommException {


    public S3DeleteException(String keyName,String keyValue, String warningMessage) {
        super(keyName,keyValue,warningMessage);
    }

}

