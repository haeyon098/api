package kr.co.apexsoft.gradnet2.user_api._support.aws.kms.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 *  KMS 복호화
 */
public class KMSDecryptException extends CustomException {

    public KMSDecryptException() {
        super();
    }

    public KMSDecryptException(String warningMessage) {
        super(warningMessage);
    }

    public KMSDecryptException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
