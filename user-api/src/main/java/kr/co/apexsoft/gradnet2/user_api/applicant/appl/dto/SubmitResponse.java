package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentResponse;
import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
public class SubmitResponse {

    private Long id;
    private Boolean documentChecked;
    private Appl.Status status;


    private DocumentResponse file;


}
