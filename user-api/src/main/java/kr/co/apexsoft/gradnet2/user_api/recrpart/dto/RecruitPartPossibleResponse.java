package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-12
 */
@Getter
@Setter
public class RecruitPartPossibleResponse {

    private String enterYear;

    private Integer recruitPartSeq;

    private String korName;

    private String engName;

    private String siteUrl;

    private String siteUrlEn;

    public RecruitPartTimeResponse possible;
}
