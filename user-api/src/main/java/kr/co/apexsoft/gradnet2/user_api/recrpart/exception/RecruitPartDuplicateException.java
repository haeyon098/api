package kr.co.apexsoft.gradnet2.user_api.recrpart.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class RecruitPartDuplicateException extends CustomException {

    public RecruitPartDuplicateException() {
        super();
    }

    public RecruitPartDuplicateException(String warningMessage) {
        super(warningMessage);
    }

    public RecruitPartDuplicateException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public RecruitPartDuplicateException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
