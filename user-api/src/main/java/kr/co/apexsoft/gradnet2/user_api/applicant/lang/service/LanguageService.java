package kr.co.apexsoft.gradnet2.user_api.applicant.lang.service;

import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.repository.LanguageRepository;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LanguageService {
    @NonNull
    private LanguageRepository languageRepository;

    private final ApexModelMapper modelMapper;

    @Transactional(readOnly = true)
    public List<LanguageResponse> findAllLang(Long id) {
        List<Language> languages = languageRepository.findByAppl_Id(id);
        List<LanguageResponse> responses = modelMapper.convert(languages, new TypeToken<List<LanguageResponse>>(){}.getType());
        return responses;
    }
}
