package kr.co.apexsoft.gradnet2.user_api._support.aws.mail.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * SES 메일
 */
public class SESMailException extends CustomException {

    public SESMailException() {
        super();
    }

    public SESMailException(String warningMessage) {
        super(warningMessage);
    }

    public SESMailException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
