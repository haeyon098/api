package kr.co.apexsoft.gradnet2.user_api._support.file;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.aws.s3.domain.FileWrapper;
import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import kr.co.apexsoft.gradnet2.user_api._support.file.exception.FileException;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FileService {

    private static final Logger logger = LoggerFactory.getLogger(FileService.class);

    @NonNull
    private FileRepository fileRepository;


    @Value("${file.baseDir}")
    private String fileBaseDir;



    public InputStream getInputStreamFromFileRepo(String filePath) {
        FileWrapper fileWrapper = fileRepository.getFileWrapperFromFileRepo(filePath);
        return fileWrapper.getInputStream();
    }

    public byte[] getBytesFromFileRepo(String filePath) throws IOException {
        InputStream inputStream = getInputStreamFromFileRepo(filePath);
        byte[] bytes = IOUtils.toByteArray(inputStream);
        return bytes;
    }

    //target 위치파일 다운로드 후 파일생성
    public File getFileFromFileRepo(String baseDir, String filePath, Long applNo) {
        File file = null;
        try {
            byte[] bytes = getBytesFromFileRepo(filePath);
            file = new File(baseDir, filePath);
            FileUtils.writeByteArrayToFile(file, bytes);
        } catch (Exception e) {
            throw new FileException(MessageUtil.getMessage("FILE_DOWN_FAIL"),applNo);
        }

        return file;
    }

    /**
     * file서버에서 files 다운로드
     * @param files
     * @param applNo
     * @return
     */
    public List<File> getFileListFromFileServer(List<DocumentResponse> files, Long applNo) {
        List<File> uploadedFiles = new ArrayList<>();
        for (DocumentResponse doc : files) {
            uploadedFiles.add(getFileFromFileRepo(fileBaseDir, doc.getFilePath(), applNo));
        }
        return uploadedFiles;
    }


}
