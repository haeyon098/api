package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class RecNotlException extends CustomException {

    public RecNotlException() {
        super();
    }

    public RecNotlException(String warningMessage) {
        super(warningMessage);
    }

    public RecNotlException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public RecNotlException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
