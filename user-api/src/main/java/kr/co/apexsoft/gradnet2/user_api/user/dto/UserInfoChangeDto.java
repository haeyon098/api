package kr.co.apexsoft.gradnet2.user_api.user.dto;

import lombok.Getter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
*
*
* @author aran
* @since 2020-03-11
*/
@Getter
public class UserInfoChangeDto {
    @NotBlank
    String userId;

    @Email
    String email;

    String password;
}
