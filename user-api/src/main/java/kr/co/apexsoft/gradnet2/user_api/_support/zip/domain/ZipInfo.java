package kr.co.apexsoft.gradnet2.user_api._support.zip.domain;

import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import lombok.Setter;

@Setter
public class ZipInfo {
    private Long applNo;
    private String schoolCode;
    private String enterYear;
    private Integer recruitPartSeq;
    private String majorCode;
    private String userId;
    private String fileBaseDir;

    public Long getApplNo() {
        return this.applNo;
    }

    public String getTargeDir() {
        return FilePathUtil.getMakeDirectoryFullPath(this.fileBaseDir,this.schoolCode, this.enterYear, this.recruitPartSeq,this.applNo );
    }

    public String getZipFileName() {
        return FilePathUtil.getZippedFileName(this.enterYear,this.majorCode,this.userId);
    }

}
