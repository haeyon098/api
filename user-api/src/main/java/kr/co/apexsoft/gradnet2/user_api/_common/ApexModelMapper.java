package kr.co.apexsoft.gradnet2.user_api._common;

import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.TypeMap;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.convention.NameTokenizers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-07-12
 */
public class ApexModelMapper {
    private final ModelMapper modelMapper;

    public ApexModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public <D> D convert(Object source, Class<D> classLiteral) {
        return modelMapper.map(source, classLiteral);
    }

    public <D> D convertToCamelCase(Object source, Class<D> classLiteral) {
        this.modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STANDARD)
                .setSourceNameTokenizer(NameTokenizers.UNDERSCORE)
                .setDestinationNameTokenizer(NameTokenizers.CAMEL_CASE);
        return modelMapper.map(source, classLiteral);
    }

    public <D, E> List<D> convert(Collection<E> sourceList, Type type) {
        return modelMapper.map(sourceList, type);
    }

    public <D, E> Page<D> convert(Page<E> sourceList, Type type, Pageable pageable) {
        List<D> list = this.convert(sourceList.getContent(), type);
        return new PageImpl<>(list, pageable, sourceList.getTotalElements());
    }

    public <S, D> TypeMap<S, D> addMappings(PropertyMap<S, D> propertyMap, Object source) {
        return this.modelMapper.addMappings(propertyMap);
    }
}
