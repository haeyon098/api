package kr.co.apexsoft.gradnet2.user_api._support.aws.kms.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 *  KMS 암호화
 */
public class KMSEncryptException extends CustomException {

    public KMSEncryptException() {
        super();
    }

    public KMSEncryptException(String warningMessage) {
        super(warningMessage);
    }

    public KMSEncryptException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
