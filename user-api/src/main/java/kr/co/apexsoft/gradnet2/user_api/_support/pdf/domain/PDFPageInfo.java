package kr.co.apexsoft.gradnet2.user_api._support.pdf.domain;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PDFPageInfo {
    private Long applNo;
    private String schoolCode;
    private String majorKorName;
    private String majorEngName;
    private String engName;
    private String korName;

    private String leftTop;
    private String leftBottom;

    public String getLeftTop() {
        if (RecruitSchool.SchoolCodeType.KDI.getValue().equals(this.schoolCode)) {
            return this.engName;
        } else {
            return this.korName + "(" + this.engName + ")";
        }
    }

    public String getLeftBottom() {
        if (RecruitSchool.SchoolCodeType.KDI.getValue().equals(this.schoolCode)) {
            return this.majorEngName;
        } else {
            return this.majorKorName;

        }
    }

    public Long getApplNo(){
        return this.applNo;
    }

}
