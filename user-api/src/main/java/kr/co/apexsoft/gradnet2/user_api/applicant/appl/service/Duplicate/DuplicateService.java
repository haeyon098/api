package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.Duplicate;

import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.DuplicateDto;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DuplicateService {

    @Value("${constraint.allowDuplicate}")
    private String allowDuplicate;

    @NonNull
    private List<DuplicateApplService> duplicateApplServices;


    /**
     * 학교별 조건에 맞는 중복체크 서비스 실행
     * @param duplicateDto
     */
    public void checkApplication(DuplicateDto duplicateDto) {
      if (!duplicateDto.isAllowRole()) {
            if ("FALSE".equalsIgnoreCase(allowDuplicate)) {
                duplicateApplServices.stream()
                        .filter(it -> it.supports(duplicateDto.getSchoolCode())).forEach(duplicateApplService -> duplicateApplService.check(duplicateDto));

            }
        }
    }
}