package kr.co.apexsoft.gradnet2.user_api._support.aws.kms;

import com.amazonaws.encryptionsdk.AwsCrypto;
import com.amazonaws.encryptionsdk.CryptoResult;
import com.amazonaws.encryptionsdk.kms.KmsMasterKey;
import com.amazonaws.encryptionsdk.kms.KmsMasterKeyProvider;
import com.amazonaws.services.kms.model.AWSKMSException;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.aws.kms.exception.KMSDecryptException;
import kr.co.apexsoft.gradnet2.user_api._support.aws.kms.exception.KMSEncryptException;
import kr.co.apexsoft.gradnet2.user_api._support.aws.kms.exception.KMSKeyException;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.CryptoService;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;

@Service
public class KmsServiceImpl implements CryptoService {

    @Autowired
    private KmsMasterKeyProvider amazonKmsProvider;

    @Value("${aws.kms.arn}")
    private String awsKmsArn;

    @Override
    public String encrypt(String plainText) {

        AwsCrypto crypto = new AwsCrypto();
        CryptoResult<byte[], KmsMasterKey> encryptResult = null;
        String ciphertext;
        try {
            encryptResult = crypto.encryptData(amazonKmsProvider, plainText.getBytes(StandardCharsets.UTF_8));
            byte[] encrypted = Base64.encodeBase64(encryptResult.getResult());
            ciphertext =new String(encrypted);

        } catch (AWSKMSException e) {
            throw new KMSKeyException(e.getMessage(), MessageUtil.getMessage("KMS_KEY_FAIL"));
        } catch (Exception e) {
            throw new KMSEncryptException(e.getMessage(), MessageUtil.getMessage("KMS_FAIL"));
        }


        return ciphertext;
    }

    @Override
    public String decrypt(String ciphertext) {

        AwsCrypto crypto = new AwsCrypto();
        CryptoResult<byte[], KmsMasterKey> decryptResult = null;
        String decryptText;
        try {
            decryptResult = crypto.decryptData(amazonKmsProvider,  Base64.decodeBase64(ciphertext.getBytes(StandardCharsets.UTF_8)));
            decryptText = new String(decryptResult.getResult());

        } catch (AWSKMSException e) {
            throw new KMSKeyException(e.getMessage(), MessageUtil.getMessage("KMS_KEY_FAIL"));
        } catch (Exception e) {
            throw new KMSDecryptException(e.getMessage(), MessageUtil.getMessage("KMS_FAIL"));
        }
        return decryptText;
    }

}
