package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplInfoProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplPartResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SchoolCommCodeRepository;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRepository;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileResponse;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileService;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.PDFPageService;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.domain.PDFPageInfo;
import kr.co.apexsoft.gradnet2.user_api._support.zip.ZIPService;
import kr.co.apexsoft.gradnet2.user_api._support.zip.domain.ZipInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.AppBirtFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplFormFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.SubmitResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.service.DocumentService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.*;

/**
 * Class Description
 *
 * @author 김효숙
 * @since
 */
@org.springframework.stereotype.Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SubmitService {
    @NonNull
    private ApplFormBirtService applFormBirtService;

    @NonNull
    private ApplService applService;

    @NonNull
    private PDFPageService pdfPageService;

    @Autowired
    private ZIPService zipService;

    @NonNull
    private FileService fileService;

    private final ApexModelMapper modelMapper;

    @NonNull
    private DocumentRepository documentRepository;

    @NonNull
    private FileRepository fileRepository;


    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private DocumentService documentService;

    @Value("${file.baseDir}")
    private String fileBaseDir;

    /**
     * 원서작성완료 화면
     *
     * @param applNo
     * @param roles
     * @return
     */
    @Transactional(readOnly = true)
    public SubmitResponse findByAppl(Long applNo, Set<Role> roles) {

        Appl appl = applService.checkApplSave(applNo, Appl.Status.SUBMIT, roles);//해당 에러 화면에서 처리  필요

        Optional<Document> document = documentRepository.findByItemCodeAndAppl_Id(DocItem.APPL_FORM.getValue(), applNo);

        return SubmitResponse.builder()
                .documentChecked(appl.getDocumentChecked())
                .id(appl.getId())
                .status(appl.getStatus())
                .file(document.isPresent() ? modelMapper.convert(document.get(), DocumentResponse.class) : null)
                .build();
    }


    /**
     * 직접 업로드한 pdf 파일 조회
     * (추천서,지원서,수험표 제외)
     *
     * @param applNo
     * @return
     */

    private List<DocumentResponse> userUploadPdfFiles(Long applNo) {

        //추천서,지원서,수험표 제외, pdf 파일 조회
        Optional<List<Document>> pdfFiles = documentRepository.findByAppl_IdAndIsPdfTrueAndItemCodeNotInOrderByFilePath(applNo, DocItem.NotUploadedDirectlyItems());


        if (pdfFiles.isPresent()) {
            return modelMapper.convert(pdfFiles.get(), new TypeToken<List<DocumentResponse>>() {
            }.getType());
        }

        //파일이 없을경우 에러 FIXME: 현재는 학업연구계획서 필수이므로, 1개이상 무조건 존재.추후 변경시 수정필요
        throw new ApplUploadFileNotFoundException(applNo, MessageUtil.getMessage("APPL_SUBMIT_FAIL"));


    }

    /**
     * pdf파일들 페이징후 zip으로 묶어서 업로드
     *
     * @param filelist
     * @param appl
     * @return
     */
    public void uploadFilePageNumberedAndZIPUpload(List<File> filelist, Appl appl) {
        ApplInfoProjection part = applRepository.findMyApplInfo(appl.getId());


        //pdf 파일에 정보삽입
        List<File> list = pdfPageService.getPageNumberedPDFs(filelist, modelMapper.convert(part, PDFPageInfo.class));

        //zip으로 묶기
        ZipInfo zipInfo = modelMapper.convert(part, ZipInfo.class);
        zipInfo.setFileBaseDir(fileBaseDir);
        File zip = zipService.getZippedFileFromPdf(list, zipInfo);

        FileRequest fileRequest = new FileRequest(zip, FilePathUtil.getS3UploadDirectrotyFullPath(part.getSchoolCode(), part.getEnterYear(),
                part.getRecruitPartSeq(), appl.getId()), zipInfo.getZipFileName());

        documentService.uploadAndSaveGradnetCustomFile(fileRequest,appl,DocGrpType.BASIC.getValue(), -1L,DocItem.ZIP.getValue(),false);

    }

    /**
     * 시스템 통해서 받은 추천서 제외 PDF파일 s3로 부터 서버에 다운
     * 파일 족수 넘버링
     * zip으로 압축
     * s3에 업로드
     *
     * @param applNo
     * @param roles
     */
    public void submit(Long applNo, Set<Role> roles) {

        //원서작성완료 가능한지 체크
        Appl appl = applService.checkApplSave(applNo, Appl.Status.SUBMIT, roles);
        if (!appl.getDocumentChecked()) {
            throw new ApplSubmitStatusException(MessageUtil.getMessage("APPL_SUBMIT_STATUS_FAIL"));
        }

        //직접 업로드한 pdf 파일 조회
        List<DocumentResponse> pdfFileList = userUploadPdfFiles(applNo);

        //s3에서 pdf리스트에 있는 파일 다운로드
        List<File> filelist = fileService.getFileListFromFileServer(pdfFileList, applNo);

        uploadFilePageNumberedAndZIPUpload(filelist, appl);
        // 작성완료 변경
        appl.changeStatus(Appl.Status.SUBMIT);


    }

    /**
     * 지원서 생성
     * 파일 업로드
     * db 반영
     *
     * @param applNo
     * @param roles
     */
    public DocumentResponse generateApplForm(Long applNo, Set<Role> roles) {
//지원서 생성 가능한지 체크
        Appl appl = applService.checkApplSave(applNo, Appl.Status.SUBMIT, roles);

        ApplFormFileInfo fileInfo = modelMapper.convert(appl, ApplFormFileInfo.class);

        // 버트 파일 객체
        FileRequest fileRequest = applFormBirtService.generateBirtFile(fileInfo);

        // db 저장
        Document document = documentService.uploadAndSaveGradnetCustomFile(fileRequest, appl, DocGrpType.BASIC.getValue(), -1L, DocItem.APPL_FORM.getValue(),true);
        appl.documentCheckedTrue();

        return modelMapper.convert(document, DocumentResponse.class);
    }


}
