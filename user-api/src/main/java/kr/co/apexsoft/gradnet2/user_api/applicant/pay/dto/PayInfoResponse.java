package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-09-28
 */
@NoArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayInfoResponse {
    private Long id;
    private String status;

    private String engName;
    private String korName;
    private String email;
    private String telNum;

    private String feeKRW;
    private String feeUSD;

    private String schoolCode;
    private String schoolEngName;
    private String schoolKorName;

    private String enterYear;

    private String courseCode;
    private String courseEngName;
    private String courseKorName;

    private String categoryCode;
    private String categoryEngName;
    private String categoryKorName;

    private String majorCode;
    private String majorEngName;
    private String majorKorName;

    private String payDueDay;

    private String productName;


    public void initProductName(String productName) {
        this.productName = productName;
    }
}
