package kr.co.apexsoft.gradnet2.user_api._support.pdf;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.domain.PDFPageInfo;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.exception.PDFPageCountException;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.exception.PDFPageException;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.exception.PDFPageInsertException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.PDPageTree;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.util.Matrix;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PDFPageService {

    @NonNull
    private ResourceLoader resourceLoader;

    private static final Logger logger = LoggerFactory.getLogger(PDFPageService.class);

    /**
     * pdf 파일의 페이지수 계산
     *
     * @param pdfFile
     * @return
     */
    public int getPdfPageCount(File pdfFile,Long applNo) {
        int pageCounts = -1;
        PDDocument pdDocument = null;
        try {
            pdDocument = PDDocument.load(pdfFile);
            pageCounts = pdDocument.getNumberOfPages();

        } catch (IOException e) {
            throw new PDFPageCountException(MessageUtil.getMessage("PDF_COUNT_FAIL"),applNo,pdfFile.getName());
        } finally {
            if (pdDocument != null) {
                try {
                    pdDocument.close();
                } catch (IOException e) {
                    throw new PDFPageCountException( MessageUtil.getMessage("PDF_COUNT_FAIL"),applNo,pdfFile.getName());
                }

            }
        }

        return pageCounts;
    }


    /**
     * PDF 파일에 텍스트를 추가한다
     * 우상단  ' 현재 페이지 / 전체 페이지 '
     * 좌상단 'PDFPageInfo'
     *
     * @param file        쪽수를 매길 파일
     * @param startPage   쪽수를 매기기 시작할 페이지(0이 아니라 1부터 시작하는 숫자)
     * @param startNo     시작 쪽수 번호
     * @param endNo       전체 쪽수 번호, 0이나 음수를 입력하면 해당 파일의 페이지수를 기준으로 startPage를 고려해서 자동 계산
     * @param pdfPageInfo pdf 추가 입력 정보
     * @return
     */
    public File insertPDFPageInfo(File file, int startPage, int startNo, int endNo, PDFPageInfo pdfPageInfo) {

        String targetFilePath = getTargetFilePath(file);
        PDDocument pdDocument = null;
        try {
            pdDocument = PDDocument.load(file);

            //pdf 파일 읽기전용 세팅
            AccessPermission ap = new AccessPermission();
            ap.setCanModify(false);
            ap.setReadOnly();
            StandardProtectionPolicy spp = new StandardProtectionPolicy("apexSecret", "", ap);

            //TODO : NanumGothicCoding: 영문.숫자.한글 지원 폰트로 다른언어지원시 변경 필요
            InputStream fontstream = resourceLoader.getResource("classpath:/pdf_font/NanumGothicCoding-Bold.ttf").getInputStream();
            PDType0Font fontNanum = PDType0Font.load(pdDocument, fontstream);
            float fontSize = 13.0f;
            PDPageTree pageTree = pdDocument.getPages();
            int length = (endNo > 0) ? endNo : pdDocument.getNumberOfPages() - startPage + 1;
            Iterator<PDPage> it = pageTree.iterator();
            int i = 1;
            while (i < startPage && it.hasNext()) {
                it.next();
                i++;
            }
            while (it.hasNext()) {
                PDPage page = it.next();
                PDRectangle pageSize = page.getMediaBox();
                String strPage = new StringBuilder().append((i++ + startNo) - startPage).append("/").append(length).toString();
                float stringWidth = fontNanum.getStringWidth(strPage) * fontSize / 1000f;
                float pageWidth = pageSize.getWidth();
                float pageHeight = pageSize.getHeight();
                Matrix m4Position = new Matrix();
                m4Position.translate(pageWidth - stringWidth - 15, pageHeight - 20);
                PDPageContentStream contentStream = new PDPageContentStream(pdDocument, page, true, true, true);
                contentStream.beginText(); //글쓰기 시작
                contentStream.setFont(fontNanum, fontSize);  //폰트 설정

                //글쓰기
                contentStream.setTextMatrix(m4Position);
                //우측 상단 페이지
                contentStream.showText(strPage);

                if (StringUtils.isNotBlank(pdfPageInfo.getLeftTop())) {
                    contentStream.setFont(fontNanum, 12);
                    contentStream.newLineAtOffset(-pageWidth + stringWidth + 30, 0);
                    contentStream.showText(String.valueOf(pdfPageInfo.getLeftTop()));
                    if (StringUtils.isNotBlank(pdfPageInfo.getLeftBottom())) {
                        contentStream.newLineAtOffset(0, -15);
                        contentStream.showText(String.valueOf(pdfPageInfo.getLeftBottom()));
                    }
                }


                contentStream.endText();  // 글쓰기 종료
                contentStream.close();
            }
            pdDocument.protect(spp); // 읽기 전용


            pdDocument.save(targetFilePath);
        } catch (Exception e) {
            //암호화 pdf . 한글.영문.숫자 외
            throw new PDFPageInsertException(MessageUtil.getMessage("PDF_PAGE_INSERT_FAIL"),pdfPageInfo.getApplNo(),targetFilePath);
        } finally {
            if (pdDocument != null) {
                try {
                    pdDocument.close();
                } catch (IOException e) {
                    throw new PDFPageException( MessageUtil.getMessage("PDF_PAGE_INSERT_FAIL"),pdfPageInfo.getApplNo(),targetFilePath);
                }
            }
        }

        return new File(targetFilePath); // S3업로드 후 로컬 파일삭제 필수
    }

    public File insertPDFPageInfo(File file, int startNo, int endNo, PDFPageInfo pdfPageInfo) {
        return insertPDFPageInfo(file,1,startNo,endNo,pdfPageInfo);
    }

    // 생성할 파일 전체 경로 반환
    private String getTargetFilePath(File file) {
        return file.getAbsolutePath();
    }



    public void deletePageNumberedPDF(File file) {
        String targetFilePath = getTargetFilePath(file);
        File numberedFile = new File(targetFilePath);
        if (numberedFile.exists())
            numberedFile.delete();
    }

    /**
     * pdf파일들에 정보삽입
     * 전체 페이징 + 추가 정보
     * @param uploaded
     * @param pageInfo
     * @return
     */
    public List<File> getPageNumberedPDFs(List<File> uploaded,PDFPageInfo pageInfo){
        int totalpageCount =0;
        int accumulatedPages =1;

        List<File> fileList =new ArrayList<>();
        for(File item : uploaded){
            int pageCounts =getPdfPageCount(item,pageInfo.getApplNo());
            totalpageCount+=pageCounts;
        }
        for(File item : uploaded){
            fileList.add(insertPDFPageInfo(item,1,accumulatedPages,totalpageCount,pageInfo));
            accumulatedPages+=getPdfPageCount(item,pageInfo.getApplNo());
        }
        return fileList;
    }

}
