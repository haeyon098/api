package kr.co.apexsoft.gradnet2.user_api.applicant.acad.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Grade;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
public class AcademyRequest {

    @NotNull
    private Long applId;

    private Long id;

    @NotNull
    private Academy.AcademyType type;

    @NotNull
    private Integer seq;


    private Boolean lasted;

    @NotNull
    private String statusCode;

    @NotNull
    private LocalDate enterDay;

    @NotNull
    private LocalDate graduateDay;

    private Grade grade;

    @Size(max = 80)
    private String schoolCollegeName;

    @NotBlank
    @Size(max = 80)
    private String schoolMajorName;


    @Size(max = 20)
    private String schoolDegreeNo;

    @NotBlank
    private String schoolCountryCode;

    @NotBlank
    private String schoolCode;

    @Size(max = 100)
    private String schoolName;


    @Size(max = 100)
    private String schoolEtcCountryName;


}
