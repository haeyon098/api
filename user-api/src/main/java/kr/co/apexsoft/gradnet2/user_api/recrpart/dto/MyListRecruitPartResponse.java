package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-08
 */
@NoArgsConstructor
@Getter
public class MyListRecruitPartResponse {
    private String korName;

    private String engName;

    @Setter
    public List<MyListRecruitPartTimeResponse> periods;
}
