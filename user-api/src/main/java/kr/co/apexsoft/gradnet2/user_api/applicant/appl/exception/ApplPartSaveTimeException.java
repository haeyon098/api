package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * 지원사항 저장
 * 시간 익셉션

 */
public class ApplPartSaveTimeException extends CustomException {


    public ApplPartSaveTimeException(String warningMessage) {
        super( warningMessage);
    }
}
