package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto;

import kr.co.apexsoft.gradnet2.entity.comcode.domain.SchoolCommCode;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
*
*
* @author aran
* @since 2020-02-26
*/
@Getter
public class SrvyQstAnsRequest {
    @NotNull
    private Long applNo;

    @NotBlank
    @Size(max = 100)
    private String ans1;

    @Size(max = 100)
    private String ans1_1;

    @NotBlank
    @Size(max = 100)
    private String ans2;

    @Size(max = 100)
    private String ans2_1;

    @NotBlank
    @Size(max = 100)
    private String ans3;

    @Size(max = 150)
    private String ans3_1;

    @NotBlank
    @Size(max = 100)
    private String ans4;

    @NotBlank
    @Size(max = 100)
    private String ans5;

    @NotBlank
    @Size(max = 150)
    private String ans6;

    @NotBlank
    @Size(max = 100)
    private String ans7;

    @NotBlank
    @Size(max = 100)
    private String ans8;

    @NotNull
    private SchoolCommCode ans9;

    @NotBlank
    @Size(max = 150)
    private String ans10;
    
    @Size(max = 100)
    private String ans11;

    @NotBlank
    @Size(max = 100)
    private String ans12;

    @Size(max = 100)
    private String ans13;

    @Size(max = 150)
    private String ans14;

    @Size(max = 100)
    private String ans15;

    @Size(max = 100)
    private String ans16;

    @NotBlank
    private String contactCode;

    private List<AlumniDTO> alumni;
}
