package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
* 빈 설문지
*
* @author aran
* @since 2020-02-27
*/
@Getter
public class SrvyAnsRequest {
    @NotNull
    private String applNo;

    private boolean answered = false;

}
