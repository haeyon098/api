package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.Duplicate;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.DuplicateDto;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.DuplicateException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * ' 학교+입학연도+회차+userId ' 중복체크
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserIdCheckService implements DuplicateApplService {

    @NonNull
    private ApplRepository applRepository;

    /**
     * 해당 서비스 사용학교시 추가
     * 현재 KDI 사용
     *
     * @param schoolCode
     * @return
     */
    @Override
    public boolean supports(String schoolCode) {
        return RecruitSchool.SchoolCodeType.KDI.getValue().equals(schoolCode);
    }

    /**
     * 지원사항저장 단계에서만 체크
     *
     * @param duplicateDto
     */
    @Override
    public void check(DuplicateDto duplicateDto) {
        if (duplicateDto.isApplPartSaveStatus()) {
            if (applRepository.countByPart_SchoolCodeAndPart_EnterYearAndPart_RecruitPartSeqAndPart_ApplPartNoAndApplicant_IdAndStatusNot(
                    duplicateDto.getSchoolCode(), duplicateDto.getEnterYear(), duplicateDto.getRecruitPartSeq(), duplicateDto.getApplPartNo(), duplicateDto.getUserNo(), Appl.Status.CANCEL)
                    > 0) {
                throw new DuplicateException(MessageUtil.getMessage("DUPLICATE_USERID_FAIL"));
            }

        }

    }


}
