package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 추천서 취소 불가
 * @author 김효숙
 */
@Getter
public class RecCancelException extends CustomException {


    public RecCancelException(String warningMessage, Long recNo) {
        super("[recNo:" + recNo + "]", warningMessage);
    }


}

