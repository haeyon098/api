package kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto;

import kr.co.apexsoft.gradnet2.user_api._support.file.FileResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-18
 */
@Getter
@AllArgsConstructor
public class EssaySubmitResponse {
    private FileResponse file;

    private String status;

    private List<EssayResponse> list;
}
