package kr.co.apexsoft.gradnet2.user_api._support.mail;

public interface MailService {

    void sendAsync(MailDto mailDto);

    void send(MailDto mailDto);

}
