package kr.co.apexsoft.gradnet2.user_api.applicant.essay.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplPartResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.essay.domain.Essay;
import kr.co.apexsoft.gradnet2.entity.applicant.essay.repository.EssayRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplSeqEssay;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.applpart.repository.ApplSeqEssayRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._support.birt.service.BirtService;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRepository;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileResponse;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.PDFPageService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.EssayFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto.EssayResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto.EssaySubmitResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@Service
@Slf4j
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class EssayService {
    @NonNull
    private EssayRepository essayRepository;

    @NonNull
    private ApplSeqEssayRepository applSeqEssayRepository;

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private BirtService birtService;

    @NonNull
    private FileRepository fileRepository;

    @NonNull
    private ApplService applService;

    @NonNull
    private DocumentRepository documentRepository;

    @Value("${file.baseDir}")
    private String fileBaseDir;

    @NonNull
    private PDFPageService pdfPageService;

    private final ApexModelMapper modelMapper;

    @Transactional(readOnly = true)
    public List<EssayResponse> findByAppl(Long id, ApplPart part) {
        List<Essay> essays = essayRepository.findByAppl_Id(id);
        return this.matchEssayAnswer(essays, part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq());
    }

    public void save(Long id,List<Essay> list,Set<Role> roles) {
        applService.checkApplSave(id, Appl.Status.ESSAY_SAVE, roles);
        essayRepository.saveAll(list);
    }

    public EssaySubmitResponse submitAndMakeDocument(Long applNo, List<Essay> list, Set<Role> roles) {
        Appl appl = applService.checkApplSave(applNo, Appl.Status.ESSAY_SAVE, roles);

        // 자기소개서 생성에 필요한 정보 조회
        List<Essay> saveList = essayRepository.saveAll(list);
        ApplPartResponse part = applRepository.findMyApplPart(applNo);

        EssayFileInfo fileInfo = modelMapper.convert(appl, EssayFileInfo.class);
        fileInfo.setFileBaseDir(fileBaseDir);

        // 버트 파일 생성
        Map<String, Object> map  = birtService.generateEssayFile(appl, part, saveList.get(0), fileInfo);

        // 파일 객체 생성&업로드
        File file = new File((String) map.get("pdfDirectoryFullPath"), (String) map.get("pdfFileName"));
        FileRequest fileRequest = new FileRequest(file, fileInfo.getS3Path(), fileInfo.getFileName(),fileInfo.getOriginFileName());

        FileResponse fileResponse = null;
        // TODO: 추후에 uploadAndSaveGradnetCustomFile() 같이 쓸수있으면 쓰도록 수정하면 좋을듯.
        Integer pdfPageCount;
        try {
            fileResponse = fileRepository.upload(fileRequest);
            pdfPageCount = pdfPageService.getPdfPageCount(fileRequest.getFile(), appl.getId());
        } catch (Exception e) {
            throw e;
        } finally {
            if(fileRequest.getFile().exists())
                fileRequest.getFile().delete();
        }

        Document document = saveDocument(appl, pdfPageCount, fileResponse);
        fileResponse.setApplDocNo(document.getId());

        appl.changeStatus(Appl.Status.ESSAY_SAVE);

        List<EssayResponse> essayResponses = this.matchEssayAnswer(saveList, part.getSchoolCode(), part.getEnterYear(),
                part.getRecruitPartSeq());
        return new EssaySubmitResponse(fileResponse, appl.getStatus().name(), essayResponses);
    }

    /**
     * 에세이 타이틀, 답변 매핑
     *
     * @param essays
     * @param schoolCode
     * @param enterYear
     * @param partSeq
     * @return
     */
    private List<EssayResponse> matchEssayAnswer(List<Essay> essays, String schoolCode, String enterYear, Integer partSeq) {
        List<ApplSeqEssay> questions = applSeqEssayRepository.findBySchoolCodeAndEnterYearAndRecruitPartSeqOrderBySeq(
                schoolCode, enterYear, partSeq);

        List<EssayResponse> results = new ArrayList<>();

        for(ApplSeqEssay question : questions) {
            String data = null;
            Long essayNo = null;
            for (Essay essay : essays) {
                if(essay.getSeq().equals(question.getSeq())) {
                    data = essay.getData();
                    essayNo = essay.getId();
                }
            }
            EssayResponse response = modelMapper.convert(question, EssayResponse.class);
            response.setEssay(essayNo, data);
            results.add(response);
        }
        return results;
    }

    /**
     * APPL_DOC 에 자기소개서 삽입
     *
     * @param appl
     * @param pdfPageCount
     * @param fileResponse
     */
    private Document saveDocument(Appl appl, Integer pdfPageCount, FileResponse fileResponse) {
        documentRepository.deleteByAppl_IdAndGroupCodeAndItemCode(appl.getId(), DocGrpType.BASIC.getValue(), DocItem.ESSAY.getValue());

        return documentRepository.save(new Document(appl, DocGrpType.BASIC.getValue(), -1L, DocItem.ESSAY.getValue(),
                pdfPageCount, fileResponse.getFileKey(), fileResponse.getOriginalFileName(), true));
    }
}
