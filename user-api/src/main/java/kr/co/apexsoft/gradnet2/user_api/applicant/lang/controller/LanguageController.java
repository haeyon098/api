package kr.co.apexsoft.gradnet2.user_api.applicant.lang.controller;

import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.service.LanguageService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@RestController
@RequestMapping("/appl/lang")
@RequiredArgsConstructor
public class LanguageController {
    @NonNull
    private LanguageService languageService;

    @GetMapping("/{applNo}")
    public ResponseEntity<List<LanguageResponse>> findAllCareer(@PathVariable("applNo") Long id) {
        return ResponseEntity.ok(languageService.findAllLang(id));
    }
}
