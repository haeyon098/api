package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import lombok.Getter;

/**
 * 서술입력항목
 * 학교,전형 공통으로 사용
 *
 */
@Getter
public class EssayFileInfo extends  AppBirtFileInfo{

    @Override
    public String getBirtFileName() {
        return FilePathUtil.getEssayBirtFileName(this.getPartSchoolCode());
    }

    @Override
    public String getFileName() {
        return FilePathUtil.getsSortingEssayFileName();
    }

    public  String getOriginFileName(){
        return FilePathUtil.getEssayFileName();
    }

}
