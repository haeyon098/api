package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-08
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class MyListResponse {
    List<MyApplAndInfoResponse> all;

    List<MyApplAndInfoResponse> proceeding;

    List<MyApplAndInfoResponse> submit;

    List<MyApplAndInfoResponse> complete;

    List<MyApplAndInfoResponse> cancel;
}
