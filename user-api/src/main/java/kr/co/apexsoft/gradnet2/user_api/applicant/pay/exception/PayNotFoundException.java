package kr.co.apexsoft.gradnet2.user_api.applicant.pay.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-14
 */
public class PayNotFoundException extends CustomException {

    public PayNotFoundException() {
        super();
    }

    public PayNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public PayNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
