package kr.co.apexsoft.gradnet2.user_api._common;

import kr.co.apexsoft.gradnet2.user_api._common.util.ValidUtil;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Map;

public class ApexAssert extends Assert {

    /**
     * @param expression
     * @param message
     * @param exceptionClass
     */
    public static void state(boolean expression, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (!expression) {
            throwException(message, exceptionClass);
        }
    }

    public static void isTrue(boolean expression, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (!expression) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * Assert that an object is {@code null}. 객체가 null이 아닌 경우 사용자가 정의한 예외를 던진다.
     */
    public static void isNull(@Nullable Object object, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (object != null) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * null인 경우 사용자 정의 예외 발생
     *
     * @param object
     * @param message
     * @param exceptionClass
     */
    public static void notNull(@Nullable Object object, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (object == null) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * 전달받은 값이 null이 거나 빈값인 경우 사용자 정의 예외 발생
     */
    public static void hasLength(@Nullable String text, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (!StringUtils.hasLength(text)) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * 전달받은 값이 null이 거나 빈값인 경우 사용자 정의 예외 발생
     */
    public static void hasText(@Nullable String text, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (!StringUtils.hasText(text)) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * 전달받은 값이 null이 거나 빈값인 경우 사용자 정의 예외 발생
     */
    public static void notEmpty(@Nullable Object[] array, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (ObjectUtils.isEmpty(array)) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * 전달받은 값이 null이 거나 빈값인 경우 사용자 정의 예외 발생
     */
    public static void notEmpty(@Nullable Collection<?> collection, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (CollectionUtils.isEmpty(collection)) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * 전달받은 값이 null이 거나 빈값인 경우 사용자 정의 예외 발생
     */
    public static void notEmpty(@Nullable Map<?, ?> map, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (CollectionUtils.isEmpty(map)) {
            throwException(message, exceptionClass);
        }
    }

    /**
     * 전달받은 값이 mail 형식이 아닌경우
     */
    public static void notMailPattern(@Nullable String mail, String message, final Class<? extends RuntimeException> exceptionClass) {
        if (!ValidUtil.isMail(mail)) {
            throwException(message, exceptionClass);
        }
    }

    private static void throwException(String message, final Class<? extends RuntimeException> exceptionClass) {
        try {
            throw exceptionClass.getDeclaredConstructor(String.class).newInstance(message);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new AssertException();
        }


    }


}
