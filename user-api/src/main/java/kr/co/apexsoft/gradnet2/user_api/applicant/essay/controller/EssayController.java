package kr.co.apexsoft.gradnet2.user_api.applicant.essay.controller;

import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.entity.applicant.essay.domain.Essay;
import kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto.EssayRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto.EssayResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto.EssaySubmitResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.essay.service.EssayService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@RestController
@RequestMapping("/appl/essay")
@RequiredArgsConstructor
public class EssayController {
    @NonNull
    private EssayService essayService;

    private final ApexModelMapper modelMapper;

    private final ApexCollectionValidator apexCollectionValidator;

    /**
     * 자기소개서 조회
     *
     * @param id
     * @return
     */
    @GetMapping("/{applNo}")
    public ResponseEntity<List<EssayResponse>> findAllEssayData(@PathVariable("applNo") Long id,
                                                                @RequestAttribute(name="applPart") ApplPart part) {
        return ResponseEntity.ok(essayService.findByAppl(id, part));
    }

    /**
     * 자기소개서 임시저장
     *
     * @param id
     * @param list
     * @return
     */
    @PostMapping("/{applNo}")
    public void save(@PathVariable("applNo") Long id,
                     @Valid @RequestBody List<EssayRequest> list, @AuthenticationPrincipal UserPrincipal userPrincipal,
                     Errors errors) {
        apexCollectionValidator.validate(list,errors);
        if(errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("ESSAY_INVALID_INFO"), errors);

        essayService.save(id,modelMapper.convert(list, new TypeToken<List<Essay>>(){}.getType()), userPrincipal.getRoles());
    }

    /**
     * 자기소개서 등록 & 파일 만들기
     *
     * @param applNo
     * @param list
     */
    @PostMapping("/submit/{applNo}")
    public ResponseEntity<EssaySubmitResponse> submitAndMakeDocument(@PathVariable("applNo") Long applNo,
                                                                     @Valid @RequestBody List<EssayRequest> list,
                                                                     @AuthenticationPrincipal UserPrincipal userPrincipal,
                                                                     Errors errors) {
        apexCollectionValidator.validate(list,errors);
        if(errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("ESSAY_INVALID_INFO"), errors);

        List<Essay> essayList = modelMapper.convert(list, new TypeToken<List<Essay>>(){}.getType());
        return ResponseEntity.ok(essayService.submitAndMakeDocument(applNo, essayList,userPrincipal.getRoles()));
    }
}
