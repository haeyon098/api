package kr.co.apexsoft.gradnet2.user_api._support.file.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
*
*
파일 사이즈
*/
public class FileSizeException extends CustomException {
    public FileSizeException(String warningMessage, Long applNo) {
        super("[applNo:" + applNo + "]",warningMessage);
    }

    public FileSizeException(String warningMessage) {
        super(warningMessage);
    }

}
