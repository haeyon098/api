package kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.EtcDocumentProjection;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.DocGrpDocResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-03
 */
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentSaveDto {
    private Appl.Status status;

    private List<DocGrpDocResponse> list;

    private List<EtcDocumentProjection> etcList;
}
