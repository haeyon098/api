package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-28
 *
 */

@Getter
public class AnswerDto {
    @NotBlank
    private String questionNo;

    @NotBlank
    @Size(max = 1000)
    private String answer;
}
