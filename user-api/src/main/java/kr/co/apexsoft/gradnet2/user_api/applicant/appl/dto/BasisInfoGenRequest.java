package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.Getter;

import javax.validation.constraints.NotNull;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-01-08
 */
@Getter
public class BasisInfoGenRequest {
    @NotNull
    private ApplicantInfoRequest applicantInfo;
    @NotNull
    private GeneralInfoDto generalInfo;
}
