package kr.co.apexsoft.gradnet2.user_api.applpart.dto;

import kr.co.apexsoft.gradnet2.entity.applpart.projection.DocumentsResponse;
import kr.co.apexsoft.gradnet2.user_api.comcode.dto.CommCodeDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-21
 */
@Getter
@AllArgsConstructor
public class DocGrpDocResponse {
    private String id;  // 상위면 DOC_GRP_CODE, 하위면 GRP_NO 들어감

    private String grpKorName;

    private String grpEngName;

    private List<? extends DocumentsResponse> documents;

    private List<DocGrpDocResponse> subGrp;

    public DocGrpDocResponse(String grpKorName, String grpEngName, List<? extends DocumentsResponse>documents, String id) {
        this.grpKorName = grpKorName;
        this.grpEngName = grpEngName;
        this.documents = documents;
        this.id = id;
    }

    public DocGrpDocResponse(String id, String grpKorName, String grpEngName, List<DocGrpDocResponse> subGrp) {
        this.id = id;
        this.grpKorName = grpKorName;
        this.grpEngName = grpEngName;
        this.subGrp = subGrp;
    }
}
