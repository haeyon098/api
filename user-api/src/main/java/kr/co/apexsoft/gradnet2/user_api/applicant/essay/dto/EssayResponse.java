package kr.co.apexsoft.gradnet2.user_api.applicant.essay.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-17
 */
@Getter
@NoArgsConstructor
public class EssayResponse {
    private Integer seq;

    private String korTitle;

    private String engTitle;

    private String korDescription;

    private String engDescription;

    private Long essayNo;    // APPL_ESSAY_NO

    private String data;

    public void setEssay(Long essayNo, String data) {
        this.essayNo = essayNo;
        this.data = data;
    }
}
