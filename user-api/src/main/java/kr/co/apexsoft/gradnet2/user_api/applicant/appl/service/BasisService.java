package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.standard.repository.CountryRepository;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.CryptoService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.*;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-01-08
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BasisService {
    @NonNull
    private ApplRepository applRepository;

    @Autowired
    private CryptoService cryptoService;

    @NonNull
    private ApplService applService;

    private final ApexModelMapper modelMapper;

    @NonNull
    private CountryRepository countryRepository;


    public BasisInfoResponse createBasisInfo(Appl appl, Long id, boolean isGen, Set<Role> roles) {
        Appl dbAppl = applService.checkApplSave(id, Appl.Status.BASIS_SAVE, roles,new DuplicateDto(isGen,appl));
        dbAppl.changeStatus(Appl.Status.BASIS_SAVE);
        appl = applInfoEncrypt(appl, isGen);

        dbAppl.initBasisInfo(appl);
        return applInfoDecrypt(applRepository.save(dbAppl));
    }

    @Transactional(readOnly = true)
    public BasisInfoResponse findBasisInfo(Long id) {

        return applInfoDecrypt(applRepository.findById(id)
                .orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND"))));
    }


    /**
     * 암호화 인코딩
     *
     * @param appl
     * @return
     */
    private Appl applInfoEncrypt(Appl appl, boolean isGen) {
        if (isGen) {
            appl.encryptRegistrationEncr(cryptoService.encrypt(appl.getApplicantInfo().getRegistrationEncr()));
        } else {
            String encryptPassport = cryptoService.encrypt(appl.getForeignerInfo().getPassportNo());
            String encryptRegistration = null, encryptVisa = null;
            if(appl.getForeignerInfo().getVisaNo() != null)
                encryptVisa = cryptoService.encrypt(appl.getForeignerInfo().getVisaNo());

            if (appl.getForeignerInfo().getForeignerRegistrationNo() != null)
                encryptRegistration = cryptoService.encrypt(appl.getForeignerInfo().getForeignerRegistrationNo());
            appl.encryptForeignerInfo(encryptPassport, encryptVisa, encryptRegistration);
        }
        return appl;
    }

    /**
     * 암호화 디코딩
     *
     * @param appl
     * @return
     */
    private BasisInfoResponse applInfoDecrypt(Appl appl) {
        ApplicantInfoResponse applicantInfoResponse = null;
        GeneralInfoDto generalInfoDto = null;
        ForeignerInfoResponse foreignerInfoResponse = null;

        if (appl.getApplicantInfo() != null) {
            applicantInfoResponse = modelMapper.convert(appl.getApplicantInfo(), ApplicantInfoResponse.class);
            String cntrName = appl.getApplicantInfo().getCntr().getId().equals("999") ? appl.getApplicantInfo().getCntr().getName() :
                    countryRepository.findById(appl.getApplicantInfo().getCntr().getId()).get().getCntrName();
            applicantInfoResponse.initCntr(appl.getApplicantInfo().getCntr().getId(), cntrName);
            if (appl.getApplicantInfo().getRegistrationEncr() != null) {
                applicantInfoResponse.initRegistrationEncr(cryptoService.decrypt(appl.getApplicantInfo().getRegistrationEncr()));
                generalInfoDto = modelMapper.convert(appl.getGeneralInfo(), GeneralInfoDto.class);
            }
            if (appl.getForeignerInfo() != null) {
                if (appl.getForeignerInfo().getPassportNo() != null) {
                    foreignerInfoResponse = modelMapper.convert(appl.getForeignerInfo(), ForeignerInfoResponse.class);
                    String encryptPassport = cryptoService.decrypt(appl.getForeignerInfo().getPassportNo());
                    String encryptRegistration = null, encryptVisa = null;
                    if(appl.getForeignerInfo().getVisaNo() != null)
                        encryptVisa = cryptoService.decrypt(appl.getForeignerInfo().getVisaNo());

                    if (appl.getForeignerInfo().getForeignerRegistrationNo() != null)
                        encryptRegistration = cryptoService.decrypt(appl.getForeignerInfo().getForeignerRegistrationNo());

                    foreignerInfoResponse.encryptForeignerInfo(encryptPassport, encryptVisa, encryptRegistration);
                }
            }
        }
        BasisInfoResponse response = new BasisInfoResponse(appl.getStatus(),
                applicantInfoResponse,
                generalInfoDto,
                foreignerInfoResponse);
        return response;
    }
}
