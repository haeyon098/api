package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 수정시 appl status 체크
 * 작성완료상태
 * @author 박지환
 */
@Getter
public class ApplUpdateStatusCompleteException extends CustomException {

    public ApplUpdateStatusCompleteException(String warningMessage, Long applNo) {
        super( "[applNo:" + applNo + "]", warningMessage);
    }

}

