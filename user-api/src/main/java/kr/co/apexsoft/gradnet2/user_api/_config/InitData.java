package kr.co.apexsoft.gradnet2.user_api._config;

import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import kr.co.apexsoft.gradnet2.entity.role.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 */
@Profile("develop")
@Component
public class InitData implements CommandLineRunner {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        this.roleRepository.saveAll(Arrays.asList(
                new Role(100L, RoleType.ROLE_USER),
                new Role(200L, RoleType.ROLE_SYS_ADMIN),
                new Role(210L, RoleType.ROLE_SCHL_ADMIN)        ));
    }
}
