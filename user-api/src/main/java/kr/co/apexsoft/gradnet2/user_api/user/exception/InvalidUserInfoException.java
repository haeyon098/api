package kr.co.apexsoft.gradnet2.user_api.user.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import lombok.Getter;
import org.springframework.validation.Errors;

/**
 * 사용자 정보 에러처리
 * @author 김혜연
 */
@Getter
public class InvalidUserInfoException extends InvalidException {
    public InvalidUserInfoException(String message, Errors errors) {
        super(message, errors);
    }

    public InvalidUserInfoException(String message) {
        super(message);
    }
}
