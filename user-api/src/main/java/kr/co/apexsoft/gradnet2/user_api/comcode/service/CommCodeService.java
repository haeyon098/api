package kr.co.apexsoft.gradnet2.user_api.comcode.service;

import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SchoolCommCode;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.user_api.comcode.dto.*;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SchoolCommCodeRepository;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SysCommCodeRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-03-14
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Transactional
public class CommCodeService {
    @NonNull
    private SysCommCodeRepository sysCommCodeRepository;

    @NonNull
    private SchoolCommCodeRepository schoolCommCodeRepository;

    private final ApexModelMapper modelMapper;


    @Transactional(readOnly = true)
    public Map<String, List<? extends CommCodeDto>> findByCodeGroups(String[] codeGrpArr, CommCodeGrpType type, SchoolCommRequest SchoolCommRequest) {
        Map<String, List<? extends CommCodeDto>> CodeListMap = new HashMap<>();
        for (String codeGrp : codeGrpArr) {
            switch (type) {
                case SCHOOL: {
                    List<SchoolCommCode> list =schoolCommCodeRepository.findAllByCodeGrpAndSchoolCodeAndEnterYearAndRecruitPartSeqAndUsedTrueOrderByCodeAsc(codeGrp, SchoolCommRequest.getSchoolCode(), SchoolCommRequest.getEnterYear(), SchoolCommRequest.getRecruitPartSeq());
                    CodeListMap.put(getRenameCodeGrp(codeGrp),  modelMapper.convert(list, new TypeToken<List<SchoolCommCodeDto>>(){}.getType()));
                    break;
                }
                case SYSTEM: {

                    List<SysCommCode> list=  sysCommCodeRepository.findAllByCodeGrpAndUsedTrueOrderByCodeAsc(codeGrp);

                    CodeListMap.put(getRenameCodeGrp(codeGrp),  modelMapper.convert(list, new TypeToken<List<SysCommCodeDto>>(){}.getType()));
                    break;
                }
            }
        }
        return CodeListMap;
    }


    private String getRenameCodeGrp(String codeGrp) {
        return JdbcUtils.convertUnderscoreNameToPropertyName(codeGrp) + "List";
    }

}
