package kr.co.apexsoft.gradnet2.user_api._support.api;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
*
*
외부통신 에러
*/
public class RestTemplateApiException extends CustomException {

    public RestTemplateApiException() {
        super();
    }

    public RestTemplateApiException(String warningMessage) {
        super(warningMessage);
    }

    public RestTemplateApiException(String message, String warningMessage) {
        super(message, warningMessage);
    }

}
