package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplIdSeqProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplicationSerachProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.DocumentProjection;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplSearchCondition;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.MyListResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplPhotoNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplRemakeFailException;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.exception.PdfPageCountException;
import kr.co.apexsoft.gradnet2.user_api.user.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplSysadminService {
    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private UserService userService;

    @NonNull
    private ApplService applService;

    @NonNull
    private CompleteService completeService;


    /**
     * 지원자 id로 지원내역 화면 보기
     *
     * @param userId
     * @return
     */
    @Transactional(readOnly = true)
    public MyListResponse getApplListByUserId(String userId) {
        Long id = userService.findByUserId(userId).getId();

        return applService.findApplList(id);

    }

    /**
     * 전체 지원자 목록 조회
     * @param condition
     * @param pageable
     * @return
     */
    @Transactional(readOnly = true)
    public Page<ApplicationSerachProjection> getApplAllList(ApplSearchCondition condition, Pageable pageable) {
        return applRepository.findApplAllListBySearchProjection(condition.getApplNo(),condition.getSchoolCode(),condition.getEnterYear(),condition.getRecruitPartSeq(),
                condition.getEmail(),condition.getPhoneNumber(),condition.getUserId(),
                condition.getKorName(),condition.getEngName(), condition.getStatus(),pageable);

    }

    /**
     * 수험번호 생성
     * 지원서 재생성
     * 수험표 재생성
     */
    public Page<ApplicationSerachProjection> remakeApplId(Long applNo,ApplSearchCondition condition,  Pageable pageable) {
        Appl appl = applRepository.findById(applNo).get();

        if (!appl.isCompleteStatus()) {
            throw new ApplRemakeFailException("수험번호 재생성 실패.지원완료상태가 아님",appl.getId());
        }
        //수험번호 생성 + 업데이트
        completeService.makeApplId(appl);

        // 지원서 재 생성 + 업로드 + db반영
        completeService.generateApplForm(appl);

        // 수험표 생성 + 업로드 + db반영
        completeService.generateSlipForm(appl);

        //검색조건 목록조회
        return  getApplAllList(condition,pageable);
    }

    /**
     * 지원서 재생성
     * 수험표 재생성
     */
    public Page<ApplicationSerachProjection> remakeApplication(Long applNo,ApplSearchCondition condition,  Pageable pageable) {
        Appl appl = applRepository.findById(applNo).get();

        if (!appl.isCompleteStatus()) {
            throw new ApplRemakeFailException("수험번호 재생성 실패.지원완료상태가 아님",appl.getId());
        }

        // 지원서 재 생성 + 업로드 + db반영
        completeService.generateApplForm(appl);

        // 수험표 생성 + 업로드 + db반영
        completeService.generateSlipForm(appl);

        //검색조건 목록조회
        return  getApplAllList(condition,pageable);
    }
}
