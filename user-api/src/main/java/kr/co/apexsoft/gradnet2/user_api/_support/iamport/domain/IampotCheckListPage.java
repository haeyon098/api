package kr.co.apexsoft.gradnet2.user_api._support.iamport.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class IampotCheckListPage {

    int total;
    int previous;
    int next;
    List<IampotPayment> list;
}
