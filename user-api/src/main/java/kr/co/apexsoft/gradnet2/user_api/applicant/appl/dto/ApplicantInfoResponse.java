package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Country;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
public class ApplicantInfoResponse {
    private String korName;
    private String engName;
    private String email;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate registrationBornDate;
    private String registrationEncr;
    private AddressDto address;
    private CountryDto cntr;
    private String gend;
    private String telNum;
    private String phoneNum;
    private String empCat;

    public void initRegistrationEncr(String registrationEncr) {
        this.registrationEncr = registrationEncr;
    }

    public void initCntr(String id,String name) {
        this.cntr = new CountryDto(id, name);
    }
}
