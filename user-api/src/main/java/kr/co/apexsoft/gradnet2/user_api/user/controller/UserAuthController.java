package kr.co.apexsoft.gradnet2.user_api.user.controller;

import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._config.security.jwt.JwtAuthenticationResponse;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api.user.dto.*;
import kr.co.apexsoft.gradnet2.user_api.user.exception.InvalidUserInfoException;
import kr.co.apexsoft.gradnet2.user_api.user.service.UserFindService;
import kr.co.apexsoft.gradnet2.user_api.user.service.UserRegisterService;
import kr.co.apexsoft.gradnet2.user_api.user.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashSet;

@RestController
@RequiredArgsConstructor
public class UserAuthController {
    @NonNull
    private UserRegisterService userRegisterService;

    @NonNull
    private UserService userService;

    @NonNull
    private UserFindService userFindService;

    private final ApexModelMapper modelMapper;

    /**
     * 일반사용자 회원가입
     *
     * @param signupDto
     * @param errors
     * @return
     */
    @PostMapping("/register/sign-up")
    public ResponseEntity<JwtAuthenticationResponse> registerUser(@Valid @RequestBody SignupDto signupDto,
                                                                  Errors errors) {
        if (errors.hasErrors())
            throw new InvalidUserInfoException(MessageUtil.getMessage("INVALID_USER_INFO"), errors);


        User user = User.builder()
                .userId(signupDto.getUserId())
                .password(signupDto.getPassword())
                .email(signupDto.getEmail())
                .roles(new HashSet<Role>())
                .build();

        User newUser = this.userRegisterService.register(user, signupDto.getRoles(), errors);

        // 회원가입 후 자동로그인 (jwt 토큰 반환)
        return ResponseEntity.ok(this.userRegisterService.login(
                newUser.getUserId(), signupDto.getPassword()));
    }

    /**
     * 아이디, 이메일 중복확인
     *
     * @param type
     * @param id
     * @return
     */
    @GetMapping("/register/check")
    public ResponseEntity<Boolean> isUse(@RequestParam String type,
                                         @RequestParam String id) {
        return ResponseEntity.ok(this.userRegisterService.isUse(type, id));
    }

    /**
     * 이미 가입한 회원이 내정보 수정시 이메일 중복체크: 내 아이디 빼고 조회
     * @param email
     * @param userPrincipal
     * @return
     */
    @GetMapping("/check/email")
    public ResponseEntity<Boolean> isChangeEmail(@RequestParam String email,
                                                 @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(this.userRegisterService.isChangeEmail(email, userPrincipal.getUser().getId()));
    }

    /**
     * 회원가입 후 이메일 인증
     *
     * @param userId
     * @param hash
     * @return
     */
    @GetMapping("/register/confirm")
    public ResponseEntity<Boolean> confirmEmail(@RequestParam String userId,
                                                @RequestParam String hash) {
        return ResponseEntity.ok(this.userRegisterService.confirmRegisterHash(userId, hash));
    }

    /**
     * 로그인
     *
     * @param loginDto
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginDto loginDto) {
        return ResponseEntity.ok(this.userRegisterService.login(loginDto.getUserId(), loginDto.getPassword()));
    }

    /**
     * 캡챠 성공 후처리
     *
     * @param userId
     * @return
     */
    @GetMapping("/captcha/success")
    public ResponseEntity<UserDto> resetPasswordCnt(@RequestParam("userId") String userId) {
        return ResponseEntity.ok(this.userService.unlockUser(userId));
    }

    /**
     * 아이디 찾기
     *
     * @param email
     * @return
     */
    @GetMapping("/find/id")
    public ResponseEntity<String> findUserId(@RequestParam String email) {
        return ResponseEntity.ok(this.userFindService.findUserId(email));
    }

    /**
     * 비밀번호 찾기
     *
     * @param userId
     * @param email
     * @return
     */
    @GetMapping("/find/password")
    public void findPassword(@RequestParam String userId, @RequestParam String email) {
        this.userFindService.findPassword(userId, email);
    }

    /**
     * 비밀번호 변경 이메일 hash 값 비교
     *
     * @param userId
     * @param hash
     * @return
     */
    @GetMapping("/find/check/hash")
    public ResponseEntity<Boolean> checkHash(@RequestParam String userId, @RequestParam String hash) {
        return ResponseEntity.ok(this.userFindService.checkPasswordModifyHash(userId, hash));
    }

    /**
     * 이메일 인증 후 비밀번호 변경
     *
     * @param changePwdDto
     * @return
     */
    @PostMapping("/find/change/password")
    public ResponseEntity<ChangePwdDto> changePassword(@Valid @RequestBody ChangePwdDto changePwdDto) {
        User user = userRegisterService.changePassword(changePwdDto.getUserId(), changePwdDto.getPassword());
        return ResponseEntity.ok(modelMapper.convert(user, ChangePwdDto.class));
    }

    /**
     * 비밀번호 일치 여부 확인 - 내 정보 변경 전
     *
     * @param password
     * @param userPrincipal
     */
    @GetMapping("/confirm/password")
    public Boolean confirmPassword(@RequestParam String password, @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return userRegisterService.confirmPassword(userPrincipal.getUserId(), password);
    }

    /**
     * 접속한 유저의 이메일 가져오기
     *
     * @param userPrincipal
     * @return
     */
    @GetMapping("/email")
    public ResponseEntity<String> getEmail(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(userPrincipal.getUser().getEmail());
    }

    /**
     * 이메일 또는 비밀번호 변경
     *
     * @param userInfoChangeDto
     */
    @PutMapping("/user")
    public ResponseEntity<UserInfoChangeDto> changeInfo(@Valid @RequestBody UserInfoChangeDto userInfoChangeDto,
                           Errors errors,
                           @AuthenticationPrincipal UserPrincipal userPrincipal) {
        if (errors.hasErrors())
            throw new InvalidUserInfoException(MessageUtil.getMessage("INVALID_USER_INFO"), errors);

        User user = userRegisterService.changeInfo(userPrincipal.getUser(), userInfoChangeDto.getEmail(), userInfoChangeDto.getPassword(), errors);
        return ResponseEntity.ok(modelMapper.convert(user, UserInfoChangeDto.class));
    }

    /**
     * 회원 탈퇴하기
     *
     * @param userPrincipal
     * @return
     */
    @PutMapping("/withdraw")
    public ResponseEntity<Boolean> withdrawUser(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(userService.withdrawUser(userPrincipal.getUser()));
    }
}
