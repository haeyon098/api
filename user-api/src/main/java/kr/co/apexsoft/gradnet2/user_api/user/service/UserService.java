package kr.co.apexsoft.gradnet2.user_api.user.service;

import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api.user.dto.UserDto;
import kr.co.apexsoft.gradnet2.user_api.user.exception.UserNotFoundException;
import kr.co.apexsoft.gradnet2.entity.user.repository.UserRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final ApexModelMapper modelMapper;

    @Transactional(readOnly = true)
    public User findById(@NonNull Long id) {
        return this.userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException(MessageUtil.getMessage("USER_NOT_FOUND"), id));
    }

    @Transactional(readOnly = true)
    public User findByUserId(@NonNull String userId) {
        return this.userRepository.findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(userId, MessageUtil.getMessage("USER_NOT_FOUND")));
    }

    @Transactional
    public UserDto unlockUser(String userId) {
        User user = this.userRepository.findByUserId(userId)
                .orElseThrow(() -> new UserNotFoundException(userId, MessageUtil.getMessage("USER_NOT_FOUND")));
        user.unlock();
        return modelMapper.convert(user, UserDto.class);
    }

    @Transactional(readOnly = true)
    public Page<User> findUserAll(String userId, Pageable pageable) {
        return userRepository.findAllByUserIdContaining(userId, pageable);
    }

    public Boolean withdrawUser(User user) {
        user.withdrawUser();

        return userRepository.save(user).checkWithdrawUser();

    }
}
