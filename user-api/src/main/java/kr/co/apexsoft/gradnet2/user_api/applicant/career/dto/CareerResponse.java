package kr.co.apexsoft.gradnet2.user_api.applicant.career.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-14
 */
@Getter
public class CareerResponse {
    private Long id;

    private Integer seq;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate joinDay;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate retireDay;

    private Boolean currently; //현재 재직여부

    private String name;

    private String compTypeCode;

    private String address;

    private String department;

    private String position;

    private String description;
}
