package kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto;

import lombok.Getter;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-27
 */
@Getter
public class DocumentRequest {
    private Long id;

    private Long groupNo;

    private String groupCode;

    private String docItemCode;

    private String docItemName;

    private String prefix;

    private Integer seq;
}
