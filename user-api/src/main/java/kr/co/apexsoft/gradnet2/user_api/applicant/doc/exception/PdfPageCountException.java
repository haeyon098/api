package kr.co.apexsoft.gradnet2.user_api.applicant.doc.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
*
*
pdf파일 페이지수
*/
public class PdfPageCountException extends CustomException {
    public PdfPageCountException(String warningMessage, Long applNo) {
        super("[applNo:" + applNo + "]",warningMessage);
    }

}
