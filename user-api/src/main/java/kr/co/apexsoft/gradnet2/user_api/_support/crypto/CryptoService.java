package kr.co.apexsoft.gradnet2.user_api._support.crypto;

public interface CryptoService {
    String encrypt(String plainText);

    String decrypt(String ciphertext);

}
