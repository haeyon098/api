package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-03-03
 */
@Getter
public class RecommendProfResponse {

    private Long id;

    private String courseEngName;

    private String majorEngName;

    private String engCntr;

    private String profName;

    private String stauts;

    private String engName;

    @JsonFormat(pattern="yyyy-MM-dd")
    private String recrPartEndDate;

    private String filePath;

    private String originFileName;

    private String schoolCode;


    public void initFile(String filePath, String originFileName) {
        this.filePath = filePath;
        this.originFileName = originFileName;
    }
}
