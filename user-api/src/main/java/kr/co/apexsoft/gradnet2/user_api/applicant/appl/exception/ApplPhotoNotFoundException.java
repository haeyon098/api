package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 * 증명사진 없을경우
 * @author
 * @since 2020-02-14
 */
public class ApplPhotoNotFoundException extends CustomException {

    public ApplPhotoNotFoundException() {
        super();
    }

    public ApplPhotoNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public ApplPhotoNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public ApplPhotoNotFoundException(String warningMessage, Long applNo) {
        super("[ applNo: "+applNo+" ]", warningMessage);
    }
}
