package kr.co.apexsoft.gradnet2.user_api.applpart.controller;

import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.ApplPartLanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.DocGrpDocResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.service.ApplPartService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-06
 */
@RestController
@RequestMapping("/appl/part")
@RequiredArgsConstructor
public class ApplPartController {
    @NonNull
    private ApplPartService applPartService;

    /**
     * 첨부파일 제출 목록 조회
     *
     * @param applNo
     * @return
     */
    @GetMapping("/documents/{applNo}")
    public ResponseEntity<List<DocGrpDocResponse>> findDocumentList(@PathVariable("applNo") Long applNo) {
        return ResponseEntity.ok(applPartService.findDocumentList(applNo));
    }

    /**
     * 어학 목록조회
     *
     * @param applNo
     * @return
     */
    @GetMapping("/language/{applNo}")
    public  ResponseEntity<List<ApplPartLanguageResponse>> findLanguageList(@PathVariable("applNo") Long applNo) {
        return ResponseEntity.ok(applPartService.getLanguageList(applNo));
    }
}
