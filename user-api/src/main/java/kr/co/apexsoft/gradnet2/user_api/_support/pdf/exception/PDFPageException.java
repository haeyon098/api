package kr.co.apexsoft.gradnet2.user_api._support.pdf.exception;


/**
 * PDF 파일 익셉션
 */
public class PDFPageException extends PDFCommException {
    public PDFPageException(String warningMessage, Long applNo,String fileName) {
        super(warningMessage,applNo,fileName);
    }
}
