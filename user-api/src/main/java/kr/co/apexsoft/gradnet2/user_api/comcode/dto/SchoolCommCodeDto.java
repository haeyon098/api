package kr.co.apexsoft.gradnet2.user_api.comcode.dto;

import lombok.Getter;

@Getter
public class SchoolCommCodeDto extends CommCodeDto {

    /*private SchoolCommRequest schoolCommRequest;*/
   /*모집학교코드*/
    private String schoolCode;

    /*입학연도*/
    private String enterYear;

    /*모집회차*/
    private Integer recruitPartSeq;
}
