package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 * 지원완료 처리 FAIL
 * @author
 * @since 2020-02-14
 */
public class ApplCompleteFailException extends CustomException {

    public ApplCompleteFailException(String warningMessage, Long applNo) {
        super("[ applNo: "+applNo+" ]", warningMessage);
    }
}
