package kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto;

import kr.co.apexsoft.gradnet2.user_api.applpart.dto.DocGrpDocResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class DocumentDeleteResponse {
    String status;
    List<DocGrpDocResponse> list;
}
