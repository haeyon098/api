package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import lombok.Getter;

/**
 * 수험표 파일 관련 정보
 *
 */
@Getter
public class ApplSlipFileInfo extends  AppBirtFileInfo{
    @Override
    public String getBirtFileName() {
        return FilePathUtil.getSlipBirtFileName(this.getPartSchoolCode(),this.getPartAdmissionCode());
    }
    @Override
    public String getFileName() {
        return FilePathUtil.getSlipFileName(this.getPartEnterYear(), this.getPartMajorCode(), this.getApplicantUserId());
    }
}
