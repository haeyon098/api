package kr.co.apexsoft.gradnet2.user_api._support.pdf.exception;


/**
 * PDF 파일에 페이지 넘버링을 할 수 없을 때 발생
 */
public class PDFPageInsertException extends PDFCommException {
    public PDFPageInsertException(String warningMessage, Long applNo,String fileName) {
        super(warningMessage,applNo,fileName);
    }
}
