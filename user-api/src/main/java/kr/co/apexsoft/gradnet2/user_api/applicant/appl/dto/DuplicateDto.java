package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.role.domain.RoleType;
import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.Set;

@Getter
public class DuplicateDto {

    private String schoolCode;

    private String enterYear;

    private Integer recruitPartSeq;

    private Set<Role> roles;

    //FIXME: 추가상태 필요시 Appl.Status 로 수정필요 (현재는 지원사항저장 단계 여부만 체크함)
    private boolean applPartSaveStatus = false;

    private String admissionCode;
    private boolean isGeneral;

    private Registration registration;
    private Long applPartNo;
    private Long userNo;

    public DuplicateDto(Appl appl, User user, boolean applPartSaveStatus) {
        this.schoolCode = appl.getPart().getSchoolCode();
        this.enterYear = appl.getPart().getEnterYear();
        this.recruitPartSeq = appl.getPart().getRecruitPartSeq();
        this.roles = user.getRoles();
        this.userNo = user.getId();
        this.applPartSaveStatus = applPartSaveStatus;
        this.applPartNo = appl.getPart().getApplPartNo();
    }


    public DuplicateDto(Appl appl, Set<Role> roles, boolean applPartSaveStatus) {
        this.schoolCode = appl.getPart().getSchoolCode();
        this.enterYear = appl.getPart().getEnterYear();
        this.recruitPartSeq = appl.getPart().getRecruitPartSeq();

        this.roles = roles;
        this.userNo = appl.getApplicant().getId();
        this.applPartSaveStatus = applPartSaveStatus;

        this.applPartNo = appl.getPart().getApplPartNo();
        this.isGeneral = appl.isGeneral();
        this.admissionCode = appl.getPart().getAdmissionCode();
    }

    public DuplicateDto(boolean isGen, Appl appl) {
        if (isGen) {
            this.registration = new Registration(appl.getApplicantInfo().getRegistrationBornDate(), appl.getApplicantInfo().getRegistrationEncr(), false);
        }

    }

    public void setRegistration(DuplicateDto duplicateDto) {
        this.registration = duplicateDto.getRegistration();
    }

    public void setRegistration(Appl appl, boolean isDataEncr) {
        if (appl.isGeneral()) {
            this.registration = new Registration(appl.getApplicantInfo().getRegistrationBornDate(), appl.getApplicantInfo().getRegistrationEncr(), isDataEncr);
        }
    }

    public boolean isAllowRole() {
        return this.getRoles().stream()
                .anyMatch(role -> (role.getType().equals(RoleType.ROLE_SYS_ADMIN))); //권한 ROLE 생긴다면 추가필요
    }

    @Getter
    @AllArgsConstructor
    public class Registration {
        private LocalDate registrationBornDate;
        private String registrationEncr;
        private boolean isDataEncr;
    }
}
