package kr.co.apexsoft.gradnet2.user_api.applicant.doc.service;

import io.micrometer.core.instrument.util.StringUtils;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.EtcDocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.applpart.projection.DocumentsResponse;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRepository;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileResponse;
import kr.co.apexsoft.gradnet2.user_api._support.pdf.PDFPageService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentDeleteResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentPathNameResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentSaveDto;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.EtcDocumentDeleteResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.exception.PdfPageCountException;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.DocGrpDocResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.service.ApplPartService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.print.Doc;
import java.io.File;
import java.util.*;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-19
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class DocumentService {

    @NonNull
    private FileRepository fileRepository;

    @NonNull
    private DocumentRepository documentRepository;

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private PDFPageService pdfPageService;

    @NonNull
    private ApplPartService applPartService;

    @NonNull
    private ApplService applService;

    private final ApexModelMapper modelMapper;


    @Value("${file.maxImgSize}")
    private String maxImgSize;

    @Value("${file.maxPdfSize}")
    private String maxPdfSize;

    @Value("${file.maxPage}")
    private String maxPage;

    /**
     * 지원자 파일 업로드
     *
     * @param applNo
     * @param fileRequest
     * @param prefix
     * @param document
     * @param roles
     * @return
     */
    private FileResponse uploadAndSave(Long applNo, FileRequest fileRequest, String prefix, Document document,
                                      Set<Role> roles) {
        Appl appl = applService.checkApplSave(applNo, Appl.Status.FILE_SAVE, roles);

        Boolean isPdf = fileRequest.getFileExt().equalsIgnoreCase("pdf");
        Integer pageCnt = null;


        // 사이즈는 파일 객체 만들때 전역으로 체크됨
        //페이지 체크
        if (isPdf) {
            pageCnt = checkPdfNumbering(fileRequest.getFile(), appl.getId());
        }

        ApplPart part = appl.getPart();

        // 업로드 파일경로
        fileRequest.setCustomFileDirPath(FilePathUtil.getS3UploadDirectrotyFullPath(part.getSchoolCode(),
                part.getEnterYear(), part.getRecruitPartSeq(), applNo));
        fileRequest.setCustomFileName(FilePathUtil.getCustomFileName(prefix, fileRequest.getOriginalFileName()));

        FileResponse fileResponse = null;

        try {
            fileResponse = fileRepository.upload(fileRequest);
        } catch (Exception e) {
            throw e;
        } finally {
            fileRequest.getFile().delete();
        }

        document.changeDocumentInfo(appl, fileRequest.getFileKey(), pageCnt, isPdf);
        Document saveDocument = saveDocument(document);
        fileResponse.setApplDocNo(saveDocument.getId());

        return fileResponse;
    }

    public List<DocGrpDocResponse> uploadDocument(Long applNo, FileRequest fileRequest, String prefix, Document document,
                                                  Set<Role> roles) {
        uploadAndSave(applNo, fileRequest, prefix, document, roles);
        return applPartService.findDocumentList(applNo);
    }

    public List<Document> uploadEtcDocumnet(Long applNo, FileRequest fileRequest, String prefix, Document document,
                                            Set<Role> roles) {
        uploadAndSave(applNo, fileRequest, prefix, document, roles);
        return findEtcDocuments(applNo);
    }


    /**
     * 페이지 수 제한 체크
     * 모든 페이지에 대한 넘버링을 테스트 할 필요가 있다 판단
     * 전체 페이지에 대한 넘버링 테스트 수행
     *
     * @param file
     * @param applNo
     * @return
     */
    private Integer checkPdfNumbering(File file, Long applNo) {
        Integer pageCnt = pdfPageService.getPdfPageCount(file, applNo);
        if (pageCnt > Integer.parseInt(maxPage)) {
            throw new PdfPageCountException(MessageUtil.getMessage("MAX_PDF_PAGE_COUNT_FAIL", new Object[]{maxPage}), applNo);
        }
        return pageCnt;
    }

    private Document saveDocument(Document document) {
        documentRepository.deleteByAppl_IdAndGroupNoAndGroupCodeAndItemCodeAndSeq(document.getAppl().getId(),
                document.getGroupNo(), document.getGroupCode(), document.getItemCode(), document.getSeq());
        return documentRepository.save(document);
    }

    public DocumentDeleteResponse deleteDocument(Long applNo, Long applDocNo, String fileKey, Set<Role> roles) {
        Appl appl = delete(applNo, applDocNo, fileKey, roles);
        return new DocumentDeleteResponse(appl.getStatus().name(), applPartService.findDocumentList(applNo)) ;
    }

    public EtcDocumentDeleteResponse deleteEtcDocument(Long applNo, Long applDocNo, String fileKey, Set<Role> roles) {
        Appl appl = delete(applNo, applDocNo, fileKey, roles);
        List<Document> list =  findEtcDocuments(applNo);
        return new EtcDocumentDeleteResponse(appl.getStatus().name(), modelMapper.convert(list,
                new TypeToken<List<EtcDocumentProjection>>(){}.getType()));
    }

    private Appl delete(Long applNo, Long applDocNo, String fileKey, Set<Role> roles) {
        Appl appl = applService.checkApplSave(applNo, Appl.Status.FILE_SAVE, roles);

        documentRepository.deleteById(applDocNo);
        fileRepository.delete(fileKey);

        appl.checkAndRollbackOneStep();
        return appl;
    }

    @Transactional(readOnly = true)
    public List<Document> findEtcDocuments(Long applNo) {
        return documentRepository.findByAppl_IdAndGroupCode(applNo, DocGrpType.ETC.getValue());
    }

    /**
     * gradnet에서 만든 파일 업로드
     * 서버에서 삭제
     * db 저장
     *
     * @param fileRequest
     * @param appl
     * @param docGrp
     * @param docItem
     * @return
     */
    public Document uploadAndSaveGradnetCustomFile(FileRequest fileRequest, Appl appl, String docGrp, Long groupNo, String docItem, boolean isPdf) {
        Integer pdfPageCount = null;
        FileResponse fileResponse = null;
        try {
            if (isPdf) {
                pdfPageCount = pdfPageService.getPdfPageCount(fileRequest.getFile(), appl.getId()); //페이지 수
            }
            fileResponse = fileRepository.upload(fileRequest); //  업로드
        } catch (Exception e) {
            throw e;
        } finally {
            if (fileRequest.getFile().exists()) {
                fileRequest.getFile().delete(); // 서버에서 파일 삭제
            }

        }
        Document document = new Document(appl, docGrp, groupNo, docItem,
                pdfPageCount, fileResponse.getFileKey(), fileResponse.getOriginalFileName(), isPdf);

        //기존  파일 삭제
        if(docItem != DocItem.EMAIL_REC.getValue()) {
            documentRepository.deleteByAppl_IdAndGroupCodeAndItemCode(appl.getId(), docGrp, docItem);
        }

        return documentRepository.save(document);
    }

    public DocumentSaveDto documentSaveStatus(Long applNo, Set<Role> roles) {
        Appl appl = applService.checkApplSave(applNo, Appl.Status.FILE_SAVE, roles);
        List<DocGrpDocResponse> list = applPartService.findDocumentList(applNo);
        Boolean checkedRequired = true;

        // required 체크
        out:
        for (DocGrpDocResponse docGrp : list) {
            if (org.springframework.util.StringUtils.isEmpty(docGrp.getSubGrp())) {
                checkedRequired = checkRequiredDocuments(docGrp);
                if (!checkedRequired) break out;
            } else {
                for (DocGrpDocResponse subGrp : docGrp.getSubGrp()) {
                    checkedRequired = checkRequiredDocuments(subGrp);
                    if (!checkedRequired) break out;
                }
            }
        }

        if (checkedRequired)
            appl.changeStatus(Appl.Status.FILE_SAVE);

        List<EtcDocumentProjection> etcList = modelMapper.convert(findEtcDocuments(applNo), new TypeToken<List<EtcDocumentProjection>>(){}.getType());
        return new DocumentSaveDto(appl.getStatus(), list, etcList);
    }

    private Boolean checkRequiredDocuments(DocGrpDocResponse group) {
        for (DocumentsResponse document : group.getDocuments()) {
            if (document.getRequired().equals("Y") && StringUtils.isEmpty(document.getApplDocNo())) {
                return false;
            }
        }
        return true;
    }

    public void documentDeleteByGroupCode(Long applNo, Long[] groupNo, String... groupCode) {
        String[] fileKeys;
        Optional<List<Document>> list = documentRepository.findByAppl_IdAndGroupCodeInAndGroupNoIn(applNo, groupCode, groupNo);

        if (list.isPresent()) {
            Object[] keys = list.get().stream().map(d -> d.getFilePath()).toArray();
            fileKeys = Arrays.asList(keys).toArray(new String[keys.length]);

            documentRepository.deleteByAppl_IdAndGroupCodeInAndGroupNoIn(applNo, groupCode, groupNo);
            fileRepository.deleteObjects(fileKeys);
        }
    }

    public DocumentPathNameResponse findDocumentFileInfo(String docItemType, Long applNo) {
        Document dbDoc;
        DocumentPathNameResponse documentResponse = new DocumentPathNameResponse();

        switch (docItemType) {
            case "APPL_FORM":
                dbDoc = documentRepository.findByItemCodeAndAppl_Id(DocItem.APPL_FORM.getValue(), applNo).get();
                documentResponse.setFilePath(dbDoc.getFilePath());
                documentResponse.setOriginFileName(dbDoc.getOriginFileName());
                break;

            case "SLIP":
                dbDoc = documentRepository.findByItemCodeAndAppl_Id(DocItem.SLIP.getValue(), applNo).get();
                documentResponse.setFilePath(dbDoc.getFilePath());
                documentResponse.setOriginFileName(dbDoc.getOriginFileName());
                break;

            case "ZIP":
                documentResponse = this.makeDocumentForZip(applNo);
                break;
        }
        return documentResponse;
    }

    private DocumentPathNameResponse makeDocumentForZip(Long applNo) {
        Optional<Appl> dbAppl = applRepository.findById(applNo);
        DocumentPathNameResponse documentResponse = new DocumentPathNameResponse();

        String dbSchoolCode = dbAppl.get().getPart().getSchoolCode();
        String dbEnterYear = dbAppl.get().getPart().getEnterYear();
        Integer dbRecruitPartSeq = dbAppl.get().getPart().getRecruitPartSeq();
        Long dbApplNo = dbAppl.get().getId();
        String dbMajCode = dbAppl.get().getPart().getMajorCode();
        String dbUserId = dbAppl.get().getApplicant().getUserId();

        String filePath = FilePathUtil.getS3ZipDownFileKey(dbSchoolCode, dbEnterYear, dbRecruitPartSeq, dbApplNo, dbMajCode, dbUserId);
        String originFileName = FilePathUtil.getZippedFileName(dbEnterYear, dbMajCode, dbUserId);

        documentResponse.setFilePath(filePath);
        documentResponse.setOriginFileName(originFileName);

        return documentResponse;
    }
}
