package kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-20
 */
@Getter
public class LanguageResquest {

    @NotNull
    private String nodeId;

    private Long id;

    @NotNull
    private Long applId;

    @NotNull
    private Language.LangType type;

    @NotNull
    private Boolean isExam;

    @NotBlank
    private String infoTypeCode;


    private String detailGrp;

    private String detailCode;

    private LocalDate examDay;

    private LocalDate expirationDay;

    private String score;

    @NotNull
    private Boolean mandatory;

    @NotNull
    private Boolean uploaded;
}
