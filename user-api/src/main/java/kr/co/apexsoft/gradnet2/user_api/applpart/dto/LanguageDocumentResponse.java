package kr.co.apexsoft.gradnet2.user_api.applpart.dto;

import kr.co.apexsoft.gradnet2.entity.applpart.projection.DocumentsResponse;

public class LanguageDocumentResponse implements DocumentsResponse {
    private String groupCode;
    private String code;
    private String korName;
    private String engName;
    private String applDocNo;
    private String fileName;
    private String docGrpNo;
    private String filePath;
    private String msgNo;
    private String korMsg;
    private String engMsg;
    private String required;


    @Override
    public String getGroupCode() {
        return this.groupCode;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getKorName() {
        return this.korName;
    }

    @Override
    public String getEngName() {
        return this.engName;
    }

    @Override
    public String getApplDocNo() {
        return this.applDocNo;
    }

    @Override
    public String getFileName() {
        return this.fileName;
    }

    @Override
    public String getDocGrpNo() {
        return this.docGrpNo;
    }

    @Override
    public String getFilePath() {
        return this.filePath;
    }

    @Override
    public String getRequired() {
        return this.required;
    }

    @Override
    public String getMsgNo() {
        return this.msgNo;
    }

    @Override
    public String getKorMsg() {
        return this.korMsg;
    }

    @Override
    public String getEngMsg() {
        return this.engMsg;
    }

    public LanguageDocumentResponse(DocumentsResponse documentsResponse, String required) {
        this.groupCode = documentsResponse.getGroupCode();
        this.code = documentsResponse.getCode();
        this.korName = documentsResponse.getKorName();
        this.engName = documentsResponse.getEngName();
        this.applDocNo = documentsResponse.getApplDocNo();
        this.fileName = documentsResponse.getFileName();
        this.docGrpNo = documentsResponse.getDocGrpNo();
        this.filePath = documentsResponse.getFilePath();
        this.required = required;
        this.msgNo = documentsResponse.getMsgNo();
        this.korMsg = documentsResponse.getKorMsg();
        this.engMsg = documentsResponse.getEngMsg();
    }
}
