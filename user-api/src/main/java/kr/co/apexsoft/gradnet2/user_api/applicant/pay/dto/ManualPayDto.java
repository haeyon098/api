package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-10-23
 */
@Getter
public class ManualPayDto {
    @NotNull
    private Long applId;

    @NotBlank
    private String schoolCode;
    @NotBlank
    private String enterYear;
    @NonNull
    private Integer recruitPartSeq;

    @NotBlank
    private String pgType;
    @NotBlank
    private String payType;
    @NotBlank
    private String currency;
    @NotNull
    private Double amount;

    private String buyerName;
    private String buyerEmail;
    private String buyerTelNum;

    @NotBlank
    private String description;
}
