package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.BizException;
import lombok.Getter;

import java.util.HashMap;

/**
 * 저장시 appl status 체크
 * 작성완료상태
 * @author 김효숙
 */
@Getter
public class ApplSaveStatusCompleteBizException extends BizException {
    public ApplSaveStatusCompleteBizException(String warningMessage, Long applNo, HashMap<String,Object> returnMap) {
        super(warningMessage, "applNo:" + applNo,returnMap);
    }

    public ApplSaveStatusCompleteBizException(String warningMessage, Long applNo) {
        super(warningMessage, "applNo:" + applNo);
    }

}

