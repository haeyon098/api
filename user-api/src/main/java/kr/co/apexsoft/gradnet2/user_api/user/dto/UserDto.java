package kr.co.apexsoft.gradnet2.user_api.user.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-25
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;

    private String userId;

    private String email;

    private Status status = Status.ACTIVE;  // 사용자 상태 코드

    private Boolean locked = false; // 계정 잠김

    public enum Status {
        ACTIVE, INACTIVE, WITHDRAW
    }
}
