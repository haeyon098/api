package kr.co.apexsoft.gradnet2.user_api._common;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;


public class AssertException extends CustomException {

    public AssertException() {
        super();
    }

    public AssertException(String warningMessage) {
        super(warningMessage);
    }

    public AssertException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
