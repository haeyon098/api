package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
public class ForeignerInfoResponse {
    private String foreignerTypeCode;
    private String foreignerRegistrationNo;
    private String passportNo;
    private String visaNo;
    private String visaTypeCode;
    private String visaTypeEtc;
    @JsonFormat(pattern="yyyy-MM-dd")
    private LocalDate visaExprDay;
    private String homeAddress;
    private String homeTelNum;
    private String korEmergencyName;
    private String korEmergencyRelation;
    private String korEmergencyTel;
    private String homeEmergencyName;
    private String homeEmergencyTel;
    private String homeEmergencyRelation;
    private Boolean overseasKorean;
    private String skype;
    private String residencyType;
    private String videoEssay;
    private String firstName;
    private String middleName;
    private String lastName;

    public void encryptForeignerInfo(String passportNo, String visaNo, String foreignerRegistrationNo) {
        this.passportNo = passportNo;
        this.visaNo = visaNo;
        this.foreignerRegistrationNo = foreignerRegistrationNo;
    }
}


