package kr.co.apexsoft.gradnet2.user_api.applpart.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import kr.co.apexsoft.gradnet2.entity.applicant.career.projection.CareerProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartLanguage;
import kr.co.apexsoft.gradnet2.entity.comcode.domain.SysCommCode;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 지원단위별 어학목록
 * 화면설계에 맞춰 지정
 *
 * @author 김효숙
 * @since 2020-07-03
 */

@Getter
@AllArgsConstructor
public class ApplPartLanguageResponse {
    private String id;
    private Integer level;
    private String krName;
    private String enName;
    private ApplPartLanguage.OptionType optionType;
    private Boolean isLeaf;


    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<ApplPartLanguageResponse> subNode;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private LeafInfoOut leafInfo;


    @Getter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class LeafInfoOut {
        private Language.LangType langType;
        private Boolean isExam;
        private String infoTypeCode;
        private String detailGrpCode;

        private Boolean mandatory;
        private Boolean uploaded;

        private CodeNameOut msg;

        @JsonInclude(JsonInclude.Include.NON_EMPTY)
        private List<CodeNameOut> details;

        private Long id;    // applLangNo
        private String detailCode;

        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDate examDay;

        @JsonFormat(pattern = "yyyy-MM-dd")
        private LocalDate expirationDay;

        private String score;
        private Boolean valueChecked =false;


        public LeafInfoOut(ApplPartLanguage language, List<SysCommCode> details, List<Language> languageResponses) {
            this.langType = language.getType();
            this.isExam = language.getIsExam();
            this.infoTypeCode = language.getInfoTypeCode();
            this.detailGrpCode = language.getDetailGrpCode();
            this.mandatory = language.getMandatory();
            this.uploaded = language.getUploaded();
            this.msg = (language.getMsgKor() != null) ? new CodeNameOut(language.getMgsNo(), language.getMsgKor(), language.getMsgEng()) : null;
            this.details = details.stream().map(code -> new CodeNameOut(code.getCode(), code.getCodeKrValue(), code.getCodeEnValue())).collect(Collectors.toList());
            languageResponses.stream()
                    .filter(languageResponse ->
                            language.getInfoTypeCode().equals(languageResponse.getInfoTypeCode()) && language.getType().equals(languageResponse.getType())).findFirst().ifPresent(
                    l -> {
                        this.valueChecked=true;
                        this.detailCode = l.getDetailCode();
                        this.examDay = l.getExamDay();
                        this.expirationDay = l.getExpirationDay();
                        this.score = l.getScore();
                        this.id = l.getId();
                    }
            );


        }

    }

    @Getter
    @AllArgsConstructor
    static class CodeNameOut {
        private String code;
        private String codeKrValue; //프론트  컴포넌트필요 이름 통일
        private String codeEnValue;

    }


}
