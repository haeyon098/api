package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 * 중복지원 실패
 * @author
 * @since 2020-08-06
 */
public class DuplicateException extends CustomException {

    public DuplicateException(String warningMessage) {
        super( warningMessage);
    }
}
