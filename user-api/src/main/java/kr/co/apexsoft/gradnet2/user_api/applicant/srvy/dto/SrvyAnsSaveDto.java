package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
* 
*
* @author aran
* @since 2020-03-05
*/
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SrvyAnsSaveDto {
    private String recruitSchoolCode;
    private Appl appl;
    private Boolean answered;
    private LocalDateTime submitDate;
}
