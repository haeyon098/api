package kr.co.apexsoft.gradnet2.user_api._support.exception;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;

/**
 * created by hanmomhanda@gmail.com
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApiError {

    @NonNull
    private HttpStatus status;

    @NonNull
    private String timestamp;

    private String exceptionName;

    private String debugMessage;

    private String warningMessage = MessageUtil.getMessage("SERVER_FAIL");

    private List<FieldError> fieldErrors;

    public ApiError(@NonNull HttpStatus status, @NonNull String timestamp, String debugMessage,
                    String warningMessage, String exceptionName) {
        this.status = status;
        this.timestamp = timestamp;
        this.debugMessage = debugMessage;
        this.warningMessage = warningMessage;
        this.exceptionName = exceptionName;
    }

    public ApiError(@NonNull HttpStatus status, @NonNull String timestamp, String debugMessage,
                    String exceptionName) {
        this.status = status;
        this.timestamp = timestamp;
        this.debugMessage = debugMessage;
        this.exceptionName = exceptionName;
    }

    public void setDebugMessage(String debugMessage) {
        this.debugMessage = debugMessage;
    }

    public void setFieldErrors(List<FieldError> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}
