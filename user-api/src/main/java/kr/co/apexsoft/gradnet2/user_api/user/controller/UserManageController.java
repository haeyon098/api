package kr.co.apexsoft.gradnet2.user_api.user.controller;

import kr.co.apexsoft.gradnet2.entity.user.domain.User;
import kr.co.apexsoft.gradnet2.user_api.user.service.UserService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-05-17
 */
@RestController
@RequestMapping("/sysadmin/users")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserManageController {
    @NonNull
    private UserService userService;

    @GetMapping
    public ResponseEntity<Page<User>> findUserAll(@RequestParam(required = false) String userId,
                                                  @PageableDefault Pageable pageable) {
        return ResponseEntity.ok(userService.findUserAll(userId, pageable));
    }
}
