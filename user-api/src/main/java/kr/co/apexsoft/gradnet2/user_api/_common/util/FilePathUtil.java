package kr.co.apexsoft.gradnet2.user_api._common.util;

import kr.co.apexsoft.gradnet2.entity.adms.domain.Adms;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.user_api._support.zip.domain.ZipInfo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class FilePathUtil {

    private static final String SLASH_WINDOW = "/";
    private static final String SLASH_UNIX = "\\";
    private static final String DOUBLE_SLASH = "//";
    private static final String ENCODED_AMP = "&amp;";
    private static final String RECOVERED_AMP = "&";
    private static final String ENCODED_SINGLEQUOTE = "&#39;";
    private static final String RECOVERED_SINGLEQUOTE = "'";

    private FilePathUtil() {
    }

    // apexTest용 파일이름
    public static String getApexTestFileName(String originalFileName) {
        return new StringBuilder()
                .append("apex_sample_file_").append(originalFileName).toString();
    }

    // apexTest용 폴더path
    public static String getUploadApexTestFileDirPath(String key) {
        return new StringBuilder()
                .append("apex/").append(key).toString();
    }

    /**
     * 버트로 만든 파일 만들어지는 경로
     *
     * @param baseDir
     * @param schoolCode
     * @param enterYear
     * @param recruitPartSeq
     * @param applNo
     * @return
     */
    public static String getMakeDirectoryFullPath(String baseDir,
                                                  String schoolCode, String enterYear, Integer recruitPartSeq,
                                                  Long applNo) {
        return new StringBuilder()
                .append(baseDir).append("/")
                .append(schoolCode).append("/")
                .append(enterYear).append("/")
                .append(recruitPartSeq).append("/")
                .append(String.valueOf(applNo))
                .toString();
    }

    /**
     * S3 업로드 경로
     *
     * @param schoolCode
     * @param enterYear
     * @param recruitPartSeq
     * @param applNo
     * @return
     */
    public static String getS3UploadDirectrotyFullPath(String schoolCode, String enterYear, Integer recruitPartSeq,
                                                       Long applNo) {
        return new StringBuilder()
                .append(schoolCode).append("/")
                .append(enterYear).append("/")
                .append(recruitPartSeq).append("/")
                .append(String.valueOf(applNo))
                .toString();
    }

    /**
     * 지원자 제출서류 file dir path
     *
     * @param key
     * @return
     */
    public static String getDocumentUploadFileDirPath(String key) {
        return new StringBuilder().append(key).toString();
    }

    public static String getCustomFileName(String prefix, String originalFileName) {
        return new StringBuilder()
                .append(prefix).append("-")
                .append(originalFileName).toString();
    }


    public static String removeSeparators(String rawOriginalFileName) {
        int index = rawOriginalFileName.lastIndexOf(SLASH_UNIX);
        String tmpFileName = rawOriginalFileName.substring(index + 1, rawOriginalFileName.length());

        index = tmpFileName.lastIndexOf(SLASH_WINDOW);

        return tmpFileName.substring(index + 1, tmpFileName.length());

    }

    /**
     * localdatetime 을 시분초 String 으로 반환
     * 파일경로에 자주 사용되는 패턴
     *
     * @return
     */
    public static String getUploadDateTime() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
    }

    public static String recoverAmpersand(String path) {
        return path.replace(ENCODED_AMP, RECOVERED_AMP)
                .replace(ENCODED_AMP, RECOVERED_AMP)
                .replace(ENCODED_AMP, RECOVERED_AMP);
    }

    public static String recoverSingleQuote(String path) {
        return path.replace(ENCODED_SINGLEQUOTE, RECOVERED_SINGLEQUOTE);
    }


    public static String getZippedFileName(String enterYear, String majCode, String userId) {
        return new StringBuilder()
                .append(getFileNamePreFix(enterYear, majCode, userId, "zip"))
                .toString();

    }



    public static String getApplFormFileName(String enterYear, String majCode, String userId) {
        return new StringBuilder()
                .append("application")
                .append("_")
                .append(getFileNamePreFix(enterYear, majCode, userId, "pdf"))
                .toString();

    }

    public static String getSlipFileName(String enterYear, String majCode, String userId) {
        return new StringBuilder()
                .append("slip")
                .append("_")
                .append(getFileNamePreFix(enterYear, majCode, userId, "pdf"))
                .toString();

    }

    private static String getFileNamePreFix(String enterYear, String majCode, String userId, String extension) {
        return new StringBuilder()
                .append(enterYear)
                .append("_")
                .append(majCode)
                .append("_")
                .append(userId)
                .append(".")
                .append(extension)
                .toString();
    }

    private static String getBirtFileName(String schoolCode, String admissionCode, String fileType) {
        return new StringBuilder()
                .append(RecruitSchool.SchoolCodeType.getLowerCaseName(schoolCode))
                .append("-")
                .append(fileType)
                .append("-")
                .append(Adms.AmdsCodeType.KR.getValue().equals(admissionCode) ? "kr" : "en")
                .toString();
    }


    private static String getBirtFileName(String schoolCode, String fileType) {
        return new StringBuilder()
                .append(RecruitSchool.SchoolCodeType.getLowerCaseName(schoolCode))
                .append("-")
                .append(fileType)
                .toString();
    }
    public static String getApplBirtFileName(String schoolCode, String admissionCode) {
        return getBirtFileName(schoolCode, admissionCode, "appl");
    }

    public static String getSlipBirtFileName(String schoolCode, String admissionCode) {
        return getBirtFileName(schoolCode, admissionCode, "slip");
    }
    public static String getEssayBirtFileName(String schoolCode) {
        return getBirtFileName(schoolCode, "essay");
    }


    public static String getS3ZipDownFileKey(String schoolCode, String enterYear, Integer recruitPartSeq,
                                          Long applNo,String majCode,String userId) {

        return new StringBuilder()
                .append(getS3UploadDirectrotyFullPath(schoolCode, enterYear, recruitPartSeq, applNo))
                .append("/")
                .append(getZippedFileName(enterYear,majCode,userId))
                .toString();
    }


    public static String getEssayFileName() {
       return "Essay.pdf";

    }

    public static String getsSortingEssayFileName() {
        return new StringBuilder()
                .append("0-0-0-")
                .append(getEssayFileName())
                .toString();

    }

    public static String getRecBirtFileName(String schoolCode) {
        return new StringBuilder()
                .append(RecruitSchool.SchoolCodeType.getLowerCaseName(schoolCode))
                .append("-")
                .append("prof")
                .append("-")
                .append("rec")
                .toString();
    }
}
