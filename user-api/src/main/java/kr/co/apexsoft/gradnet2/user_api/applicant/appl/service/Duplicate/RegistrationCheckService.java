package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.Duplicate;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.CryptoService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.DuplicateDto;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.DuplicateException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * ' 학교 +입학연도 + 회차 + 국내 +지원완료+주민번호 '  중복체크
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationCheckService implements DuplicateApplService {

    @NonNull
    private CryptoService cryptoService;

    @NonNull
    private ApplRepository applRepository;

    /**
     * 현재 임시로 연대 세팅
     *
     * @param schoolCode
     * @return
     */
    @Override
    public boolean supports(String schoolCode) {
        return RecruitSchool.SchoolCodeType.KDI.getValue().equals(schoolCode);
    }

    /**
     * 국내전형 & 주민번호 기준 체크
     * 지원사항 저장 단계 제외 모든 단계 체크
     *
     * @param duplicateDto
     */
    @Override
    public void check(DuplicateDto duplicateDto) {
        if (!duplicateDto.isApplPartSaveStatus() && duplicateDto.isGeneral()) {
            String encr = duplicateDto.getRegistration().isDataEncr() ? cryptoService.decrypt(duplicateDto.getRegistration().getRegistrationEncr()) : duplicateDto.getRegistration().getRegistrationEncr();
            List<String> candidateList = applRepository.findRegistration(duplicateDto.getSchoolCode(), duplicateDto.getEnterYear(), duplicateDto.getRecruitPartSeq(),
                    duplicateDto.getAdmissionCode(), duplicateDto.getRegistration().getRegistrationBornDate(), Appl.Status.COMPLETE);
            for (String regst : candidateList) {
                if (cryptoService.decrypt(regst).equals(encr)) {
                    //같은값이어도 암호화다를수있음(복호화 비교필요)
                    throw new DuplicateException(MessageUtil.getMessage("DUPLICATE_REGISTRATION_FAIL"));
                }
            }
        }
    }

}
