package kr.co.apexsoft.gradnet2.user_api._support.exception;

import lombok.Getter;

import java.util.HashMap;

/**
 * 사용자 비즈니스 익셉션
 * 남용주의
 *
 * @author 김효숙
 */
@Getter
public class BizException extends RuntimeException {

    private String warningMessage;

    private HashMap<String, Object> returnMap;

    private String addlog;

    public BizException() {
        super();
    }

    public BizException(String warningMessage,  String addlog, HashMap<String, Object> returnMap) {
        this.addlog = addlog;
        this.warningMessage = warningMessage;
        this.returnMap = returnMap;
    }


    public BizException(String warningMessage,  String addlog) {
        this.addlog = addlog;
        this.warningMessage = warningMessage;
    }

}
