package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 * 재생성 처리
 */
public class ApplRemakeFailException extends CustomException {

    public ApplRemakeFailException(String warningMessage, Long applNo) {
        super("[ applNo: "+applNo+" ]", warningMessage);
    }
}
