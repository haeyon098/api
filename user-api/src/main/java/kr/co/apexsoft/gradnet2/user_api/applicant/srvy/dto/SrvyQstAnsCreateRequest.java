package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.Getter;
import lombok.Setter;

/**
* 설문답변 생성용
*
* @author aran
* @since 2020-02-27
*/
@Getter
@Setter
public class SrvyQstAnsCreateRequest {
    private Long srvyQstNo;
    private String recruitSchoolCode;
    private Long srvyNo;
    private Appl appl;
    private Boolean isMultiAnswer;
    private String answer;
}
