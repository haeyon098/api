package kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend_professor.domain.RecommendInformation;
import kr.co.apexsoft.gradnet2.user_api._common.ApexCollectionValidator;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.service.RecommendService;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto.RecommendProfRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto.RecommendProfResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.dto.RecommendProfTempRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend_professor.service.RecommendProfessorService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-01-06
 */
@RestController
@RequestMapping("/rec/prof")
@RequiredArgsConstructor
public class RecommendProfessorController {

    @NonNull
    private RecommendProfessorService recommendProfessorService;

    @NonNull
    private RecommendService recommendService;

    private final ApexModelMapper modelMapper;

    private final ApexCollectionValidator apexCollectionValidator;
       /**
     * 추천서 내용
     * 임시 저장
     *
     * @param recommendProfTempRequest
     * @return
     */
    @PostMapping("/temp")
    public ResponseEntity<RecommendProfResponse> tempSaveRecommendProf(@Valid @RequestBody RecommendProfTempRequest recommendProfTempRequest) {


        return ResponseEntity.ok(recommendProfessorService.tempSaveRecommendProf(modelMapper.convert(recommendProfTempRequest, RecommendInformation.class),
                recommendProfTempRequest.getApplNo(), recommendProfTempRequest.getKey()));
    }

    /**
     * 추천서 내용
     * 저장 및 버트 생성
     *
     * @param recommendProfRequest
     * @param errors
     * @return
     */
    @PostMapping
    public void saveRecommendProf(@Valid @RequestBody RecommendProfRequest recommendProfRequest,
                                                                       Errors errors) {
        apexCollectionValidator.validate(recommendProfRequest.getAnswerList(), errors);
        if (errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("RECOMMEND_ANS_INVALID_INFO"), errors);

        recommendProfessorService.saveRecommendProf(modelMapper.convert(recommendProfRequest, RecommendInformation.class),
                recommendProfRequest.getApplNo(), recommendProfRequest.getKey());
    }


    /**
     * 추천서 내용
     * 조회
     *
     * @param key
     * @param userPrincipal
     * @return
     */
    @GetMapping
    public ResponseEntity<RecommendProfResponse> findRecommendProf(@RequestParam("key") String key,
                                                                   @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(recommendProfessorService.findRecommendProf(key));
    }


    /**
     * 추천서 메일 OPENED
     * 한 건 조회
     *
     * @param key
     * @param
     * @return
     */
    @GetMapping("/open")
    public ResponseEntity<kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendProfResponse> openRecommend(@RequestParam("key") String key) {
        return ResponseEntity.ok(recommendService.changeStatusRecommend(key, Recommend.status.OPENED));
    }


    /**
     * 추천서 메일 CREATING
     * 한 건 조회
     *
     * @param key
     * @param
     * @return
     */
    @GetMapping("/create")
    public ResponseEntity<kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendProfResponse> creatingRecommend(@RequestParam("key") String key) {
        return ResponseEntity.ok(recommendService.changeStatusRecommend(key, Recommend.status.CREATING));
    }

    /**
     * 추천서 완료처리
     * 한 건 조회
     *
     * @param key
     * @param
     * @return
     */
    @GetMapping("/complete")
    public ResponseEntity<kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendProfResponse> completeRecommend(@RequestParam("key") String key) {
        return ResponseEntity.ok(recommendService.changeStatusRecommend(key, Recommend.status.COMPLETED));
    }
}
