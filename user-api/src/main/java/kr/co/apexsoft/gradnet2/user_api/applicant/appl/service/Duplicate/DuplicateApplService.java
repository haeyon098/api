package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.Duplicate;

import kr.co.apexsoft.gradnet2.user_api._common.ApexSchoolSupportable;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.DuplicateDto;

public interface DuplicateApplService extends ApexSchoolSupportable {

    void check(DuplicateDto duplicateDto);

}
