package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import lombok.Getter;
import lombok.Setter;

/**
 * 버트 파일에서 공동으로 사용
 */
@Getter
@Setter
public abstract class AppBirtFileInfo {
    private Long id;
    private String partSchoolCode;
    private String partEnterYear;
    private Integer partRecruitPartSeq;
    private String partAdmissionCode;
    private String partMajorCode;

    private String applicantUserId;
    private String fileBaseDir;

    private String fileType;

    public Long getApplNo() {
        return this.id;
    }

    public String getTargeDir() {
        return FilePathUtil.getMakeDirectoryFullPath(this.fileBaseDir, this.partSchoolCode, this.partEnterYear, this.partRecruitPartSeq, this.id);
    }

    public String getS3Path() {
        return FilePathUtil.getS3UploadDirectrotyFullPath(this.partSchoolCode, this.partEnterYear, this.partRecruitPartSeq, this.id);
    }

    public abstract String getBirtFileName();

    public abstract String getFileName();

}
