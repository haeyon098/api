package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-08
 */
@Getter
@Setter
public class MyListRecruitPartTimeResponse {
    private RecruitPartTime.TimeType type;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startDate;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endDate;

    private String description;
}
