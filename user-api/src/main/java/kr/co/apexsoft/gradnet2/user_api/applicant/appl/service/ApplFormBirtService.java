package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.apexsoft.gradnet2.entity.adms.domain.Adms;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.projection.AcademyProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.repository.AcademyRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplBasisProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.career.projection.CareerProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.DocumentProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.projection.LanguageProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.repository.LanguageRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.projection.SrvyQstAnsProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.srvy.repository.SrvyQstAnsRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.CryptoService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.AppBirtFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplFormDataNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplPhotoNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 지원서 생성 서비스
 */
@Service
@Transactional
public class ApplFormBirtService extends ApplBirtService {
    @Autowired
    private ApplService applService;


    @Autowired
    private CryptoService cryptoService;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private ApplRepository applRepository;


    @Autowired
    private AcademyRepository academyRepository;

    @Autowired
    private LanguageRepository languageRepository;

    @Autowired
    private CareerRepository careerRepository;

    @Autowired
    private SrvyQstAnsRepository srvyQstAnsRepository;

    @Value("${file.baseDir}")
    private String fileBaseDir;

    @Override
    public Map<String, Object> setBirtInfo(AppBirtFileInfo fileInfo) {
        Map<String, Object> rptInfoMap = new HashMap<String, Object>();
        //1.baseInfo
        rptInfoMap.putAll(getBaseInfo(fileInfo.getApplNo(), fileInfo.getPartAdmissionCode()));

        //2. 파일정보 (제출첨부파일)
        Map<String, Object> documentInfo = getFileDocumentInfo(fileInfo);
        rptInfoMap.putAll(documentInfo);

        //3. 학력정보
        rptInfoMap.putAll(getAcadInfo(fileInfo.getApplNo()));
        //4. 어학 정보
        rptInfoMap.putAll(getLangInfo(fileInfo.getApplNo()));

        // 5. 경력 정보
        rptInfoMap.putAll(getCareerInfo(fileInfo.getApplNo()));

        //6. 학교별 추가 데이터
        rptInfoMap.putAll((getSchoolInfo(fileInfo)));


        return rptInfoMap;
    }

    /**
     * 공통사용 baseInfo
     *
     * @param applNo
     * @return
     */
    private Map<String, Object> getBaseInfo(Long applNo, String admissionCode) {
        ApplBasisProjection part = applRepository.findMyApplBasis(applNo); /*TODO:기타코드 999 하드코딩*/
        if (part == null) {
            throw new ApplFormDataNotFoundException(MessageUtil.getMessage("APPL_FORM_DATA_NOT_FOUND"), applNo);
        }
        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> basis = oMapper.convertValue(part, Map.class);


        // 국내
        if (Adms.AmdsCodeType.KR.getValue().equals(admissionCode)) {
            //수험번호
            basis.put("applId", StringUtils.isBlank(part.getApplId()) ? "지원 미완료(지원서 작성 중)" : part.getApplId());
            //주민등록번호
            basis.put("rgstNo", gettData(part.getRgstBronDate()) + "-" + getDecryptData(part.getRegistrationEncr()));
        } else if (Adms.AmdsCodeType.EN.getValue().equals(admissionCode)) {// 국제
            //수험번호
            basis.put("applId", StringUtils.isBlank(part.getApplId()) ? "Proceeding application" : part.getApplId());

            basis.put("fornRgstNo", getDecryptData(part.getFornRgstEncr()));
            basis.put("passportNo", getDecryptData(part.getPassportEncr()));
            basis.put("visaNo", getDecryptData(part.getVisaEncr()));
        }

        return basis;
    }

    private String gettData(String target) {
        return StringUtils.isBlank(target) ? "" : target.substring(2);
    }

    private String getDecryptData(String target) {
        return StringUtils.isBlank(target) ? "" : cryptoService.decrypt(target);
    }

    /**
     * 파일 정보 추출
     * 증명사진
     * 제출 첨부파일 목록
     *
     * @param fileInfo
     * @return
     */
    private Map<String, Object> getFileDocumentInfo(AppBirtFileInfo fileInfo) {
        //업로드한 파일 가져오기
        Map<String, Object> documentInfo = new HashMap<String, Object>();
        List<DocumentProjection> docList = documentRepository.findUploadDoc(fileInfo.getApplNo(), DocItem.ETC.getValue(),
                fileInfo.getPartSchoolCode(), fileInfo.getPartEnterYear(), fileInfo.getPartRecruitPartSeq(),
                DocGrpType.LANG.getValue(),DocGrpType.CAREER.getValue(),
                DocGrpType.Academy(),
                DocItem.NotUploadedDirectlyItems()
                );

        DocumentProjection photo = docList.stream().filter(doc -> doc.getDocItemCode().equals(DocItem.PHOTO.getValue())).findFirst()
                .orElseThrow(() -> new ApplPhotoNotFoundException(MessageUtil.getMessage("APPL_PHOTO_NOT_FOUND"), fileInfo.getApplNo()));

        documentInfo.put("documents", docList);
        documentInfo.put("photoFilePath", photo.getFilePath());
        return documentInfo;
    }


    /**
     * 학교별 추가 항목 
     * TODO: 추후 학교별로 서베이처럼 테이블생성이 생기면 모듈로 분리필요
     *
     * @return
     */
    private Map<String, Object> getSchoolInfo(AppBirtFileInfo fileInfo) {

        Map<String, Object> schoolInfo = new HashMap<String, Object>();

        //kdi
        if (RecruitSchool.SchoolCodeType.KDI.getValue().equals(fileInfo.getPartSchoolCode())) {
            //설문데이터
            schoolInfo.putAll(getSurveyInfo(fileInfo.getApplNo()));
        }

        return schoolInfo;
    }

    /**
     * 학력정보
     *
     * @param applNo
     * @return
     */
    private Map<String, Object> getAcadInfo(Long applNo) {
        Map<String, Object> info = new HashMap<String, Object>();
        List<AcademyProjection> academys = academyRepository.findAcademyByApplNo(applNo, "999", Arrays.asList(Academy.AcademyType.values())); /*TODO:기타코드 999 하드코딩*/
        info.put("academys", academys);

        return info;
    }

    /**
     * 어학정보
     *
     * @param applNo
     * @return
     */
    private Map<String, Object> getLangInfo(Long applNo) {
        Map<String, Object> info = new HashMap<String, Object>();
        List<LanguageProjection> list = languageRepository.findLangInfoByApplNo(applNo);
        info.put("languages", list);
        return info;
    }

    /**
     * 경력정보
     * @param applNo
     * @return
     */
    private Map<String, Object> getCareerInfo(Long applNo) {
        Map<String, Object> info = new HashMap<String, Object>();
        List<CareerProjection> careerList = careerRepository.findCareerInfo(applNo);
        info.put("careers", careerList);
         careerList.stream().filter(CareerProjection::getCurrently).findFirst().ifPresent(c->info.put("curCompAddr",c.getAddress()));
        return info;
    }

    /**
     * 설문정보
     * @param applNo
     * @return
     */
    private Map<String, Object> getSurveyInfo(Long applNo) {
        Map<String, Object> info = new HashMap<String, Object>();

        // TODO: 설문에 대한 구조 변경 후 재개발 필요
        String tempCode = "N/A";
        String tempName = "N/A";
        String answer2 = "N/A";
        String answer3 = "N/A";
        String answer4 = "N/A";

        List<SrvyQstAnsProjection> srvyQAstAnsList = srvyQstAnsRepository.findSrvyQstAnsForReport(applNo);
        if (!srvyQAstAnsList.isEmpty()){
            String answer1 = srvyQAstAnsList.get(0).getAnswer();
            tempCode = answer1.split("\\)%")[0];
            tempName = answer1.split("\\)%")[1];
            answer2 = srvyQAstAnsList.get(1).getAnswer();
            answer3 = srvyQAstAnsList.get(2).getAnswer();
            answer4 = srvyQAstAnsList.get(3).getAnswer();
        }

        info.put("srvyInterCode", tempCode);
        info.put("srvyInterName", tempName);
        info.put("srvyTopic", answer2);
        info.put("publicSecYN", answer3);
        info.put("srvySupervisor", answer4);
        return info;
    }

}
