package kr.co.apexsoft.gradnet2.user_api.applicant.career.controller;

import kr.co.apexsoft.gradnet2.user_api.applicant.career.dto.CareerResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.career.service.CareerService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-13
 */
@RestController
@RequestMapping("/appl/career")
@RequiredArgsConstructor
public class CareerController {
    @NonNull
    private CareerService careerService;

    @GetMapping("/{applNo}")
    public ResponseEntity<List<CareerResponse>> findAllCareer(@PathVariable("applNo") Long id) {
        return ResponseEntity.ok(careerService.findAllCareer(id));
    }
}
