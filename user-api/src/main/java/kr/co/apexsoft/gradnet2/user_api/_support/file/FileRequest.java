package kr.co.apexsoft.gradnet2.user_api._support.file;

import kr.co.apexsoft.gradnet2.user_api._common.ApexAssert;
import kr.co.apexsoft.gradnet2.user_api._common.util.FilePathUtil;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.aws.s3.exception.S3UploadException;
import kr.co.apexsoft.gradnet2.user_api._support.file.exception.FileConvertException;
import kr.co.apexsoft.gradnet2.user_api._support.file.exception.FileSizeException;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Getter

public class FileRequest {

    private static final Logger logger = LoggerFactory.getLogger(FileRequest.class);

    private String fileExt;

    private String fileName;

    private String originalFileName;

    private Long fileSize;

    private File file;

    private String fileDirPath;


    public FileRequest(String fileDirPath, String fileName) {
        this.file = new File(fileDirPath, fileName);
        this.originalFileName = fileName;
        this.fileName = fileName;
        this.fileDirPath = fileDirPath;
        this.fileSize = file.length();
    }

    public FileRequest(File file, String fileDirPath,String fileName) {
        this.file = file;
        this.originalFileName = fileName;
        this.fileName = fileName;
        this.fileDirPath = fileDirPath;
        this.fileSize=file.length();
    }

    public FileRequest(File file, String fileDirPath,String fileName,String originalFileName) {
        this.file = file;
        this.originalFileName = originalFileName;
        this.fileName = fileName;
        this.fileDirPath = fileDirPath;
        this.fileSize=file.length();
    }

    public FileRequest(StandardMultipartHttpServletRequest multipartReq,String maxImgSize,String maxPdfSize) {
        Map<String, MultipartFile> fileMap = multipartReq.getFileMap();
        Set<Map.Entry<String, MultipartFile>> entrySet = fileMap.entrySet();

        ApexAssert.isTrue(entrySet.size() == 1, MessageUtil.getMessage("FILE_CONVERT_FAIL"), S3UploadException.class);
        Map.Entry<String, MultipartFile> fieldName_MultipartFile = entrySet.iterator().next();
        MultipartFile multipartFile = fieldName_MultipartFile.getValue();


        this.originalFileName = FilePathUtil.removeSeparators(multipartFile.getOriginalFilename());

        this.file = convert(multipartFile).orElseThrow(() -> new FileConvertException(MessageUtil.getMessage("S3_UPLOAD_FAIL")));

        this.fileName = this.originalFileName; // 따로 지정하지 않는경우, 원본파일명으로 됨
        this.fileExt = FilenameUtils.getExtension(this.originalFileName);
        this.fileSize =  checkFileSize(multipartFile.getSize(),maxImgSize,maxPdfSize);

    }

    /**
     * 모든 파일 객체에 대해서 사이즈 검사
     * @param fileSize
     * @param maxImgSize
     * @param maxPdfSize
     * @return
     */
    private Long checkFileSize(Long fileSize,String maxImgSize,String maxPdfSize ){
        Boolean isPdf = this.fileExt.equalsIgnoreCase("pdf");
        String maxSize = isPdf ? maxPdfSize: maxImgSize ;
        String msg = isPdf ? "MAX_PDF_SIZE_FAIL" : "MAX_IMAGE_SIZE_FAIL";

        if (fileSize > Double.parseDouble(maxSize) * 1024 * 1024) {
            throw new FileSizeException( MessageUtil.getMessage(msg,new Object[]{maxSize}));
        }
        return fileSize;
    }

    private Optional<File> convert(MultipartFile file) {
        File convertFile = null;
        try {
            convertFile = File.createTempFile("upload", "temp");
        } catch (IOException e) {
            return Optional.empty();
        } finally {
            convertFile.deleteOnExit();
        }

        try {
            file.transferTo(convertFile);
        } catch (IOException e) {
            return Optional.empty();
        }
        return Optional.of(convertFile);
    }

    public void setCustomFileDirPath(String fileDirPath) {
        this.fileDirPath = fileDirPath;
    }

    public void setCustomFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileKey() {
        return this.fileDirPath + "/" + this.fileName;
    }
}
