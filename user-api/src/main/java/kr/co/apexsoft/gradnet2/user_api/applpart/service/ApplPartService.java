package kr.co.apexsoft.gradnet2.user_api.applpart.service;

import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.repository.AcademyRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.career.domain.Career;
import kr.co.apexsoft.gradnet2.entity.applicant.career.repository.CareerRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.domain.Language;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.projection.LanguageProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.lang.repository.LanguageRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartDocument;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPartLanguage;
import kr.co.apexsoft.gradnet2.entity.applpart.projection.DocumentsResponse;
import kr.co.apexsoft.gradnet2.entity.applpart.repository.ApplPartDocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.repository.ApplPartLanguageRepository;
import kr.co.apexsoft.gradnet2.entity.comcode.repository.SysCommCodeRepository;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.lang.dto.LanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.ApplPartLanguageResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.DocGrpDocResponse;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.LanguageDocumentResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-06
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ApplPartService {
    @NonNull
    private SysCommCodeRepository sysCommCodeRepository;

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private ApplPartDocumentRepository applPartDocumentRepository;

    @NonNull
    private CareerRepository careerRepository;

    @NonNull
    private AcademyRepository academyRepository;

    @NonNull
    private LanguageRepository languageRepository;

    @NonNull
    private ApplPartLanguageRepository applPartLanguageRepository;

    private final ApexModelMapper modelMapper;


    @Transactional(readOnly = true)
    public List<DocGrpDocResponse> findDocumentList(Long applNo) {
        List<DocGrpDocResponse> list = new ArrayList<>();
        List<DocumentsResponse> documents;
        ApplPart part = applRepository.findById(applNo).orElseThrow(
                () -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND"))).getPart();

        List<ApplPartDocument> groupList = applPartDocumentRepository.findDcoumentGrpGroupBy(part);

        // group마다 리스트 조회해서 넣기
        for (ApplPartDocument group : groupList) {
            List<DocGrpDocResponse> subGrp = new ArrayList<>();
            DocGrpDocResponse response = null;

            if (group.isCareerGroup()) {         // 경력
                List<Career> careers = careerRepository.findByAppl_IdOrderBySeq(applNo);
                for (Career career : careers) {
                    documents = getDocumentListEachGrpCode(part, group.getGrpCode(), applNo, career.getId());
                    subGrp.add(new DocGrpDocResponse(career.getName(), null, documents, career.getId().toString()));
                }
                response = new DocGrpDocResponse(group.getGrpCode(), group.getGrpKorName(), group.getGrpEngName(), subGrp);
            } else if (group.isUnivGroup()) {    // 대학
                List<Academy> academies = academyRepository.findByAppl_IdAndTypeOrderBySeq(applNo, Academy.AcademyType.UNIV);
                subGrp = addEduSubGroup(academies, group.getGrpCode(), part, applNo);
                response = new DocGrpDocResponse(group.getGrpCode(), group.getGrpKorName(), group.getGrpEngName(), subGrp);
            } else if (group.isGradGroup()) {    // 대학원
                List<Academy> academies = academyRepository.findByAppl_IdAndTypeOrderBySeq(applNo, Academy.AcademyType.GRAD);
                subGrp = addEduSubGroup(academies, group.getGrpCode(), part, applNo);
                response = new DocGrpDocResponse(group.getGrpCode(), group.getGrpKorName(), group.getGrpEngName(), subGrp);
            } else if (group.isLangGroup()) {    // 어학
                List<LanguageProjection> languages = languageRepository.findLangInfoByApplNo(applNo);
                //파일업로드만 추출
                languages= languages.stream().filter(languageProjection -> languageProjection.getUploadYn().endsWith("Y")).collect(Collectors.toList());
                for (LanguageProjection language : languages) {
                    documents =getDocumentListEachGrpCode(part, group.getGrpCode(), applNo, language.getId());
                   List<LanguageDocumentResponse> languageDocumentResponseList = documents.stream().map(documentsResponse ->
                        new LanguageDocumentResponse(documentsResponse,documentsResponse.getRequired().endsWith("Y")&& language.getMdtYn().endsWith("Y") ?"Y":"N")
                    ).collect(Collectors.toList());
                    subGrp.add(new DocGrpDocResponse(language.getExamKorName(), language.getExamName(), languageDocumentResponseList, language.getId().toString()));
                }
                response = new DocGrpDocResponse(group.getGrpCode(), group.getGrpKorName(), group.getGrpEngName(), subGrp);
            } else {                            // 그 외
                documents = applPartDocumentRepository.findDocumentListAsNotGrp(part.getSchoolCode(), part.getEnterYear(),
                        part.getRecruitPartSeq(), part.getApplPartNo(), group.getGrpCode(), applNo);
                response = new DocGrpDocResponse(group.getGrpCode(), group.getGrpKorName(), group.getGrpEngName(), documents, null);
            }

            if (response != null)
                list.add(response);
        }

        return list;
    }

    private List<DocGrpDocResponse> addEduSubGroup(List<Academy> academies, String grpCode, ApplPart part, Long applNo) {
        List<DocGrpDocResponse> group = new ArrayList<>();
        List<DocumentsResponse> documents;
        for (Academy academy : academies) {
            documents = getDocumentListEachGrpCode(part, grpCode, applNo, academy.getId());
            group.add(new DocGrpDocResponse(academy.getSchool().getKorName(), academy.getSchool().getEngName(),
                    documents, academy.getId().toString()));
        }
        return group;
    }

    private List<DocumentsResponse> getDocumentListEachGrpCode(ApplPart part, String grpCode, Long applNo, Long docGrpNo) {
        return applPartDocumentRepository.findDocumentList(part.getSchoolCode(), part.getEnterYear(), part.getRecruitPartSeq(),
                part.getApplPartNo(), grpCode, applNo, docGrpNo);
    }

    /**
     * 지원단위별 어학 목록
     *
     * @param applNo
     * @return
     */
    public List<ApplPartLanguageResponse>  getLanguageList(Long applNo) {
        //지원단위
        ApplPart part = applRepository.findById(applNo).orElseThrow(
                () -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND"))).getPart();


        //최상위는 한개 & 부모노드==NULL 이어야함
        List<ApplPartLanguage> list = applPartLanguageRepository.findByPartAndParentNode_IdIsNull(part);
        List<Language> languages = languageRepository.findByAppl_Id(applNo);
     return   list.stream().map(applPartLangProjection -> makeLanguageResponse(applPartLangProjection,languages)).collect(Collectors.toList());

    }

    /**
     * ApplPartLanguage 에서 ApplPartLanguageResponse 재귀
     * detailsGrp 시스템코드 조회 필요하므로 ApplPartLanguageResponse 클래스로 처리하지 않음
     *
     * @param language
     * @return
     */
    private ApplPartLanguageResponse makeLanguageResponse(ApplPartLanguage partLanguage, List<Language> language){

        return new ApplPartLanguageResponse(
                partLanguage.getId(), partLanguage.getLevel(), partLanguage.getNodeKorName(), partLanguage.getNodeEngName(), partLanguage.getOptionType(),
                partLanguage.getIsLeaf(),
                partLanguage.getSubNode().stream().map(partLanguage1 -> makeLanguageResponse(partLanguage1,language)).collect(Collectors.toList()),
                partLanguage.getIsLeaf() ? new ApplPartLanguageResponse.LeafInfoOut(partLanguage,sysCommCodeRepository.findAllByCodeGrpAndUsedTrueOrderByCodeAsc(partLanguage.getDetailGrpCode()),language) : null);

    }

}
