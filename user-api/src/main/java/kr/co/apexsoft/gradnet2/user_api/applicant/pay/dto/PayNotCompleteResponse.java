package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayNotCompleteResponse {

    int iamportCount;
    int gradnetCount;
    Object list;


}
