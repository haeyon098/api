package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.service;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.TimeDto;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.repository.DocumentRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.domain.Recommend;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.projection.RecommendInfoResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.recommend.repository.RecommedRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.entity.recpart.repository.RecruitPartRepository;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.ApexAssert;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.crypto.HashUtil;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.user_api._support.mail.exception.MailValidException;
import kr.co.apexsoft.gradnet2.user_api.adms.exception.RecruitSchoolNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendListResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendProfResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto.RecommendResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-05
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RecommendService {

    @NonNull
    private RecommedRepository recommedRepository;

    @NonNull
    private ApplRepository applRepository;

    private final ApexModelMapper modelMapper;

    @Value("${front.url}")
    private String frontUrl;

    @NonNull
    private MailService mailService;

    @NonNull
    private ApplService applService;

    @NonNull
    private DocumentRepository documentRepository;

    @NonNull
    private RecruitPartRepository recruitPartRepository;


    @Autowired
    private ResourceLoader resourceLoader;

    public RecommendResponse sendRecommend(Recommend recommend, Long applNo, Set<Role> roles) {
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));

        //추천서 요청 마감시간 종료여부 체크 && 시간, 시스템관리자 권한
        applService.checkApplSaveTime(applNo, roles, RecruitPartTime.TimeType.RECOMMEND_REQUEST, "REC_PERIOD_FAIL");

        //  추천서 요청 가능하지 않는 상태 체크
        if (appl.isRecRequestNotAvailable()) {
            throw new RecReqeustStatusFailException(MessageUtil.getMessage("REC_REQUEST_FAIL"), appl.getId());
        }

        //추천서 2개 초과인지 체크
        checkCountRec(appl);
        recommend.initStsCode(Recommend.status.SENT.getCode());
        recommend.initAppl(appl);
        recommend = recommedRepository.save(recommend);
        recommend.initKey(recKeyEncrypt(recommend, applNo));
        RecommendInfoResponse recommendInfoResponse = recommedRepository.findRecInfo(recommend.getId());
        recommend.initTitle(recommendInfoResponse.getRecrPartEngName());

        //메일전송 정보 저장
        List<String> mailList = new ArrayList<String>();
        Map<String, String> replacements = new HashMap<>();

        TimeDto time = applRepository.findRecruitPartPeriod(applNo, RecruitPartTime.TimeType.RECOMMEND);
        LocalDateTime endDay = time.getEndDay();

        replacements.put("url", frontUrl+ "/recommend/"+recommend.getKey());
        replacements.put("profName",recommendInfoResponse.getProfName());
        replacements.put("recrShclEngName",recommendInfoResponse.getRecrShclEngName());
        replacements.put("recDeadline", endDay.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")) );
        replacements.put("name",appl.getApplicantInfo().getEngName());
        replacements.put("nationality",recommendInfoResponse.getEngCntr());
        replacements.put("program",recommendInfoResponse.getCourseEngName());
        replacements.put("subField",recommendInfoResponse.getMajorEngName());

        mailList.add(recommend.getEmail());

        // 이메일 유효한지 확인
        for (String mailAddr : mailList) {
            ApexAssert.notMailPattern(mailAddr, MessageUtil.getMessage("MAIL_ADDRESS_CHECK_FAIL"), MailValidException.class);
        }
        //메일전송
        MailDto dto = MailDto.builder()
                .to(mailList)
                .subject(recommendInfoResponse.getEnterYear()+" "+recommendInfoResponse.getRecrShclEngName()+ " admission Recommendation letter Request")
                .templateId("recommend-request.html")
                .replacements(replacements)
                .resourceLoader(resourceLoader)
                .build();

        mailService.send(dto);

        return modelMapper.convert(recommend,RecommendResponse.class);
    }



    @Transactional(readOnly = true)
    public RecommendListResponse findRecommends(Long applNo) {
        ApplPart part = applRepository.findById(applNo).orElseThrow(
                () -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND"))).getPart();

        List<Recommend> recommendList = recommedRepository.findByAppl_Id(applNo);

        Set<RecruitPartTime> times = recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeq(part.getSchoolCode(),
                part.getEnterYear(), part.getRecruitPartSeq()).orElseThrow(
                        () -> new RecruitSchoolNotFoundException(MessageUtil.getMessage("RECRUIT_PART_NOT_FOUND"))).getTime();

        LocalDateTime requestStartDate = null, requestEndDate = null, recommendEndDate = null;

        for(RecruitPartTime time : times) {
            if(time.getType().equals(RecruitPartTime.TimeType.RECOMMEND_REQUEST)) {
                requestStartDate = time.getStartDate();
                requestEndDate = time.getEndDate();
            } else if (time.getType().equals(RecruitPartTime.TimeType.RECOMMEND)) {
                recommendEndDate = time.getEndDate();
            }
        }

        return new RecommendListResponse(modelMapper.convert(recommendList, new TypeToken<List<RecommendResponse>>(){}.getType()),
                requestStartDate, requestEndDate, recommendEndDate);
    }

    private String recKeyEncrypt(Recommend recommend,  Long applNo) {
        String key = recommend.getId() + "," + applNo + "," + recommend.getName() + "," + recommend.getEmail();
        return HashUtil.generateHash(key);

    }

    private void checkCountRec(Appl appl) {
        Long countRec = recommedRepository.countByAppl(appl);
        if(countRec >=2) {
            throw new RecCountException(MessageUtil.getMessage("COUNT_SAVE_FAIL"), appl.getId());
        }
    }



    public RecommendProfResponse changeStatusRecommend(String key, Recommend.status status) {
        if(!recommedRepository.findByKey(key).isPresent()) {
            throw new RecNotlException(key, MessageUtil.getMessage("REC_NOT_FOUND"));
        }

        Recommend recommend = recommedRepository.findByKey(key).get();
        if (status.getCode().equals(Recommend.status.CREATING.getCode()) && recommend.getStsCode().equals(Recommend.status.COMPLETED.getCode())) {
            throw new RecCompleteException(MessageUtil.getMessage("REC_COMPLETE"), recommend.getId());
        }
        RecommendProfResponse response = modelMapper.convert(recommedRepository.findRecInfo(recommend.getId()), RecommendProfResponse.class);

        if(status.getCode().equals(Recommend.status.OPENED.getCode()) && recommend.getStsCode().equals(Recommend.status.COMPLETED.getCode())) {
            //Doc filePath 추출 다운로드위해서
            List<Document> documents = documentRepository.findByAppl_IdAndItemCode(recommend.getAppl().getId(), DocItem.EMAIL_REC.getValue());

            for (Document doc: documents) {
                String filePath = doc.getFilePath();
                String fileName = filePath.substring(filePath.lastIndexOf("/")+5);
                String result = fileName.substring(0, fileName.indexOf("_"));
                if(Long.parseLong(result) == recommend.getId()) {
                    response.initFile(doc.getFilePath(), doc.getOriginFileName());
                }
            }
        }
        else {
            recommend.initStsCode(status.getCode());
        }
        recommedRepository.save(recommend);

        return response;

    }


    public List<RecommendResponse> cancelRecommend(Long recNo, Set<Role> roles) {
        Recommend recommend = recommedRepository.findById(recNo)
                .orElseThrow(() -> new RecNotlException( MessageUtil.getMessage("REC_NOT_FOUND")));
        applService.checkApplSaveTime(recommend.getAppl().getId(), roles, RecruitPartTime.TimeType.RECOMMEND_REQUEST, "REC_CANCEL_FAIL");

        if(recommend.getStsCode().equals(Recommend.status.SENT.getCode())) {
            recommedRepository.delete(recommend);
        } else {
            throw new RecCancelException(MessageUtil.getMessage("REC_CANCEL_FAIL"), recommend.getId());
        }

        List<Recommend> list = recommedRepository.findByAppl_Id(recommend.getAppl().getId());

        return modelMapper.convert(list, new TypeToken<List<RecommendResponse>>(){}.getType());

    }
    
}
