package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
public class ApplicantInfoRequest {
    @Size(max = 10)
    private String korName;
    @NotBlank
    @Size(max = 50)
    private String engName;
    @NotBlank
    @Size(max = 50)
    private String email;
    @NotNull
    private LocalDate registrationBornDate;
    private String registrationEncr;
    @NotNull
    private AddressDto address;
    @NotNull
    private CountryDto cntr;
    @NotBlank
    private String gend;
    @NotBlank
    @Size(max = 30)
    private String telNum;
    @NotBlank
    @Size(max = 30)
    private String phoneNum;
    private String empCat;
}
