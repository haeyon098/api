package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.dto;

import kr.co.apexsoft.gradnet2.user_api.comcode.dto.SchoolCommCodeDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
* 설문답변(설문지)
*
* @author aran
* @since 2020-02-27
*/
@Getter
@Setter
@AllArgsConstructor
public class SrvyAnsResponse {
    private Integer srvyNo;
    private String recruitSchoolCode;
    private Integer applNo;
    private boolean isAnswered;

    private List<AlumniDTO> alumniList;

    private List<SchoolCommCodeDto> schoolCommCodeList;
    private List<SchoolCommCodeDto> schoolCommCodeListforDoctor;
}
