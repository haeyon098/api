package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import lombok.Getter;
import lombok.Setter;

/**
 * 시스템 관리자
 * 전제 지원자 목록 조회
 * 검색 조건
 */
@Getter
@Setter
public class ApplSearchCondition {
    private Long applNo;
    private String userId;
    private String email;
    private String phoneNumber;
    private String engName;
    private String korName;
    private Appl.Status status;
    private String schoolCode;
    private String enterYear;
    private Integer recruitPartSeq;

}