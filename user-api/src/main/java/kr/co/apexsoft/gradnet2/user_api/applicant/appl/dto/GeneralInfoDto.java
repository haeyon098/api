package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author homo.efficio@gmail.com
 * created on 2020-02-26
 */
@Getter
public class GeneralInfoDto {
    @NotBlank
    @Size(max = 50)
    private String engFirstName;
    @NotBlank
    @Size(max = 50)
    private String engLastName;
    @NotBlank
    @Size(max = 25)
    private String emergencyName;
    @NotBlank
    private String emergencyRelation;
    @NotBlank
    @Size(max = 30)
    private String emergencyTelNum;
    @Size(max = 100)
    private String handicapGrade;
    @Size(max = 100)
    private String handicapType;
    private Boolean isGmp;
}
