package kr.co.apexsoft.gradnet2.user_api._support.pdf.exception;


/**
 * pdf 페이징_카운트
 */
public class PDFPageCountException extends PDFCommException {
    public PDFPageCountException(String warningMessage, Long applNo,String fileName) {
        super(warningMessage,applNo,fileName);
    }
}