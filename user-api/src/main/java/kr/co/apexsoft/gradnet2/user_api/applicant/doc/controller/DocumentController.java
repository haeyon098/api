package kr.co.apexsoft.gradnet2.user_api.applicant.doc.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.Document;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileUtils;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.*;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.EtcDocumentProjection;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.service.DocumentService;
import kr.co.apexsoft.gradnet2.user_api.applpart.dto.DocGrpDocResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-19
 */
@RestController
@RequestMapping("/appl/doc")
@RequiredArgsConstructor
public class DocumentController {
    @NonNull
    private DocumentService documentService;

    private final ApexModelMapper modelMapper;

    @Value("${file.maxImgSize}")
    private String maxImgSize;

    @Value("${file.maxPdfSize}")
    private String maxPdfSize;

    /**
     * 지원자 첨부파일 업로드
     *
     * @param request
     * @param applNo
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/{applNo}")
    public ResponseEntity<List<DocGrpDocResponse>> uploadDocument(HttpServletRequest request,
                                                       @PathVariable("applNo") Long applNo,
                                                       @AuthenticationPrincipal UserPrincipal userPrincipal) throws JsonProcessingException {
        FileRequest fileRequest = FileUtils.getFileRequest(request,maxImgSize,maxPdfSize);

        ObjectMapper mapper = new ObjectMapper();
        DocumentRequest documentRequest = null;
        try {
            documentRequest = mapper.readValue(request.getParameter("request.data"), DocumentRequest.class);
        } catch(UnrecognizedPropertyException e) {
            throw new InvalidException(MessageUtil.getMessage("DOC_REQUEST_FAIL"));
        }

        Document document = modelMapper.convert(documentRequest, Document.class);
        document.changeFileName(fileRequest.getOriginalFileName());

        return ResponseEntity.ok(documentService.uploadDocument(applNo, fileRequest, documentRequest.getPrefix(),
                document,userPrincipal.getRoles()));
    }

    /**
     * 기타파일 업로드
     *
     * @param request
     * @param applNo
     * @param userPrincipal
     * @return
     * @throws JsonProcessingException
     */
    @PostMapping("/etc/{applNo}")
    public ResponseEntity<List<EtcDocumentProjection>> uploadEtcDocument(HttpServletRequest request,
                                                       @PathVariable("applNo") Long applNo,
                                                       @AuthenticationPrincipal UserPrincipal userPrincipal) throws JsonProcessingException {
        FileRequest fileRequest = FileUtils.getFileRequest(request,maxImgSize,maxPdfSize);
        DocumentRequest documentRequest =extractParameters(request.getParameter("request.data"));
        Document document = modelMapper.convert(documentRequest, Document.class);
        document.changeFileName(fileRequest.getOriginalFileName());

        List<Document> list = documentService.uploadEtcDocumnet(applNo, fileRequest, documentRequest.getPrefix(),
                document,userPrincipal.getRoles());

        return ResponseEntity.ok(modelMapper.convert(list, new TypeToken<List<EtcDocumentProjection>>(){}.getType()));
    }

    /**
     * 파일정보 json 추출
     *
     * @param data
     * @return
     */
    private DocumentRequest extractParameters(String data) {
        ObjectMapper mapper = new ObjectMapper();
        DocumentRequest documentRequest = null;
        try {
            documentRequest = mapper.readValue(data, DocumentRequest.class);
        } catch(UnrecognizedPropertyException e) {
            throw new InvalidException(MessageUtil.getMessage("DOC_REQUEST_FAIL"));
        } catch (JsonProcessingException e) {
            throw new InvalidException(MessageUtil.getMessage("DOC_REQUEST_FAIL"));
        }

        return documentRequest;
    }

    /**
     * 지원자 첨부파일 삭제
     *
     * @param key
     * @param applDocNo
     */
    @DeleteMapping("/{applNo}/{applDocNo}")
    public ResponseEntity<DocumentDeleteResponse> deleteDocument(@RequestParam("fileKey") String key,
                                                                 @PathVariable Long applDocNo,
                                                                 @PathVariable Long applNo,
                                                                 @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(documentService.deleteDocument(applNo, applDocNo, key, userPrincipal.getRoles()));
    }

    /**
     * 기타파일 삭제
     *
     * @param key
     * @param applDocNo
     * @param applNo
     * @param userPrincipal
     * @return
     */
    @DeleteMapping("/etc/{applNo}/{applDocNo}")
    public ResponseEntity<EtcDocumentDeleteResponse> deleteEtcDocument(@RequestParam("fileKey") String key,
                                                                       @PathVariable Long applDocNo,
                                                                       @PathVariable Long applNo,
                                                                       @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(documentService.deleteEtcDocument(applNo, applDocNo, key, userPrincipal.getRoles()));
    }

    /**
     * 기타파일 조회
     *
     * @param applNo
     * @return
     */
    @GetMapping("/etc/{applNo}")
    public ResponseEntity<List<EtcDocumentProjection>> findEtcDocuments(@PathVariable Long applNo) {
        List<Document> list = documentService.findEtcDocuments(applNo);
        return ResponseEntity.ok(modelMapper.convert(list, new TypeToken<List<EtcDocumentProjection>>(){}.getType()));
    }

    /**
     * 첨부파일 저장
     *
     * @param applNo
     * @return
     */
    @GetMapping("/save/{applNo}")
    public ResponseEntity<DocumentSaveDto> saveDocumentStatus(@PathVariable Long applNo,
                                                              @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(documentService.documentSaveStatus(applNo, userPrincipal.getRoles()));
    }

    /**
     * 지원서, 수험표 파일 다운로드를 위한 조회
     *
     * @return
     */
    @GetMapping("/{docType}/{applNo}")
    public ResponseEntity<DocumentPathNameResponse> findDocumentFileInfo(@PathVariable String docType, @PathVariable Long applNo) {
        return ResponseEntity.ok(documentService.findDocumentFileInfo(docType, applNo));
    }
}
