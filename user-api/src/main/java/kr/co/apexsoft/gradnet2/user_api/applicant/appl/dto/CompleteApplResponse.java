package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class CompleteApplResponse {

    private Long applNo;
    private String schoolCode;

    private String enterYear;

    private Integer recruitPartSeq;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime applDate;
}
