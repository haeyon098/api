package kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto;

import lombok.Getter;
import lombok.Setter;

/**
*
*
* @author aran
* @since 2020-03-05
*/
@Getter
@Setter
public class DocumentPathNameResponse {
    private String originFileName;

    private String filePath;
}
