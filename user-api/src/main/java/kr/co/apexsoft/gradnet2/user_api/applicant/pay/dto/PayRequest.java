package kr.co.apexsoft.gradnet2.user_api.applicant.pay.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayEnums;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-16
 */
@Getter
@ToString
public class PayRequest {
    private Long applId;

    @NotBlank
    private String impUid;
    @NotBlank
    private String merchantUid;

    private Long payDate;
    @NotBlank
    private String pgType;
    @NotBlank
    private String payType;
    private PayEnums.Currency currency;
    private Double amount;

    private String cardApplyNum;

    private String buyerName;
    private String buyerEmail;
    private String buyerTelNum;

    private String vbankName;
    private String vbankNum;
    private String vbankHolder;
    private String vbankDueDate;

    public PayRequest() {
    }
}
