package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;

import kr.co.apexsoft.gradnet2.entity.recpart.projection.RecruitPartGuideResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Class Description
 *
 * @author 최은주
 * @since 2020-06-22
 */
@Getter
@Setter
public class RecruitPartPossibleForViewResponse {

    private String enterYear;

    private Integer recruitPartSeq;

    private String korName;

    private String engName;

    private String siteUrl;

    private String siteUrlEn;

    public RecruitPartTimeResponse possible;

    List<RecruitPartGuideResponse> recruitPartGuides;
}
