package kr.co.apexsoft.gradnet2.user_api.user.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * 사용자 disable 예외처리
 */
public class UserDisableException extends CustomException {

    public UserDisableException() {
        super();
    }

    public UserDisableException(String warningMessage) {
        super(warningMessage);
    }

    public UserDisableException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
