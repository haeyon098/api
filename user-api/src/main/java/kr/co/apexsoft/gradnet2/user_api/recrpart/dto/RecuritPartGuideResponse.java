package kr.co.apexsoft.gradnet2.user_api.recrpart.dto;


import lombok.Getter;

/**
 * Class Description
 *
 * @author 최은주
 * @since 2020-06-22
 */
@Getter
public class RecuritPartGuideResponse {
    private String recrSchlCode;

    private String entrYear;

    private int recrPartSeq;

    private int orderSeq;

    private String siteUrl;

    private String siteTitleKor;

    private String siteTitleEng;
}
