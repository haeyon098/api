package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.dto;


import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * Class Description
 *
 * @author 박지환
 * @since 2020-02-25
 */
@Getter
public class RecommendResponse {

    private Long id;

    private String name;

    private String email;

    private String institute;

    private String position;

    private String phoneNum;

    private String key;

    private String stsCode;

}
