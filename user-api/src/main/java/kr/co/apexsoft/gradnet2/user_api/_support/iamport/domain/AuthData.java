
package kr.co.apexsoft.gradnet2.user_api._support.iamport.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthData {

    private String imp_key;

    private String imp_secret;


    public AuthData(String imp_key, String imp_secret) {
        this.imp_key =imp_key;
        this.imp_secret= imp_secret;
    }
}

