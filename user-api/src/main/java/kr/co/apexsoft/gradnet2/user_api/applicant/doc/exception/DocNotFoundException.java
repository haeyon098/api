package kr.co.apexsoft.gradnet2.user_api.applicant.doc.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
*
*
* @author aran
* @since 2020-03-04
*/
public class DocNotFoundException extends CustomException {
    public DocNotFoundException() {
        super();
    }
    public DocNotFoundException(String warningMessage) {
        super(warningMessage);
    }
    public DocNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }
    public DocNotFoundException(String warningMessage, Long id) { super("[ srvyNo: "+id+" ]", warningMessage); }
}
