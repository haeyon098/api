package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplId;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplIdSeqProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.standard.repository.CountryRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class 박지환
 *
 * @author 홍길동
 * @since 2020-12-02
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ApplIdKdiService implements ApplIdService {

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private CountryRepository countryRepository;

    public void makeApplId(Appl appl) {
        // KDI RULE_NO 생성
        String ruleNo = getRuleNo(appl);
        // 수험번호 생성 : 결과 SEQ_YEAR:20(두자리) / RULE_NO : 3112 (숫자네자리) / NEW_SEQ:00001 (숫자네자리)
        ApplIdSeqProjection applIdSeq = applRepository.findApplId(appl.getPart().getSchoolCode(),
                appl.getPart().getEnterYear(), appl.getPart().getRecruitPartSeq(), ruleNo);

        // 채번테이블 seq 업데이트
        applRepository.updateApplIdSeq_Seq(appl.getPart().getSchoolCode(), appl.getPart().getEnterYear(),
                appl.getPart().getRecruitPartSeq(), applIdSeq.getNewSeq(), applIdSeq.getRuleNo());

        appl.changeApplId(applIdSeq.getApplId());
    }

    public String getRuleNo(Appl appl) {
        String smstCode =  appl.getPart().getSemesterTypeCode().substring(appl.getPart().getSemesterTypeCode().length()-1);
        String corsCode =  appl.getPart().getCourseCode().substring(appl.getPart().getCourseCode().length()-1);
        String cntrCode = appl.getApplicantInfo().getCntr().getId().equals("999") ?
                "0": countryRepository.findById( appl.getApplicantInfo().getCntr().getId()).get().getGrpCode();

        String majCode = getKDIRuleMajCode(appl.getPart().getMajorCode());

        return smstCode + corsCode + majCode + cntrCode;
    }


    private String getKDIRuleMajCode(String dbMajCode) {
        String majCode;

        if (dbMajCode.equals("MPP")) {
            majCode = "1";
        }else if (dbMajCode.equals("MDP")) {
            majCode = "2";
        }else if (dbMajCode.equals("MPM")) {
            majCode = "3";
        }else if (dbMajCode.equals("MIPD")) {
            majCode = "4";
        }else if (dbMajCode.equals("MPPM")) {
            majCode = "5";
        }else if (dbMajCode.equals("PP")) {
            majCode = "1";
        }else if (dbMajCode.equals("DP")) {
            majCode = "2";
        }else if (dbMajCode.equals("PM")) {
            majCode = "3";
        }else {
            majCode = "0";
        }
        return majCode;
    }
}
