package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import kr.co.apexsoft.gradnet2.entity.adms.domain.Adms;
import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplIdSeqProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplInfoProjection;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.domain.DocItem;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.ApplPart;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.applpart.repository.ApplPartRepository;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.entity.standard.repository.CountryRepository;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailDto;
import kr.co.apexsoft.gradnet2.user_api._support.mail.MailService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplFormFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplSlipFileInfo;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplCompleteFailException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplId.ApplIdKdiService;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.service.DocumentService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 지원 완료 처리
 *
 * @author 김효숙
 * @since
 */
@org.springframework.stereotype.Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CompleteService {
    @NonNull
    private ApplSlipBirtService slipBirtService;

    @NonNull
    private ApplFormBirtService applFormBirtService;

    @NonNull
    private ApplService applService;


    private final ApexModelMapper modelMapper;

    @NonNull
    private MailService mailService;
    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private DocumentService documentService;

    @NonNull
    private ApplIdKdiService applIdKdiService;

    @Autowired
    private ResourceLoader resourceLoader;


    @Value("${file.baseDir}")
    private String fileBaseDir;

    /**
     * 지원 완료 처리
     * TODO: 트랜잭션 분리 기준 논의 필요
     *
     * @param appl
     */
    public void complete(Appl appl) {
        makeApplId(appl);
        appl.changeComplete();

        // 지원서 재 생성 + 업로드 + db반영
        generateApplForm(appl);

        // 수험표 생성 + 업로드 + db반영
        generateSlipForm(appl);

        //지원완료 메일 전송 비동기
        mailService.sendAsync(setCompleteMail(appl.getId()));
    }

    /**
     * 지원 완료 처리 - 이메일 전송안함
     * TODO: 트랜잭션 분리 기준 논의 필요
     *
     * @param appl
     */
    public void completeNotSendEmail(Appl appl) {
        makeApplId(appl);
        appl.changeComplete();

        // 지원서 재 생성 + 업로드 + db반영
        generateApplForm(appl);

        // 수험표 생성 + 업로드 + db반영
        generateSlipForm(appl);
    }

    /**
     * 지원완료 메일 처리
     * @param kor
     * @param eng
     * @return
     */
    private String getKorEngName(String kor, String eng) {
        return kor + "(" + eng + ")";
    }

    public void sendCompleteMail(Long applNo) {
        mailService.send(setCompleteMail(applNo));
    }

    private MailDto setCompleteMail(Long applNo) {
        Map<String, String> replacements = new HashMap<>();

        ApplInfoProjection data = applRepository.findMyApplInfo(applNo);
        replacements.put("recrKorName", data.getRecrPartKorName());
        replacements.put("recrEngName", data.getRecrPartEngName());

        //국내전형 한글이름,영문이름 // 국제전형 영문이름
        replacements.put("userName", Adms.AmdsCodeType.KR.getValue().equals(data.getAdmissionCode()) ?
                getKorEngName(data.getKorName(), data.getEngName()) : data.getEngName());
        replacements.put("userId", data.getUserId());
        replacements.put("corsName", getKorEngName(data.getCorsKorName(), data.getCorsEngName()));
        replacements.put("categoryName", getKorEngName(data.getCategoryKorName(), data.getCategoryEngName()));
        replacements.put("majorName", getKorEngName(data.getMajorKorName(), data.getMajorEngName()));
        replacements.put("applId", data.getApplId());
        MailDto dto = MailDto.builder()
                .to(Arrays.asList(data.getEmailAddr()))
                .subject(getKorEngName("[" + data.getSchoolKorName() + "]" + "입학 신청 절차 완료 알림", "[" + data.getSchoolEngName() + "]" + "Notice of Application Completion"))
                .templateId("complete.html")
                .replacements(replacements)
                .resourceLoader(resourceLoader)
                .build();
        return dto;
    }


    public void generateApplForm(Appl appl) {
        ApplFormFileInfo fileInfo = modelMapper.convert(appl, ApplFormFileInfo.class);

        // 버트 파일 객체
        FileRequest fileRequest = applFormBirtService.generateBirtFile(fileInfo);

        // db 저장
        documentService.uploadAndSaveGradnetCustomFile(fileRequest, appl, DocGrpType.BASIC.getValue(), -1L, DocItem.APPL_FORM.getValue(), true);


    }

    /**
     * 수험표
     * 파일 업로드
     * db 반영
     */
    public void generateSlipForm(Appl appl) {
        ApplSlipFileInfo fileInfo = modelMapper.convert(appl, ApplSlipFileInfo.class);

        // 버트 파일 객체
        FileRequest fileRequest = slipBirtService.generateBirtFile(fileInfo);

        // 업로드 및 db 반영
        documentService.uploadAndSaveGradnetCustomFile(fileRequest, appl, DocGrpType.BASIC.getValue(), -1L, DocItem.SLIP.getValue(), true);


    }

    @Transactional(readOnly = true)
    public Appl checkApplCompleteSataus(Long applNo, Set<Role> roles) {
        //appl인터셉터를 안타는 서비스 호출 고려 익셉션 처리
        Appl appl = applRepository.findById(applNo).orElseThrow(() -> new ApplNotFoundException(MessageUtil.getMessage("APPL_NOT_FOUND")));
        HashMap<String, Object> map = new HashMap<>();

        //지원완료 처리 기간 체크
        applService.checkApplSaveTime(appl.getId(), roles, RecruitPartTime.TimeType.COMPLETE, "APPL_COMPLETE_FAIL_DEADLINE");
        // 지원완료 여부 체크
        if (appl.isCompleteStatus()) {
            throw new ApplCompleteFailException(MessageUtil.getMessage("APPL_COMPLETE_FAIL_STATUS_COMPLETE"), appl.getId());
        }
        // 지원취소 여부 체크
        if (appl.isCancelStatus()) {
            throw new ApplCompleteFailException(MessageUtil.getMessage("APPL_COMPLETE_FAIL_STATUS_CANCEL"), appl.getId());
        }

        //SUBMIT 이상만 가능 (SUBMIT과 지원완료 사이에 상태 가능성염두 필요)
        checkSubmitOver(appl.getPart().getSchoolCode(), appl.getStatus(), applNo);
        return appl;
    }

    @Transactional(readOnly = true)
    public void checkSubmitOver(String schoolCode, Appl.Status status, Long id) {
        Appl.Status[] statusArr = RecruitSchool.SchoolCodeType.getSteps(schoolCode);
        int dbIndex = Arrays.asList(statusArr).indexOf(status); //db 저장된 상태

        int tryIndex = Arrays.asList(statusArr).indexOf(Appl.Status.SUBMIT);
        if (dbIndex < tryIndex) {
            throw new ApplCompleteFailException(MessageUtil.getMessage("APPL_SAVE_FAIL_STATUS_NOT_SUBMIT"), id);
        }
    }
    /**
     * 수험번호
     * 생성
     * (학교별 분기)
     */
    public void makeApplId(Appl appl) {

        if (RecruitSchool.SchoolCodeType.KDI.getValue().equals(appl.getPart().getSchoolCode())) {
            applIdKdiService.makeApplId(appl);
        } else {
        //TODO: 다른학교 들어올때, 수험번호 생성 부분 추가
            appl.changeApplId("00000000");
        }
    }





}
