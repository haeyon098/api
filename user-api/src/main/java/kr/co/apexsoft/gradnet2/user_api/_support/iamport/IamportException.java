package kr.co.apexsoft.gradnet2.user_api._support.iamport;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
*
*
아임포트
*/
public class IamportException extends CustomException {

    public IamportException() {
        super();
    }

    public IamportException(String warningMessage) {
        super(warningMessage);
    }

    public IamportException(String message, String warningMessage) {
        super(message, warningMessage);
    }

}
