package kr.co.apexsoft.gradnet2.user_api.applicant.appl.service;

import kr.co.apexsoft.gradnet2.user_api._support.birt.service.BirtService;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileRequest;
import kr.co.apexsoft.gradnet2.user_api._support.file.FileService;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.AppBirtFileInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * 지원서 ,수험표 공통 서비스
 */
@Service
@Transactional
public abstract class ApplBirtService {

    @Value("${file.baseDir}")
    private String fileBaseDir;

    @Autowired
    private FileService fileService;

    @Autowired
    private BirtService birtService;

    public abstract Map<String, Object> setBirtInfo(AppBirtFileInfo fileInfo);

    public FileRequest generateBirtFile(AppBirtFileInfo fileInfo) {
        fileInfo.setFileBaseDir(fileBaseDir);

        Map<String, Object> rptInfoMap = new HashMap<String, Object>();
        //데이터 삽입
        rptInfoMap.putAll(setBirtInfo(fileInfo));



        //증명사진
        File photoFile = fileService.getFileFromFileRepo(fileBaseDir, String.valueOf(rptInfoMap.get("photoFilePath")), fileInfo.getApplNo());
        rptInfoMap.put("photoUrl", photoFile.getAbsolutePath());


        //버트 만들기
        Map<String, Object> returndata = birtService.generateApplFormAndSlip(rptInfoMap, fileInfo);

        // 사진 파일 삭제
        if (photoFile.exists()) {
            photoFile.delete();
        }

        //업로드 위한 파일 객체 생성
        File file = new File((String) returndata.get("pdfDirectoryFullPath"), (String) returndata.get("pdfFileName"));
        FileRequest fileRequest = new FileRequest(file, fileInfo.getS3Path(), fileInfo.getFileName());

        return fileRequest;

    }
}
