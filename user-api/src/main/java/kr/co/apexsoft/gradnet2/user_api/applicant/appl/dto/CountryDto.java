package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import lombok.Getter;

/**
 * @author 박지환
 * created on 2020-03-27
 */
@Getter
public class CountryDto {
    private String id;

    private String name;


    public CountryDto() {}

    public CountryDto(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
