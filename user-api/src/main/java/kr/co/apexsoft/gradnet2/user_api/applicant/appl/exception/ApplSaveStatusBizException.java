package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.BizException;
import lombok.Getter;

import java.util.HashMap;

/**
 * 저장시 appl status 체크
 * @author 김효숙
 */
@Getter
public class ApplSaveStatusBizException extends BizException {


    public ApplSaveStatusBizException(String warningMessage,Long applNo,HashMap<String,Object> returnMap) {
        super(warningMessage, "applNo:" + applNo,returnMap);
    }



}

