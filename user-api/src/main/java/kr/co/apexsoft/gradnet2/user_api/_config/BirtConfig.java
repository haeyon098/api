package kr.co.apexsoft.gradnet2.user_api._config;

import kr.co.apexsoft.gradnet2.user_api._support.birt.core.BirtEngineFactory;
import kr.co.apexsoft.gradnet2.user_api._support.birt.core.PdfSingleFormatBirtSaveToFile;
import kr.co.apexsoft.gradnet2.user_api._support.birt.core.PdfSingleFormatBirtView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.logging.Level;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2019-12-10
 */
@Configuration
public class BirtConfig {
    @Bean("birtEngineFactory")
    public BirtEngineFactory birtEngineFactory() {
        BirtEngineFactory birtEngineFactory = new BirtEngineFactory();
        birtEngineFactory.setLogLevel(Level.SEVERE);
        return birtEngineFactory;
    }

    @Bean("pdfSingleFormatBirtView")
    public PdfSingleFormatBirtView pdfSingleFormatBirtView(@Autowired BirtEngineFactory birtEngineFactory) {
        PdfSingleFormatBirtView pdfSingleFormatBirtView = new PdfSingleFormatBirtView();
        pdfSingleFormatBirtView.setBirtEngine(birtEngineFactory.getObject());
        pdfSingleFormatBirtView.setReportFormatRequestParameter("reportFormat");
        pdfSingleFormatBirtView.setReportNameRequestParameter("reportName");
        pdfSingleFormatBirtView.setDocumentNameRequestParameter("documentName");
        pdfSingleFormatBirtView.setReportsDirectory("reports");
        return pdfSingleFormatBirtView;
    }

    @Bean("pdfSingleFormatBirtSaveToFile")
    public PdfSingleFormatBirtSaveToFile pdfSingleFormatBirtSaveToFile(@Autowired BirtEngineFactory birtEngineFactory) {
        PdfSingleFormatBirtSaveToFile pdfSingleFormatBirtSaveToFile = new PdfSingleFormatBirtSaveToFile();
        pdfSingleFormatBirtSaveToFile.setBirtEngine(birtEngineFactory.getObject());
        pdfSingleFormatBirtSaveToFile.setReportFormatRequestParameter("reportFormat");
        pdfSingleFormatBirtSaveToFile.setReportNameRequestParameter("reportName");
        pdfSingleFormatBirtSaveToFile.setDocumentNameRequestParameter("documentName");
        pdfSingleFormatBirtSaveToFile.setReportsDirectory("reports");
        return pdfSingleFormatBirtSaveToFile;
    }
}
