package kr.co.apexsoft.gradnet2.user_api.applicant.appl.controller;

import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.SubmitResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.SubmitService;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.dto.DocumentResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/appl/submit")
@RequiredArgsConstructor
public class SubmitController {
    @NonNull
    private SubmitService submitService;


    /***
     * 작성완료 화면 조회
     * @param id
     * @param userPrincipal
     * @return
     */
    @GetMapping("/{applNo}")
    public ResponseEntity<SubmitResponse> findAppl(@PathVariable("applNo") Long id, @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(submitService.findByAppl(id,userPrincipal.getRoles()));
    }

    /**
     * 작성완료 처리
     * @param id
     * @param userPrincipal
     */
    @PostMapping("/{applNo}")
    public void submit(@PathVariable("applNo") Long id, @AuthenticationPrincipal UserPrincipal userPrincipal) {
      submitService.submit(id,userPrincipal.getRoles());
    }

    /**
     * 지원서 생성
     * @param id
     * @param userPrincipal
     */
    @PostMapping("/generate/{applNo}")
    public ResponseEntity<DocumentResponse>  generate(@PathVariable("applNo") Long id, @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(submitService.generateApplForm(id,userPrincipal.getRoles()));
    }


}
