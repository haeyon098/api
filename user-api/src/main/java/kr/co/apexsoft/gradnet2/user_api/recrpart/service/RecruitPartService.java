package kr.co.apexsoft.gradnet2.user_api.recrpart.service;

import kr.co.apexsoft.gradnet2.entity.adms.domain.RecruitSchool;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPart;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.entity.recpart.repository.RecruitPartRepository;
import kr.co.apexsoft.gradnet2.entity.recpart.repository.RecruitPartTimeRepository;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.adms.exception.RecruitSchoolNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.*;
import kr.co.apexsoft.gradnet2.user_api.recrpart.exception.RecruitPartDuplicateException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-06
 */
@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RecruitPartService {
    private final ApexModelMapper modelMapper;

    @NonNull
    private RecruitPartRepository recruitPartRepository;

    @NonNull
    private RecruitPartTimeRepository recruitPartTimeRepository;

    public List<RecruitPartPossibleResponse> findAllByRecruitPart(String schoolCode) {
        List<RecruitPart> list = recruitPartRepository.findByRecruitSchoolCodeAndStartDateBeforeAndEndDateAfter(schoolCode,
                LocalDateTime.now(), LocalDateTime.now());

        List<RecruitPartPossibleResponse> responses = modelMapper.convert(list, new TypeToken<List<RecruitPartPossibleResponse>>(){}.getType());

        for(int i=0;i<list.size();i++) {
            responses.get(i).setPossible(modelMapper.convert(list.get(i).getSubmit(), RecruitPartTimeResponse.class));
        }
        return responses;
    }


    //학교별 메인화면 모집회차정보 조회( 가이드 링크 테이블 분리 후 추가 )
    public List<RecruitPartPossibleForViewResponse> findAllForViewByRecruitPart(String schoolCode) {
        List<RecruitPart> list = recruitPartRepository.findByRecruitSchoolCodeAndStartDateBeforeAndEndDateAfter(schoolCode,
                LocalDateTime.now(), LocalDateTime.now());

        List<RecruitPartPossibleForViewResponse> responses = modelMapper.convert(list, new TypeToken<List<RecruitPartPossibleForViewResponse>>(){}.getType());

        for(int i=0;i<list.size();i++) {
            responses.get(i).setRecruitPartGuides(recruitPartRepository.findGuideByRecruitSchool(schoolCode, list.get(i).getEnterYear(), list.get(i).getSeq()));
            responses.get(i).setPossible(modelMapper.convert(list.get(i).getSubmit(), RecruitPartTimeResponse.class));
        }

        return responses;
    }

    public List<RecruitPartForBannerResponse> findAllByRecruitPartForBanner() {
        List<RecruitPart> list = recruitPartRepository
                .findByStartDateBeforeAndEndDateAfter(LocalDateTime.now(), LocalDateTime.now());
        List<RecruitPartForBannerResponse> responses = modelMapper.convert(list, new TypeToken<List<RecruitPartForBannerResponse>>(){}.getType());

        for(RecruitPartForBannerResponse response : responses) {
            response.setSchoolName(RecruitSchool.SchoolCodeType.getUpperCaseName(response.getSchoolCode()));
        }

        return responses;
    }

    public Page<RecruitPart> findAllRecruitPart(String schoolCode, Pageable pageable) {
        return recruitPartRepository.findByRecruitSchoolCodeContaining(schoolCode, pageable);
    }

    public RecruitPartDto findRecruitPartOne(String schoolCode, String enterYear, Integer recruitPartSeq) {
        RecruitPart recruitPart =  recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeq(schoolCode, enterYear, recruitPartSeq).orElseThrow(() -> new RuntimeException(
                String.format("학교 [%s], 연도 [%s], 회차 [%d] 인 RecruitPart 가 없습니다.", schoolCode, enterYear, recruitPartSeq)));

        RecruitPartDto response = convertResponse(recruitPart);
        modelMapper.convert(response.getTime(), new TypeToken<List<RecruitPartTimeForSysadminDto>>(){}.getType());
        return response;
    }

    @Transactional
    public RecruitPart create(RecruitPart recruitPart) {
        checkDuplicated(recruitPart.getRecruitSchoolCode(), recruitPart.getEnterYear(), recruitPart.getSeq());

        save(recruitPart);

        RecruitPart savedRecruitPart = recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeq(recruitPart.getRecruitSchoolCode(), recruitPart.getEnterYear(),
                recruitPart.getSeq()).orElseThrow(()-> new RecruitSchoolNotFoundException(MessageUtil.getMessage("RECRUIT_PART_NOT_FOUND")));
        return savedRecruitPart;
    }

    private void checkDuplicated(String schoolCode, String enterYear, Integer seq) {
        if(recruitPartRepository.findByRecruitSchoolCodeAndEnterYearAndSeq(schoolCode, enterYear, seq).isPresent())
            throw new RecruitPartDuplicateException("동일한 [학교, 입학년도, 모집학교]가 존재합니다.");
    }

    @Transactional
    void save(RecruitPart recruitPart) {
        recruitPartRepository.save(recruitPart);
        recruitPartTimeRepository.saveAll(recruitPart.getTime());
    }

    @Transactional
    public RecruitPartDto saveRecruitPart(RecruitPart recruitPart) {
        RecruitPart savedRecruitPart = recruitPartRepository.save(recruitPart);
        return convertResponse(savedRecruitPart);
    }

    private RecruitPartDto convertResponse(RecruitPart recruitPart) {
        RecruitPartDto response = modelMapper.convert(recruitPart, RecruitPartDto.class);
        response.setSchoolName(RecruitSchool.SchoolCodeType.getUpperCaseName(recruitPart.getRecruitSchoolCode()));
        return response;
    }

    @Transactional
    public List<RecruitPartTimeForSysadminDto> saveRecruitPartTime(List<RecruitPartTime> list) {
        List<RecruitPartTime> times = recruitPartTimeRepository.saveAll(list);
        return modelMapper.convert(times, new TypeToken<List<RecruitPartTimeForSysadminDto>>(){}.getType());
    }
}
