package kr.co.apexsoft.gradnet2.user_api.applicant.acad.service;

import kr.co.apexsoft.gradnet2.entity.applicant.acad.domain.Academy;
import kr.co.apexsoft.gradnet2.entity.applicant.acad.repository.AcademyRepository;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.repository.ApplRepository;
import kr.co.apexsoft.gradnet2.entity.applpart.domain.DocGrpType;
import kr.co.apexsoft.gradnet2.entity.role.domain.Role;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception.ApplNotFoundException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import kr.co.apexsoft.gradnet2.user_api.applicant.doc.service.DocumentService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Class Description
 *
 * @author 김효숙
 * @since 2020-02-12
 */
@Service
@Transactional
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AcademyService {
    @NonNull
    private AcademyRepository academyRepository;

    @NonNull
    private ApplRepository applRepository;

    @NonNull
    private  ApplService applService;

    @NonNull
    private DocumentService documentService;

    @Transactional(readOnly = true)
    public Map<Object,Object> findAcademyByApplNo(Long applNo) {

        Map<Object,Object> map = new HashMap<>();
        for (Academy.AcademyType name : Academy.AcademyType.values()) {
            map.put(name, academyRepository.findAcademyByApplNo(applNo, "999",Arrays.asList(name))); //TODO :기타코드
        }

        map.put("applStatus",applRepository.findApplStatus(applNo));

        return map;
    }

    public Map<Object,Object> createAcad(List<Academy> academy, Long applNo,Set<Role> roles) {
        Appl appl =  applService.checkApplSave(applNo, Appl.Status.ACAD_SAVE,roles);
        Long[] upadatedDocGrpNos = this.getUpdateAcademyRow(applNo, academy);

        this.deleteNotInclude(applNo, academy);
        academyRepository.saveAll(academy);

        // 학력 관련 APPL_DOC, S3 파일 삭제
        documentService.documentDeleteByGroupCode(applNo, upadatedDocGrpNos, DocGrpType.UNDERGRADUATE.getValue(),
                DocGrpType.GRADUATE.getValue());

        appl.changeStatus(Appl.Status.ACAD_SAVE);
        return findAcademyByApplNo(applNo);
    }

    private void deleteNotInclude(Long applNo, List<Academy> academy) {
        Long[] ids = academy.stream().filter(a -> a.getId() != null).map(a -> a.getId()).toArray(Long[]::new);
        if(ids.length == 0) academyRepository.deleteByAppl_Id(applNo);
        else academyRepository.deleteByAppl_IdAndIdNotIn(applNo, ids);
    }

    /**
     * 학교코드, 학교이름 변경된 건만 학력 id 추출
     *
     * @param applNo
     * @param requests
     * @return
     */
    private Long[] getUpdateAcademyRow(Long applNo, List<Academy> requests) {
        List<Academy> savedList = academyRepository.findByAppl_Id(applNo);
        for(Academy request : requests) {
            if(request.getId() != null) savedList.removeIf(a -> a.notChangeSchoolName(request.getId(), request.getSchool().getCode(), request.getSchool().getName()));
        }
        return savedList.stream().map(a -> a.getId() ).toArray(Long[]::new);
    }

}
