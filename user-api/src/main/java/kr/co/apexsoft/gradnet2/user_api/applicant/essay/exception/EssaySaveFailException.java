package kr.co.apexsoft.gradnet2.user_api.applicant.essay.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-02-14
 */
public class EssaySaveFailException extends CustomException {

    public EssaySaveFailException() {
        super();
    }

    public EssaySaveFailException(String warningMessage) {
        super(warningMessage);
    }

    public EssaySaveFailException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public EssaySaveFailException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
