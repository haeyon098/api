package kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.MyApplResponse;
import kr.co.apexsoft.gradnet2.entity.applicant.doc.projection.CompleteDocumentProjection;
import kr.co.apexsoft.gradnet2.user_api.recrpart.dto.MyListRecruitPartResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-03-05
 */
@AllArgsConstructor
@Getter
public class MyApplAndInfoResponse {
    private MyApplResponse appl;

    private List<CompleteDocumentProjection> documents;

    private MyListRecruitPartResponse part;

}