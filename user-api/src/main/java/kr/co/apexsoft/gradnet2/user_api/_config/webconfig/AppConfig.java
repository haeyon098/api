package kr.co.apexsoft.gradnet2.user_api._config.webconfig;

import kr.co.apexsoft.gradnet2.entity._common.EnumMapper;
import kr.co.apexsoft.gradnet2.entity.applicant.pay.domain.PayEnums;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-09-11
 */
@Configuration
public class AppConfig {

    @Bean
    public EnumMapper enumMapper() {
        EnumMapper enumMapper = new EnumMapper();
        enumMapper.put("PayType", PayEnums.PayType.class);
        enumMapper.put("PG", PayEnums.PG.class);
        enumMapper.put("Currency", PayEnums.Currency.class);
        return enumMapper;
    }
}
