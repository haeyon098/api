package kr.co.apexsoft.gradnet2.user_api.applicant.appl.controller;

import kr.co.apexsoft.gradnet2.entity.applicant.appl.domain.Appl;
import kr.co.apexsoft.gradnet2.entity.applicant.appl.projection.ApplPartResponse;
import kr.co.apexsoft.gradnet2.entity.recpart.domain.RecruitPartTime;
import kr.co.apexsoft.gradnet2.user_api._common.ApexModelMapper;
import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import kr.co.apexsoft.gradnet2.user_api._config.security.UserPrincipal;
import kr.co.apexsoft.gradnet2.user_api._support.exception.InvalidException;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplPartRequest;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.ApplResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.dto.MyListResponse;
import kr.co.apexsoft.gradnet2.user_api.applicant.appl.service.ApplService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-01-06
 */
@RestController
@RequestMapping("/appl")
@RequiredArgsConstructor
public class ApplController {
    @NonNull
    private ApplService applService;

    private final ApexModelMapper modelMapper;

    /**
     * 지원사항 저장
     *
     * @param applPartRequest
     * @param errors
     * @param userPrincipal
     * @return
     */
    @PostMapping("/part")
    public ResponseEntity<ApplPartResponse> createApplPart(@Valid @RequestBody ApplPartRequest applPartRequest,
                                                           Errors errors,
                                                           @AuthenticationPrincipal UserPrincipal userPrincipal) {
        if(errors.hasErrors())
            throw new InvalidException(MessageUtil.getMessage("APPL_PART_INVALID_INFO"));

        return ResponseEntity.ok(applService.createAppl(modelMapper.convert(applPartRequest, Appl.class), userPrincipal.getUser(),userPrincipal.getRoles()));
    }

    /**
     * 지원사항 조회
     *
     * @param id
     * @return
     */
    @GetMapping("/part/{applNo}")
    public ResponseEntity<ApplPartResponse> findApplPart(@PathVariable("applNo") Long id,
                                                         @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(modelMapper.convert(applService.findApplPart(id), ApplPartResponse.class));
    }

    /**
     * Appl 한건 조회
     *
     * @param id
     * @return
     */
    @GetMapping("/{applNo}")
    public ResponseEntity<ApplResponse> findAppl(@PathVariable("applNo") Long id) {
        return ResponseEntity.ok(modelMapper.convert(applService.findAppl(id), ApplResponse.class));
    }

    /**
     * 내 원서 리스트
     *
     * @param userPrincipal
     * @return
     */
    @GetMapping("/list")
    public ResponseEntity<MyListResponse> findApplPart(@AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(applService.findApplList(userPrincipal.getUser().getId()));
    }

    /**
     * 원서 취소
     *
     * @param id
     * @param userPrincipal
     * @return
     */
    @DeleteMapping("/{applNo}")
    public ResponseEntity<MyListResponse> cancelAppl(@PathVariable("applNo") Long id,
                                                     @AuthenticationPrincipal UserPrincipal userPrincipal) {
        return ResponseEntity.ok(applService.cancelAppl(id, userPrincipal.getUser()));
    }


    /**
     * 수정/삭제
     * 가능 여부 조회
     *
     * @param timeType
     * @param applNo
     * @return
     */
    @GetMapping("/checkTime/{timeType}/{applNo}")
    public void findAllByRecruitPart(@PathVariable("timeType") String timeType,
                                                        @PathVariable("applNo") Long applNo,
                                                        @AuthenticationPrincipal UserPrincipal userPrincipal) {
        applService.checkApplCancelUpdate(applNo, RecruitPartTime.TimeType.valueOf(timeType),userPrincipal.getRoles());
    }

}
