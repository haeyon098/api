package kr.co.apexsoft.gradnet2.user_api._support.api;

import kr.co.apexsoft.gradnet2.user_api._common.util.MessageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.InvocationTargetException;

@Slf4j
@Service
public class ApiCallService<T> {

    @Autowired
    RestTemplate restTemplate;


    public T callApiEndpoint(String url, HttpMethod httpMethod, HttpHeaders httpHeaders, Object body, Class<T> clazz
            , Class<? extends RuntimeException> exceptionClass, String message) {
        try {
            return restTemplate.exchange(url, httpMethod, new HttpEntity<>(body, httpHeaders), clazz).getBody();
        } catch (Exception e) {
            try {
                throw exceptionClass.getDeclaredConstructor(String.class, String.class).newInstance(e.getMessage(), MessageUtil.getMessage(message));
            } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException err) {
                throw new RestTemplateApiException(MessageUtil.getMessage("SERVER_FAIL"));
            }
        }
    }


}
