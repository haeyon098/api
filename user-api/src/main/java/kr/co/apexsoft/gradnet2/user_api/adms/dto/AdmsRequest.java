package kr.co.apexsoft.gradnet2.user_api.adms.dto;

import lombok.Getter;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;

/**
 * Class Description
 *
 * @author 김혜연
 * @since 2020-02-10
 */
@Getter
public class AdmsRequest {
    @NotBlank
    private String schoolCode;

    @NotBlank
    private String enterYear;

    @NonNull
    private Integer recruitPartSeq;

    private String semesterTypeCode;

    private String admissionCode;

    private String courseCode;

    private String categoryCode;

    private String majorCode;
}
