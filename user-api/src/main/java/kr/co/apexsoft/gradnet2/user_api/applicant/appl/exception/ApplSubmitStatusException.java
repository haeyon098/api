package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;


public class ApplSubmitStatusException extends CustomException {

    public ApplSubmitStatusException() {
        super();
    }


    public ApplSubmitStatusException( String warningMessage) {
        super(warningMessage);
    }

}
