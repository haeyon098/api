package kr.co.apexsoft.gradnet2.user_api.applicant.srvy.exception;
import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * Class Description
 *
 * @author 홍길동
 * @since 2020-03-06
 */
public class SrvyOfficePostalFailException  extends CustomException {

    public SrvyOfficePostalFailException(String warningMessage, Long applNo) {
        super(("applNo "+applNo), warningMessage);
    }
}
