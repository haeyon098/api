package kr.co.apexsoft.gradnet2.user_api._support.mail.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * 메일
 */
public class MailValidException extends CustomException {

    public MailValidException() {
        super();
    }

    public MailValidException(String warningMessage) {
        super(warningMessage);
    }

    public MailValidException(String message, String warningMessage) {
        super(message, warningMessage);
    }
}
