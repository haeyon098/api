package kr.co.apexsoft.gradnet2.user_api.applicant.recommend.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;
import lombok.Getter;

/**
 * 추천서 마감시간 체크
 * @author 박지환
 */
@Getter
public class RecPeriodException extends CustomException {


    public RecPeriodException(String warningMessage, Long recNo) {
        super("[recNo:" + recNo + "]", warningMessage);
    }


}

