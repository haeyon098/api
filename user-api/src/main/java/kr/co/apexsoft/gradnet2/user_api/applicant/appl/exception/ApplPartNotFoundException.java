package kr.co.apexsoft.gradnet2.user_api.applicant.appl.exception;

import kr.co.apexsoft.gradnet2.user_api._support.exception.CustomException;

/**
 * created by hanmomhanda@gmail.com
 */
public class ApplPartNotFoundException extends CustomException {

    public ApplPartNotFoundException() {
        super();
    }

    public ApplPartNotFoundException(String warningMessage) {
        super(warningMessage);
    }

    public ApplPartNotFoundException(String message, String warningMessage) {
        super(message, warningMessage);
    }

    public ApplPartNotFoundException(String warningMessage, Long id) {
        super("[ userNo: "+id+" ]", warningMessage);
    }
}
