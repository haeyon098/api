package kr.co.apexsoft.gradnet2.user_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class})  // birt import때문에 필요
@EntityScan("kr.co.apexsoft.gradnet2.entity")
@EnableJpaRepositories("kr.co.apexsoft.gradnet2.entity")
public class UserApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserApiApplication.class, args);
    }

}
